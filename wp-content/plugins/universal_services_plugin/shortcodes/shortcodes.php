<?php
/**
 * ThemeREX Shortcodes
*/


// ---------------------------------- [trx_accordion] ---------------------------------------

themerex_require_shortcode('trx_accordion', 'themerex_sc_accordion');

function themerex_sc_accordion($atts, $content=null){	
	if (themerex_sc_in_shortcode_blogger()) return '';
    extract(themerex_sc_html_decode(shortcode_atts(array(
		// Individual params
		"initial" => "1",
		"counter" => "off",
		"icon_closed" => "icon-plus",
		"icon_opened" => "icon-minus",
		// Common params
		"id" => "",
		"class" => "",
		"css" => "",
		"animation" => "",
		"top" => "",
		"bottom" => "",
		"left" => "",
		"right" => ""
    ), $atts)));
	$css .= themerex_get_css_position_from_values($top, $right, $bottom, $left);
	$style = 1;
	$initial = max(0, (int) $initial);
	global $THEMEREX_GLOBALS;
	$THEMEREX_GLOBALS['sc_accordion_counter'] = 0;
	$THEMEREX_GLOBALS['sc_accordion_show_counter'] = themerex_sc_param_is_on($counter);
	$THEMEREX_GLOBALS['sc_accordion_icon_closed'] = empty($icon_closed) || themerex_sc_param_is_inherit($icon_closed) ? "icon-plus-2" : $icon_closed;
	$THEMEREX_GLOBALS['sc_accordion_icon_opened'] = empty($icon_opened) || themerex_sc_param_is_inherit($icon_opened) ? "icon-minus-2" : $icon_opened;
	wp_enqueue_script('jquery-ui-accordion', false, array('jquery','jquery-ui-core'), null, true);
	$output = '<div' . ($id ? ' id="'.esc_attr($id).'"' : '') 
			. ' class="sc_accordion sc_accordion_style_'.esc_attr($style)
				. (!empty($class) ? ' '.esc_attr($class) : '')
				. (themerex_sc_param_is_on($counter) ? ' sc_show_counter' : '') 
			. '"'
			. ($css!='' ? ' style="'.esc_attr($css).'"' : '') 
			. ' data-active="' . ($initial-1) . '"'
			. (!themerex_sc_param_is_off($animation) ? ' data-animation="'.esc_attr(themerex_sc_get_animation_classes($animation)).'"' : '')
			. '>'
			. do_shortcode($content)
			. '</div>';
	return apply_filters('themerex_shortcode_output', $output, 'trx_accordion', $atts, $content);
}


themerex_require_shortcode('trx_accordion_item', 'themerex_sc_accordion_item');

function themerex_sc_accordion_item($atts, $content=null) {
	if (themerex_sc_in_shortcode_blogger()) return '';
	extract(themerex_sc_html_decode(shortcode_atts( array(
		// Individual params
		"icon_closed" => "",
		"icon_opened" => "",
		"title" => "",
		// Common params
		"id" => "",
		"class" => "",
		"css" => ""
	), $atts)));
	global $THEMEREX_GLOBALS;
	$THEMEREX_GLOBALS['sc_accordion_counter']++;
	if (empty($icon_closed) || themerex_sc_param_is_inherit($icon_closed)) $icon_closed = $THEMEREX_GLOBALS['sc_accordion_icon_closed'] ? $THEMEREX_GLOBALS['sc_accordion_icon_closed'] : "icon-plus-2";
	if (empty($icon_opened) || themerex_sc_param_is_inherit($icon_opened)) $icon_opened = $THEMEREX_GLOBALS['sc_accordion_icon_opened'] ? $THEMEREX_GLOBALS['sc_accordion_icon_opened'] : "icon-minus-2";
	$output = '<div' . ($id ? ' id="'.esc_attr($id).'"' : '') 
			. ' class="sc_accordion_item' 
			. (!empty($class) ? ' '.esc_attr($class) : '')
			. ($THEMEREX_GLOBALS['sc_accordion_counter'] % 2 == 1 ? ' odd' : ' even') 
			. ($THEMEREX_GLOBALS['sc_accordion_counter'] == 1 ? ' first' : '') 
			. '">'
			. '<div class="sc_accordion_title">'
			. ($title)
			. (!themerex_sc_param_is_off($icon_closed) ? '<span class="sc_accordion_icon sc_accordion_icon_closed '.esc_attr($icon_closed).'"></span>' : '')
			. (!themerex_sc_param_is_off($icon_opened) ? '<span class="sc_accordion_icon sc_accordion_icon_opened '.esc_attr($icon_opened).'"></span>' : '')
			. ($THEMEREX_GLOBALS['sc_accordion_show_counter'] ? '<span class="sc_items_counter">'.($THEMEREX_GLOBALS['sc_accordion_counter']).'</span>' : '')
			. '</div>'
			. '<div class="sc_accordion_content"'
				. ($css!='' ? ' style="'.esc_attr($css).'"' : '') 
				. '>'
				. do_shortcode($content) 
			. '</div>'
			. '</div>';
	return apply_filters('themerex_shortcode_output', $output, 'trx_accordion_item', $atts, $content);
}

// ---------------------------------- [/trx_accordion] ---------------------------------------



// ---------------------------------- [trx_audio] ---------------------------------------

themerex_require_shortcode("trx_audio", "themerex_sc_audio");

function themerex_sc_audio($atts, $content = null) {
	if (themerex_sc_in_shortcode_blogger()) return '';
	extract(themerex_sc_html_decode(shortcode_atts(array(
		// Individual params
		"title" => "",
		"author" => "",
		"image" => "",
		"mp3" => '',
		"wav" => '',
		"src" => '',
		"url" => '',
		"align" => '',
		"controls" => "",
		"autoplay" => "",
		"frame" => "on",
		// Common params
		"id" => "",
		"class" => "",
		"css" => "",
		"animation" => "",
		"width" => '',
		"height" => '',
		"top" => "",
		"bottom" => "",
		"left" => "",
		"right" => ""
    ), $atts)));
	if ($src=='' && $url=='' && isset($atts[0])) {
		$src = $atts[0];
	}
	if ($src=='') {
		if ($url) $src = $url;
		else if ($mp3) $src = $mp3;
		else if ($wav) $src = $wav;
	}
	if ($image > 0) {
		$attach = wp_get_attachment_image_src( $image, 'full' );
		if (isset($attach[0]) && $attach[0]!='')
			$image = $attach[0];
	}
	$css .= themerex_get_css_position_from_values($top, $right, $bottom, $left);
	$css .= ($width != ''  ? ' width:'.esc_attr($width).'px;'   : '');

	$data = ($title != ''  ? ' data-title="'.esc_attr($title).'"'   : '')
			. ($author != '' ? ' data-author="'.esc_attr($author).'"' : '')
			. ($image != ''  ? ' data-image="'.esc_url($image).'"'   : '')
			. ($align && $align!='none' ? ' data-align="'.esc_attr($align).'"' : '')
			. (!themerex_sc_param_is_off($animation) ? ' data-animation="'.esc_attr(themerex_sc_get_animation_classes($animation)).'"' : '');
	$audio = '<audio'
		. ($id ? ' id="'.esc_attr($id).'"' : '')
		. ' class="sc_audio' . (!empty($class) ? ' '.esc_attr($class) : '') . '"'
		. ' src="'.esc_url($src).'"'
		. (themerex_sc_param_is_on($controls) ? ' controls="controls"' : '')
		. (themerex_sc_param_is_on($autoplay) && is_single() ? ' autoplay="autoplay"' : '')
		//. ' width="'.esc_attr($width).'" height="'.esc_attr($height).'"'
		. ($css!='' ? ' style="'.esc_attr($css).'"' : '')
		. ($data)
		. '></audio>';
		
		
	if ( themerex_get_custom_option('substitute_audio')=='no') { 
		if (themerex_sc_param_is_on($frame)) $audio = themerex_get_audio_frame($audio, $image, $css); 
	} else { 
		if ((isset($_GET['vc_editable']) && $_GET['vc_editable']=='true') && (isset($_POST['action']) && $_POST['action']=='vc_load_shortcode')) {
			$audio = themerex_substitute_audio($audio, false);
		}
	}
	if (themerex_get_theme_option('use_mediaelement')=='yes')
		wp_enqueue_script('wp-mediaelement');
	return apply_filters('themerex_shortcode_output', $audio, 'trx_audio', $atts, $content);
}
// ---------------------------------- [/trx_audio] ---------------------------------------



// ---------------------------------- [trx_blogger] ---------------------------------------

themerex_require_shortcode('trx_blogger', 'themerex_sc_blogger');

global $THEMEREX_GLOBALS;
$THEMEREX_GLOBALS['sc_blogger_busy'] = false;

function themerex_sc_blogger($atts, $content=null){	
	if (themerex_sc_in_shortcode_blogger(true)) return '';
    extract(themerex_sc_html_decode(shortcode_atts(array(
		// Individual params
		"style" => "regular",
		"filters" => "no",
		"post_type" => "post",
		"ids" => "",
		"cat" => "",
		"count" => "3",
		"columns" => "",
		"offset" => "",
		"orderby" => "date",
		"order" => "desc",
		"only" => "no",
		"descr" => "",
		"readmore" => "",
		"loadmore" => "no",
		"location" => "default",
		"dir" => "horizontal",
		"hover" => themerex_get_theme_option('hover_style'),
		"hover_dir" => themerex_get_theme_option('hover_dir'),
		"scroll" => "no",
		"controls" => "no",
		"rating" => "no",
		"info" => "no",
		"links" => "yes",
		"date_format" => "",
		// Common params
		"id" => "",
		"class" => "",
		"css" => "",
		"animation" => "",
		"width" => "",
		"height" => "",
		"top" => "",
		"bottom" => "",
		"left" => "",
		"right" => ""
    ), $atts)));

	$css .= themerex_get_css_position_from_values($top, $right, $bottom, $left, $width, $height);
	$width  = themerex_prepare_css_value($width);
	$height = themerex_prepare_css_value($height);
	
	global $post, $THEMEREX_GLOBALS;

	$THEMEREX_GLOBALS['sc_blogger_busy'] = true;
	$THEMEREX_GLOBALS['sc_blogger_counter'] = 0;

	if (empty($id)) $id = "sc_blogger_".str_replace('.', '', mt_rand());
	
	if ($style=='date' && empty($date_format)) $date_format = 'd.m+Y';

	if (!empty($ids)) {
		$posts = explode(',', str_replace(' ', '', $ids));
		$count = count($posts);
	}
	
	if ($descr == '') $descr = themerex_get_custom_option('post_excerpt_maxlength'.($columns > 1 ? '_masonry' : ''));

	if (!themerex_sc_param_is_off($scroll)) {
		themerex_enqueue_slider();
		if (empty($id)) $id = 'sc_blogger_'.str_replace('.', '', mt_rand());
	}
	


	$layout_sizes = array(
		'masonry_1' => '750x585',
		'masonry_2' => '750x585',
		'masonry_3' => '400x315',
		'masonry_4' => '250x268',
		'classic_2' => '750x585',
		'classic_3' => '400x315',
		'classic_4' => '250x268',
		'portfolio_2' => '750x422',
		'portfolio_3' => '400x225',
		'portfolio_4' => '250x141',
		'grid_2' => '750x422',
		'grid_3' => '400x225',
		'grid_4' => '250x141',
		'square_2' => '750x750',
		'square_3' => '400x400',
		'square_4' => '250x250',
		'courses_2' => '750x329',
		'courses_3' => '400x273',
		'courses_4' => '250x250',
		'news' => '66x66',
		'date' => '0x0',
	);

	$class = apply_filters('themerex_filter_blog_class',
				'sc_blogger'
				. ' layout_'.esc_attr($style)
				. ' template_'.esc_attr(themerex_get_template_name($style))
				. (!empty($class) ? ' '.esc_attr($class) : '')
				. ' ' . esc_attr(themerex_get_template_property($style, 'container_classes'))
				. ' sc_blogger_' . ($dir=='vertical' ? 'vertical' : 'horizontal')
				. (themerex_sc_param_is_on($scroll) && themerex_sc_param_is_on($controls) ? ' sc_scroll_controls sc_scroll_controls_type_top sc_scroll_controls_'.esc_attr($dir) : '')
				. ($descr == 0 ? ' no_description' : ''),
				array('style'=>$style, 'dir'=>$dir, 'descr'=>$descr)
	);

	$container = apply_filters('themerex_filter_blog_container', themerex_get_template_property($style, 'container'), array('style'=>$style, 'dir'=>$dir));
	$container_start = $container_end = '';
	if (!empty($container)) {
		$container = explode('%s', $container);
		$container_start = !empty($container[0]) ? $container[0] : '';
		$container_end = !empty($container[1]) ? $container[1] : '';
	}

	$output = ($style=='list' ? '<ul' : '<div')
			. ($id ? ' id="'.esc_attr($id).'"' : '') 
			. ' class="'.esc_attr($class).'"'
			. ($layout_sizes[$style] != '' ? ' data-size="'.$layout_sizes[$style].'"' : '')
			. ($css!='' ? ' style="'.esc_attr($css).'"' : '')
			. (!themerex_sc_param_is_off($animation) ? ' data-animation="'.esc_attr(themerex_sc_get_animation_classes($animation)).'"' : '')
		. '>'
		. ($container_start)
		. ($dir=='horizontal' && $columns > 1 && themerex_get_template_property($style, 'need_columns') ? '<div class="columns_wrap">' : '')
		. (themerex_sc_param_is_on($scroll) 
			? '<div id="'.esc_attr($id).'_scroll" class="sc_scroll sc_scroll_'.esc_attr($dir).' sc_slider_noresize swiper-slider-container scroll-container"'
				. ' style="'.($dir=='vertical' ? 'height:'.($height != '' ? $height : "230px").';' : 'width:'.($width != '' ? $width.';' : "100%;")).'"'
				. '>'
				. '<div class="sc_scroll_wrapper swiper-wrapper">' 
					. '<div class="sc_scroll_slide swiper-slide">' 
			: '');

	if (themerex_get_template_property($style, 'need_isotope')) {
		if (!themerex_sc_param_is_off($filters))
			$output .= '<div class="isotope_filters"></div>';
		if ($columns<1) $columns = themerex_substr($style, -1);
		$output .= '<div class="isotope_wrap" data-columns="'.max(1, min(4, $columns)).'">';
	}

	$args = array(
		'post_status' => current_user_can('read_private_pages') && current_user_can('read_private_posts') ? array('publish', 'private') : 'publish',
		'posts_per_page' => $count,
		'ignore_sticky_posts' => true,
		'order' => $order=='asc' ? 'asc' : 'desc',
		'orderby' => 'date',
	);

	if ($offset > 0 && empty($ids)) {
		$args['offset'] = $offset;
	}

	$args = themerex_query_add_sort_order($args, $orderby, $order);
	if (!themerex_sc_param_is_off($only)) $args = themerex_query_add_filters($args, $only);
	$args = themerex_query_add_posts_and_cats($args, $ids, $post_type, $cat);

	$query = new WP_Query( $args );

	$flt_ids = array();

	while ( $query->have_posts() ) 
	{ 
		$query->the_post();

		$THEMEREX_GLOBALS['sc_blogger_counter']++;

		$args = array(
			'layout' => $style,
			'show' => false,
			'number' => $THEMEREX_GLOBALS['sc_blogger_counter'],
			'add_view_more' => false,
			'posts_on_page' => ($count > 0 ? $count : $query->found_posts),
			// Additional options to layout generator
			"location" => $location,
			"descr" => $descr,
			"readmore" => $readmore,
			"loadmore" => $loadmore,
			"reviews" => themerex_sc_param_is_on($rating),
			"dir" => $dir,
			"scroll" => themerex_sc_param_is_on($scroll),
			"info" => themerex_sc_param_is_on($info),
			"links" => themerex_sc_param_is_on($links),
			"orderby" => $orderby,
			"columns_count" => $columns,
			"date_format" => $date_format,
			// Get post data
			'strip_teaser' => false,
			'content' => themerex_get_template_property($style, 'need_content'),
			'terms_list' => !themerex_sc_param_is_off($filters) || themerex_get_template_property($style, 'need_terms'),
			'filters' => themerex_sc_param_is_off($filters) ? '' : $filters,
			'hover' => $hover,
			'hover_dir' => $hover_dir
		);
		$post_data = themerex_get_post_data($args);
		$output .= themerex_show_post_layout($args, $post_data);
	
		if (!themerex_sc_param_is_off($filters)) {
			if ($filters == 'tags') {			// Use tags as filter items
				if (!empty($post_data['post_terms'][$post_data['post_taxonomy_tags']]->terms)) {
					foreach ($post_data['post_terms'][$post_data['post_taxonomy_tags']]->terms as $tag) {
						$flt_ids[$tag->term_id] = $tag->name;
					}
				}
			}
		}

	}

	wp_reset_postdata();

	// Close isotope wrapper
	if (themerex_get_template_property($style, 'need_isotope'))
		$output .= '</div>';

	// Isotope filters list
	
	if (!themerex_sc_param_is_off($filters)) {
		$filters_list = '';
		if ($filters == 'categories') {			// Use categories as filter items
		/*
			$taxonomy = themerex_get_taxonomy_categories_by_post_type($post_type);
			$portfolio_parent = $cat ? max(0, themerex_get_parent_taxonomy_by_property($cat, 'show_filters', 'yes', true, $taxonomy)) : 0;
			$args2 = array(
				'type'			=> $post_type,
				'child_of'		=> $portfolio_parent,
				'orderby'		=> 'name',
				'order'			=> 'ASC',
				'hide_empty'	=> 1,
				'hierarchical'	=> 0,
				'exclude'		=> '',
				'include'		=> '',
				'number'		=> '',
				'taxonomy'		=> $taxonomy,
				'pad_counts'	=> false
			);
			$portfolio_list = get_categories($args2);
			if (count($portfolio_list) > 0) {
				$filters_list .= '<a href="#" data-filter="*" class="theme_button active">'.__('All', 'themerex').'</a>';
				foreach ($portfolio_list as $cat) {
					$filters_list .= '<a href="#" data-filter=".flt_'.esc_attr($cat->term_id).'" class="theme_button">'.($cat->name).'</a>';
				}
			}*/
			$key = array();
			for($i=0; $i < sizeof($posts); $i++)
			{
				$post_categories = wp_get_post_categories( $posts[$i] );
			
				foreach($post_categories as $c){
					$cat = get_category( $c );
					$key[$cat->term_id] = '1';
					$key[$cat->name] = '2';
				}
			}
			
			if (count($key) > 0) {
				$filters_list .= '<a href="#" data-filter="*" class="theme_button active">'.__('All', 'themerex').'</a>';
				
				$cats = array_keys($key);
				for($i=0; $i < sizeof($cats); $i++){
					$filters_list .= '<a href="#" data-filter=".flt_'.esc_attr($cats[$i]).'" class="theme_button">'.($cats[$i+1]).'</a>';
					$i++;
				}
			}
			
		} else {								// Use tags as filter items
			if (count($flt_ids) > 0) {
				$filters_list .= '<a href="#" data-filter="*" class="theme_button active">'.__('All', 'themerex').'</a>';
				foreach ($flt_ids as $flt_id=>$flt_name) {
					$filters_list .= '<a href="#" data-filter=".flt_'.esc_attr($flt_id).'" class="theme_button">'.($flt_name).'</a>';
				}
			}
		}
		if ($filters_list) {
			$output .= '<script type="text/javascript">'
				. 'jQuery(document).ready(function () {'
					. 'jQuery("#'.esc_attr($id).' .isotope_filters").append("'.addslashes($filters_list).'");'
				. '});'
				. '</script>';
		}
	}
	$output	.= (themerex_sc_param_is_on($scroll) 
			? '</div></div><div id="'.esc_attr($id).'_scroll_bar" class="sc_scroll_bar sc_scroll_bar_'.esc_attr($dir).' '.esc_attr($id).'_scroll_bar"></div></div>'
				. (!themerex_sc_param_is_off($controls) ? '<div class="sc_scroll_controls_wrap"><a class="sc_scroll_prev" href="#"></a><a class="sc_scroll_next" href="#"></a></div>' : '')
			: '')
		. ($dir=='horizontal' && $columns > 1 && themerex_get_template_property($style, 'need_columns') ? '</div>' : '')
		. ($container_end)
		. ($style == 'list' ? '</ul>' : '</div>');

	// Add template specific scripts and styles
	do_action('themerex_action_blog_scripts', $style);
	
	$THEMEREX_GLOBALS['sc_blogger_busy'] = false;
	
	return apply_filters('themerex_shortcode_output', $output, 'trx_blogger', $atts, $content);
}

function themerex_sc_in_shortcode_blogger($from_blogger = false) {
	if (!$from_blogger) return false;
	global $THEMEREX_GLOBALS;
	return $THEMEREX_GLOBALS['sc_blogger_busy'];
}
// ---------------------------------- [/trx_blogger] ---------------------------------------



// ---------------------------------- [trx_br] ---------------------------------------

themerex_require_shortcode("trx_br", "themerex_sc_br");

function themerex_sc_br($atts, $content = null) {
	if (themerex_sc_in_shortcode_blogger()) return '';
	extract(themerex_sc_html_decode(shortcode_atts(array(
		"clear" => ""
    ), $atts)));
	$output = in_array($clear, array('left', 'right', 'both', 'all')) 
		? '<div class="clearfix" style="clear:' . str_replace('all', 'both', $clear) . '"></div>'
		: '<br />';
	return apply_filters('themerex_shortcode_output', $output, 'trx_br', $atts, $content);
}
// ---------------------------------- [/trx_br] ---------------------------------------



// ---------------------------------- [trx_button] ---------------------------------------

themerex_require_shortcode('trx_button', 'themerex_sc_button');

function themerex_sc_button($atts, $content=null){	
	if (themerex_sc_in_shortcode_blogger()) return '';
    extract(themerex_sc_html_decode(shortcode_atts(array(
		// Individual params
		"size" => "small",
		"icon" => "",
		"color" => "#fff",
		"bg_color" => "#67d3e0",
		"link" => "",
		"target" => "",
		"align" => "",
		"rel" => "",
		"popup" => "no",
		// Common params
		"id" => "",
		"class" => "",
		"css" => "",
		"animation" => "",
		"width" => "",
		"height" => "",
		"top" => "",
		"bottom" => "",
		"left" => "",
		"right" => ""
    ), $atts)));

    $type = "square";
	$style = "filled";
	$bg_style = "link";
	
	$bd_color = $bg_color !== '' ? hex2rgb1($bg_color) : '';
	
	$css .= themerex_get_css_position_from_values($top, $right, $bottom, $left, $width, $height)
		. ($color !== '' ? 'color:' . esc_attr($color) .';' : '')
		. ($bg_color !== '' ? 'background-color:' . esc_attr($bg_color). '; box-shadow: 0 3px rgb('
		. ($bd_color[0] > 30 ? $bd_color[0] - 30 : $bd_color[0] + 30) .','
		. ($bd_color[1] > 30 ? $bd_color[1] - 30 : $bd_color[1] + 30) .','
		. ($bd_color[2] > 30 ? $bd_color[2] - 30 : $bd_color[2] + 30) .') ;' : '');
		
	if (themerex_sc_param_is_on($popup)) themerex_enqueue_popup('magnific');
	$output = '<a href="' . (empty($link) ? '#' : $link) . '"'
		. (!empty($target) ? ' target="'.esc_attr($target).'"' : '')
		. (!empty($rel) ? ' rel="'.esc_attr($rel).'"' : '')
		. (!themerex_sc_param_is_off($animation) ? ' data-animation="'.esc_attr(themerex_sc_get_animation_classes($animation)).'"' : '')
		. ' class="sc_button sc_button_' . esc_attr($type) 
				. ' sc_button_style_' . esc_attr($style) 
				. ' sc_button_bg_' . esc_attr($bg_style)
				. ' sc_button_size_' . esc_attr($size)
				. ($align && $align!='none' ? ' align'.esc_attr($align) : '') 
				. (!empty($class) ? ' '.esc_attr($class) : '')
				. ($icon!='' ? '  sc_button_iconed '. esc_attr($icon) : '') 
				. (themerex_sc_param_is_on($popup) ? ' popup_link' : '') 
				. '"'
		. ($id ? ' id="'.esc_attr($id).'"' : '') 
		. ($css!='' ? ' style="'.esc_attr($css).'"' : '') 
		. '>'
		. do_shortcode($content)
		. '</a>';
	return apply_filters('themerex_shortcode_output', $output, 'trx_button', $atts, $content);
}

// ---------------------------------- [/trx_button] ---------------------------------------





// ---------------------------------- [trx_chat] ---------------------------------------

themerex_require_shortcode('trx_chat', 'themerex_sc_chat');

function themerex_sc_chat($atts, $content=null){	
	if (themerex_sc_in_shortcode_blogger()) return '';
    extract(themerex_sc_html_decode(shortcode_atts(array(
		// Individual params
		"photo" => "",
		"title" => "",
		"link" => "",
		// Common params
		"id" => "",
		"class" => "",
		"css" => "",
		"animation" => "",
		"width" => "",
		"height" => "",
		"top" => "",
		"bottom" => "",
		"left" => "",
		"right" => ""
    ), $atts)));
	$css .= themerex_get_css_position_from_values($top, $right, $bottom, $left, $width, $height);
	$title = $title=='' ? $link : $title;
	if (!empty($photo)) {
		if ($photo > 0) {
			$attach = wp_get_attachment_image_src( $photo, 'full' );
			if (isset($attach[0]) && $attach[0]!='')
				$photo = $attach[0];
		}
		$photo = themerex_get_resized_image_tag($photo, 75, 75);
	}
	$content = do_shortcode($content);
	if (themerex_substr($content, 0, 2)!='<p') $content = '<p>' . ($content) . '</p>';
	$output = '<div' . ($id ? ' id="'.esc_attr($id).'"' : '') 
			. ' class="sc_chat' . (!empty($class) ? ' '.esc_attr($class) : '') . '"' 
			. (!themerex_sc_param_is_off($animation) ? ' data-animation="'.esc_attr(themerex_sc_get_animation_classes($animation)).'"' : '')
			. ($css ? ' style="'.esc_attr($css).'"' : '') 
			. '>'
				. '<div class="sc_chat_inner">'
					. ($photo ? '<div class="sc_chat_avatar">'.($photo).'</div>' : '')
					. ($title == '' ? '' : ('<div class="sc_chat_title">' . ($link!='' ? '<a href="'.esc_url($link).'">' : '') . ($title) . ($link!='' ? '</a>' : '') . '</div>'))
					. '<div class="sc_chat_content">'.($content).'</div>'
				. '</div>'
			. '</div>';
	return apply_filters('themerex_shortcode_output', $output, 'trx_chat', $atts, $content);
}

// ---------------------------------- [/trx_chat] ---------------------------------------




// ---------------------------------- [trx_columns] ---------------------------------------


themerex_require_shortcode('trx_columns', 'themerex_sc_columns');

function themerex_sc_columns($atts, $content=null){	
	if (themerex_sc_in_shortcode_blogger()) return '';
    extract(themerex_sc_html_decode(shortcode_atts(array(
		// Individual params
		"count" => "2",
		"fluid" => "no",
		// Common params
		"id" => "",
		"class" => "",  
		"css" => "", 
		"animation" => "",
		"width" => "",
		"height" => "",
		"top" => "",
		"bottom" => "",
		"left" => "",
		"right" => ""
    ), $atts)));
	$css .= themerex_get_css_position_from_values($top, $right, $bottom, $left, $width, $height);
	$count = max(1, min(12, (int) $count));
	global $THEMEREX_GLOBALS;
	$THEMEREX_GLOBALS['sc_columns_counter'] = 1;
	$THEMEREX_GLOBALS['sc_columns_after_span2'] = false;
	$THEMEREX_GLOBALS['sc_columns_after_span3'] = false;
	$THEMEREX_GLOBALS['sc_columns_after_span4'] = false;
	$THEMEREX_GLOBALS['sc_columns_count'] = $count;
	$output = '<div' . ($id ? ' id="'.esc_attr($id).'"' : '') 
			. ' class="columns_wrap sc_columns'
				. ' columns_' . (themerex_sc_param_is_on($fluid) ? 'fluid' : 'nofluid') 
				. ' sc_columns_count_' . esc_attr($count)
				. (!empty($class) ? ' '.esc_attr($class) : '') 
			. '"'
			. ($css!='' ? ' style="'.esc_attr($css).'"' : '')
			. (!themerex_sc_param_is_off($animation) ? ' data-animation="'.esc_attr(themerex_sc_get_animation_classes($animation)).'"' : '')
			. '>'
				. do_shortcode($content)
			. '</div>';
	return apply_filters('themerex_shortcode_output', $output, 'trx_columns', $atts, $content);
}


themerex_require_shortcode('trx_column_item', 'themerex_sc_column_item');

function themerex_sc_column_item($atts, $content=null) {
	if (themerex_sc_in_shortcode_blogger()) return '';
	extract(themerex_sc_html_decode(shortcode_atts( array(
		// Individual params
		"span" => "1",
		"align" => "",
		"color" => "",
		"bg_color" => "",
		"bg_image" => "",
		// Common params
		"id" => "",
		"class" => "",
		"css" => "",
		"animation" => ""
	), $atts)));
	$css .= ($align !== '' ? 'text-align:' . esc_attr($align) . ';' : '') 
		. ($color !== '' ? 'color:' . esc_attr($color) . ';' : '');
	$span = max(1, min(11, (int) $span));
	global $THEMEREX_GLOBALS;
	$output = '<div' . ($id ? ' id="'.esc_attr($id).'"' : '') . ' class="column-'.($span > 1 ? esc_attr($span) : 1).'_'.esc_attr($THEMEREX_GLOBALS['sc_columns_count']).' sc_column_item sc_column_item_'.esc_attr($THEMEREX_GLOBALS['sc_columns_counter']) 
				. (!empty($class) ? ' '.esc_attr($class) : '')
				. ($THEMEREX_GLOBALS['sc_columns_counter'] % 2 == 1 ? ' odd' : ' even') 
				. ($THEMEREX_GLOBALS['sc_columns_counter'] == 1 ? ' first' : '') 
				. ($span > 1 ? ' span_'.esc_attr($span) : '') 
				. ($THEMEREX_GLOBALS['sc_columns_after_span2'] ? ' after_span_2' : '') 
				. ($THEMEREX_GLOBALS['sc_columns_after_span3'] ? ' after_span_3' : '') 
				. ($THEMEREX_GLOBALS['sc_columns_after_span4'] ? ' after_span_4' : '') 
				. '"'
				. ($css!='' ? ' style="'.esc_attr($css).'"' : '')
				. (!themerex_sc_param_is_off($animation) ? ' data-animation="'.esc_attr(themerex_sc_get_animation_classes($animation)).'"' : '')
				. '>'
				. ($bg_color!=='' || $bg_image !== '' ? '<div class="sc_column_item_inner" style="'
						. ($bg_color !== '' ? 'background-color:' . esc_attr($bg_color) . ';' : '')
						. ($bg_image !== '' ? 'background-image:url(' . esc_url($bg_image) . ');' : '')
						. '">' : '')
					. do_shortcode($content)
				. ($bg_color!=='' || $bg_image !== '' ? '</div>' : '')
				. '</div>';
	$THEMEREX_GLOBALS['sc_columns_counter'] += $span;
	$THEMEREX_GLOBALS['sc_columns_after_span2'] = $span==2;
	$THEMEREX_GLOBALS['sc_columns_after_span3'] = $span==3;
	$THEMEREX_GLOBALS['sc_columns_after_span4'] = $span==4;
	return apply_filters('themerex_shortcode_output', $output, 'trx_column_item', $atts, $content);
}

// ---------------------------------- [/trx_columns] ---------------------------------------



// ---------------------------------- [trx_contact_form] ---------------------------------------

themerex_require_shortcode("trx_contact_form", "themerex_sc_contact_form");

function themerex_sc_contact_form($atts, $content = null) {
	if (themerex_sc_in_shortcode_blogger()) return '';
	extract(themerex_sc_html_decode(shortcode_atts(array(
		// Individual params
		"custom" => "no",
		"action" => "",
		"title" => "",
		"description" => "",
		"align" => "",
		// Common params
		"id" => "",
		"class" => "",
		"css" => "",
		"animation" => "",
		"width" => "",
		"top" => "",
		"bottom" => "",
		"left" => "",
		"right" => ""
    ), $atts)));
	if (empty($id)) $id = "sc_contact_form_".str_replace('.', '', mt_rand());
	$css .= themerex_get_css_position_from_values($top, $right, $bottom, $left, $width);
	// Load core messages
	themerex_enqueue_messages();
	global $THEMEREX_GLOBALS;
	$THEMEREX_GLOBALS['sc_contact_form_id'] = $id;
	$THEMEREX_GLOBALS['sc_contact_form_counter'] = 0;
	$content = do_shortcode($content);
    static $cnt = 0;
    $cnt++;
    $privacy = trx_addons_get_privacy_text();
	$output = '<div ' . ($id ? ' id="'.esc_attr($id).'"' : '') 
			. 'class="sc_contact_form sc_contact_form_'.($content != '' && themerex_sc_param_is_on($custom) ? 'custom' : 'standard') 
				. (!empty($align) && !themerex_sc_param_is_off($align) ? ' align'.esc_attr($align) : '') 
				. (!empty($class) ? ' '.esc_attr($class) : '') 
				. '"'
			. ($css!='' ? ' style="'.esc_attr($css).'"' : '') 
			. (!themerex_sc_param_is_off($animation) ? ' data-animation="'.esc_attr(themerex_sc_get_animation_classes($animation)).'"' : '')
			. '>'
		. ($title ? '<h5 class="sc_contact_form_title">' . ($title) . '</h5>' : '')
		. ($description ? '<p class="sc_contact_form_description">' . ($description) . '</p>' : '')
		. '<form' . ($id ? ' id="'.esc_attr($id).'"' : '') . ' data-formtype="'.($content ? 'custom' : 'contact').'" method="post" action="' . esc_url($action ? $action : $THEMEREX_GLOBALS['ajax_url']) . '">'
		. ($content != '' && themerex_sc_param_is_on($custom)
			? $content 
			: '<div class="sc_contact_form_info">'
					.'<div class="sc_contact_form_item sc_contact_form_field label_over"><label class="required" for="sc_contact_form_username">' . __('Name', 'themerex') . '</label><input id="sc_contact_form_username" type="text" name="username" placeholder="' . __('Name ', 'themerex') . '"></div>'
					. '<div class="sc_contact_form_item sc_contact_form_field label_over"><label class="required" for="sc_contact_form_email">' . __('E-mail', 'themerex') . '</label><input id="sc_contact_form_email" type="text" name="email" placeholder="' . __('E-mail ', 'themerex') . '"></div>'
				.'</div>'
				.'<div class="sc_contact_form_item sc_contact_form_message label_over"><label class="required" for="sc_contact_form_message">' . __('Message', 'themerex') . '</label><textarea id="sc_contact_form_message" name="message" placeholder="' . __('Message ', 'themerex') . '"></textarea></div>' .
				((!empty($privacy)) ? '<div class="sc_form_field sc_form_field_checkbox">
                  <input type="checkbox" id="i_agree_privacy_policy_sc_form_' . esc_attr($cnt) . '" name="i_agree_privacy_policy" class="sc_form_privacy_checkbox" value="1">
                  <label for="i_agree_privacy_policy_sc_form_' . esc_attr($cnt) . '">' . $privacy . '</label></div>' : '')
        .'<div class="sc_contact_form_item sc_contact_form_button" ' . (!empty($privacy) ? ' disabled="disabled"' : '') . '><button>'.__('SEND', 'themerex').'</button></div>'
		.'<div class="result sc_infobox"></div>'
		.'</form>'
		.'</div>');
	return apply_filters('themerex_shortcode_output', $output, 'trx_contact_form', $atts, $content);
}


themerex_require_shortcode('trx_form_item', 'themerex_sc_contact_form_item');

function themerex_sc_contact_form_item($atts, $content=null) {
	if (themerex_sc_in_shortcode_blogger()) return '';
	extract(themerex_sc_html_decode(shortcode_atts( array(
		// Individual params
		"type" => "text",
		"name" => "",
		"value" => "",
		"checked" => "",
		"align" => "",
		"label" => "",
		"label_position" => "top",
		// Common params
		"id" => "",
		"class" => "",
		"css" => "",
		"animation" => "",
		"top" => "",
		"bottom" => "",
		"left" => "",
		"right" => ""
	), $atts)));
	global $THEMEREX_GLOBALS;
	$THEMEREX_GLOBALS['sc_contact_form_counter']++;
	$css .= themerex_get_css_position_from_values($top, $right, $bottom, $left);
	if (empty($id)) $id = ($THEMEREX_GLOBALS['sc_contact_form_id']).'_'.($THEMEREX_GLOBALS['sc_contact_form_counter']);
	$label = $type!='button' && $type!='submit' && $label ? '<label for="' . esc_attr($id) . '"' . (themerex_sc_param_is_on($checked) ? ' class="selected"' : '') . '>' . esc_attr($label) . '</label>' : $label;
	$output = '<div class="sc_contact_form_item sc_contact_form_item_'.esc_attr($type)
					.' sc_contact_form_'.($type == 'textarea' ? 'message' : ($type == 'button' || $type == 'submit' ? 'button' : 'field'))
					.' label_'.esc_attr($label_position)
					.($class ? ' '.esc_attr($class) : '')
					.($align && $align!='none' ? ' align'.esc_attr($align) : '')
				.'"'
				. ($css!='' ? ' style="'.esc_attr($css).'"' : '') 
				. (!themerex_sc_param_is_off($animation) ? ' data-animation="'.esc_attr(themerex_sc_get_animation_classes($animation)).'"' : '')
				. '>'
		. ($type!='button' && $type!='submit' && ($label_position=='top' || $label_position=='left') ? $label : '')
		. ($type == 'textarea' 
			? '<textarea id="' . esc_attr($id) . '" name="' . esc_attr($name ? $name : $id) . '">' . esc_attr($value) . '</textarea>'
			: ($type=='button' || $type=='submit' 
				? '<button id="' . esc_attr($id) . '">'.($label ? $label : $value).'</button>'
				: '<input type="'.($type ? $type : 'text').'" id="' . esc_attr($id) . '" name="' . esc_attr($name ? $name : $id) . '" value="' . esc_attr($value) . '"' 
					. (themerex_sc_param_is_on($checked) ? ' checked="checked"' : '') . '>'
				)
			)
		. ($type!='button' && $type!='submit' && $label_position=='bottom' ? $label : '')
		. '</div>';
	return apply_filters('themerex_shortcode_output', $output, 'trx_form_item', $atts, $content);
}


// AJAX Callback: Send contact form data
if ( !function_exists( 'sc_contact_form_send' ) ) {
	function themerex_sc_contact_form_send() {
		global $_REQUEST;
	
		if ( !wp_verify_nonce( $_REQUEST['nonce'], 'ajax_nonce' ) )
			die();
	
		$response = array('error'=>'');
		if (!($contact_email = themerex_get_theme_option('contact_email')) && !($contact_email = themerex_get_theme_option('admin_email'))) 
			$response['error'] = __('Unknown admin email!', 'themerex');
		else {
			$type = themerex_substr($_REQUEST['type'], 0, 7);
			parse_str($_POST['data'], $post_data);

			if ($type=='contact') {
				$user_name	= themerex_strshort($post_data['username'],	100);
				$user_email	= themerex_strshort($post_data['email'],	100);
				$user_subj	= themerex_strshort($post_data['subject'],	100);
				$user_msg	= themerex_strshort($post_data['message'],	themerex_get_theme_option('message_maxlength_contacts'));
		
				$subj = sprintf(__('Site %s - Contact form message from %s', 'themerex'), get_bloginfo('site_name'), $user_name);
				$msg = "\n".__('Name:', 'themerex')   .' '.esc_html($user_name)
					.  "\n".__('E-mail:', 'themerex') .' '.esc_html($user_email)
					.  "\n".__('Subject:', 'themerex').' '.esc_html($user_subj)
					.  "\n".__('Message:', 'themerex').' '.esc_html($user_msg);

			} else {

				$subj = sprintf(__('Site %s - Custom form data', 'themerex'), get_bloginfo('site_name'));
				$msg = '';
				foreach ($post_data as $k=>$v)
					$msg .= "\n{$k}: $v";
			}

			$msg .= "\n\n............. " . get_bloginfo('site_name') . " (" . home_url() . ") ............";

			$mail = themerex_get_theme_option('mail_function');
			if (!@$mail($contact_email, $subj, $msg)) {
				$response['error'] = __('Error send message!', 'themerex');
			}
		
			echo json_encode($response);
			die();
		}
	}
}

// ---------------------------------- [/trx_contact_form] ---------------------------------------



// ---------------------------------- [trx_content] ---------------------------------------

themerex_require_shortcode('trx_content', 'themerex_sc_content');

function themerex_sc_content($atts, $content=null){	
	if (themerex_sc_in_shortcode_blogger()) return '';
    extract(themerex_sc_html_decode(shortcode_atts(array(
		// Common params
		"id" => "",
		"class" => "",
		"css" => "",
		"animation" => "",
		"top" => "",
		"bottom" => ""
    ), $atts)));
	$css .= themerex_get_css_position_from_values('!'.($top), '', '!'.($bottom));
	$output = '<div' . ($id ? ' id="'.esc_attr($id).'"' : '') 
		. ' class="sc_content content_wrap' . ($class ? ' '.esc_attr($class) : '') . '"'
		. (!themerex_sc_param_is_off($animation) ? ' data-animation="'.esc_attr(themerex_sc_get_animation_classes($animation)).'"' : '')
		. ($css!='' ? ' style="'.esc_attr($css).'"' : '').'>' 
		. do_shortcode($content) 
		. '</div>';
	return apply_filters('themerex_shortcode_output', $output, 'trx_content', $atts, $content);
}
// ---------------------------------- [/trx_content] ---------------------------------------



// ---------------------------------- [trx_countdown] ---------------------------------------

themerex_require_shortcode("trx_countdown", "themerex_sc_countdown");

function themerex_sc_countdown($atts, $content = null) {
	if (themerex_sc_in_shortcode_blogger()) return '';
	extract(themerex_sc_html_decode(shortcode_atts(array(
		// Individual params
		"date" => "",
		"time" => "",
		"style" => "2",
		"align" => "center",
		// Common params
		"id" => "",
		"class" => "",
		"css" => "",
		"animation" => "",
		"top" => "",
		"bottom" => "",
		"left" => "",
		"right" => "",
		"width" => "",
		"height" => ""
    ), $atts)));
	if (empty($id)) $id = "sc_countdown_".str_replace('.', '', mt_rand());
	$css .= themerex_get_css_position_from_values($top, $right, $bottom, $left, $width, $height);
	if (empty($interval)) $interval = 1;
	
	if($style == 3){
	wp_enqueue_style('themerex-flipclock-style', themerex_get_file_url('js/flipclock/flipclock.css'), array(), null);
	wp_enqueue_script( 'themerex-flipclock-script', themerex_get_file_url('js/flipclock/flipclock.js'), array('jquery'), null, true );	
	
	$output = '<div' . ($id ? ' id="'.esc_attr($id).'"' : '')
		. ' class="sc_countdown sc_countdown_style_3' . (!empty($align) && $align!='none' ? ' align'.esc_attr($align) : '') . (!empty($class) ? ' '.esc_attr($class) : '') .'"'
		. ($css ? ' style="'.esc_attr($css).'"' : '')
		. ' data-date="'.esc_attr(empty($date) ? date('Y-m-d') : $date).'"'
		. ' data-time="'.esc_attr(empty($time) ? '00:00:00' : $time).'"'
		. ' data-style="'.$style.'"'
		. (!themerex_sc_param_is_off($animation) ? ' data-animation="'.esc_attr(themerex_sc_get_animation_classes($animation)).'"' : '')
		. '></div>';
	}
	else{
	wp_enqueue_script( 'themerex-jquery-plugin-script', themerex_get_file_url('js/countdown/jquery.plugin.js'), array('jquery'), null, true );	
	wp_enqueue_script( 'themerex-countdown-script', themerex_get_file_url('js/countdown/jquery.countdown.js'), array('jquery'), null, true );	
	$output = '<div' . ($id ? ' id="'.esc_attr($id).'"' : '')
		. ' class="sc_countdown sc_countdown_style_' . esc_attr(max(1, min(2, $style))) . (!empty($align) && $align!='none' ? ' align'.esc_attr($align) : '') . (!empty($class) ? ' '.esc_attr($class) : '') .'"'
		. ($css ? ' style="'.esc_attr($css).'"' : '')
		. ' data-date="'.esc_attr(empty($date) ? date('Y-m-d') : $date).'"'
		. ' data-time="'.esc_attr(empty($time) ? '00:00:00' : $time).'"'
		. ' data-style="'.$style.'"'
		. (!themerex_sc_param_is_off($animation) ? ' data-animation="'.esc_attr(themerex_sc_get_animation_classes($animation)).'"' : '')
		. '>'
			. '<div class="sc_countdown_item sc_countdown_weeks">'
				. '<span class="sc_countdown_digits"><span></span><span></span></span>'
				. '<span class="sc_countdown_label">'.__('Weeks', 'themerex').'</span>'
			. '</div>'
			. '<div class="sc_countdown_separator">:</div>'
			. '<div class="sc_countdown_item sc_countdown_days">'
				. '<span class="sc_countdown_digits"><span></span><span></span></span>'
				. '<span class="sc_countdown_label">'.__('Days', 'themerex').'</span>'
			. '</div>'
			. '<div class="sc_countdown_separator">:</div>'
			. '<div class="sc_countdown_item sc_countdown_hours">'
				. '<span class="sc_countdown_digits"><span></span><span></span></span>'
				. '<span class="sc_countdown_label">'.__('Hours', 'themerex').'</span>'
			. '</div>'
			. '<div class="sc_countdown_separator">:</div>'
			. '<div class="sc_countdown_item sc_countdown_minutes">'
				. '<span class="sc_countdown_digits"><span></span><span></span></span>'
				. '<span class="sc_countdown_label">'.__('Minutes', 'themerex').'</span>'
			. '</div>'
			. '<div class="sc_countdown_separator">:</div>'
			. '<div class="sc_countdown_item sc_countdown_seconds">'
				. '<span class="sc_countdown_digits"><span></span><span></span></span>'
				. '<span class="sc_countdown_label">'.__('Seconds', 'themerex').'</span>'
			. '</div>'
			. '<div class="sc_countdown_placeholder hide"></div>'
		. '</div>';
	}
	return apply_filters('themerex_shortcode_output', $output, 'trx_countdown', $atts, $content);
}
// ---------------------------------- [/trx_countdown] ---------------------------------------
						


// ---------------------------------- [trx_dropcaps] ---------------------------------------

themerex_require_shortcode('trx_dropcaps', 'themerex_sc_dropcaps');

function themerex_sc_dropcaps($atts, $content=null){
	if (themerex_sc_in_shortcode_blogger()) return '';
    extract(themerex_sc_html_decode(shortcode_atts(array(
		// Individual params
		"style" => "1",
		// Common params
		"id" => "",
		"class" => "",
		"css" => "",
		"animation" => "",
		"top" => "",
		"bottom" => "",
		"left" => "",
		"right" => ""
    ), $atts)));
	$css .= themerex_get_css_position_from_values($top, $right, $bottom, $left);
	$style = min(4, max(1, $style));
	$content = do_shortcode($content);
	$output = themerex_substr($content, 0, 1) == '<' 
		? $content 
		: '<div' . ($id ? ' id="'.esc_attr($id).'"' : '') 
			. ' class="sc_dropcaps sc_dropcaps_style_' . esc_attr($style) . (!empty($class) ? ' '.esc_attr($class) : '') . '"'
			. ($css ? ' style="'.esc_attr($css).'"' : '')
			. (!themerex_sc_param_is_off($animation) ? ' data-animation="'.esc_attr(themerex_sc_get_animation_classes($animation)).'"' : '')
			. '>' 
				. '<span class="sc_dropcaps_item">' . trim(themerex_substr($content, 0, 1)) . '</span>' . trim(themerex_substr($content, 1))
		. '</div>';
	return apply_filters('themerex_shortcode_output', $output, 'trx_dropcaps', $atts, $content);
}
// ---------------------------------- [/trx_dropcaps] ---------------------------------------



// ---------------------------------- [trx_emailer] ---------------------------------------

themerex_require_shortcode("trx_emailer", "themerex_sc_emailer");

function themerex_sc_emailer($atts, $content = null) {
	if (themerex_sc_in_shortcode_blogger()) return '';
	extract(themerex_sc_html_decode(shortcode_atts(array(
		// Individual params
		"group" => "",
		"open" => "yes",
		"align" => "",
		// Common params
		"id" => "",
		"class" => "",
		"css" => "",
		"animation" => "",
		"top" => "",
		"bottom" => "",
		"left" => "",
		"right" => "",
		"width" => "",
		"height" => ""
    ), $atts)));
	$css .= themerex_get_css_position_from_values($top, $right, $bottom, $left, $width, $height);
	// Load core messages
	themerex_enqueue_messages();
	$output = '<div' . ($id ? ' id="'.esc_attr($id).'"' : '')
				. ' class="sc_emailer' . ($align && $align!='none' ? ' align' . esc_attr($align) : '') . (themerex_sc_param_is_on($open) ? ' sc_emailer_opened' : '') . (!empty($class) ? ' '.esc_attr($class) : '') . '"' 
				. ($css ? ' style="'.esc_attr($css).'"' : '') 
				. (!themerex_sc_param_is_off($animation) ? ' data-animation="'.esc_attr(themerex_sc_get_animation_classes($animation)).'"' : '')
				. '>'
			. '<form class="sc_emailer_form">'
			. '<input type="text" class="sc_emailer_input" name="email" value="" placeholder="'.__('Please, enter you email address.', 'themerex').'">'
			. '<a href="#" class="sc_emailer_button icon-mail-1" title="'.__('Submit', 'themerex').'" data-group="'.($group ? $group : __('E-mailer subscription', 'themerex')).'"></a>'
			. '</form>'
		. '</div>';
	return apply_filters('themerex_shortcode_output', $output, 'trx_emailer', $atts, $content);
}
// ---------------------------------- [/trx_emailer] ---------------------------------------




// ---------------------------------- [trx_gap] ---------------------------------------

themerex_require_shortcode("trx_gap", "themerex_sc_gap");
		
function themerex_sc_gap($atts, $content = null) {
	if (themerex_sc_in_shortcode_blogger()) return '';
	$output = themerex_sc_gap_start() . do_shortcode($content) . themerex_sc_gap_end();
	return apply_filters('themerex_shortcode_output', $output, 'trx_gap', $atts, $content);
}

function themerex_sc_gap_start() {
	return '<!-- #TRX_GAP_START# -->';
}

function themerex_sc_gap_end() {
	return '<!-- #TRX_GAP_END# -->';
}

function themerex_sc_gap_wrapper($str) {
	// Move VC row and column and wrapper inside gap
	$str_new = preg_replace('/(<div\s+class="vc_row[^>]*>)[\r\n\s]*(<div\s+class="vc_col[^>]*>)[\r\n\s]*(<div\s+class="wpb_wrapper[^>]*>)[\r\n\s]*('.themerex_sc_gap_start().')/i', '\\4\\1\\2\\3', $str);
	if ($str_new != $str) $str = preg_replace('/('.themerex_sc_gap_end().')[\r\n\s]*(<\/div>)[\r\n\s]*(<\/div>)[\r\n\s]*(<\/div>)/i', '\\2\\3\\4\\1', $str_new);
	// Gap layout
	return str_replace(
			array(
				themerex_sc_gap_start(),
				themerex_sc_gap_end()
			),
			array(
				themerex_close_all_wrappers(false) . '<div class="sc_gap">',
				'</div>' . themerex_open_all_wrappers(false)
			),
			$str
		); 
}
// ---------------------------------- [/trx_gap] ---------------------------------------



// ---------------------------------- [trx_googlemap] ---------------------------------------

themerex_require_shortcode("trx_googlemap", "themerex_sc_google_map");


function themerex_sc_google_map($atts, $content = null) {
	if (themerex_sc_in_shortcode_blogger()) return '';
	extract(themerex_sc_html_decode(shortcode_atts(array(
		// Individual params
		"address" => "90 Merrion Square W, Dublin 2, Ireland",
		"latlng" => "",
		"zoom" => 16,
		"style" => 'default',
		// Common params
		"id" => "",
		"class" => "",
		"css" => "",
		"animation" => "",
		"width" => "",
		"height" => "400",
		"top" => "",
		"bottom" => "",
		"left" => "",
		"right" => ""
    ), $atts)));
	$css .= themerex_get_css_position_from_values($top, $right, $bottom, $left, $width, $height);
	if (empty($id)) $id = 'sc_googlemap_'.str_replace('.', '', mt_rand());
	if (empty($address) && empty($latlng)) {
		$latlng = themerex_get_custom_option('googlemap_latlng');
		if (empty($latlng))	$address = themerex_get_custom_option('googlemap_address');
	}
	if (empty($style)) $style = themerex_get_custom_option('googlemap_style');
	$api_key = themerex_get_theme_option('api_google');
	wp_enqueue_script( 'googlemap', themerex_get_protocol().'://maps.google.com/maps/api/js'.($api_key ? '?key='.$api_key : ''), array(), null, true );
	wp_enqueue_script( 'themerex-googlemap-script', themerex_get_file_url('js/core.googlemap.js'), array(), null, true );

	$output = '<div id="'.esc_attr($id).'"'
		. ' class="sc_googlemap'. (!empty($class) ? ' '.esc_attr($class) : '').'"'
		. ($css!='' ? ' style="'.esc_attr($css).'"' : '') 
		. (!themerex_sc_param_is_off($animation) ? ' data-animation="'.esc_attr(themerex_sc_get_animation_classes($animation)).'"' : '')
		. ' data-address="'.esc_attr($address).'"'
		. ' data-latlng="'.esc_attr($latlng).'"'
		. ' data-zoom="'.esc_attr($zoom).'"'
		. ' data-style="'.esc_attr($style).'"'
		. ' data-point="'.esc_attr(themerex_get_custom_option('googlemap_marker')).'"'
		. '></div>';
	return apply_filters('themerex_shortcode_output', $output, 'trx_googlemap', $atts, $content);
}
// ---------------------------------- [/trx_googlemap] ---------------------------------------




// ---------------------------------- [trx_hide] ---------------------------------------


themerex_require_shortcode('trx_hide', 'themerex_sc_hide');


function themerex_sc_hide($atts, $content=null){	
	if (themerex_sc_in_shortcode_blogger()) return '';
    extract(themerex_sc_html_decode(shortcode_atts(array(
		// Individual params
		"selector" => "",
		"hide" => "on",
		"delay" => 0
    ), $atts)));
	$selector = trim(chop($selector));
	$output = $selector == '' ? '' : 
		'<script type="text/javascript">
			jQuery(document).ready(function() {
				'.($delay>0 ? 'setTimeout(function() {' : '').'
				jQuery("'.esc_attr($selector).'").' . ($hide=='on' ? 'hide' : 'show') . '();
				'.($delay>0 ? '},'.($delay).');' : '').'
			});
		</script>';
	return apply_filters('themerex_shortcode_output', $output, 'trx_hide', $atts, $content);
}
// ---------------------------------- [/trx_hide] ---------------------------------------



// ---------------------------------- [trx_highlight] ---------------------------------------

themerex_require_shortcode('trx_highlight', 'themerex_sc_highlight');


function themerex_sc_highlight($atts, $content=null){	
	if (themerex_sc_in_shortcode_blogger()) return '';
    extract(themerex_sc_html_decode(shortcode_atts(array(
		// Individual params
		"color" => "",
		"bg_color" => "",
		"font_size" => "",
		"type" => "1",
		// Common params
		"id" => "",
		"class" => "",
		"css" => ""
    ), $atts)));
	$css .= ($color != '' ? 'color:' . esc_attr($color) . ';' : '')
		.($bg_color != '' ? 'background-color:' . esc_attr($bg_color) . ';' : '')
		.($font_size != '' ? 'font-size:' . esc_attr(themerex_prepare_css_value($font_size)) . '; line-height: 1em;' : '');
	$output = '<span' . ($id ? ' id="'.esc_attr($id).'"' : '') 
			. ' class="sc_highlight'.($type>0 ? ' sc_highlight_style_'.esc_attr($type) : ''). (!empty($class) ? ' '.esc_attr($class) : '').'"'
			. ($css!='' ? ' style="'.esc_attr($css).'"' : '')
			. '>' 
			. do_shortcode($content) 
			. '</span>';
	return apply_filters('themerex_shortcode_output', $output, 'trx_highlight', $atts, $content);
}
// ---------------------------------- [/trx_highlight] ---------------------------------------


// ---------------------------------- [trx_icon] ---------------------------------------


themerex_require_shortcode('trx_icon', 'themerex_sc_icon');


function themerex_sc_icon($atts, $content=null){	
	if (themerex_sc_in_shortcode_blogger()) return '';
    extract(themerex_sc_html_decode(shortcode_atts(array(
		// Individual params
		"icon" => "",
		"color" => "",
		"bg_color" => "",
		"bg_shape" => "",
		"bg_style" => "",
		"font_size" => "",
		"font_weight" => "",
		"align" => "",
		"link" => "",
		// Common params
		"id" => "",
		"class" => "",
		"css" => "",
		"top" => "",
		"bottom" => "",
		"left" => "",
		"right" => ""
    ), $atts)));
	$css .= themerex_get_css_position_from_values($top, $right, $bottom, $left);
	$css2 = ($font_weight != '' && $font_weight != 'inherit' ? 'font-weight:'. esc_attr($font_weight).';' : '')
		. ($font_size != '' ? 'font-size:' . esc_attr(themerex_prepare_css_value($font_size)) . ';line-height:' . esc_attr(themerex_prepare_css_value($font_size)) . ';' : '')
		. ($color != '' ? 'color:'.esc_attr($color).';' : '')
		. ($bg_color != '' ? 'background-color:'.esc_attr($bg_color).';border-color:'.esc_attr($bg_color).';' : '')
	;
	$output = $icon!='' 
		? ($link ? '<a href="'.esc_url($link).'"' : '<span') . ($id ? ' id="'.esc_attr($id).'"' : '')
			. ' class="sc_icon '.esc_attr($icon)
				. ($bg_shape && !themerex_sc_param_is_inherit($bg_shape) ? ' sc_icon_shape_'.esc_attr($bg_shape) : '')
				. ($bg_style && !themerex_sc_param_is_inherit($bg_style) ? ' sc_icon_bg_'.esc_attr($bg_style) : '')
				. ($align && $align!='none' ? ' align'.esc_attr($align) : '') 
				. (!empty($class) ? ' '.esc_attr($class) : '')
			.'"'
			.($css || $css2 ? ' style="'.($css ? 'display:block;' : '') . ($css) . ($css2) . '"' : '')
			.'>'
			.($link ? '</a>' : '</span>')
		: '';
	return apply_filters('themerex_shortcode_output', $output, 'trx_icon', $atts, $content);
}

// ---------------------------------- [/trx_icon] ---------------------------------------



// ---------------------------------- [trx_image] ---------------------------------------


themerex_require_shortcode('trx_image', 'themerex_sc_image');


function themerex_sc_image($atts, $content=null){	
	if (themerex_sc_in_shortcode_blogger()) return '';
    extract(themerex_sc_html_decode(shortcode_atts(array(
		// Individual params
		"title" => "",
		"align" => "",
		"shape" => "square",
		"src" => "",
		"url" => "",
		"icon" => "",
		// Common params
		"id" => "",
		"class" => "",
		"animation" => "",
		"css" => "",
		"top" => "",
		"bottom" => "",
		"left" => "",
		"right" => "",
		"width" => "",
		"height" => ""
    ), $atts)));
	$css .= themerex_get_css_position_from_values('!'.($top), '!'.($right), '!'.($bottom), '!'.($left), $width, $height);
	$src = $src!='' ? $src : $url;
	if ($src > 0) {
		$attach = wp_get_attachment_image_src( $src, 'full' );
		if (isset($attach[0]) && $attach[0]!='')
			$src = $attach[0];
	}
	if (!empty($width) || !empty($height)) {
		$w = !empty($width) && strlen(intval($width)) == strlen($width) ? $width : null;
		$h = !empty($height) && strlen(intval($height)) == strlen($height) ? $height : null;
		if ($w || $h) $src = themerex_get_resized_image_url($src, $w, $h);
	}
	$output = empty($src) ? '' : ('<figure' . ($id ? ' id="'.esc_attr($id).'"' : '') 
		. ' class="sc_image ' . ($align && $align!='none' ? ' align' . esc_attr($align) : '') . (!empty($shape) ? ' sc_image_shape_'.esc_attr($shape) : '') . (!empty($class) ? ' '.esc_attr($class) : '') . '"'
		. (!themerex_sc_param_is_off($animation) ? ' data-animation="'.esc_attr(themerex_sc_get_animation_classes($animation)).'"' : '')
		. ($css!='' ? ' style="'.esc_attr($css).'"' : '')
		. '>'
			. '<img src="'.esc_url($src).'" alt="img" />'
			. (trim($title) || trim($icon) ? '<figcaption><span'.($icon ? ' class="'.esc_attr($icon).'"' : '').'></span> ' . ($title) . '</figcaption>' : '')
		. '</figure>');
	return apply_filters('themerex_shortcode_output', $output, 'trx_image', $atts, $content);
}

// ---------------------------------- [/trx_image] ---------------------------------------




// ---------------------------------- [trx_infobox] ---------------------------------------

themerex_require_shortcode('trx_infobox', 'themerex_sc_infobox');


function themerex_sc_infobox($atts, $content=null){	
	if (themerex_sc_in_shortcode_blogger()) return '';
    extract(themerex_sc_html_decode(shortcode_atts(array(
		// Individual params
		"style" => "regular",
		"closeable" => "yes",
		"title" => "",
		"before_title" => "",
		"color" => "",
		"bg_color" => "",
		// Common params
		"id" => "",
		"class" => "",
		"animation" => "",
		"css" => "",
		"top" => "",
		"bottom" => "",
		"left" => "",
		"right" => ""
    ), $atts)));
	
	$css .= themerex_get_css_position_from_values($top, $right, $bottom, $left)
		. ($color !== '' ? 'color:' . esc_attr($color) .';' : '')
		. ($bg_color !== '' ? 'background-color:' . esc_attr($bg_color) .';' : '');
	if (empty($title)) {
		if ($title=='none')
			$title = '';
		else if ($style=='regular')
			$title = 'General Message';
		else if ($style=='success')
			$title = 'Success Message';
		else if ($style=='info')
			$title = 'Information Message';
		else if ($style=='error')
			$title = 'Warning!';
	}
	$output = '<div' . ($id ? ' id="'.esc_attr($id).'"' : '') 
			. ' class="sc_infobox sc_infobox_style_' . esc_attr($style) 
				. (themerex_sc_param_is_on($closeable) ? ' sc_infobox_closeable' : '') 
				. (!empty($class) ? ' '.esc_attr($class) : '') 
				. '"'
			. (!themerex_sc_param_is_off($animation) ? ' data-animation="'.esc_attr(themerex_sc_get_animation_classes($animation)).'"' : '')
			. ($css!='' ? ' style="'.esc_attr($css).'"' : '')
			. '>'
			. ($before_title != '' ? '<div class="sc_infobox_before_title">'.$before_title.'</div>' : '')
			. '<div class="sc_infobox_title">'.$title.'</div>'
			. do_shortcode($content) 
			. '</div>';
	return apply_filters('themerex_shortcode_output', $output, 'trx_infobox', $atts, $content);
}

// ---------------------------------- [/trx_infobox] ---------------------------------------



// ---------------------------------- [trx_islands] ---------------------------------------

themerex_require_shortcode('trx_islands', 'themerex_sc_islands');


function themerex_sc_islands($atts, $content=null){	
	if (themerex_sc_in_shortcode_blogger()) return '';
    extract(themerex_sc_html_decode(shortcode_atts(array(
		// Common params
		"id" => "",
		"class" => "",
		"css" => "",
		"top" => "",
		"bottom" => "",
		"left" => "",
		"right" => "",
		"height" => "",
		"width" => ""
    ), $atts)));
	$css .= themerex_get_css_position_from_values($top, $right, $bottom, $left, $width, $height);

	$output = '<ul class="sc_islands '.($class != '' ? $class : '').'"'
			. ($id ? ' id="'.esc_attr($id).'"' : '')
			. ($css!='' ? ' style="'.esc_attr($css).'"' : ''). '>'
			. do_shortcode($content)
			. '</ul>';
	return apply_filters('themerex_shortcode_output', $output, 'trx_islands', $atts, $content);
}


themerex_require_shortcode('trx_islands_item', 'themerex_sc_islands_item');

function themerex_sc_islands_item($atts, $content=null) {
	if (themerex_sc_in_shortcode_blogger()) return '';
	extract(themerex_sc_html_decode(shortcode_atts( array(
		// Individual params
		"type" => "1",
		"color" => "#67d3e0",
		"value" => "",
		"left" => "",
		"right" => ""
	), $atts)));
	
	$css = ($color !== '' ? 'color:'.$color.';' : '')
		  .($left !== '' ? 'left:'.$left.';' : '')
		  .($right !== '' ? 'right:'.$right.';' : '');
		  
	$output = '<li class="sc_islands_item sc_item_type_'.$type.'" style="'.$css.'">'
			  .($value !== '' ? '<div class="sc_item_value"><div class="value">'.$value.'</div><div class="line"></div><div class="circle"></div></div>' : '')
			  .'</li>';
	return apply_filters('themerex_shortcode_output', $output, 'trx_islands_item', $atts, $content);
}

// ---------------------------------- [/trx_islands] ---------------------------------------




// ---------------------------------- [trx_line] ---------------------------------------


themerex_require_shortcode('trx_line', 'themerex_sc_line');

function themerex_sc_line($atts, $content=null){	
	if (themerex_sc_in_shortcode_blogger()) return '';
    extract(themerex_sc_html_decode(shortcode_atts(array(
		// Individual params
		"style" => "solid",
		"color" => "",
		// Common params
		"id" => "",
		"class" => "",
		"animation" => "",
		"css" => "",
		"width" => "",
		"height" => "",
		"top" => "",
		"bottom" => "",
		"left" => "",
		"right" => ""
    ), $atts)));
	$css .= themerex_get_css_position_from_values($top, $right, $bottom, $left, $width)
		.($height !='' ? 'border-top-width:' . esc_attr($height) . 'px;' : '')
		.($style != '' ? 'border-top-style:' . esc_attr($style) . ';' : '')
		.($color != '' ? 'border-top-color:' . esc_attr($color) . ';' : '');
	$output = '<div' . ($id ? ' id="'.esc_attr($id) . '"' : '') 
			. ' class="sc_line' . ($style != '' ? ' sc_line_style_'.esc_attr($style) : '') . (!empty($class) ? ' '.esc_attr($class) : '') . '"'
			. (!themerex_sc_param_is_off($animation) ? ' data-animation="'.esc_attr(themerex_sc_get_animation_classes($animation)).'"' : '')
			. ($css!='' ? ' style="'.esc_attr($css).'"' : '')
			. '></div>';
	return apply_filters('themerex_shortcode_output', $output, 'trx_line', $atts, $content);
}

// ---------------------------------- [/trx_line] ---------------------------------------




// ---------------------------------- [trx_list] ---------------------------------------

themerex_require_shortcode('trx_list', 'themerex_sc_list');


function themerex_sc_list($atts, $content=null){	
	if (themerex_sc_in_shortcode_blogger()) return '';
    extract(themerex_sc_html_decode(shortcode_atts(array(
		// Individual params
		"style" => "ul",
		"icon" => "icon-right-2",
		"icon_color" => "",
		"color" => "",
		// Common params
		"id" => "",
		"class" => "",
		"animation" => "",
		"css" => "",
		"top" => "",
		"bottom" => "",
		"left" => "",
		"right" => ""
    ), $atts)));
	$css .= themerex_get_css_position_from_values($top, $right, $bottom, $left)
		. ($color !== '' ? 'color:' . esc_attr($color) .';' : '');
	if (trim($style) == '' || (trim($icon) == '' && $style=='iconed')) $style = 'ul';
	global $THEMEREX_GLOBALS;
	$THEMEREX_GLOBALS['sc_list_counter'] = 0;
	$THEMEREX_GLOBALS['sc_list_icon'] = empty($icon) || themerex_sc_param_is_inherit($icon) ? "icon-right-2" : $icon;
	$THEMEREX_GLOBALS['sc_list_icon_color'] = $icon_color;
	$THEMEREX_GLOBALS['sc_list_style'] = $style;
	$output = '<' . ($style=='ol' ? 'ol' : 'ul')
			. ($id ? ' id="'.esc_attr($id).'"' : '')
			. ' class="sc_list sc_list_style_' . esc_attr($style) . (!empty($class) ? ' '.esc_attr($class) : '') . '"'
			. ($css!='' ? ' style="'.esc_attr($css).'"' : '')
			. (!themerex_sc_param_is_off($animation) ? ' data-animation="'.esc_attr(themerex_sc_get_animation_classes($animation)).'"' : '')
			. '>'
			. do_shortcode($content)
			. '</' .($style=='ol' ? 'ol' : 'ul') . '>';
	return apply_filters('themerex_shortcode_output', $output, 'trx_list', $atts, $content);
}


themerex_require_shortcode('trx_list_item', 'themerex_sc_list_item');

function themerex_sc_list_item($atts, $content=null) {
	if (themerex_sc_in_shortcode_blogger()) return '';
	extract(themerex_sc_html_decode(shortcode_atts( array(
		// Individual params
		"color" => "",
		"icon" => "",
		"icon_color" => "",
		"title" => "",
		"link" => "",
		"target" => "",
		// Common params
		"id" => "",
		"class" => "",
		"css" => "",
		"bottom" => "",
		"top" => ""
	), $atts)));
	global $THEMEREX_GLOBALS;
	$THEMEREX_GLOBALS['sc_list_counter']++;
	$css .= themerex_get_css_position_from_values($top, '', $bottom, '');
	$css .= $color !== '' ? 'color:' . esc_attr($color) .';' : '';
	if (trim($icon) == '' || themerex_sc_param_is_inherit($icon)) $icon = $THEMEREX_GLOBALS['sc_list_icon'];
	if (trim($color) == '' || themerex_sc_param_is_inherit($icon_color)) $icon_color = $THEMEREX_GLOBALS['sc_list_icon_color'];
	$output = '<li' . ($id ? ' id="'.esc_attr($id).'"' : '') 
		. ' class="sc_list_item' 
		. (!empty($class) ? ' '.esc_attr($class) : '')
		. ($THEMEREX_GLOBALS['sc_list_counter'] % 2 == 1 ? ' odd' : ' even') 
		. ($THEMEREX_GLOBALS['sc_list_counter'] == 1 ? ' first' : '')  
		. '"' 
		. ($css!='' ? ' style="'.esc_attr($css).'"' : '')
		. ($title ? ' title="'.esc_attr($title).'"' : '') 
		. '><span>' 
		. (!empty($link) ? '<a href="'.esc_url($link).'"' . (!empty($target) ? ' target="'.esc_attr($target).'"' : '') . '>' : '')
		. ($THEMEREX_GLOBALS['sc_list_style']=='iconed' && $icon!='' ? '<span class="sc_list_icon '.esc_attr($icon).'"'.($icon_color !== '' ? ' style="color:'.esc_attr($icon_color).';"' : '').'></span>' : '')
		. do_shortcode($content)
		. (!empty($link) ? '</a>': '')
		. '</span></li>';
	return apply_filters('themerex_shortcode_output', $output, 'trx_list_item', $atts, $content);
}

// ---------------------------------- [/trx_list] ---------------------------------------



// ---------------------------------- [trx_number] ---------------------------------------

themerex_require_shortcode('trx_number', 'themerex_sc_number');

function themerex_sc_number($atts, $content=null){	
	if (themerex_sc_in_shortcode_blogger()) return '';
    extract(themerex_sc_html_decode(shortcode_atts(array(
		// Individual params
		"value" => "",
		"align" => "",
		// Common params
		"id" => "",
		"class" => "",
		"animation" => "",
		"css" => "",
		"top" => "",
		"bottom" => "",
		"left" => "",
		"right" => ""
    ), $atts)));
	$css .= themerex_get_css_position_from_values($top, $right, $bottom, $left);
	$output = '<div' . ($id ? ' id="'.esc_attr($id).'"' : '') 
			. ' class="sc_number' 
				. (!empty($align) ? ' align'.esc_attr($align) : '') 
				. (!empty($class) ? ' '.esc_attr($class) : '') 
				. '"'
			. (!themerex_sc_param_is_off($animation) ? ' data-animation="'.esc_attr(themerex_sc_get_animation_classes($animation)).'"' : '')
			. ($css!='' ? ' style="'.esc_attr($css).'"' : '')
			. '>';
	for ($i=0; $i < themerex_strlen($value); $i++) {
		$output .= '<span class="sc_number_item">' . trim(themerex_substr($value, $i, 1)) . '</span>';
	}
	$output .= '</div>';
	return apply_filters('themerex_shortcode_output', $output, 'trx_number', $atts, $content);
}

// ---------------------------------- [/trx_number] ---------------------------------------


// ---------------------------------- [trx_popup] ---------------------------------------

themerex_require_shortcode('trx_popup', 'themerex_sc_popup');

function themerex_sc_popup($atts, $content=null){	
	if (themerex_sc_in_shortcode_blogger()) return '';
    extract(themerex_sc_html_decode(shortcode_atts(array(
		// Common params
		"id" => "",
		"class" => "",
		"css" => "",
		"top" => "",
		"bottom" => "",
		"left" => "",
		"right" => ""
    ), $atts)));
	$css .= themerex_get_css_position_from_values($top, $right, $bottom, $left);
	themerex_enqueue_popup('magnific');
	$output = '<div' . ($id ? ' id="'.esc_attr($id).'"' : '') 
			. ' class="sc_popup mfp-with-anim mfp-hide' . ($class ? ' '.esc_attr($class) : '') . '"'
			. ($css!='' ? ' style="'.esc_attr($css).'"' : '')
			. '>' 
			. do_shortcode($content) 
			. '</div>';
	return apply_filters('themerex_shortcode_output', $output, 'trx_popup', $atts, $content);
}
// ---------------------------------- [/trx_popup] ---------------------------------------



// ---------------------------------- [trx_quote] ---------------------------------------


themerex_require_shortcode('trx_quote', 'themerex_sc_quote');

function themerex_sc_quote($atts, $content=null){	
	if (themerex_sc_in_shortcode_blogger()) return '';
    extract(themerex_sc_html_decode(shortcode_atts(array(
		// Individual params
		"title" => "",
		"cite" => "",
		"style" => "1",
		// Common params
		"id" => "",
		"class" => "",
		"animation" => "",
		"css" => "",
		"width" => "",
		"top" => "",
		"bottom" => "",
		"left" => "",
		"right" => ""
    ), $atts)));
	$css .= themerex_get_css_position_from_values($top, $right, $bottom, $left, $width);
	$cite_param = $cite != '' ? ' cite="'.esc_attr($cite).'"' : '';
	$title = $title=='' ? $cite : $title;
	$content = do_shortcode($content);
	if (themerex_substr($content, 0, 2)!='<p') $content = '<p>' . ($content) . '</p>';
	$output = '<blockquote' 
		. ($id ? ' id="'.esc_attr($id).'"' : '') . ($cite_param) 
		. ' class="sc_quote'. (!empty($class) ? ' '.esc_attr($class) : '').' sc_quote_style_'.$style.'"' 
		. (!themerex_sc_param_is_off($animation) ? ' data-animation="'.esc_attr(themerex_sc_get_animation_classes($animation)).'"' : '')
		. ($css!='' ? ' style="'.esc_attr($css).'"' : '') 
		. '>'
			. ($content)
			. ($title == '' ? '' : ('<p class="sc_quote_title">- ' . ($cite!='' ? '<a href="'.esc_url($cite).'">' : '') . ($title) . ($cite!='' ? '</a>' : '') . '</p>'))
		.'</blockquote>';
	return apply_filters('themerex_shortcode_output', $output, 'trx_quote', $atts, $content);
}

// ---------------------------------- [/trx_quote] ---------------------------------------



// ---------------------------------- [trx_reviews] ---------------------------------------

themerex_require_shortcode("trx_reviews", "themerex_sc_reviews");
						

function themerex_sc_reviews($atts, $content = null) {
	if (themerex_sc_in_shortcode_blogger()) return '';
    extract(themerex_sc_html_decode(shortcode_atts(array(
		// Individual params
		"align" => "right",
		// Common params
		"id" => "",
		"class" => "",
		"animation" => "",
		"css" => "",
		"top" => "",
		"bottom" => "",
		"left" => "",
		"right" => ""
    ), $atts)));
	$css .= themerex_get_css_position_from_values($top, $right, $bottom, $left);
	$output = themerex_sc_param_is_off(themerex_get_custom_option('show_sidebar_main'))
		? '<div' . ($id ? ' id="'.esc_attr($id).'"' : '') 
					. ' class="sc_reviews'
						. ($align && $align!='none' ? ' align'.esc_attr($align) : '')
						. ($class ? ' '.esc_attr($class) : '')
						. '"'
					. ($css!='' ? ' style="'.esc_attr($css).'"' : '')
					. (!themerex_sc_param_is_off($animation) ? ' data-animation="'.esc_attr(themerex_sc_get_animation_classes($animation)).'"' : '')
					. '>'
				. trim(themerex_sc_reviews_placeholder())
				. '</div>'
		: '';
	return apply_filters('themerex_shortcode_output', $output, 'trx_reviews', $atts, $content);
}

function themerex_sc_reviews_placeholder() {
	return '<!-- #TRX_REVIEWS_PLACEHOLDER# -->';
}
	
function themerex_sc_reviews_wrapper($str) {
	$placeholder = themerex_sc_reviews_placeholder();
	if (themerex_strpos($str, $placeholder)!==false) {
		global $THEMEREX_GLOBALS;
		if (!empty($THEMEREX_GLOBALS['reviews_markup'])) {
			$str = str_replace($placeholder, $THEMEREX_GLOBALS['reviews_markup'],	$str);
			$THEMEREX_GLOBALS['reviews_markup'] = '';
		}
	}
	return $str;
}

// ---------------------------------- [/trx_reviews] ---------------------------------------



// ---------------------------------- [trx_search] ---------------------------------------


themerex_require_shortcode('trx_search', 'themerex_sc_search');


function themerex_sc_search($atts, $content=null){	
	if (themerex_sc_in_shortcode_blogger()) return '';
    extract(themerex_sc_html_decode(shortcode_atts(array(
		// Individual params
		"style" => "regular",
		"open" => "fixed",
		"ajax" => "",
		"title" => __('Search ...', 'themerex'),
		// Common params
		"id" => "",
		"class" => "",
		"animation" => "",
		"css" => "",
		"top" => "",
		"bottom" => "",
		"left" => "",
		"right" => ""
    ), $atts)));
	$css .= themerex_get_css_position_from_values($top, $right, $bottom, $left);
	if (empty($ajax)) $ajax = themerex_get_theme_option('use_ajax_search');
	// Load core messages
	themerex_enqueue_messages();
	$output = '<div' . ($id ? ' id="'.esc_attr($id).'"' : '') . ' class="search_wrap search_style_'.esc_attr($style)
					. (!themerex_sc_param_is_off($open) ? ' search_opened' : '')
					. ($open=='fixed' ? ' search_fixed' : '')
					. (themerex_sc_param_is_on($ajax) ? ' search_ajax' : '')
					. ($class ? ' '.esc_attr($class) : '')
					. '"'
				. ($css!='' ? ' style="'.esc_attr($css).'"' : '')
				. (!themerex_sc_param_is_off($animation) ? ' data-animation="'.esc_attr(themerex_sc_get_animation_classes($animation)).'"' : '')
				. ' title="' . __('Open/close search form', 'themerex') . '">
					<a href="#" class="search_icon icon-23"></a>
					<div class="search_form_wrap">
						<form role="search" method="get" class="search_form" action="' . esc_url( home_url( '/' ) ) . '">
							<button type="submit" class="search_submit icon-23" title="' . __('Start search', 'themerex') . '"></button>
							<input type="text" class="search_field" placeholder="' . esc_attr($title) . '" value="' . esc_attr(get_search_query()) . '" name="s" title="'.esc_attr($title).'" />
						</form>
					</div>
					<div class="search_results widget_area bg_tint_light"><a class="search_results_close icon-cancel"></a><div class="search_results_content"></div></div>
			</div>';
	return apply_filters('themerex_shortcode_output', $output, 'trx_search', $atts, $content);
}

// ---------------------------------- [/trx_search] ---------------------------------------




// ---------------------------------- [trx_section] and [trx_block] and [trx_container]---------------------------------------

themerex_require_shortcode('trx_section', 'themerex_sc_section');
themerex_require_shortcode('trx_block', 'themerex_sc_section');
themerex_require_shortcode('trx_container', 'themerex_sc_section');


global $THEMEREX_GLOBALS;
$THEMEREX_GLOBALS['sc_section_dedicated'] = '';

function themerex_sc_section($atts, $content=null){	
	if (themerex_sc_in_shortcode_blogger()) return '';
    extract(themerex_sc_html_decode(shortcode_atts(array(
		// Individual params
		"dedicated" => "no",
		"align" => "none",
		"columns" => "none",
		"pan" => "no",
		"scroll" => "no",
		"scroll_dir" => "horizontal",
		"scroll_controls" => "no",
		"color" => "",
		"bg_tint" => "",
		"bg_color" => "",
		"bg_image" => "",
		"bg_overlay" => "",
		"bg_padding" => "15",
		"bg_texture" => "",
		"font_size" => "",
		"font_weight" => "",
		// Common params
		"id" => "",
		"class" => "",
		"animation" => "",
		"css" => "",
		"width" => "",
		"height" => "",
		"top" => "",
		"bottom" => "",
		"left" => "",
		"right" => ""
    ), $atts)));

	if ($bg_image > 0) {
		$attach = wp_get_attachment_image_src( $bg_image, 'full' );
		if (isset($attach[0]) && $attach[0]!='')
			$bg_image = $attach[0];
	}

	if ($bg_overlay > 0) {
		if ($bg_color=='') $bg_color = apply_filters('themerex_filter_get_theme_bgcolor', '');
		$rgb = themerex_hex2rgb1($bg_color);
	}

	$css .= themerex_get_css_position_from_values(($top), ($right), ($bottom),($left))
		.($color !== '' ? 'color:' . esc_attr($color) . ';' : '')
		.($bg_color !== '' && $bg_overlay==0 ? 'background-color:' . esc_attr($bg_color) . ';' : '')
		.($bg_image !== '' ? 'background-image:url(' . esc_url($bg_image) . ');' : '')
		.($bg_image !== '' ||  $bg_color !== '' ? ' border-radius: 4px; box-shadow: 1px 2px #e8eaec;' : '')
		.(!themerex_sc_param_is_off($pan) ? 'position:relative;' : '')
		.($font_size != '' ? 'font-size:' . esc_attr(themerex_prepare_css_value($font_size)) . '; line-height: 1.3em;' : '')
		.($font_weight != '' && $font_weight != 'inherit' ? 'font-weight:' . esc_attr($font_weight) . ';' : '')
		.($height != '' ? 'height:'.esc_attr($height).';' : '')
		.($width != '' ? 'width:'.esc_attr($width).';' : '')
		.(intval($right) < 0 ? 'max-width: calc(100% - '.intval($right).'px);' : '');
		
//	$css_dim = themerex_get_css_position_from_values('', '', '', '', $width, $height);
//	if ($bg_image == '' && $bg_color == '' && $bg_overlay==0 && $bg_texture==0 && themerex_strlen($bg_texture)<2) $css .= $css_dim;
	
	$width  = themerex_prepare_css_value($width);
	$height = themerex_prepare_css_value($height);

	if ((!themerex_sc_param_is_off($scroll) || !themerex_sc_param_is_off($pan)) && empty($id)) $id = 'sc_section_'.str_replace('.', '', mt_rand());

	if (!themerex_sc_param_is_off($scroll)) themerex_enqueue_slider();

	$output = '<div' . ($id ? ' id="'.esc_attr($id).'"' : '') 
			. ' class="sc_section' 
				. ($class ? ' ' . esc_attr($class) : '') 
				. (intval($right) < 0 ? ' minusRight' : '')
				. ($bg_tint ? ' bg_tint_' . esc_attr($bg_tint) : '') 
				. ($align && $align!='none' ? ' align'.esc_attr($align) : '') 
				. (!empty($columns) && $columns!='none' ? ' column-'.esc_attr($columns) : '') 
				. (themerex_sc_param_is_on($scroll) && !themerex_sc_param_is_off($scroll_controls) ? ' sc_scroll_controls sc_scroll_controls_'.esc_attr($scroll_dir).' sc_scroll_controls_type_'.esc_attr($scroll_controls) : '')
				. '"'
			. (!themerex_sc_param_is_off($animation) ? ' data-animation="'.esc_attr(themerex_sc_get_animation_classes($animation)).'"' : '')
			. ($css!='' ? ' style="'.esc_attr($css).'"' : '').'>' 
			. ($bg_image !== '' || $bg_color !== '' || $bg_overlay>0 || $bg_texture>0 || themerex_strlen($bg_texture)>2
				? '<div class="sc_section_overlay'.($bg_texture>0 ? ' texture_bg_'.esc_attr($bg_texture) : '') . '"'
					. ' style="' . ($bg_overlay>0 ? 'background-color:rgba('.(int)$rgb['r'].','.(int)$rgb['g'].','.(int)$rgb['b'].','.min(1, max(0, $bg_overlay)).');' : '')
						. (themerex_strlen($bg_texture)>2 ? 'background-image:url('.esc_url($bg_texture).');' : '')
						. ($bg_padding > 0 ? ' padding:'.$bg_padding.'px;' : '')
						. '"'
						. ($bg_overlay > 0 ? ' data-overlay="'.esc_attr($bg_overlay).'" data-bg_color="'.esc_attr($bg_color).'"' : '')
						. '>'
						. '<div class="sc_section_content">'
				: '')
			. (themerex_sc_param_is_on($scroll) 
				? '<div id="'.esc_attr($id).'_scroll" class="sc_scroll sc_scroll_'.esc_attr($scroll_dir).' swiper-slider-container scroll-container"'
					. ' style="'.($height != '' ? 'height:'.esc_attr($height).';' : '') . ($width != '' ? 'width:'.esc_attr($width).';' : '').'"'
					. '>'
					. '<div class="sc_scroll_wrapper swiper-wrapper">' 
					. '<div class="sc_scroll_slide swiper-slide">' 
				: '')
			. (themerex_sc_param_is_on($pan) 
				? '<div id="'.esc_attr($id).'_pan" class="sc_pan sc_pan_'.esc_attr($scroll_dir).'">' 
				: '')
			. do_shortcode($content)
			. (themerex_sc_param_is_on($pan) ? '</div>' : '')
			. (themerex_sc_param_is_on($scroll) 
				? '</div></div><div id="'.esc_attr($id).'_scroll_bar" class="sc_scroll_bar sc_scroll_bar_'.esc_attr($scroll_dir).' '.esc_attr($id).'_scroll_bar"></div></div>'
					. (!themerex_sc_param_is_off($scroll_controls) ? '<div class="sc_scroll_controls_wrap"><a class="sc_scroll_prev" href="#"></a><a class="sc_scroll_next" href="#"></a></div>' : '')
				: '')
			. ($bg_image !== '' || $bg_color !== '' || $bg_overlay > 0 || $bg_texture>0 || themerex_strlen($bg_texture)>2 ? '</div></div>' : '')
		. '</div>';
	if (themerex_sc_param_is_on($dedicated)) {
	    global $THEMEREX_GLOBALS;
		if ($THEMEREX_GLOBALS['sc_section_dedicated']=='') {
			$THEMEREX_GLOBALS['sc_section_dedicated'] = $output;
		}
		$output = '';
	}
	return apply_filters('themerex_shortcode_output', $output, 'trx_section', $atts, $content);
}

function themerex_sc_get_dedicated_content() {
    global $THEMEREX_GLOBALS;
    return $THEMEREX_GLOBALS['sc_section_dedicated'];
}

// ---------------------------------- [/trx_section] ---------------------------------------




// ---------------------------------- [trx_skills] ---------------------------------------


themerex_require_shortcode('trx_skills', 'themerex_sc_skills');


function themerex_sc_skills($atts, $content=null){	
	if (themerex_sc_in_shortcode_blogger()) return '';
    extract(themerex_sc_html_decode(shortcode_atts(array(
		// Individual params
		"max_value" => "100",
		"type" => "bar",
		"layout" => "",
		"dir" => "",
		"pie_compact" => "on",
		"pie_cutout" => 0,
		"style" => "1",
		"columns" => "",
		"align" => "",
		"color" => "",
		"bg_color" => "",
		"border_color" => "",
		"title" => "",
		"subtitle" => __("Skills", "themerex"),
		"count" => "",
		// Common params
		"id" => "",
		"class" => "",
		"animation" => "",
		"css" => "",
		"width" => "",
		"height" => "",
		"top" => "",
		"bottom" => "",
		"left" => "",
		"right" => ""
    ), $atts)));
	//New
	$pie_fill = false;
	if($type == 'arc') { $type = 'pie'; $pie_cutout = 50; }
	
    global $THEMEREX_GLOBALS;
	$THEMEREX_GLOBALS['sc_skills_counter'] = 0;
	$THEMEREX_GLOBALS['sc_skills_columns'] = 1;
	$THEMEREX_GLOBALS['sc_skills_height']  = 0;
	$THEMEREX_GLOBALS['sc_skills_type']    = $type;
	$THEMEREX_GLOBALS['sc_skills_pie_compact'] = $pie_compact;
	$THEMEREX_GLOBALS['sc_skills_pie_cutout']  = max(0, min(100, $pie_cutout));
	$THEMEREX_GLOBALS['sc_skills_color']   = $color;
	$THEMEREX_GLOBALS['sc_skills_bg_color']= $bg_color;
	$THEMEREX_GLOBALS['sc_skills_border_color']= $border_color;
	$THEMEREX_GLOBALS['sc_skills_legend']  = '';
	$THEMEREX_GLOBALS['sc_skills_data']    = '';
	themerex_enqueue_diagram($type);
	if ($type!='arc') {
		if ($layout=='' || ($layout=='columns' && $columns<1)) $layout = 'rows';
		if ($layout=='columns') $THEMEREX_GLOBALS['sc_skills_columns'] = $columns;
		if ($type=='bar') {
			if ($dir == '') $dir = 'horizontal';
			if ($dir == 'vertical' && $height < 1) $height = 300;
		}
	}
	if (empty($id)) $id = 'sc_skills_diagram_'.str_replace('.','',mt_rand());
	if ($max_value < 1) $max_value = 100;
	if ($style) {
		$style = max(1, min(4, $style));
		$THEMEREX_GLOBALS['sc_skills_style'] = $style;
	}
	$THEMEREX_GLOBALS['sc_skills_max'] = $max_value;
	$THEMEREX_GLOBALS['sc_skills_dir'] = $dir;
	$THEMEREX_GLOBALS['sc_skills_height'] = themerex_prepare_css_value($height);
	$css .= themerex_get_css_position_from_values($top, $right, $bottom, $left, $width, '');
	$content = do_shortcode($content);
	$output = ($type!='arc' && ($type!='pie' || !themerex_sc_param_is_on($pie_compact)) && $title!='' ? '<h3 class="sc_skills_title">'.($title).'</h3>' : '')
			. '<div id="'.esc_attr($id).'"' 
				. ' class="sc_skills sc_skills_' . esc_attr($type) 
					. ($type=='bar' ? ' sc_skills_'.esc_attr($dir) : '') 
					. ($type=='pie' ? ' sc_skills_compact_'.esc_attr($pie_compact) : '') 
					. (!empty($class) ? ' '.esc_attr($class) : '') 
					. ($align && $align!='none' ? ' align'.esc_attr($align) : '') 
					. '"'
				. ($css!='' ? ' style="'.esc_attr($css).'"' : '')
				. (!themerex_sc_param_is_off($animation) ? ' data-animation="'.esc_attr(themerex_sc_get_animation_classes($animation)).'"' : '')
				. ' data-type="'.esc_attr($type).'"'
				. ' data-subtitle="'.esc_attr($subtitle).'"'
				. ($type=='bar' ? ' data-dir="'.esc_attr($dir).'"' : '')
			. '>'
				. ($layout == 'columns' ? '<div class="columns_wrap sc_skills_'.esc_attr($layout).' sc_skills_columns_'.esc_attr($columns).'">' : '')
				. ($type=='arc' 
					? ('<div class="sc_skills_legend">'.($title!='' ? '<h6 class="sc_skills_title">'.($title).'</h6>' : '').($THEMEREX_GLOBALS['sc_skills_legend']).'</div>'
						. '<div id="'.esc_attr($id).'_diagram" class="sc_skills_arc_canvas"></div>'
						. '<div class="sc_skills_data" style="display:none;">' . ($THEMEREX_GLOBALS['sc_skills_data']) . '</div>'
					  )
					: '')
				. ($type=='pie' && themerex_sc_param_is_on($pie_compact)
					? (($count != 1 ? '<div class="sc_skills_legend">'.($title!='' ? '<h6 class="sc_skills_title">'.($title).'</h6>' : '').($THEMEREX_GLOBALS['sc_skills_legend']).'</div>' : '')
						. '<div id="'.esc_attr($id).'_pie" class="sc_skills_item">'
							. '<canvas id="'.esc_attr($id).'_pie" class="sc_skills_pie_canvas"></canvas>'
							. '<div class="sc_skills_data" style="display:none;">' . ($THEMEREX_GLOBALS['sc_skills_data']) . '</div>'
						. '</div>'
						.($count == 1 ? '<div class="sc_skills_legend">'.($title!='' ? '<h6 class="sc_skills_title">'.($title).'</h6>' : '').($THEMEREX_GLOBALS['sc_skills_legend']).'</div>' : '')
					  )
					: '')
				. ($content)
				. ($layout == 'columns' ? '</div>' : '')
			. '</div>';
	return apply_filters('themerex_shortcode_output', $output, 'trx_skills', $atts, $content);
}


themerex_require_shortcode('trx_skills_item', 'themerex_sc_skills_item');

function themerex_sc_skills_item($atts, $content=null) {
	if (themerex_sc_in_shortcode_blogger()) return '';
	extract(themerex_sc_html_decode(shortcode_atts( array(
		// Individual params
		"title" => "",
		"value" => "",
		"color" => "",
		"bg_color" => "",
		"border_color" => "",
		"style" => "",
		// Common params
		"id" => "",
		"class" => "",
		"css" => ""
	), $atts)));
	global $THEMEREX_GLOBALS;
	$THEMEREX_GLOBALS['sc_skills_counter']++;
	$ed = themerex_substr($value, -1)=='%' ? '%' : '';
	$value = str_replace('%', '', $value);
	if ($THEMEREX_GLOBALS['sc_skills_max'] < $value) $THEMEREX_GLOBALS['sc_skills_max'] = $value;
	$percent = round((float)$value / $THEMEREX_GLOBALS['sc_skills_max'] * 100);
	$start = 0;
	$stop = $value;
	$steps = 100;
	$step = max(1, round($THEMEREX_GLOBALS['sc_skills_max']/$steps));
	$speed = mt_rand(10,40);
	$animation = round(((float)$stop - $start) / $step * $speed);
	$title_block = '<div class="sc_skills_info"><div class="sc_skills_label">' . ($title) . '</div></div>';
	$old_color = $color;
	if (empty($color)) $color = $THEMEREX_GLOBALS['sc_skills_color'];
	if (empty($color)) $color = themerex_get_custom_option('link_color');
	$color = apply_filters('themerex_filter_get_link_color', $color);
	if (empty($bg_color)) $bg_color = $THEMEREX_GLOBALS['sc_skills_bg_color'];
	if (empty($bg_color)) $bg_color = '#f4f7f9';
	if (empty($border_color)) $border_color = $THEMEREX_GLOBALS['sc_skills_border_color'];
	if (empty($border_color)) $border_color = '#ffffff';
	if (empty($style)) $style = $THEMEREX_GLOBALS['sc_skills_style'];
	$style = max(1, min(4, $style));
	$output = '';
	if ($THEMEREX_GLOBALS['sc_skills_type'] == 'arc' || ($THEMEREX_GLOBALS['sc_skills_type'] == 'pie' && themerex_sc_param_is_on($THEMEREX_GLOBALS['sc_skills_pie_compact']))) {
		if ($THEMEREX_GLOBALS['sc_skills_type'] == 'arc' && empty($old_color)) {
			$rgb = themerex_hex2rgb1($color);
			$color = 'rgba('.(int)$rgb['r'].','.(int)$rgb['g'].','.(int)$rgb['b'].','.(1 - 0.1*($THEMEREX_GLOBALS['sc_skills_counter']-1)).')';
		}
		$THEMEREX_GLOBALS['sc_skills_legend'] .= '<div class="sc_skills_legend_item"><span class="sc_skills_legend_marker" style="background-color:'.esc_attr($color).'"></span><span class="sc_skills_legend_value">' . ($value) . '</span><span class="sc_skills_legend_title">' . ($title) . '</span></div>';
		$THEMEREX_GLOBALS['sc_skills_data'] .= '<div' . ($id ? ' id="'.esc_attr($id).'"' : '')
			. ' class="'.esc_attr($THEMEREX_GLOBALS['sc_skills_type']).'"'
			. ($THEMEREX_GLOBALS['sc_skills_type']=='pie'
				? ( ' data-start="'.esc_attr($start).'"'
					. ' data-stop="'.esc_attr($stop).'"'
					. ' data-step="'.esc_attr($step).'"'
					. ' data-steps="'.esc_attr($steps).'"'
					. ' data-max="'.esc_attr($THEMEREX_GLOBALS['sc_skills_max']).'"'
					. ' data-speed="'.esc_attr($speed).'"'
					. ' data-duration="'.esc_attr($animation).'"'
					. ' data-color="'.esc_attr($color).'"'
					. ' data-bg_color="'.esc_attr($bg_color).'"'
					. ' data-border_color="'.esc_attr($border_color).'"'
					. ' data-cutout="'.esc_attr($THEMEREX_GLOBALS['sc_skills_pie_cutout']).'"'
					. ' data-easing="easeOutCirc"'
					. ' data-ed="'.esc_attr($ed).'"'
					)
				: '')
			. '><input type="hidden" class="text" value="'.esc_attr($title).'" /><input type="hidden" class="percent" value="'.esc_attr($percent).'" /><input type="hidden" class="color" value="'.esc_attr($color).'" /></div>';
	} else {
		$output .= ($THEMEREX_GLOBALS['sc_skills_columns'] > 0 ? '<div class="sc_skills_column column-1_'.esc_attr($THEMEREX_GLOBALS['sc_skills_columns']).'">' : '')
				. ($THEMEREX_GLOBALS['sc_skills_type']=='bar' && $THEMEREX_GLOBALS['sc_skills_dir']=='horizontal' ? $title_block : '')
				. '<div' . ($id ? ' id="'.esc_attr($id).'"' : '') 
					. ' class="sc_skills_item' . ($style ? ' sc_skills_style_'.esc_attr($style) : '') 
						. (!empty($class) ? ' '.esc_attr($class) : '')
						. ($THEMEREX_GLOBALS['sc_skills_counter'] % 2 == 1 ? ' odd' : ' even') 
						. ($THEMEREX_GLOBALS['sc_skills_counter'] == 1 ? ' first' : '') 
						. '"'
					. ($THEMEREX_GLOBALS['sc_skills_height'] !='' || $css ? ' style="height: '.esc_attr($THEMEREX_GLOBALS['sc_skills_height']).';'.($css).'"' : '')
				. '>';
		if (in_array($THEMEREX_GLOBALS['sc_skills_type'], array('bar', 'counter'))) {
			$output .= '<div class="sc_skills_count"' . ($THEMEREX_GLOBALS['sc_skills_type']=='bar' && $color ? ' style="background-color:' . esc_attr($color) . '; border-color:' . esc_attr($color) . '"' : '') . '>'
						. '<div class="sc_skills_total"'
							. ' data-start="'.esc_attr($start).'"'
							. ' data-stop="'.esc_attr($stop).'"'
							. ' data-step="'.esc_attr($step).'"'
							. ' data-max="'.esc_attr($THEMEREX_GLOBALS['sc_skills_max']).'"'
							. ' data-speed="'.esc_attr($speed).'"'
							. ' data-duration="'.esc_attr($animation).'"'
							. ' data-ed="'.esc_attr($ed).'">'
							. ($start) . ($ed)
						.'</div>'
					. '</div>';
		} else if ($THEMEREX_GLOBALS['sc_skills_type']=='pie') {
			if (empty($id)) $id = 'sc_skills_canvas_'.str_replace('.','',mt_rand());
			$output .= '<canvas id="'.esc_attr($id).'"></canvas>'
				. '<div class="sc_skills_total"'
					. ' data-start="'.esc_attr($start).'"'
					. ' data-stop="'.esc_attr($stop).'"'
					. ' data-step="'.esc_attr($step).'"'
					. ' data-steps="'.esc_attr($steps).'"'
					. ' data-max="'.esc_attr($THEMEREX_GLOBALS['sc_skills_max']).'"'
					. ' data-speed="'.esc_attr($speed).'"'
					. ' data-duration="'.esc_attr($animation).'"'
					. ' data-color="'.esc_attr($color).'"'
					. ' data-bg_color="'.esc_attr($bg_color).'"'
					. ' data-border_color="'.esc_attr($border_color).'"'
					. ' data-cutout="'.esc_attr($THEMEREX_GLOBALS['sc_skills_pie_cutout']).'"'
					. ' data-easing="easeOutCirc"'
					. ' data-ed="'.esc_attr($ed).'">'
					. ($start) . ($ed)
				.'</div>';
		}
		$output .= 
				  ($THEMEREX_GLOBALS['sc_skills_type']=='counter' ? $title_block : '')
				. '</div>'
				. ($THEMEREX_GLOBALS['sc_skills_type']=='bar' && $THEMEREX_GLOBALS['sc_skills_dir']=='vertical' || $THEMEREX_GLOBALS['sc_skills_type'] == 'pie' ? $title_block : '')
				. ($THEMEREX_GLOBALS['sc_skills_columns'] > 0 ? '</div>' : '');
	}
	return apply_filters('themerex_shortcode_output', $output, 'trx_skills_item', $atts, $content);
}

// ---------------------------------- [/trx_skills] ---------------------------------------




// ---------------------------------- [trx_slider] ---------------------------------------

themerex_require_shortcode('trx_slider', 'themerex_sc_slider');


function themerex_sc_slider($atts, $content=null){	
	if (themerex_sc_in_shortcode_blogger()) return '';
    extract(themerex_sc_html_decode(shortcode_atts(array(
		// Individual params
		"engine" => themerex_get_custom_option('substitute_slider_engine'),
		"custom" => "no",
		"alias" => "",
		"post_type" => "post",
		"ids" => "",
		"cat" => "",
		"count" => "0",
		"offset" => "",
		"orderby" => "date",
		"order" => 'desc',
		"controls" => "no",
		"pagination" => "no",
		"titles" => "no",
		"descriptions" => themerex_get_custom_option('slider_info_descriptions'),
		"links" => "no",
		"align" => "",
		"interval" => "",
		"date_format" => "",
		"crop" => "yes",
		"autoheight" => "no",
		// Common params
		"id" => "",
		"class" => "",
		"animation" => "",
		"css" => "",
		"width" => "",
		"height" => "",
		"top" => "",
		"bottom" => "",
		"left" => "",
		"right" => ""
    ), $atts)));

	if (empty($width) && $pagination!='full') $width = "100%";
	if (empty($height) && ($pagination=='full' || $pagination=='over')) $height = 250;
	if (!empty($height) && themerex_sc_param_is_on($autoheight)) $autoheight = "off";
	if (empty($interval)) $interval = mt_rand(5000, 10000);
	
	global $THEMEREX_GLOBALS;
	$THEMEREX_GLOBALS['sc_slider_engine'] = $engine;
	$THEMEREX_GLOBALS['sc_slider_width']  = themerex_prepare_css_value($width);
	$THEMEREX_GLOBALS['sc_slider_height'] = themerex_prepare_css_value($height);
	$THEMEREX_GLOBALS['sc_slider_links']  = themerex_sc_param_is_on($links);
	$THEMEREX_GLOBALS['sc_slider_bg_image'] = false;
	$THEMEREX_GLOBALS['sc_slider_crop_image'] = $crop;

	if (empty($id)) $id = "sc_slider_".str_replace('.', '', mt_rand());
	
	$ms = themerex_get_css_position_from_values($top, $right, $bottom, $left);
	$ws = themerex_get_css_position_from_values('', '', '', '', $width);
	$hs = themerex_get_css_position_from_values('', '', '', '', '', $height);

	$css .= (!in_array($pagination, array('full', 'over')) ? $ms : '') . ($hs) . ($ws);
	
	if ($engine!='swiper' && in_array($pagination, array('full', 'over'))) $pagination = 'yes';
	
	$output = (in_array($pagination, array('full', 'over')) 
				? '<div class="sc_slider_pagination_area sc_slider_pagination_'.esc_attr($pagination)
						. ($align!='' && $align!='none' ? ' align'.esc_attr($align) : '')
						. '"'
					. (!themerex_sc_param_is_off($animation) ? ' data-animation="'.esc_attr(themerex_sc_get_animation_classes($animation)).'"' : '')
					. (($ms).($hs) ? ' style="'.esc_attr(($ms).($hs)).'"' : '') 
					.'>' 
				: '')
			. '<div' . ($id ? ' id="'.esc_attr($id).'"' : '') 
			. ' class="sc_slider sc_slider_' . esc_attr($engine)
				. ($engine=='swiper' ? ' swiper-slider-container' : '')
				. (!empty($class) ? ' '.esc_attr($class) : '')
				. (themerex_sc_param_is_on($autoheight) ? ' sc_slider_height_auto' : '')
				. ($hs ? ' sc_slider_height_fixed' : '')
				. (themerex_sc_param_is_on($controls) ? ' sc_slider_controls' : ' sc_slider_nocontrols')
				. (themerex_sc_param_is_on($pagination) ? ' sc_slider_pagination' : ' sc_slider_nopagination')
				. (!in_array($pagination, array('full', 'over')) && $align!='' && $align!='none' ? ' align'.esc_attr($align) : '')
				. '"'
			. (!in_array($pagination, array('full', 'over')) && !themerex_sc_param_is_off($animation) ? ' data-animation="'.esc_attr(themerex_sc_get_animation_classes($animation)).'"' : '')
			. (!empty($width) && themerex_strpos($width, '%')===false ? ' data-old-width="' . esc_attr($width) . '"' : '')
			. (!empty($height) && themerex_strpos($height, '%')===false ? ' data-old-height="' . esc_attr($height) . '"' : '')
			. ((int) $interval > 0 ? ' data-interval="'.esc_attr($interval).'"' : '')
			. ($css!='' ? ' style="'.esc_attr($css).'"' : '')
		. '>';

	themerex_enqueue_slider($engine);

	if ($engine=='revo') {
		if (themerex_exists_revslider() && !empty($alias))
			$output .= do_shortcode('[rev_slider '.esc_attr($alias).']');
		else
			$output = '';
	} else if ($engine=='swiper') {
		
		$caption = '';

		$output .= '<div class="slides'
			.($engine=='swiper' ? ' swiper-wrapper' : '').'"'
			.($engine=='swiper' && $THEMEREX_GLOBALS['sc_slider_bg_image'] ? ' style="'.esc_attr($hs).'"' : '')
			.'>';

		$content = do_shortcode($content);
		
		if (themerex_sc_param_is_on($custom) && $content) {
			$output .= $content;
		} else {
			global $post;
	
			if (!empty($ids)) {
				$posts = explode(',', $ids);
				$count = count($posts);
			}
		
			$args = array(
				'post_type' => 'post',
				'post_status' => 'publish',
				'posts_per_page' => $count,
				'ignore_sticky_posts' => true,
				'order' => $order=='asc' ? 'asc' : 'desc',
			);
	
			if ($offset > 0 && empty($ids)) {
				$args['offset'] = $offset;
			}
	
			$args = themerex_query_add_sort_order($args, $orderby, $order);
			$args = themerex_query_add_filters($args, 'thumbs');
			$args = themerex_query_add_posts_and_cats($args, $ids, $post_type, $cat);

			$query = new WP_Query( $args );

			$post_number = 0;
			$pagination_items = '';
			$show_image 	= 1;
			$show_types 	= 0;
			$show_date 		= 1;
			$show_author 	= 0;
			$show_links 	= 0;
			$show_counters	= 'views';	//comments | rating
			
			while ( $query->have_posts() ) { 
				$query->the_post();
				$post_number++;
				$post_id = get_the_ID();
				$post_type = get_post_type();
				$post_title = get_the_title();
				$post_link = get_permalink();
				$post_date = get_the_date('F j, Y').' at '.get_the_date('g:i a');
				$post_attachment = wp_get_attachment_url(get_post_thumbnail_id($post_id));
				$post_likes = themerex_get_post_likes($post_id);
				
				if (themerex_sc_param_is_on($crop)) {
					$post_attachment = $THEMEREX_GLOBALS['sc_slider_bg_image']
						? themerex_get_resized_image_url($post_attachment, !empty($width) && (float) $width.' ' == $width.' ' ? $width : null, !empty($height) && (float) $height.' ' == $height.' ' ? $height : null)
						: themerex_get_resized_image_tag($post_attachment, !empty($width) && (float) $width.' ' == $width.' ' ? $width : null, !empty($height) && (float) $height.' ' == $height.' ' ? $height : null);
				} else if (!$THEMEREX_GLOBALS['sc_slider_bg_image']) {
					$post_attachment = '<img src="'.esc_url($post_attachment).'" alt="img">';
				}
				$post_accent_color = '';
				$post_category = '';
				$post_category_link = '';

				if (in_array($pagination, array('full', 'over'))) {
					$old_output = $output;
					$output = '';
					require(themerex_get_file_dir('templates/parts/widgets-posts.php'));
					$pagination_items .= $output;
					$output = $old_output;
				}
				$output .= '<div' 
					. ' class="'.esc_attr($engine).'-slide"'
					. ' data-style="'.esc_attr(($ws).($hs)).'"'
					. ' style="'
						. ($THEMEREX_GLOBALS['sc_slider_bg_image'] ? 'background-image:url(' . esc_url($post_attachment) . ');' : '') . ($ws) . ($hs)
						. '"'
					. '>' 
					. (themerex_sc_param_is_on($links) ? '<a href="'.esc_url(get_permalink()).'" title="'.esc_attr($post_title).'">' : '')
					. (!$THEMEREX_GLOBALS['sc_slider_bg_image'] ? $post_attachment : '')
					;
				$caption = $engine=='swiper' ? '' : $caption;
				if (!themerex_sc_param_is_off($titles)) {
					$post_hover_bg  = themerex_get_custom_option('link_color', null, $post_id);
					$post_bg = '';
					if ($post_hover_bg!='' && !themerex_is_inherit_option($post_hover_bg)) {
						$rgb = themerex_hex2rgb1($post_hover_bg);
						$post_hover_ie = str_replace('#', '', $post_hover_bg);
						$post_bg = "background-color: rgba({$rgb['r']},{$rgb['g']},{$rgb['b']},0.8);";
					}
					$caption .= '<div class="sc_slider_info' . ($titles=='fixed' ? ' sc_slider_info_fixed' : '') . ($engine=='swiper' ? ' content-slide' : '') . '"'.($post_bg!='' ? ' style="'.esc_attr($post_bg).'"' : '').'>';
					$post_descr = themerex_get_post_excerpt();
					if (themerex_get_custom_option("slider_info_category")=='yes') { // || empty($cat)) {
						// Get all post's categories
						$post_tax = themerex_get_taxonomy_categories_by_post_type($post_type);
						if (!empty($post_tax)) {
							$post_terms = themerex_get_terms_by_post_id(array('post_id'=>$post_id, 'taxonomy'=>$post_tax));
							if (!empty($post_terms[$post_tax])) {
								if (!empty($post_terms[$post_tax]->closest_parent)) {
									$post_category = $post_terms[$post_tax]->closest_parent->name;
									$post_category_link = $post_terms[$post_tax]->closest_parent->link;
									//$post_accent_color = themerex_taxonomy_get_inherited_property($post_tax, $post_terms[$post_tax]->closest_parent->term_id, 'link_color');
								}
							}
						}
					}
					$output_reviews = '';
					$post_link = get_permalink();
					if (themerex_get_custom_option("slider_info_category")=='yes') $caption .= $output_reviews;
					$caption .= '<h2 class="sc_slider_subtitle"><a href="'.esc_url($post_link).'">'.($post_title).'</a></h2>';
					if (themerex_get_custom_option("slider_info_category")!='yes') $caption .= $output_reviews;
					if($post_likes > 0){
						$output_reviews .= '<div class="post_item_likes icon-heart-3">'.esc_attr($post_likes).'</div>';
					}
					if ($post_date) {
						$caption .= '<div class="sc_slider_date">'.$post_date.'</div>';
					}
					/*if ($descriptions > 0) {
						$caption .= '<div class="sc_slider_descr">'.trim(themerex_strshort($post_descr, $descriptions)).'</div>';
					}*/
					$caption .= '</div>';
				}
				$output .= ($engine=='swiper' ? $caption : '') . (themerex_sc_param_is_on($links) ? '</a>' : '' ) . '</div>';
			}
			wp_reset_postdata();
		}

		$output .= '</div>';
		if ($engine=='swiper') {
			if (themerex_sc_param_is_on($controls))
				$output .= '<div class="sc_slider_controls_wrap"><a class="sc_slider_prev" href="#"></a><a class="sc_slider_next" href="#"></a></div>';
			if (themerex_sc_param_is_on($pagination))
				$output .= '<div class="sc_slider_pagination_wrap"></div>';
		}
	
	} else
		$output = '';
	
	if (!empty($output)) {
		$output .= '</div>';
		if ($pagination_items) {
			$output .= '
				<div class="sc_slider_pagination widget_area"'.($hs ? ' style="'.esc_attr($hs).'"' : '').'>
					<div id="'.esc_attr($id).'_scroll" class="sc_scroll sc_scroll_vertical swiper-slider-container scroll-container"'.($hs ? ' style="'.esc_attr($hs).'"' : '').'>
						<div class="sc_scroll_wrapper swiper-wrapper">
							<div class="sc_scroll_slide swiper-slide">
								'.balanceTags($pagination_items, true).'
							</div>
						</div>
						<div id="'.esc_attr($id).'_scroll_bar" class="sc_scroll_bar sc_scroll_bar_vertical"></div>
					</div>
				</div>';
			$output .= '</div>';
		}
	}

	return apply_filters('themerex_shortcode_output', $output, 'trx_slider', $atts, $content);
}


themerex_require_shortcode('trx_slider_item', 'themerex_sc_slider_item');

function themerex_sc_slider_item($atts, $content=null) {
	if (themerex_sc_in_shortcode_blogger()) return '';
	extract(themerex_sc_html_decode(shortcode_atts( array(
		// Individual params
		"src" => "",
		"url" => "",
		// Common params
		"id" => "",
		"class" => "",
		"css" => ""
	), $atts)));
	global $THEMEREX_GLOBALS;
	$src = $src!='' ? $src : $url;
	if ($src > 0) {
		$attach = wp_get_attachment_image_src( $src, 'full' );
		if (isset($attach[0]) && $attach[0]!='')
			$src = $attach[0];
	}

	if ($src && themerex_sc_param_is_on($THEMEREX_GLOBALS['sc_slider_crop_image'])) {
		$src = $THEMEREX_GLOBALS['sc_slider_bg_image']
			? themerex_get_resized_image_url($src, !empty($THEMEREX_GLOBALS['sc_slider_width']) && themerex_strpos($THEMEREX_GLOBALS['sc_slider_width'], '%')===false ? $THEMEREX_GLOBALS['sc_slider_width'] : null, !empty($THEMEREX_GLOBALS['sc_slider_height']) && themerex_strpos($THEMEREX_GLOBALS['sc_slider_height'], '%')===false ? $THEMEREX_GLOBALS['sc_slider_height'] : null)
			: themerex_get_resized_image_tag($src, !empty($THEMEREX_GLOBALS['sc_slider_width']) && themerex_strpos($THEMEREX_GLOBALS['sc_slider_width'], '%')===false ? $THEMEREX_GLOBALS['sc_slider_width'] : null, !empty($THEMEREX_GLOBALS['sc_slider_height']) && themerex_strpos($THEMEREX_GLOBALS['sc_slider_height'], '%')===false ? $THEMEREX_GLOBALS['sc_slider_height'] : null);
	} else if ($src && !$THEMEREX_GLOBALS['sc_slider_bg_image']) {
		$src = '<img src="'.esc_url($src).'" alt="img">';
	}

	$css .= ($THEMEREX_GLOBALS['sc_slider_bg_image'] ? 'background-image:url(' . esc_url($src) . ');' : '')
			. (!empty($THEMEREX_GLOBALS['sc_slider_width'])  ? 'width:'  . esc_attr($THEMEREX_GLOBALS['sc_slider_width'])  . ';' : '')
			. (!empty($THEMEREX_GLOBALS['sc_slider_height']) ? 'height:' . esc_attr($THEMEREX_GLOBALS['sc_slider_height']) . ';' : '');

	$content = do_shortcode($content);

	$output = '<div' . ($id ? ' id="'.esc_attr($id).'"' : '').' class="'.esc_attr($THEMEREX_GLOBALS['sc_slider_engine']).'-slide' . (!empty($class) ? ' '.esc_attr($class) : '') . '"'
			. ($css ? ' style="'.esc_attr($css).'"' : '')
			.'>' 
			. ($src && themerex_sc_param_is_on($THEMEREX_GLOBALS['sc_slider_links']) ? '<a href="'.esc_url($src).'">' : '')
			. ($src && !$THEMEREX_GLOBALS['sc_slider_bg_image'] ? $src : $content)
			. ($src && themerex_sc_param_is_on($THEMEREX_GLOBALS['sc_slider_links']) ? '</a>' : '')
		. '</div>';
	return apply_filters('themerex_shortcode_output', $output, 'trx_slider_item', $atts, $content);
}
// ---------------------------------- [/trx_slider] ---------------------------------------



// ---------------------------------- [trx_table] ---------------------------------------

themerex_require_shortcode('trx_table', 'themerex_sc_table');

function themerex_sc_table($atts, $content=null){	
	if (themerex_sc_in_shortcode_blogger()) return '';
    extract(themerex_sc_html_decode(shortcode_atts(array(
		// Individual params
		"align" => "",
		// Common params
		"id" => "",
		"class" => "",
		"animation" => "",
		"css" => "",
		"top" => "",
		"bottom" => "",
		"left" => "",
		"right" => "",
		"width" => ""
    ), $atts)));
	$css .= themerex_get_css_position_from_values($top, $right, $bottom, $left, $width);
	$content = str_replace(
				array('<p><table', 'table></p>', '><br />'),
				array('<table', 'table>', '>'),
				html_entity_decode($content, ENT_COMPAT, 'UTF-8'));
	$output = '<div' . ($id ? ' id="'.esc_attr($id).'"' : '') 
			. ' class="sc_table' 
				. (!empty($align) && $align!='none' ? ' align'.esc_attr($align) : '') 
				. (!empty($class) ? ' '.esc_attr($class) : '') 
				. '"'
			. (!themerex_sc_param_is_off($animation) ? ' data-animation="'.esc_attr(themerex_sc_get_animation_classes($animation)).'"' : '')
			. ($css!='' ? ' style="'.esc_attr($css).'"' : '') 
			.'>' 
			. balanceTags(do_shortcode($content), true) 
			. '</div>';
	return apply_filters('themerex_shortcode_output', $output, 'trx_table', $atts, $content);
}

// ---------------------------------- [/trx_table] ---------------------------------------



// ---------------------------------- [trx_tabs] ---------------------------------------

themerex_require_shortcode("trx_tabs", "themerex_sc_tabs");


function themerex_sc_tabs($atts, $content = null) {
	if (themerex_sc_in_shortcode_blogger()) return '';
    extract(themerex_sc_html_decode(shortcode_atts(array(
		// Individual params
		"initial" => "1",
		"scroll" => "no",
		// Common params
		"id" => "",
		"class" => "",
		"animation" => "",
		"css" => "",
		"width" => "",
		"height" => "",
		"top" => "",
		"bottom" => "",
		"left" => "",
		"right" => ""
    ), $atts)));
	$style = "1";
	$css .= themerex_get_css_position_from_values($top, $right, $bottom, $left, $width);

	if (!themerex_sc_param_is_off($scroll)) themerex_enqueue_slider();
	if (empty($id)) $id = 'sc_tabs_'.str_replace('.', '', mt_rand());

	global $THEMEREX_GLOBALS;
	$THEMEREX_GLOBALS['sc_tab_counter'] = 0;
	$THEMEREX_GLOBALS['sc_tab_scroll'] = $scroll;
	$THEMEREX_GLOBALS['sc_tab_height'] = themerex_prepare_css_value($height);
	$THEMEREX_GLOBALS['sc_tab_id']     = $id;
	$THEMEREX_GLOBALS['sc_tab_titles'] = array();

	$content = do_shortcode($content);

	$sc_tab_titles = $THEMEREX_GLOBALS['sc_tab_titles'];

	$initial = max(1, min(count($sc_tab_titles), (int) $initial));

	$tabs_output = '<div' . ($id ? ' id="'.esc_attr($id).'"' : '') 
						. ' class="sc_tabs sc_tabs_style_'.esc_attr($style) . (!empty($class) ? ' '.esc_attr($class) : '') . '"'
						. ($css!='' ? ' style="'.esc_attr($css).'"' : '') 
						. (!themerex_sc_param_is_off($animation) ? ' data-animation="'.esc_attr(themerex_sc_get_animation_classes($animation)).'"' : '')
						. ' data-active="' . ($initial-1) . '"'
						. '>'
					.'<ul class="sc_tabs_titles">';
	$titles_output = '';
	for ($i = 0; $i < count($sc_tab_titles); $i++) {
		$classes = array('sc_tabs_title');
		if ($i == 0) $classes[] = 'first';
		else if ($i == count($sc_tab_titles) - 1) $classes[] = 'last';
		$titles_output .= '<li class="'.join(' ', $classes).'">'
							. '<a href="#'.esc_attr($sc_tab_titles[$i]['id']).'" class="theme_button" id="'.esc_attr($sc_tab_titles[$i]['id']).'_tab">' . ($sc_tab_titles[$i]['title']) . '</a>'
							. '</li>';
	}

	wp_enqueue_script('jquery-ui-tabs', false, array('jquery','jquery-ui-core'), null, true);
	wp_enqueue_script('jquery-effects-fade', false, array('jquery','jquery-effects-core'), null, true);

	$tabs_output .= $titles_output
		. '</ul>' 
		. balanceTags($content, true) 
		.'</div>';
	return apply_filters('themerex_shortcode_output', $tabs_output, 'trx_tabs', $atts, $content);
}


themerex_require_shortcode("trx_tab", "themerex_sc_tab");

function themerex_sc_tab($atts, $content = null) {
	if (themerex_sc_in_shortcode_blogger()) return '';
    extract(themerex_sc_html_decode(shortcode_atts(array(
		// Individual params
		"tab_id" => "",		// get it from VC
		"title" => "",		// get it from VC
		// Common params
		"id" => "",
		"class" => "",
		"css" => ""
    ), $atts)));
    global $THEMEREX_GLOBALS;
	$THEMEREX_GLOBALS['sc_tab_counter']++;
	if (empty($id))
		$id = !empty($tab_id) ? $tab_id : ($THEMEREX_GLOBALS['sc_tab_id']).'_'.($THEMEREX_GLOBALS['sc_tab_counter']);
	$sc_tab_titles = $THEMEREX_GLOBALS['sc_tab_titles'];
	if (isset($sc_tab_titles[$THEMEREX_GLOBALS['sc_tab_counter']-1])) {
		$sc_tab_titles[$THEMEREX_GLOBALS['sc_tab_counter']-1]['id'] = $id;
		if (!empty($title))
			$sc_tab_titles[$THEMEREX_GLOBALS['sc_tab_counter']-1]['title'] = $title;
	} else {
		$sc_tab_titles[] = array(
			'id' => $id,
			'title' => $title
		);
	}
	$THEMEREX_GLOBALS['sc_tab_titles'] = $sc_tab_titles;
	$output = '<div id="'.esc_attr($id).'"'
				.' class="sc_tabs_content' 
					. ($THEMEREX_GLOBALS['sc_tab_counter'] % 2 == 1 ? ' odd' : ' even') 
					. ($THEMEREX_GLOBALS['sc_tab_counter'] == 1 ? ' first' : '') 
					. (!empty($class) ? ' '.esc_attr($class) : '') 
					. '"'
					. ($css!='' ? ' style="'.esc_attr($css).'"' : '') 
					. '>' 
			. (themerex_sc_param_is_on($THEMEREX_GLOBALS['sc_tab_scroll']) 
				? '<div id="'.esc_attr($id).'_scroll" class="sc_scroll sc_scroll_vertical" style="height:'.($THEMEREX_GLOBALS['sc_tab_height'] != '' ? $THEMEREX_GLOBALS['sc_tab_height'] : '200px').';"><div class="sc_scroll_wrapper swiper-wrapper"><div class="sc_scroll_slide swiper-slide">' 
				: '')
			. do_shortcode($content) 
			. (themerex_sc_param_is_on($THEMEREX_GLOBALS['sc_tab_scroll']) 
				? '</div></div><div id="'.esc_attr($id).'_scroll_bar" class="sc_scroll_bar sc_scroll_bar_vertical '.esc_attr($id).'_scroll_bar"></div></div>' 
				: '')
		. '</div>';
	return apply_filters('themerex_shortcode_output', $output, 'trx_tab', $atts, $content);
}
// ---------------------------------- [/trx_tabs] ---------------------------------------




// ---------------------------------- [trx_team] ---------------------------------------


themerex_require_shortcode('trx_team', 'themerex_sc_team');


function themerex_sc_team($atts, $content=null){	
	if (themerex_sc_in_shortcode_blogger()) return '';
    extract(themerex_sc_html_decode(shortcode_atts(array(
		// Individual params
		"style" => 1,
		"columns" => 3,
		"custom" => "no",
		"ids" => "",
		"cat" => "",
		"count" => 3,
		"offset" => "",
		"orderby" => "date",
		"order" => "asc",
		// Common params
		"id" => "",
		"class" => "",
		"animation" => "",
		"css" => "",
		"top" => "",
		"bottom" => "",
		"left" => "",
		"right" => ""
    ), $atts)));
	$css .= themerex_get_css_position_from_values($top, $right, $bottom, $left);
	$count = max(1, (int) $count);
	$columns = max(1, min(4, (int) $columns));
	if (themerex_sc_param_is_off($custom) && $count < $columns) $columns = $count;
	global $THEMEREX_GLOBALS;
	$style = max(1, min(2, $style));
	$THEMEREX_GLOBALS['sc_team_style'] = $style;
	$THEMEREX_GLOBALS['sc_team_columns'] = $columns;
	$THEMEREX_GLOBALS['sc_team_counter'] = 0;
	$output = '<div' . ($id ? ' id="'.esc_attr($id).'"' : '') 
				. ' class="sc_team sc_team_style_'.esc_attr($style).(!empty($class) ? ' '.esc_attr($class) : '').'"'
				. ($css!='' ? ' style="'.esc_attr($css).'"' : '') 
				. (!themerex_sc_param_is_off($animation) ? ' data-animation="'.esc_attr(themerex_sc_get_animation_classes($animation)).'"' : '')
				. '>'
				. '<div class="sc_columns columns_wrap">';

	$content = do_shortcode($content);
		
	if (themerex_sc_param_is_on($custom) && $content) {
		$output .= $content;
	} else {
		global $post;
	
		if (!empty($ids)) {
			$posts = explode(',', $ids);
			$count = count($posts);
		}
		
		$args = array(
			'post_type' => 'team',
			'post_status' => 'publish',
			'posts_per_page' => $count,
			'ignore_sticky_posts' => true,
			'order' => $order=='asc' ? 'asc' : 'desc',
		);
	
		if ($offset > 0 && empty($ids)) {
			$args['offset'] = $offset;
		}
	
		$args = themerex_query_add_sort_order($args, $orderby, $order);
		$args = themerex_query_add_posts_and_cats($args, $ids, 'team', $cat, 'team_group');

		$query = new WP_Query( $args );

		$post_number = 0;
			
		while ( $query->have_posts() ) { 
			$query->the_post();
			$post_number++;
			$post_id = get_the_ID();
			$name = apply_filters('the_title', get_the_title());
			$descr = apply_filters('the_excerpt', get_the_excerpt());
			$post_meta = get_post_meta($post_id, 'team_data', true);
			$position = $post_meta['team_member_position'];
			$link = !empty($post_meta['team_member_link']) ? $post_meta['team_member_link'] : get_permalink($post_id);
			$email = $post_meta['team_member_email'];
			$photo = wp_get_attachment_url(get_post_thumbnail_id($post_id));
			if (empty($photo)) {
				if (!empty($email))
					$photo = get_avatar($email, 326*min(2, max(1, themerex_get_theme_option("retina_ready"))));
			} else {
				$photo = themerex_get_resized_image_tag($photo, 326, 340);
			}
			$socials = '';
			$soc_list = $post_meta['team_member_socials'];
			if (is_array($soc_list) && count($soc_list)>0) {
				$soc_str = '';
				foreach ($soc_list as $sn=>$sl) {
					if (!empty($sl))
						$soc_str .= (!empty($soc_str) ? '|' : '') . ($sn) . '=' . ($sl);
				}
				if (!empty($soc_str))
					$socials = do_shortcode('[trx_socials socials="'.esc_attr($soc_str).'"][/trx_socials]');
			}
			$output .= 	'<div class="column-1_'.esc_attr($columns) . '">'
							. '<div' . ($id ? ' id="'.esc_attr(($id).'_'.($post_number)).'"' : '') 
									. ' class="sc_team_item sc_team_item_' . esc_attr($post_number) 
										. ($post_number % 2 == 1 ? ' odd' : ' even') 
										. ($post_number == 1 ? ' first' : '') 
									. '">'
								. '<div class="sc_team_item_avatar">'
									. ($photo)
									. ($style==2 
										? '<div class="sc_team_item_hover"><div class="sc_team_item_socials">' . ($socials) . '</div></div>'
										: '')
								. '</div>'
								. '<div class="sc_team_item_info">'
									.($email != ''? '<div class="sc_team_item_email_button icon-mail-3"></div>'
									. '<div class="sc_team_item_email">'.$email.'</div>'  : '')
									. '<h6 class="sc_team_item_title">' . ($link ? '<a href="'.esc_url($link).'">' : '') . ($name) . ($link ? '</a>' : '') . '</h6>'
									. '<div class="sc_team_item_position">' . ($position) . '</div>'/*
									. ($style==1 
										? ($descr != '' ? '<div class="sc_team_item_description">' . ($descr) . '</div>' : '') . ($socials)
										: '')*/
								. '</div>'
							. '</div>'
						. '</div>';
		}
		wp_reset_postdata();
	}

	$output .= '</div>'
			. '</div>';
	return apply_filters('themerex_shortcode_output', $output, 'trx_team', $atts, $content);
}


themerex_require_shortcode('trx_team_item', 'themerex_sc_team_item');

function themerex_sc_team_item($atts, $content=null) {
	if (themerex_sc_in_shortcode_blogger()) return '';
	extract(themerex_sc_html_decode(shortcode_atts( array(
		// Individual params
		"user" => "",
		"member" => "",
		"name" => "",
		"position" => "",
		"photo" => "",
		"email" => "",
		"link" => "",
		"socials" => "",
		// Common params
		"id" => "",
		"class" => "",
		"animation" => "",
		"css" => ""
	), $atts)));
	global $THEMEREX_GLOBALS;
	$THEMEREX_GLOBALS['sc_team_counter']++;
	$descr = trim(chop(do_shortcode($content)));
	if (!empty($socials)) $socials = do_shortcode('[trx_socials socials="'.esc_attr($socials).'"][/trx_socials]');
	if (!empty($user) && $user!='none' && ($user_obj = get_user_by('login', $user)) != false) {
		$meta = get_user_meta($user_obj->ID);
		if (empty($email))		$email = $user_obj->data->user_email;
		if (empty($name))		$name = $user_obj->data->display_name;
		if (empty($position))	$position = isset($meta['user_position'][0]) ? $meta['user_position'][0] : '';
		if (empty($descr))		$descr = isset($meta['description'][0]) ? $meta['description'][0] : '';
		if (empty($socials))	$socials = themerex_show_user_socials(array('author_id'=>$user_obj->ID, 'echo'=>false));
	}
	if (!empty($member) && $member!='none' && ($member_obj = (intval($member) > 0 ? get_post($member, OBJECT) : get_page_by_title($member, OBJECT, 'team'))) != null) {
		if (empty($name))		$name = $member_obj->post_title;
		if (empty($descr))		$descr = $member_obj->post_excerpt;
		$post_meta = get_post_meta($member_obj->ID, 'team_data', true);
		if (empty($position))	$position = $post_meta['team_member_position'];
		if (empty($link))		$link = !empty($post_meta['team_member_link']) ? $post_meta['team_member_link'] : get_permalink($member_obj->ID);
		if (empty($email))		$email = $post_meta['team_member_email'];
		if (empty($photo)) 		$photo = wp_get_attachment_url(get_post_thumbnail_id($member_obj->ID));
		if (empty($socials)) {
			$socials = '';
			$soc_list = $post_meta['team_member_socials'];
			if (is_array($soc_list) && count($soc_list)>0) {
				$soc_str = '';
				foreach ($soc_list as $sn=>$sl) {
					if (!empty($sl))
						$soc_str .= (!empty($soc_str) ? '|' : '') . ($sn) . '=' . ($sl);
				}
				if (!empty($soc_str))
					$socials = do_shortcode('[trx_socials socials="'.esc_attr($soc_str).'"][/trx_socials]');
			}
		}
	}
	if (empty($photo)) {
		if (!empty($email)) $photo = get_avatar($email, 350*min(2, max(1, themerex_get_theme_option("retina_ready"))));
	} else {
		if ($photo > 0) {
			$attach = wp_get_attachment_image_src( $photo, 'full' );
			if (isset($attach[0]) && $attach[0]!='')
				$photo = $attach[0];
		}
		$photo = themerex_get_resized_image_tag($photo, 350, 290);
	}
	$output = !empty($name) && !empty($position) 
		? '<div class="column-1_'.esc_attr($THEMEREX_GLOBALS['sc_team_columns']) 
						. (!empty($class) ? ' '.esc_attr($class) : '') 
					. '"'
					. ($css!='' ? ' style="'.esc_attr($css).'"' : '') 
					. (!themerex_sc_param_is_off($animation) ? ' data-animation="'.esc_attr(themerex_sc_get_animation_classes($animation)).'"' : '')
					. '>'
				. '<div' . ($id ? ' id="'.esc_attr($id).'"' : '') 
						. ' class="sc_team_item sc_team_item_' . esc_attr($THEMEREX_GLOBALS['sc_team_counter']) 
							. ($THEMEREX_GLOBALS['sc_team_counter'] % 2 == 1 ? ' odd' : ' even') 
							. ($THEMEREX_GLOBALS['sc_team_counter'] == 1 ? ' first' : '') 
						. '">'
					. '<div class="sc_team_item_avatar">'
						. ($photo)
						. ($THEMEREX_GLOBALS['sc_team_style']==2 
							? '<div class="sc_team_item_hover"><div class="sc_team_item_socials">' . ($socials) . '</div></div>'
							: '')
					. '</div>'
					. '<div class="sc_team_item_info">'
						. ($email != '' ? '<div class="sc_team_item_email_button icon-mail-3"></div><div class="sc_team_item_email">'.$email.'</div>' : '')
						. '<h6 class="sc_team_item_title">' . ($link ? '<a href="'.esc_url($link).'">' : '') . ($name) . ($link ? '</a>' : '') . '</h6>'
						. '<div class="sc_team_item_position">' . ($position) . '</div>'
						. ($THEMEREX_GLOBALS['sc_team_style']==1 
							? '<div class="sc_team_item_description">' . ($descr) . '</div>' . ($socials)
							: '')
					. '</div>'
				. '</div>'
			. '</div>'
		: '';
	return apply_filters('themerex_shortcode_output', $output, 'trx_team_item', $atts, $content);
}

// ---------------------------------- [/trx_team] ---------------------------------------



// ---------------------------------- [trx_testimonials] ---------------------------------------


themerex_require_shortcode('trx_testimonials', 'themerex_sc_testimonials');


function themerex_sc_testimonials($atts, $content=null){	
	if (themerex_sc_in_shortcode_blogger()) return '';
    extract(themerex_sc_html_decode(shortcode_atts(array(
		// Individual params
		"controls" => "yes",
		"interval" => "",
		"autoheight" => "no",
		"align" => "",
		"custom" => "no",
		"ids" => "",
		"cat" => "",
		"count" => "3",
		"offset" => "",
		"orderby" => "date",
		"order" => "desc",
		"bg_tint" => "",
		"bg_color" => "",
		"bg_image" => "",
		"bg_overlay" => "",
		"bg_texture" => "",
		// Common params
		"id" => "",
		"class" => "",
		"animation" => "",
		"css" => "",
		"width" => "",
		"height" => "",
		"top" => "",
		"bottom" => "",
		"left" => "",
		"right" => ""
    ), $atts)));

	if (empty($id)) $id = "sc_testimonials_".str_replace('.', '', mt_rand());
	if (empty($width)) $width = "100%";
	if (!empty($height) && themerex_sc_param_is_on($autoheight)) $autoheight = "no";
	if (empty($interval)) $interval = mt_rand(5000, 10000);

	if ($bg_image > 0) {
		$attach = wp_get_attachment_image_src( $bg_image, 'full' );
		if (isset($attach[0]) && $attach[0]!='')
			$bg_image = $attach[0];
	}

	if ($bg_overlay > 0) {
		if ($bg_color=='') $bg_color = apply_filters('themerex_filter_get_theme_bgcolor', '');
		$rgb = themerex_hex2rgb1($bg_color);
	}
	
	global $THEMEREX_GLOBALS;
	$THEMEREX_GLOBALS['sc_testimonials_width']  = themerex_prepare_css_value($width);
	$THEMEREX_GLOBALS['sc_testimonials_height'] = themerex_prepare_css_value($height);
	
	$ms = themerex_get_css_position_from_values($top, $right, $bottom, $left);
	$ws = themerex_get_css_position_from_values('', '', '', '', $width);
	$hs = themerex_get_css_position_from_values('', '', '', '', '', $height);

	$css .= ($ms) . ($hs) . ($ws);
	
	themerex_enqueue_slider('swiper');

	$output = ($bg_color!='' || $bg_image!='' || $bg_overlay>0 || $bg_texture>0 || themerex_strlen($bg_texture)>2
				? '<div class="sc_testimonials_wrap sc_section'
						. ($bg_tint ? ' bg_tint_' . esc_attr($bg_tint) : '') 
						. '"'
					.' style="'
						. ($bg_color !== '' && $bg_overlay==0 ? 'background-color:' . esc_attr($bg_color) . ';' : '')
						. ($bg_image !== '' ? 'background-image:url(' . esc_url($bg_image) . ');' : '')
						. '"'
					. (!themerex_sc_param_is_off($animation) ? ' data-animation="'.esc_attr(themerex_sc_get_animation_classes($animation)).'"' : '')
					. '>'
					. '<div class="sc_section_overlay'.($bg_texture>0 ? ' texture_bg_'.esc_attr($bg_texture) : '') . '"'
							. ' style="' . ($bg_overlay>0 ? 'background-color:rgba('.(int)$rgb['r'].','.(int)$rgb['g'].','.(int)$rgb['b'].','.min(1, max(0, $bg_overlay)).');' : '')
								. (themerex_strlen($bg_texture)>2 ? 'background-image:url('.esc_url($bg_texture).');' : '')
								. '"'
								. ($bg_overlay > 0 ? ' data-overlay="'.esc_attr($bg_overlay).'" data-bg_color="'.esc_attr($bg_color).'"' : '')
								. '>' 
				: '')
			. '<div' . ($id ? ' id="'.esc_attr($id).'"' : '') 
			. ' class="sc_testimonials sc_slider_swiper swiper-slider-container sc_slider_nopagination'
				. (themerex_sc_param_is_on($controls) ? ' sc_slider_controls' : ' sc_slider_nocontrols')
				. (themerex_sc_param_is_on($autoheight) ? ' sc_slider_height_auto' : '')
				. ($hs ? ' sc_slider_height_fixed' : '')
				. (!empty($class) ? ' '.esc_attr($class) : '')
				. ($align!='' && $align!='none' ? ' align'.esc_attr($align) : '')
				. '"'
			. ($bg_color=='' && $bg_image=='' && $bg_overlay==0 && ($bg_texture=='' || $bg_texture=='0') && !themerex_sc_param_is_off($animation) ? ' data-animation="'.esc_attr(themerex_sc_get_animation_classes($animation)).'"' : '')
			. (!empty($width) && themerex_strpos($width, '%')===false ? ' data-old-width="' . esc_attr($width) . '"' : '')
			. (!empty($height) && themerex_strpos($height, '%')===false ? ' data-old-height="' . esc_attr($height) . '"' : '')
			. ((int) $interval > 0 ? ' data-interval="'.esc_attr($interval).'"' : '')
			. ($css!='' ? ' style="'.esc_attr($css).'"' : '')
		. '>'
		. '<div class="slides swiper-wrapper">';

	$content = do_shortcode($content);
		
	if (themerex_sc_param_is_on($custom) && $content) {
		$output .= $content;
	} else {
		global $post;
	
		if (!empty($ids)) {
			$posts = explode(',', $ids);
			$count = count($posts);
		}
		
		$args = array(
			'post_type' => 'testimonial',
			'post_status' => 'publish',
			'posts_per_page' => $count,
			'ignore_sticky_posts' => true,
			'order' => $order=='asc' ? 'asc' : 'desc',
		);
	
		if ($offset > 0 && empty($ids)) {
			$args['offset'] = $offset;
		}
	
		$args = themerex_query_add_sort_order($args, $orderby, $order);
		$args = themerex_query_add_posts_and_cats($args, $ids, 'testimonial', $cat, 'testimonial_group');

		$query = new WP_Query( $args );

		$post_number = 0;
			
		while ( $query->have_posts() ) { 
			$query->the_post();
			$post_number++;
			$post_id = get_the_ID();
			$post_title = get_the_title();
			$post_meta = get_post_meta($post_id, 'testimonial_data', true);
			$author = $post_meta['testimonial_author'];
			$link = $post_meta['testimonial_link'];
			$email = $post_meta['testimonial_email'];
			$content = apply_filters('the_content', get_the_content());
			$photo = wp_get_attachment_url(get_post_thumbnail_id($post_id));
			if (empty($photo)) {
				if (!empty($email))
					$photo = get_avatar($email, 70*min(2, max(1, themerex_get_theme_option("retina_ready"))));
			} else {
				$photo = themerex_get_resized_image_tag($photo, 70, 70);
			}

			$output .= '<div class="swiper-slide" data-style="'.esc_attr(($ws).($hs)).'" style="'.esc_attr(($ws).($hs)).'">'
						. '<div class="sc_testimonial_item">'
							. ($photo ? '<div class="sc_testimonial_avatar">'.($photo).'</div>' : '')
							. '<div class="sc_testimonial_content">' . ($content) . '</div>'
							. ($author ? '<div class="sc_testimonial_author">' . ($link ? '<a href="'.esc_url($link).'">'.($author).'</a>' : $author) . '</div>' : '')
						. '</div>'
					. '</div>';
		}
		wp_reset_postdata();
	}

	$output .= '</div>';
	if (themerex_sc_param_is_on($controls))
		$output .= '<div class="sc_slider_controls_wrap"><a class="sc_slider_prev" href="#"></a><a class="sc_slider_next" href="#"></a></div>';

	$output .= '</div>'
				. ($bg_color!='' || $bg_image!='' || $bg_overlay>0 || $bg_texture>0 || themerex_strlen($bg_texture)>2
					?  '</div></div>'
					: '');
	return apply_filters('themerex_shortcode_output', $output, 'trx_testimonials', $atts, $content);
}


themerex_require_shortcode('trx_testimonials_item', 'themerex_sc_testimonials_item');

function themerex_sc_testimonials_item($atts, $content=null){	
	if (themerex_sc_in_shortcode_blogger()) return '';
    extract(themerex_sc_html_decode(shortcode_atts(array(
		// Individual params
		"author" => "",
		"link" => "",
		"photo" => "",
		"email" => "",
		// Common params
		"id" => "",
		"class" => "",
		"css" => "",
    ), $atts)));
    global $THEMEREX_GLOBALS;
	if (empty($photo)) {
		if (!empty($email))
			$photo = get_avatar($email, 70*min(2, max(1, themerex_get_theme_option("retina_ready"))));
	} else {
		if ($photo > 0) {
			$attach = wp_get_attachment_image_src( $photo, 'full' );
			if (isset($attach[0]) && $attach[0]!='')
				$photo = $attach[0];
		}
		$photo = themerex_get_resized_image_tag($photo, 70, 70);
	}

	$css2 = (!empty($THEMEREX_GLOBALS['sc_testimonials_width'])  ? 'width:'  . esc_attr($THEMEREX_GLOBALS['sc_testimonials_width'])  . ';' : '')
			. (!empty($THEMEREX_GLOBALS['sc_testimonials_height']) ? 'height:' . esc_attr($THEMEREX_GLOBALS['sc_testimonials_height']) . ';' : '');

	$content = do_shortcode($content);

	$output = '<div class="swiper-slide"' . ($css2 ? ' data-style="'.esc_attr($css2).'" style="'.esc_attr($css2).'"' : '') . '>'
			. '<div' . ($id ? ' id="'.esc_attr($id).'"' : '').' class="sc_testimonial_item' . (!empty($class) ? ' '.esc_attr($class) : '') . '"' . ($css ? ' style="'.esc_attr($css).'"' : '') . '>'
				. ($photo ? '<div class="sc_testimonial_avatar">'.($photo).'</div>' : '')
				. '<div class="sc_testimonial_content">' . ($content) . '</div>'
				. ($author ? '<div class="sc_testimonial_author">' . ($link ? '<a href="'.esc_url($link).'">'.($author).'</a>' : $author) . '</div>' : '')
			. '</div>'
		. '</div>';
	return apply_filters('themerex_shortcode_output', $output, 'trx_testimonials_item', $atts, $content);
}

// ---------------------------------- [/trx_testimonials] ---------------------------------------




// ---------------------------------- [trx_title] ---------------------------------------


themerex_require_shortcode('trx_title', 'themerex_sc_title');

function themerex_sc_title($atts, $content=null){	
	if (themerex_sc_in_shortcode_blogger()) return '';
    extract(themerex_sc_html_decode(shortcode_atts(array(
		// Individual params
		"type" => "1",
		"style" => "regular",
		"align" => "",
		"font_weight" => "",
		"font_size" => "",
		"color" => "",
		"icon" => "",
		"image" => "",
		"picture" => "",
		"image_size" => "small",
		"position" => "left",
		"font_family" => "",
		// Common params
		"id" => "",
		"class" => "",
		"animation" => "",
		"css" => "",
		"width" => "",
		"top" => "",
		"bottom" => "",
		"left" => "",
		"right" => ""
    ), $atts)));
	$css .= themerex_get_css_position_from_values($top, $right, $bottom, $left, $width)
		.($align && $align!='none' && $align!='inherit' ? 'text-align:' . esc_attr($align) .';' : '')
		.($color ? 'color:' . esc_attr($color) .';' : '')
		.($font_weight && $font_weight != 'inherit' ? 'font-weight:' . esc_attr($font_weight) .';' : '')
		.($font_size   ? 'font-size:' . esc_attr($font_size) .'px;' : '')
		.($font_family !== '' ? 'font-family: '.$font_family.', sans-serif;' : '');
		
	$type = min(6, max(1, $type));
	if ($picture > 0) {
		$attach = wp_get_attachment_image_src( $picture, 'full' );
		if (isset($attach[0]) && $attach[0]!='')
			$picture = $attach[0];
	}
	$pic = $style!='iconed' 
		? '' 
		: '<span class="sc_title_icon sc_title_icon_'.esc_attr($position).'  sc_title_icon_'.esc_attr($image_size).($icon!='' && $icon!='none' ? ' '.esc_attr($icon) : '').'"'.'>'
			.($picture ? '<img src="'.esc_url($picture).'" alt="img" />' : '')
			.(empty($picture) && $image && $image!='none' ? '<img src="'.esc_url(themerex_strpos($image, 'http:')!==false ? $image : themerex_get_file_url('images/icons/'.($image).'.png')).'" alt="img" />' : '')
			.'</span>';
	$output = '<h' . esc_attr($type) . ($id ? ' id="'.esc_attr($id).'"' : '')
			. ' class="sc_title sc_title_'.esc_attr($style)
				.($align && $align!='none' && $align!='inherit' ? ' sc_align_' . esc_attr($align) : '')
				.(!empty($class) ? ' '.esc_attr($class) : '')
				.'"'
			. ($css!='' ? ' style="'.esc_attr($css).'"' : '')
			. (!themerex_sc_param_is_off($animation) ? ' data-animation="'.esc_attr(themerex_sc_get_animation_classes($animation)).'"' : '')
			. '>'
				. ($pic)
				. ($style=='divider' ? '<span class="sc_title_divider_before"'.($color ? ' style="background-color: '.esc_attr($color).'"' : '').'></span>' : '')
				. do_shortcode($content) 
				. ($style=='divider' ? '<span class="sc_title_divider_after"'.($color ? ' style="background-color: '.esc_attr($color).'"' : '').'></span>' : '')
			. '</h' . esc_attr($type) . '>';
	return apply_filters('themerex_shortcode_output', $output, 'trx_title', $atts, $content);
}

// ---------------------------------- [/trx_title] ---------------------------------------



// ---------------------------------- [trx_text] ---------------------------------------Ok

themerex_require_shortcode('trx_text', 'themerex_sc_text');

function themerex_sc_text($atts, $content=null){	
	if (themerex_sc_in_shortcode_blogger()) return '';
    extract(themerex_sc_html_decode(shortcode_atts(array(
        "id" => "",
        "align" => "",
        "font_weight" => "inherit", 
        "color" => "", 
		"background" => "",
        "spacing" => "", 
        "uppercase" => "", 
        "height" => "", 
        "font_size" => "", 
        "style" => "",
		"font_family" => "",
        "icon" => "",
        "url" => "",
        "css" => "",
        "class" => "",
        "top" => "",
        "bottom" => "",
        "left" => "",
        "right" => ""
    ), $atts)));
    
    $s = ($top !== '' ? 'margin-top:' . $top . 'px;' : '') 
	. ($bottom !== '' ? 'margin-bottom:' . $bottom . 'px;' : '') 
	. ($left !== '' ? 'margin-left:' . $left . 'px;' : '') 
	. ($right !== '' ? 'margin-right:' . $right . 'px;' : '') 
	. ($font_weight && $font_weight != 'inherit' ? 'font-weight:' . $font_weight . ';' : '') 
	. ($color !== '' ? 'color:' . $color . ';' : '') 
	. ($background !== '' ? 'background:' . $background . ';' : '') 
	. ($spacing !== '' ? 'letter-spacing: ' . $spacing . 'px;' : '') 
	. ($uppercase == 'yes' || $uppercase == 'on' ? 'text-transform: uppercase;' : '') 
	. ($font_size !== '' ? 'font-size: ' . $font_size . 'px;' : '') 
	. ($font_size !== '' ? 'line-height: ' . ($font_size * 1.5) . 'px;' : '') 
	. ($height !== '' ? 'line-height: ' . $height . 'px;' : '') 
	. ($align !== '' ? 'text-align: ' . $align . ';' : '') 
	. ($font_family !== '' ? 'font-family: ' . $font_family . ', sans-serif;' : '') 
	. ($style !== '' ? 'font-style:' . $style . ';' : '')
	. $css;
    
    
    $icon = $icon == 'icon-arrows_slim_right' ? 'arrow' : $icon;

    $output = '';
    if ($url == '') {
        $output = '<p ' . ($id ? ' id="sc_text_' . $id . '"' : '') . ' class="sc_text '.$class.''.($background !== '' ?  ' padding' : '') .''.($icon != '' ? ' sc_iconed '.$icon : '').'"'.($s != '' ? ' style="' . $s . '"' : '').'>' . do_shortcode($content).'</p>';
    } else {
        $output = '<a href="' . $url . '" ' . ($id ? ' id="sc_text_' . $id . '"' : '') . ' class="sc_text sc_text_style_icon '.$class.' '. $icon.'"' . ($s != '' ? ' style="' . $s . '"' : '') . '>' . do_shortcode($content) .'</a>';
    }
	
	return apply_filters('themerex_shortcode_output', $output, 'trx_text', $atts, $content);
}


// ---------------------------------- [/trx_text] ---------------------------------------



// ---------------------------------- [trx_toggles] ---------------------------------------


themerex_require_shortcode('trx_toggles', 'themerex_sc_toggles');

function themerex_sc_toggles($atts, $content=null){	
	if (themerex_sc_in_shortcode_blogger()) return '';
    extract(themerex_sc_html_decode(shortcode_atts(array(
		// Individual params
		"style" => "1",
		"counter" => "off",
		"icon_closed" => "icon-plus-2",
		"icon_opened" => "icon-minus-2",
		// Common params
		"id" => "",
		"class" => "",
		"animation" => "",
		"css" => "",
		"top" => "",
		"bottom" => "",
		"left" => "",
		"right" => ""
    ), $atts)));
	$css .= themerex_get_css_position_from_values($top, $right, $bottom, $left);
	global $THEMEREX_GLOBALS;
	$THEMEREX_GLOBALS['sc_toggle_counter'] = 0;
	$THEMEREX_GLOBALS['sc_toggle_style']   = max(1, min(2, $style));
	$THEMEREX_GLOBALS['sc_toggle_show_counter'] = themerex_sc_param_is_on($counter);
	$THEMEREX_GLOBALS['sc_toggles_icon_closed'] = empty($icon_closed) || themerex_sc_param_is_inherit($icon_closed) ? "icon-plus-2" : $icon_closed;
	$THEMEREX_GLOBALS['sc_toggles_icon_opened'] = empty($icon_opened) || themerex_sc_param_is_inherit($icon_opened) ? "icon-minus-2" : $icon_opened;
	wp_enqueue_script('jquery-effects-slide', false, array('jquery','jquery-effects-core'), null, true);
	$output = '<div' . ($id ? ' id="'.esc_attr($id).'"' : '') 
			. ' class="sc_toggles sc_toggles_style_'.esc_attr($style)
				. (!empty($class) ? ' '.esc_attr($class) : '')
				. (themerex_sc_param_is_on($counter) ? ' sc_show_counter' : '') 
				. '"'
			. (!themerex_sc_param_is_off($animation) ? ' data-animation="'.esc_attr(themerex_sc_get_animation_classes($animation)).'"' : '')
			. ($css!='' ? ' style="'.esc_attr($css).'"' : '') 
			. '>'
			. do_shortcode($content)
			. '</div>';
	return apply_filters('themerex_shortcode_output', $output, 'trx_toggles', $atts, $content);
}


themerex_require_shortcode('trx_toggles_item', 'themerex_sc_toggles_item');

//[trx_toggles_item]
function themerex_sc_toggles_item($atts, $content=null) {
	if (themerex_sc_in_shortcode_blogger()) return '';
	extract(themerex_sc_html_decode(shortcode_atts( array(
		// Individual params
		"title" => "",
		"open" => "",
		"icon_closed" => "",
		"icon_opened" => "",
		// Common params
		"id" => "",
		"class" => "",
		"css" => ""
	), $atts)));
	global $THEMEREX_GLOBALS;
	$THEMEREX_GLOBALS['sc_toggle_counter']++;
	if (empty($icon_closed) || themerex_sc_param_is_inherit($icon_closed)) $icon_closed = $THEMEREX_GLOBALS['sc_toggles_icon_closed'] ? $THEMEREX_GLOBALS['sc_toggles_icon_closed'] : "icon-plus-2";
	if (empty($icon_opened) || themerex_sc_param_is_inherit($icon_opened)) $icon_opened = $THEMEREX_GLOBALS['sc_toggles_icon_opened'] ? $THEMEREX_GLOBALS['sc_toggles_icon_opened'] : "icon-minus-2";
	$css .= themerex_sc_param_is_on($open) ? 'display:block;' : '';
	$output = '<div' . ($id ? ' id="'.esc_attr($id).'"' : '') 
				. ' class="sc_toggles_item'.(themerex_sc_param_is_on($open) ? ' sc_active' : '')
				. (!empty($class) ? ' '.esc_attr($class) : '')
				. ($THEMEREX_GLOBALS['sc_toggle_counter'] % 2 == 1 ? ' odd' : ' even') 
				. ($THEMEREX_GLOBALS['sc_toggle_counter'] == 1 ? ' first' : '')
				. '">'
				. '<h5 class="sc_toggles_title'.(themerex_sc_param_is_on($open) ? ' ui-state-active' : '').'">'
				. (!themerex_sc_param_is_off($icon_closed) ? '<span class="sc_toggles_icon sc_toggles_icon_closed '.esc_attr($icon_closed).'"></span>' : '')
				. (!themerex_sc_param_is_off($icon_opened) ? '<span class="sc_toggles_icon sc_toggles_icon_opened '.esc_attr($icon_opened).'"></span>' : '')
				. ($THEMEREX_GLOBALS['sc_toggle_show_counter'] ? '<span class="sc_items_counter">'.($THEMEREX_GLOBALS['sc_toggle_counter']).'</span>' : '')
				. ($title) 
				. '</h5>'
				. '<div class="sc_toggles_content"'
					. ($css!='' ? ' style="'.esc_attr($css).'"' : '') 
					.'>' 
					. do_shortcode($content) 
				. '</div>'
			. '</div>';
	return apply_filters('themerex_shortcode_output', $output, 'trx_toggles_item', $atts, $content);
}

// ---------------------------------- [/trx_toggles] ---------------------------------------





// ---------------------------------- [trx_tooltip] ---------------------------------------


themerex_require_shortcode('trx_tooltip', 'themerex_sc_tooltip');


function themerex_sc_tooltip($atts, $content=null){	
	if (themerex_sc_in_shortcode_blogger()) return '';
    extract(themerex_sc_html_decode(shortcode_atts(array(
		// Individual params
		"title" => "",
		// Common params
		"id" => "",
		"class" => "",
		"css" => ""
    ), $atts)));
	$output = '<span' . ($id ? ' id="'.esc_attr($id).'"' : '') 
				. ' class="sc_tooltip_parent'. (!empty($class) ? ' '.esc_attr($class) : '').'"'
				. ($css!='' ? ' style="'.esc_attr($css).'"' : '') 
				. '>'
					. do_shortcode($content)
					. '<span class="sc_tooltip">' . ($title) . '</span>'
				. '</span>';
	return apply_filters('themerex_shortcode_output', $output, 'trx_tooltip', $atts, $content);
}
// ---------------------------------- [/trx_tooltip] ---------------------------------------



// ---------------------------------- [trx_video] ---------------------------------------

themerex_require_shortcode("trx_video", "themerex_sc_video");

//[trx_video id="unique_id" url="http://player.vimeo.com/video/20245032?title=0&amp;byline=0&amp;portrait=0" width="" height=""]
function themerex_sc_video($atts, $content = null) {
	if (themerex_sc_in_shortcode_blogger()) return '';
	extract(themerex_sc_html_decode(shortcode_atts(array(
		// Individual params
		"url" => '',
		"src" => '',
		"image" => '',
		"ratio" => '16:9',
		"autoplay" => 'off',
		"align" => '',
		"bg_image" => '',
		"bg_top" => '',
		"bg_bottom" => '',
		"bg_left" => '',
		"bg_right" => '',
		"frame" => "on",
		// Common params
		"id" => "",
		"class" => "",
		"animation" => "",
		"css" => "",
		"width" => '',
		"height" => '',
		"top" => "",
		"bottom" => "",
		"left" => "",
		"right" => ""
    ), $atts)));

	if (empty($autoplay)) $autoplay = 'off';
	
	$ratio = empty($ratio) ? "16:9" : str_replace(array('/','\\','-'), ':', $ratio);
	$ratio_parts = explode(':', $ratio);
	if (empty($height) && empty($width)) $width='100%';
	$ed = themerex_substr($width, -1);
	if (empty($height) && !empty($width) && $ed!='%') {
		$height = round($width / $ratio_parts[0] * $ratio_parts[1]);
	}
	if (!empty($height) && empty($width)) {
		$width = round($height * $ratio_parts[0] / $ratio_parts[1]);
	}
	$css .= themerex_get_css_position_from_values($top, $right, $bottom, $left);
	$css_dim = themerex_get_css_position_from_values('', '', '', '', $width, $height);
	$css_bg = themerex_get_css_paddings_from_values($bg_top, $bg_right, $bg_bottom, $bg_left);

	if ($src=='' && $url=='' && isset($atts[0])) {
		$src = $atts[0];
	}
	$url = $src!='' ? $src : $url;
	if ($image!='' && themerex_sc_param_is_off($image))
		$image = '';
	else {
		if (themerex_sc_param_is_on($autoplay) && is_single())
			$image = '';
		else {
			if ($image > 0) {
				$attach = wp_get_attachment_image_src( $image, 'full' );
				if (isset($attach[0]) && $attach[0]!='')
					$image = $attach[0];
			}
			if ($bg_image) {
				$thumb_sizes = themerex_get_thumb_sizes(array(
					'layout' => 'grid_3'
				));
				if (!is_single() || !empty($image)) $image = themerex_get_resized_image_url(empty($image) ? get_the_ID() : $image, $thumb_sizes['w'], $thumb_sizes['h'], null, false, false, false);
			} else
				if (!is_single() || !empty($image)) $image = themerex_get_resized_image_url(empty($image) ? get_the_ID() : $image, $ed!='%' ? $width : null, $height);
			if (empty($image) && (!is_single() || themerex_sc_param_is_off($autoplay)))
				$image = themerex_get_video_cover_image($url);
		}
	}
	if ($bg_image > 0) {
		$attach = wp_get_attachment_image_src( $bg_image, 'full' );
		if (isset($attach[0]) && $attach[0]!='')
			$bg_image = $attach[0];
	}
	if ($bg_image) {
		$css_bg .= $css . 'background-image: url('.esc_url($bg_image).');';
		$css = $css_dim;
	} else {
		$css .= $css_dim;
	}

	$url = themerex_get_video_player_url($src!='' ? $src : $url);
	
	$video = '<video' . ($id ? ' id="' . esc_attr($id) . '"' : '') 
		. ' class="sc_video'. (!empty($class) ? ' '.esc_attr($class) : '').'"'
		. ' src="' . esc_url($url) . '"'
		. ' width="' . esc_attr($width) . '" height="' . esc_attr($height) . '"' 
		. ' data-width="' . esc_attr($width) . '" data-height="' . esc_attr($height) . '"' 
		. ' data-ratio="'.esc_attr($ratio).'"'
		. ($image ? ' poster="'.esc_attr($image).'" data-image="'.esc_attr($image).'"' : '') 
		. (!themerex_sc_param_is_off($animation) ? ' data-animation="'.esc_attr(themerex_sc_get_animation_classes($animation)).'"' : '')
		. ($align && $align!='none' ? ' data-align="'.esc_attr($align).'"' : '')
		. ($bg_image ? ' data-bg-image="'.esc_attr($bg_image).'"' : '') 
		. ($css_bg!='' ? ' data-style="'.esc_attr($css_bg).'"' : '') 
		. ($css!='' ? ' style="'.esc_attr($css).'"' : '') 
		. (($image && themerex_get_theme_option('substitute_video')=='yes') || (themerex_sc_param_is_on($autoplay) && is_single()) ? ' autoplay="autoplay"' : '') 
		. ' controls="controls" loop="loop"'
		. '>'
		. '</video>';
	if (themerex_get_custom_option('substitute_video')=='no') {
		if (themerex_sc_param_is_on($frame)) $video = themerex_get_video_frame($video, $image, $css, $css_bg);
	} else {
		if ((isset($_GET['vc_editable']) && $_GET['vc_editable']=='true') && (isset($_POST['action']) && $_POST['action']=='vc_load_shortcode')) {
			$video = themerex_substitute_video($video, $width, $height, false);
		}
	}
	if (themerex_get_theme_option('use_mediaelement')=='yes')
		wp_enqueue_script('wp-mediaelement');
	return apply_filters('themerex_shortcode_output', $video, 'trx_video', $atts, $content);
}
// ---------------------------------- [/trx_video] ---------------------------------------


// ---------------------------------- [trx_icons_skills] ---------------------------------------


themerex_require_shortcode('trx_icons_skills', 'themerex_sc_icons_skills');

function themerex_sc_icons_skills($atts, $content=null){	
	if (themerex_sc_in_shortcode_blogger()) return '';
    extract(themerex_sc_html_decode(shortcode_atts(array(
		"id" => "",
		"icon" => "icon-heart",
		"color" => "#67d3e0",
		"size" => "40",
		"count" => "10",
		"value" => "5",
		"top" => "",
		"bottom" => "", 
		"left" => "",
		"right" => "",
		"width" => ""
    ), $atts)));
	$THEMEREX_sc_chart_id = $id;
	
	$s = ($top !== '' ? ' margin-top:'.$top.'px;' : '')
		.($bottom !== '' ? ' margin-bottom:'.$bottom.'px;' : '')
		.($left !== '' ? ' margin-left:'.$left.'px;' : '')
		.($right !== '' ? ' margin-right:'.$right.'px;' : '')
		.($width !== '' ? ' max-width:'.$width.'px;' : '')
		.($size !== '' ? 'font-size:'.$size.'px;' : '');
		
	$output = '<div' . ($id ? ' id="' . $id . '"' : '') 
		. ' class="sc_icons_skills "'
		. ($s!='' ? ' style="'.$s.'"' : '').'>';
	for($i = 0; $i < $count; $i++)
	{
		if($i <= $value) $output .= '<div class="sc_icons_item active '.$icon.'" data-color="'.$color.'"></div>';
		else   $output .= '<div class="sc_icons_item '.$icon.'"></div>';
	}
	$output .= '</div>';
	return apply_filters('themerex_shortcode_output', $output, 'trx_icons_skills', $atts, $content);
}
// ---------------------------------- [/trx_icons_skills] ---------------------------------------


// ---------------------------------- [chart] ----------------------------------------Ok

themerex_require_shortcode('trx_chart', 'themerex_sc_chart');


function themerex_sc_chart($atts, $content=null){	
	if (themerex_sc_in_shortcode_blogger()) return '';
    extract(themerex_sc_html_decode(shortcode_atts(array(
		"id" => "",
		"max_value" => "",
		"top" => "",
		"bottom" => "",
		"left" => "",
		"right" => ""
    ), $atts)));
	global $THEMEREX_GLOBALS;
	$THEMEREX_GLOBALS['sc_skills_id']  = $id;
	$THEMEREX_GLOBALS['max_value']  = $max_value;
	
	/*scripts & styles*/
	wp_enqueue_script( 'diagram-chart', themerex_get_file_url('/js/diagram/chart.min.js'), array('jquery'), null, true );
	wp_enqueue_script( 'diagram-raphael', themerex_get_file_url('/js/diagram/diagram.raphael.js'), array('jquery'), null, true );
	
	$s = ($top !== ''    ? 'margin-top:' . $top . 'px;' : '')
		.($bottom !== '' ? 'margin-bottom:' . $bottom . 'px;' : '')
		.($left !== ''   ? 'margin-left:' . $left . 'px;' : '')
		.($right !== ''  ? 'margin-right:' . $right . 'px;' : '');

	$content = do_shortcode($content);
	$output = '<div class="sc_chart_diagram" style="'.$s.'">'
                .$content
            .'</div>';
			
	return apply_filters('themerex_shortcode_output', $output, 'trx_chart', $atts, $content);
}


themerex_require_shortcode('trx_chart_item', 'themerex_sc_chart_item');

//[trx_chart_item]
function themerex_sc_chart_item($atts, $content=null) {
	if (themerex_sc_in_shortcode_blogger()) return '';
	extract(themerex_sc_html_decode(shortcode_atts( array(
		"value" => "75",
		"value_color" => "#5cbece",
		"color" => "#89e1ee",
		"dark_color" => "#2a2f43",
		"width" => "190",
		"top" => "",
		"bottom" => "",
		"left" => "",
		"right" => ""
	), $atts)));
	global $THEMEREX_GLOBALS;
	$id = $THEMEREX_GLOBALS['sc_skills_id'].'_sk_'.mt_rand(0,1000);
	$max_value = $THEMEREX_GLOBALS['max_value'];
	
	$value = $value * 100 / $max_value;
	if((int) $value > 100) $value = '100';

	$ed = themerex_substr($value, -1)=='%' ? '%' : '';
	$value = (int) str_replace('%', '', $value);
	$percent = round($value / 100 * 100);
	
	$css = ($width !== ''    ? 'width:' . $width . 'px;' : '')
		  .($top !== ''    ? 'margin-top:' . $top . 'px;' : '')
		  .($bottom !== '' ? 'margin-bottom:' . $bottom . 'px;' : '')
		  .($left !== ''   ? 'margin-left:' . $left . 'px;' : '')
		  .($right !== ''  ? 'margin-right:' . $right . 'px;' : '');
	
	$content = do_shortcode($content);
	$output = '<div id="'.$id.'" class="sc_chart_item" style="'.$css.'">'
	                            .'<div class="sc_chart_item_canvas">'
								.'<canvas id="canvas_'.$id.'" height="'.$width.'" width="'.$width.'" data-percent="'.$percent.'" data-color="'. $color.'" data-darkcolor="'. $dark_color.'"></canvas>'
								.'<div class="sc_chart_item_value" style="line-height:'.$width.'px; color: '.$value_color.';">'.$value.'</div>'
								.'</div>'
	                            .' <div class="sc_chart_content">' 
	                                .'<span>'.$content.'</span>'
	                            .'</div>' 
	                        .'</div>';
							
	return apply_filters('themerex_shortcode_output', $output, 'trx_chart_item', $atts, $content);
}
// ---------------------------------- [/chart] ---------------------------------------


// ---------------------------------- [trx_graph] ---------------------------------------Ok

themerex_require_shortcode('trx_graph', 'themerex_sc_graph');

function themerex_sc_graph($atts, $content=null){
	if (themerex_sc_in_shortcode_blogger()) return '';
	extract(themerex_sc_html_decode(shortcode_atts( array(
		"id" => "",
		"labels" => "Label1, Label2, Label3, Label4",
		"name" => "",

		"top" => "",
		"bottom" => "",
		"left" => "",
		"right" => "",
		"width" => "",
		"height" => "220"
	), $atts)));
	wp_enqueue_script( 'diagram-chart', themerex_get_file_url('/js/diagram/chart.min.js'), array('jquery'), null, true );
	wp_enqueue_script( 'diagram-raphael', themerex_get_file_url('/js/diagram/diagram.raphael.js'), array('jquery'), null, true );
	wp_enqueue_script( 'graph', themerex_get_file_url('/js/diagram/Graph.js'), array('jquery'), null, true );
	
	
	 $ed_l = ($left !== 'auto' ? themerex_substr($left, -1)=='%' ? '%' : 'px' : '');
     $ed_r = ($right !== 'auto' ? themerex_substr($right, -1)=='%' ? '%' : 'px' : '');
	 $s = ($top !== '' ? 'margin-top:'.$top.'px;' : '')
		 .($bottom !== '' ? 'margin-bottom:'.$bottom.'px;' : '')
		 .($left !== '' ? 'margin-left:' . $left .$ed_l. ';' : '')
		 .($right !== '' ? 'margin-right:' . $right .$ed_r. ';' : '')
		 .($width !== '' ? 'width:'.$width.'px;' : '');

	$content = do_shortcode($content);   
	$output = ($name != '' ? '<h2 class="tw-chart-title">'.$name.'</h2>' : '');
	$output .= '<div class="tw-chart-graph tw-animate tw-redraw with-list-desc" data-zero="false" data-labels="'.$labels.'"'
			.' data-type="Curve" data-item-height="'.$height.'" data-animation-delay="0" data-animation-offset="90%" style="'.$s.'">'
			.'<ul class="data" style="display: none;">'
			.$content
			.'</ul>'
			.'<canvas></canvas>'
			.'</div>';	
	return apply_filters('themerex_shortcode_output', $output, 'trx_graph', $atts, $content);
}
// ---------------------------------- [/trx_graph] ---------------------------------------


// ---------------------------------- [trx_graph_item] ---------------------------------------Ok
themerex_require_shortcode('trx_graph_item', 'themerex_sc_graph_item');

function themerex_sc_graph_item($atts, $content=null){
	if (themerex_sc_in_shortcode_blogger()) return '';
	extract(themerex_sc_html_decode(shortcode_atts( array(
		"datas" => "30,50,40,70",
		"color" => "#76D3E1",
	), $atts)));
	if($content == '') $content = "Attribute";
	$output =  '<li data-datas="'.$datas.'" data-fill-color="'.$color.'" data-fill-text="'.$content.'"></li>';    
	
	return apply_filters('themerex_shortcode_output', $output, 'trx_graph_item', $atts, $content);	
}
// ---------------------------------- [/trx_graph_item] ---------------------------------------


// ---------------------------------- [/sidebar] ---------------------------------------

themerex_require_shortcode('trx_sidebar', 'themerex_sc_trx_sidebar');

function themerex_sc_trx_sidebar($atts, $content=null){
	if (themerex_sc_in_shortcode_blogger()) return '';
	extract(themerex_sc_html_decode(shortcode_atts( array(
		"name" => "",
		"columns" => "",
		"top" => "",
		"bottom" => "",
		"left" => "",
		"layout" => "",
		"right" => ""
	), $atts)));

	$columns = max(1,min(6,$columns));

	$s = ($top != '' ? 'padding-top:'.$top.'px;' : '')
		.($bottom != '' ? 'padding-bottom:'.$bottom.'px;' : '')
		.($left != '' ? 'padding-left:'.$left.'px;' : '')
		.($right != '' ? 'padding-right:'.$right.'px;' : '');

	global $THEMEREX_GLOBALS;
	$THEMEREX_GLOBALS['current_sidebar'] = 'shortcode';

			
	if(!empty($name)) {
		ob_start();
		dynamic_sidebar($name);
		$sidebar_content = ob_get_contents();
		ob_end_clean();
	}

		
	$output = '<div class="sc_sidebar_selector"'.($s != '' ? ' style="'.$s.'"' : '').'><div class="sc_columns_'.$columns.' sc_columns_indent"><div class="widget_area">'.$sidebar_content.'</div></div></div>';
	return apply_filters('themerex_shortcode_output', $output, 'trx_sidebar', $atts, $content);	
}
// ---------------------------------- [/sidebar] ---------------------------------------


// ---------------------------------- [/rocks] ---------------------------------------Ok
themerex_require_shortcode('trx_rocks', 'themerex_sc_rocks');

function themerex_sc_rocks($atts, $content=null){	
	if (themerex_sc_in_shortcode_blogger()) return '';
	extract(themerex_sc_html_decode(shortcode_atts( array(
		"id" => "",
		"title" => "",
		"count" => "",
		"maximum" => "",
		"text_color" => "#9b9ead",
		"value_color" => "#2a2f43",
		"top" => "",
		"bottom" => "",
		"left" => "",
		"right" => "",
		"width" => ""
    ), $atts)));

	$s = ($top !== '' ? 'margin-top:' . $top . 'px;' : '')
		.($bottom !== '' ? 'margin-bottom:' . $bottom . 'px;' : '')
		.($left !== '' ? 'margin-left:' . $left . ((int) $left > 0 || (int) $left < 0 ? 'px' : '') . ';' : '')
		.($right !== '' ? 'margin-right:' . $right . ((int) $right > 0 || (int) $right < 0 ? 'px' : '') . ';' : '')
		.($width !== '' ? 'max-width:' . $width . ((int) $width > 0 ? 'px' : '') . ';' : 'width: 100%;');


	global $THEMEREX_sc_rocks_counter, $THEMEREX_sc_rocks_height, $THEMEREX_sc_rocks_data, $THEMEREX_sc_rocks_max, $THEMEREX_sc_rocks_value_color, $THEMEREX_sc_rocks_text_color;
	$THEMEREX_sc_rocks_text_color = $text_color;
	$THEMEREX_sc_rocks_value_color = $value_color;
	$THEMEREX_sc_rocks_counter = 0;
	$THEMEREX_sc_rocks_columns = 0;
	$THEMEREX_sc_rocks_width = 0;
	$THEMEREX_sc_rocks_data = '';

	if ($maximum < 1) $maximum = 100;
	//if ($style) $THEMEREX_sc_skills_style = $style = max(1, min(4, $style));
	$THEMEREX_sc_rocks_max = $maximum;
	if ($id == '') $id = 'sc_rocks_'.str_replace('.','',mt_rand());

	$content = do_shortcode($content);
	$output =  '<div class="sc_rocks_skills '.($count != '' ? 'sc_rocks_count_'.$count.'' : '' ).'" '.($s!='' ? ' style="'.$s.'"' : '').'>'
                .'<div class="sc_rocks_inner">'
                	.$THEMEREX_sc_rocks_data
                .'</div>'
            .'</div>';
	return apply_filters('themerex_shortcode_output', $output, 'trx_rocks', $atts, $content);	
}


themerex_require_shortcode('trx_rocks_item', 'themerex_sc_rocks_item');

//[trx_rocks_item]
function themerex_sc_rocks_item($atts, $content=null) {
	if (themerex_sc_in_shortcode_blogger()) return '';
	extract(themerex_sc_html_decode(shortcode_atts( array(
		"id" => "",
		"title" => "",
		"level" => ""
	), $atts)));
	global $THEMEREX_sc_rocks_counter, $THEMEREX_sc_rocks_height, $THEMEREX_sc_rocks_data, $THEMEREX_sc_rocks_max, $THEMEREX_sc_rocks_text_color,	$THEMEREX_sc_rocks_value_color;
	$THEMEREX_sc_rocks_counter++;
	if($level > $THEMEREX_sc_rocks_max) $level = $THEMEREX_sc_rocks_max;
	$ed = themerex_substr($level, -1)=='%' ? '%' : '';
	$level = (int) str_replace('%', '', $level);
	$percent = round($level / $THEMEREX_sc_rocks_max * 100);
	$title_block = '<div class="sc_rocks_info">'.$title.'</div>';
	$output = '';

	$THEMEREX_sc_rocks_data .= '<div class="sc_rocks_row" style="color: '.$THEMEREX_sc_rocks_value_color.';">'
	                               .'<span class="sc_rocks_value">'.$level.'</span>'
	                               .'<div class="sc_rocks_progressbar">'
	                                    .'<div class="sc_rocks_progress" data-process="'.$percent.'">'
	                                       .'<div class="sc_rocks_before"></div>'
	                                        .'<div class="sc_rocks_after"></div>'
	                                    .'</div>'
	                                    .'<div class="sc_rocks_foot"></div>'
	                                    .'<div class="sc_rocks_shadow"></div>'
	                                .'</div>'
	                                .'<span class="sc_rocks_caption" style="color: '.$THEMEREX_sc_rocks_text_color.';">'.$title.'</span>'
                           		.'</div>';


	return apply_filters('themerex_shortcode_output', $output, 'trx_rocks_item', $atts, $content);	
}

// ---------------------------------- [/rocks] ---------------------------------------



// ---------------------------------- [vacancies] ---------------------------------------

themerex_require_shortcode('trx_vacancies', 'themerex_sc_trx_vacancies');

function themerex_sc_trx_vacancies($atts, $content=null){
	if (themerex_sc_in_shortcode_blogger()) return '';
	extract(themerex_sc_html_decode(shortcode_atts( array(
		"top" => "",
		"bottom" => "",
		"left" => "",
		"right" => ""
	), $atts)));


	$s = ($top != '' ? 'padding-top:'.$top.'px;' : '')
		.($bottom != '' ? 'padding-bottom:'.$bottom.'px;' : '')
		.($left != '' ? 'padding-left:'.$left.'px;' : '')
		.($right != '' ? 'padding-right:'.$right.'px;' : '');
	global $THEMEREX_VACANCY_TABLE;
	require(themerex_get_file_dir('templates/parts/vacancy.php'));
	$output = $THEMEREX_VACANCY_TABLE;
	return apply_filters('themerex_shortcode_output', $output, 'trx_vacancies', $atts, $content);	
}
// ---------------------------------- [/vacancies] ---------------------------------------




// ---------------------------------- [trx_socials] ---------------------------------------


themerex_require_shortcode('trx_socials', 'themerex_sc_socials');


function themerex_sc_socials($atts, $content=null){	
	if (themerex_sc_in_shortcode_blogger()) return '';
    extract(themerex_sc_html_decode(shortcode_atts(array(
		// Individual params
		"size" => "small",	// tiny | small | large
		"socials" => "",
		"custom" => "no",
		// Common params
		"id" => "",
		"class" => "",
		"animation" => "",
		"css" => "",
		"top" => "",
		"bottom" => "",
		"left" => "",
		"right" => ""
    ), $atts)));
	$css .= themerex_get_css_position_from_values($top, $right, $bottom, $left);
	global $THEMEREX_GLOBALS;
	$THEMEREX_GLOBALS['sc_social_icons'] = false;
	if (!empty($socials)) {
		$allowed = explode('|', $socials);
		$list = array();
		for ($i=0; $i<count($allowed); $i++) {
			$s = explode('=', $allowed[$i]);
			if (!empty($s[1])) {
				$list[] = array(
					'icon'	=> themerex_get_socials_url($s[0]),
					'url'	=> $s[1]
					);
			}
		}
		if (count($list) > 0) $THEMEREX_GLOBALS['sc_social_icons'] = $list;
	} else if (themerex_sc_param_is_off($custom))
		$content = do_shortcode($content);
	if ($THEMEREX_GLOBALS['sc_social_icons']===false) $THEMEREX_GLOBALS['sc_social_icons'] = themerex_get_custom_option('social_icons');
	$output = themerex_prepare_socials($THEMEREX_GLOBALS['sc_social_icons']);
	$output = $output
		? '<div' . ($id ? ' id="'.esc_attr($id).'"' : '') 
			. ' class="sc_socials sc_socials_size_' . esc_attr($size) . (!empty($class) ? ' '.esc_attr($class) : '') . '"' 
			. ($css!='' ? ' style="'.esc_attr($css).'"' : '') 
			. (!themerex_sc_param_is_off($animation) ? ' data-animation="'.esc_attr(themerex_sc_get_animation_classes($animation)).'"' : '')
			. '>' 
			. ($output)
			. '</div>'
		: '';
	return apply_filters('themerex_shortcode_output', $output, 'trx_socials', $atts, $content);
}



themerex_require_shortcode('trx_social_item', 'themerex_sc_social_item');

function themerex_sc_social_item($atts, $content=null){	
	if (themerex_sc_in_shortcode_blogger()) return '';
    extract(themerex_sc_html_decode(shortcode_atts(array(
		// Individual params
		"name" => "",
		"url" => "",
		"icon" => ""
    ), $atts)));
	global $THEMEREX_GLOBALS;
	if (!empty($name) && empty($icon) && file_exists(themerex_get_socials_dir($name.'.png')))
		$icon = themerex_get_socials_url($name.'.png');
	if (!empty($icon) && !empty($url)) {
		if ($THEMEREX_GLOBALS['sc_social_icons']===false) $THEMEREX_GLOBALS['sc_social_icons'] = array();
		$THEMEREX_GLOBALS['sc_social_icons'][] = array(
			'icon' => $icon,
			'url' => $url
		);
	}
	return '';
}

// ---------------------------------- [/trx_socials] ---------------------------------------



// ---------------------------------- [trx_grid] ---------------------------------------

themerex_require_shortcode('trx_grid', 'themerex_sc_grid');


function themerex_sc_grid($atts, $content=null){	
	if (themerex_sc_in_shortcode_blogger()) return '';
    extract(themerex_sc_html_decode(shortcode_atts(array(
		"id" => "",
		"class" => "",
		"css" => "",
		"top" => "",
		"bottom" => "",
		"left" => "",
		"right" => ""
    ), $atts)));
	
	wp_enqueue_script( 'grid-script', themerex_get_file_url('js/grid/grid.js'), array('jquery'), null, true );
	
	$css .= themerex_get_css_position_from_values(($top), ($right), ($bottom),($left));
	
	$output = '<div class="grid '.$class.'" '.($id != '' ? 'id="'.$id.'"' : '').' '.($css != '' ? 'style="'.$css.'"' : '').'><div class="grid-sizer"></div><div class="gutter-sizer"></div>'
			  .do_shortcode($content)
			  .'</div>';
			  
	return apply_filters('themerex_shortcode_output', $output, 'trx_grid', $atts, $content);
}

// ---------------------------------- [/trx_grid] ---------------------------------------

// ---------------------------------- [trx_grid_item] ---------------------------------------

themerex_require_shortcode('trx_grid_item', 'themerex_sc_grid_item');


function themerex_sc_grid_item($atts, $content=null){	
	if (themerex_sc_in_shortcode_blogger()) return '';
    extract(themerex_sc_html_decode(shortcode_atts(array(
		"id" => "",
		"class" => "",
		'width' => '1',
		'responsive' => '' //big, small
    ), $atts)));
	
	
	
	$output = '<div class="grid-item'.($width != '' ? ' grid-item--width'.$width : '').''.($responsive != '' ? ' responsive_'.$responsive : '').' '.$class.'" '.($id != '' ? 'id="'.$id.'"' : '').'>'
			  .do_shortcode($content)
			  .'</div>';
			  
	return apply_filters('themerex_shortcode_output', $output, 'trx_grid_item', $atts, $content);
}

// ---------------------------------- [/trx_grid_item] ---------------------------------------

?>