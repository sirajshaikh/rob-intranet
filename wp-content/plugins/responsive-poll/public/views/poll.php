<?php

if ($poll["poll_is_live"]){



$var = '<div class="weblator-poll-container" data-poll-id="' . $poll["id"] . '">

    <div class="weblator-poll-loading">Loading</div>

    <div class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title">' . stripslashes($poll["poll_name"]) . '</h3>
        </div>

        <div class="panel-body weblator-poll-vote">

            <div class="alert alert-success">Thank you for voting</div>
            <div class="alert alert-warning">You have already voted on this poll!</div>
            <div class="alert alert-danger">Please select an option!</div>

            <ul>';
                foreach($options as $key => $option){
                    $var .= '<li><input type="radio" data-index="' . $key . '" name="weblator-chart-options" id="option-' . $option["id"] . '" data-poll-id="' . $poll["id"] . '" value="' . $option["id"] . '"/>
                        <label for="option-' . $option["id"] . '">' . stripslashes($option["option_value"]) . '</label>
                    </li>';
                }

            $var .= '</ul></div>';

    $var .= '<div class="panel-footer">
            <div class="button-vote">
                <button class="btn btn-default vote-button weblator-poll-submit"><i class="fa fa-spinner fa-spin vote-spin"></i> Vote</button>
            </div>
            <div class="weblator-view-results">';

            if ( $total_votes == 0 )
                $var .= '<a href="#" class="weblator-view-poll btn btn-default" disabled="disabled">View Results</a>';
            else
                $var .= '<a href="#" class="weblator-view-poll btn btn-default">View Results</a>';


                $var .= '<a href="#" class="weblator-hide-poll btn btn-default">Hide Results</a>

            </div>

        </div>
    </div>';




	if ( $poll["poll_max_width"] > 0 )
		$width = $poll["poll_max_width"] . "px";
	else
        $width = "100%";


    $var .= "<div class='width-control' style='width:" . $width . "'>";

    if ($poll["poll_chart_type"] == 2 || $poll["poll_chart_type"] == 3 || $poll ["poll_chart_type"] == 6){

        $legendStyle = "style='font-style:" . $poll["chart_legend_font_style"] . "; color:" . $poll["chart_legend_font_colour"] . "; '";

        if ($poll["chart_legend"]){

            if ( substr($poll["chart_legend_position"], 0, -1) == "t" )
                $var .= "<ul " . $legendStyle . " data-font-size='" . $poll["chart_legend_font_size"] . "' id=\"legend-" . $poll["id"] . "\" class=\"legend " . $poll["chart_legend_position"] . "\"></ul>";
        }
    }

    $var .= '<canvas data-width="'.$width.'" data-weblator-poll-id="' . $poll["id"] . '" id="weblator-chart-' . $poll["id"] . '" class="weblator-chart"></canvas>';

    if ($poll["poll_chart_type"] == 2 || $poll["poll_chart_type"] == 3 || $poll ["poll_chart_type"] == 6){

        $legendStyle = "style='font-style:" . $poll["chart_legend_font_style"] . "; color:" . $poll["chart_legend_font_colour"] . "; '";

        if ($poll["chart_legend"]){

            if ( substr($poll["chart_legend_position"], 0, -1) == "b" )
                $var .= "<ul " . $legendStyle . " data-font-size='" . $poll["chart_legend_font_size"] . "' id=\"legend-" . $poll["id"] . "\" class=\"legend " . $poll["chart_legend_position"] . "\"></ul>";
        }
    }

    $var .= '</div>';



 $var .= '</div>';
}
