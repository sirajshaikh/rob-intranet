<?php
/**
 * Alliance skin file for theme.
 */

// Disable direct call
if ( ! defined( 'ABSPATH' ) ) { exit; }

// Theme init
if (!function_exists('themerex_skin_theme_setup_alliance')) {
	add_action( 'themerex_action_init_theme', 'themerex_skin_theme_setup_alliance', 1 );
	function themerex_skin_theme_setup_alliance() {

		// Add skin fonts in the used fonts list
		add_filter('themerex_filter_used_fonts',			'themerex_filter_used_fonts_alliance');
		// Add skin fonts (from Google fonts) in the main fonts list (if not present).
		add_filter('themerex_filter_list_fonts',			'themerex_filter_list_fonts_alliance');

		// Add skin stylesheets
		add_action('themerex_action_add_styles',			'themerex_action_add_styles_alliance');
		// Add skin inline styles
		add_filter('themerex_filter_add_styles_inline',		'themerex_filter_add_styles_inline_alliance');
		// Add skin responsive styles
		add_action('themerex_action_add_responsive',		'themerex_action_add_responsive_alliance');
		// Add skin responsive inline styles
		add_filter('themerex_filter_add_responsive_inline',	'themerex_filter_add_responsive_inline_alliance');

		// Add skin scripts
		add_action('themerex_action_add_scripts',			'themerex_action_add_scripts_alliance');
		// Add skin scripts inline
		add_action('themerex_action_add_scripts_inline',	'themerex_action_add_scripts_inline_alliance');

		//New 
		// Return main color 1
		add_filter('themerex_filter_get_main_color_1',			'themerex_filter_get_main_color_1_alliance', 10, 1);
		// Return main color 2
		add_filter('themerex_filter_get_main_color_2',			'themerex_filter_get_main_color_2_alliance', 10, 1);
		// Return button hover
		add_filter('themerex_filter_get_button_hover',			'themerex_filter_get_button_hover_alliance', 10, 1);
		// Return button hover shadow
		add_filter('themerex_filter_get_button_hover_shadow',	'themerex_filter_get_button_hover_shadow_alliance', 10, 1);
		// Return user_bg_1
		add_filter('themerex_filter_get_user_bg_1',	'themerex_filter_get_user_bg_1_alliance', 10, 1);
		// Return user_bg_2
		add_filter('themerex_filter_get_user_bg_2',	'themerex_filter_get_user_bg_2_alliance', 10, 1);
		//Ends New 
		
	}
}





//------------------------------------------------------------------------------
// Skin's fonts
//------------------------------------------------------------------------------

// Add skin fonts in the used fonts list
if (!function_exists('themerex_filter_used_fonts_alliance')) {
	//Handler of add_filter('themerex_filter_used_fonts', 'themerex_filter_used_fonts_alliance');
	function themerex_filter_used_fonts_alliance($theme_fonts) {
		$theme_fonts['Roboto'] = 1;
		$theme_fonts['Love Ya Like A Sister'] = 1;
		return $theme_fonts;
	}
}

// Add skin fonts (from Google fonts) in the main fonts list (if not present).
// To use custom font-face you not need add it into list in this function
// How to install custom @font-face fonts into the theme?
// All @font-face fonts are located in "theme_name/css/font-face/" folder in the separate subfolders for the each font. Subfolder name is a font-family name!
// Place full set of the font files (for each font style and weight) and css-file named stylesheet.css in the each subfolder.
// Create your @font-face kit by using Fontsquirrel @font-face Generator (http://www.fontsquirrel.com/fontface/generator)
// and then extract the font kit (with folder in the kit) into the "theme_name/css/font-face" folder to install
if (!function_exists('themerex_filter_list_fonts_alliance')) {
	//Handler of add_filter('themerex_filter_list_fonts', 'themerex_filter_list_fonts_alliance');
	function themerex_filter_list_fonts_alliance($list) {
		// Example:
		// if (!isset($list['Advent Pro'])) {
		//		$list['Advent Pro'] = array(
		//			'family' => 'sans-serif',																						// (required) font family
		//			'link'   => 'Advent+Pro:100,100italic,300,300italic,400,400italic,500,500italic,700,700italic,900,900italic',	// (optional) if you use Google font repository
		//			'css'    => themerex_get_file_url('/css/font-face/Advent-Pro/stylesheet.css')									// (optional) if you use custom font-face
		//			);
		// }
		if (!isset($list['Roboto']))				$list['Roboto'] = array('family'=>'sans-serif');
		if (!isset($list['Love Ya Like A Sister']))	$list['Love Ya Like A Sister'] = array('family'=>'cursive', 'link'=>'Love+Ya+Like+A+Sister:400&subset=latin');
		return $list;
	}
}


//------------------------------------------------------------------------------
// Skin's stylesheets
//------------------------------------------------------------------------------
// Add skin stylesheets
if (!function_exists('themerex_action_add_styles_alliance')) {
	//Handler of add_action('themerex_action_add_styles', 'themerex_action_add_styles_alliance');
	function themerex_action_add_styles_alliance() {
		// Add stylesheet files
		wp_enqueue_style( 'themerex-skin-style', themerex_get_file_url('skins/alliance/skin.css'), array(), null );
	}
}

// Add skin inline styles
if (!function_exists('themerex_filter_add_styles_inline_alliance')) {
	//Handler of add_filter('themerex_filter_add_styles_inline', 'themerex_filter_add_styles_inline_alliance');
	function themerex_filter_add_styles_inline_alliance($custom_style) {
		//New 
		$custom_style_orig = $custom_style;
		// Color scheme
		$scheme = themerex_get_custom_option('color_scheme');
		if (empty($scheme)) $scheme = 'original';

		global $THEMEREX_GLOBALS;
		
		//New 
		$custom_style = $custom_style_orig;
		// Main color 1
		$clr = themerex_get_custom_option('main_color_1');
		if (empty($clr) && $scheme!= 'original')	$clr = apply_filters('themerex_filter_get_main_color_1', '');
		if (!empty($clr)) {
			$THEMEREX_GLOBALS['color_schemes'][$scheme]['main_color_1'] = $clr;
			//$rgb = themerex_hex2rgb($clr);
			$bd_color = hex2rgb1($clr);
	
			$box_shadow = 'rgb('
				. ($bd_color[0] > 30 ? $bd_color[0] - 30 : $bd_color[0] + 30) .','
				. ($bd_color[1] > 30 ? $bd_color[1] - 30 : $bd_color[1] + 30) .','
				. ($bd_color[2] > 30 ? $bd_color[2] - 30 : $bd_color[2] + 30) .')';
		
			$custom_style .= '
				.sc_accordion.sc_accordion_style_1 .sc_accordion_item .sc_accordion_title:hover,
				.sc_tabs.sc_tabs_style_1 .sc_tabs_titles li a:hover,
				.sc_tooltip_parent,
				.page_top_wrap.page_top_title .breadcrumbs,
				.widget_area ul li:before,
				.widget_area ul li .count,
				.widget_area ul li.recentcomments span.comment-author-link a,
				.widget_area .widget_product_tag_cloud a:hover,
				.widget_area .widget_tag_cloud a:hover,
				.post_info.post_info_bottom .post_info_item,
				.widget_area ul li.recentcomments .comment-author-link,
				blockquote.sc_quote.sc_quote_style_2 .sc_quote_title,
				.sc_blogger.template_masonry .post_content.isotope_item_content .post_title a:hover,
				.sc_blogger.template_masonry .post_content.isotope_item_content .post_date,
				.sc_blogger.template_masonry .post_format_link .post_featured .content a:hover,
				.sc_blogger.template_classic .post_content.isotope_item_content .post_title a:hover,
				.sc_blogger.template_classic .post_content.isotope_item_content .post_date,
				.sc_blogger.template_classic .post_format_link .post_featured .content a:hover,
				.isotope_filters a.active,
				.isotope_filters a:hover,
				.menu_main_wrap .menu_main_nav li > a:hover,
				.menu_main_wrap .menu_main_nav li > a:hover:after,
				.sc_blogger.layout_news .post_date,
				.widget_area ul li a:hover,
				.widget_area .post_title a:hover,
				.sc_table.recent_jobs td a:hover,
				.sc_list.sc_list_style_ul li:before,
				li.bbp-topic-title a:hover,
				.widget_area .widget_display_replies ul li a,
				.widget_area .widget_display_replies a.bbp-reply-topic-title:hover,
				.search_wrap.search_style_regular .search_icon,
				.menu_user_wrap .menu_user_nav li li > a:hover,
				.popup_wrap .popup_close:hover,
				.pagination_wrap .pager_next,
				.pagination_wrap .pager_prev,
				.pagination_wrap .pager_last,
				.pagination_wrap .pager_first,
				.pagination_single > .pager_numbers,
				.pagination_slider .pager_cur:hover,
				.pagination_slider .pager_cur:focus,
				.pagination_pages > .active,
				.pagination_pages > a.pager_dot,
				.pagination_wrap .pager_next:hover,
				.pagination_wrap .pager_prev:hover,
				.pagination_wrap .pager_last:hover,
				.pagination_wrap .pager_first:hover,
				.menu_main_wrap .menu_main_nav > li.open > a,
				.menu_main_wrap .menu_main_nav > li.open > a:after,
				#buddypress div.item-list-tabs ul li a:hover,
				#booking_month_nav_prev .booking_month_nav_button:hover:before,
				#booking_month_nav_next .booking_month_nav_button:hover:before,
				#booking_slot_form div.booking_float_left.booking_height_20.booking_line_20 a:hover,
				#booking_slot_form div.booking_font_custom.booking_float_right.text_right:hover,
				.close_booking:hover,
				.widget_area .widget_text a,
				.widget_area .post_info a,
				a:hover,
				.copyright_wrap a,
				.widget_area .widget_calendar td a.day_wrap,
				.page_top_wrap .breadcrumbs a.breadcrumbs_item,
				a,
				.comments_list_wrap .comment_info > .comment_date > .comment_date_value,
				.comments_list_wrap .comment_info > .comment_time,
				.isotope_item_courses .post_featured .post_title a:hover
				{ 
					color: '.esc_attr($clr).';
				}
				
				.menu_user_wrap .menu_user_nav li li > a:hover
				{ 
					color: '.esc_attr($clr).' !important;
				}
				
				.sc_accordion .sc_accordion_item .sc_accordion_title.ui-state-active,
				.sc_table table thead tr,
				.sc_audio_player.sc_audio_image .mejs-button.mejs-playpause-button.mejs-play button:hover,
				.sc_audio_player.sc_audio_info .mejs-button.mejs-playpause-button.mejs-play button,
				.sc_video_frame.hover_icon:before,
				.sc_slider_controls_wrap a,
				input[type="submit"],
				input[type="button"],
				button,
				.sc_button,
				.sc_dropcaps.sc_dropcaps_style_1 .sc_dropcaps_item,
				blockquote:after,
				blockquote.sc_quote,
				.sc_infobox.sc_infobox_style_info,
				.widget_area .widget_product_tag_cloud a,
				.widget_area .widget_tag_cloud a,
				.widget_area .widget_product_search .search_button,
				.widget_area .widget_search .search_button,
				.post_item.post_item_excerpt.post_format_link,
				.sc_tabs.sc_tabs_style_2 .sc_tabs_titles li.ui-state-active a,
				.reviews_stars.reviews_style_heart,
				.reviews_block .reviews_max_level_100 .reviews_stars_hover,
				.reviews_block .reviews_item.reviews_slider,
				.sc_team_style_2 .sc_team_item_email_button:before,
				.sc_blogger.template_masonry .post_format_quote .content,
				.sc_blogger.template_classic .post_format_quote .content,
				.sc_blogger.template_portfolio .isotope_item_courses .post_category,
				.isotope_filters a,
				.sc_skills_counter .sc_skills_item.sc_skills_style_2 .sc_skills_count,
				.weblator-poll-container .btn-default,
				.pagination_single a,
				.pagination_slider .pager_cur,
				.pagination_pages > a,
				.pagination_pages > span,
				.scroll_to_top,
				.reviews_block .reviews_summary .reviews_item:before,
				.sticky .sticky_label,
				.menu_main_wrap .menu_main_nav > li > ul,
				.sc_tabs.sc_tabs_style_1 .sc_tabs_titles li.ui-state-active a,
				#bbpress-forums div.bbp-topic-content a,
				#buddypress button, #buddypress a.button, #buddypress input[type="submit"], 
				#buddypress input[type="button"], #buddypress input[type="reset"], 
				#buddypress ul.button-nav li a, #buddypress div.generic-button a,
				#buddypress .comment-reply-link, a.bp-title-button, #buddypress div.item-list-tabs ul li.selected a,
				#buddypress div.item-list-tabs ul li.current a, #buddypress div.item-list-tabs ul li.selected a
				{ 
					background-color: '.esc_attr($clr).' 
				}
				
				.menu_main_wrap .menu_main_nav_area ul::-webkit-scrollbar-thumb  {background-color: '.esc_attr($clr).';}
				
				
				.sc_audio_player.sc_audio_image .mejs-controls .mejs-pause.mejs-button button:hover,
				.sc_audio_player.sc_audio_image .mejs-controls .mejs-unmute button:hover,
				.sc_audio_player.sc_audio_info .mejs-controls .mejs-time-rail .mejs-time-current,
				.sc_audio_player.sc_audio_info .mejs-controls .mejs-pause button,
				.sc_audio_player.sc_audio_info .mejs-controls .mejs-mute button,
				.sc_audio_player.sc_audio_info .mejs-controls .mejs-unmute button,
				.sc_blogger.template_masonry .post_format_audio .sc_audio .mejs-controls .mejs-time-rail .mejs-time-current,
				.sc_blogger.template_classic .post_format_audio .sc_audio .mejs-controls .mejs-time-rail .mejs-time-current,
				.sc_audio_player.sc_audio_info .mejs-controls .mejs-mute button,
				.sc_tooltip_parent .sc_tooltip,
				.sc_tooltip_parent .sc_tooltip:before,
				.menu_main_wrap .menu_main_nav > li > ul > li > ul
				{ 
					background-color: '.esc_attr($clr).' !important 
				}
				
				.sc_tooltip_parent,
				.widget_area .widget_product_tag_cloud a,
				.widget_area .widget_tag_cloud a,
				.isotope_filters a,
				.widget_area .sc_tabs.sc_tabs_style_2 .sc_tabs_content 
				{ 
					border-color: '.esc_attr($clr).' 
				}
				
				.sc_accordion .sc_accordion_item .sc_accordion_title.ui-state-active:after 
				{ 
					border-color: '.esc_attr($clr).' transparent transparent transparent 
				}
				
				.sc_tabs.sc_tabs_style_1 .sc_tabs_titles li.ui-state-active a:before 
				{ 
					border-color: transparent transparent transparent '.esc_attr($clr).' 
				}
				
				button,
				div.bbp-submit-wrapper button,
				input[type="submit"],
				#buddypress a.disabled, #buddypress button.disabled, 
				#buddypress button.pending, #buddypress div.pending a, 
				#buddypress input[type=submit].disabled, 
				#buddypress input[type=submit].pending, 
				#buddypress input[type=submit][disabled=disabled], 
				#buddypress input[type=button].disabled, 
				#buddypress input[type=button].pending, 
				#buddypress input[type=reset].disabled, 
				#buddypress input[type=reset].pending
				{ 
					box-shadow: 0 3px '.esc_attr($box_shadow).'; 
				}
				
				.main_font_1 { color: '.esc_attr($clr).' }
				
				.main_background_1 { background-color: '.esc_attr($clr).' }
			';
		}
		// Main color 2
		$clr = themerex_get_custom_option('main_color_2');
		if (empty($clr) && $scheme!= 'original')	$clr = apply_filters('themerex_filter_get_main_color_2', '');
		if (!empty($clr)) {
			$THEMEREX_GLOBALS['color_schemes'][$scheme]['main_color_2'] = $clr;
			//$rgb = themerex_hex2rgb($clr);
			$custom_style .= '
				.sc_slider_swiper .sc_slider_info .sc_slider_reviews .reviews_stars.reviews_style_heart:before,
				del, .sc_highlight_style_3,
				.page_top_wrap .page_title,
				.page_top_wrap .breadcrumbs a.breadcrumbs_item:hover,
				.comments_list_wrap .comment_reply a
				{
					color: '.esc_attr($clr).';
				}
				.sc_audio_player.sc_audio_image .mejs-button.mejs-playpause-button.mejs-play button,
				.sc_audio_player.sc_audio_info .mejs-button.mejs-playpause-button.mejs-play button:hover,
				.sc_slider_controls_wrap a:hover,
				.sc_highlight_style_2,
				.sc_dropcaps.sc_dropcaps_style_2 .sc_dropcaps_item,
				.sc_infobox.sc_infobox_style_error,
				.page_top_wrap .page_title .page_icon
				{
					background-color: '.esc_attr($clr).';
				}
				.main_color_2
				{
					background: '.esc_attr($clr).';
				}
				.sc_audio_player.sc_audio_image .mejs-controls .mejs-time-rail .mejs-time-current,
				.sc_audio_player.sc_audio_image .mejs-controls .mejs-pause button,
				.sc_audio_player.sc_audio_image .mejs-controls .mejs-mute button,
				.sc_audio_player.sc_audio_image .mejs-controls .mejs-unmute button,
				.sc_audio_player.sc_audio_info .mejs-controls .mejs-pause.mejs-button button:hover,
				.sc_audio_player.sc_audio_image .mejs-controls .mejs-mute button,
				.sc_audio_player.sc_audio_info .mejs-controls .mejs-unmute button:hover
				{
					background: '.esc_attr($clr).' !important;
				}
				.main_color_2
				{
					border-color: '.esc_attr($clr).'; 
				}
				.flip-clock-divider.hours + ul.flip .inn, 
				.flip-clock-divider.hours + ul.flip + ul.flip .inn, 
				.flip-clock-divider.minutes + ul.flip .inn, 
				.flip-clock-divider.minutes + ul.flip + ul.flip .inn, 
				.flip-clock-divider.seconds + ul.flip .inn, 
				.flip-clock-divider.seconds + ul.flip + ul.flip .inn
				{
					color: '.esc_attr($clr).' !important;
				}



				.main_font_2 {
				    color: '.esc_attr($clr).';
				}
				.main_background_2{
				    background-color: '.esc_attr($clr).';
				}
			';
		}
		
		// button hover
		$clr = themerex_get_custom_option('button_hover');
		if (empty($clr) && $scheme!= 'original')	$clr = apply_filters('themerex_filter_get_button_hover', '');
		if (!empty($clr)) {
			$THEMEREX_GLOBALS['color_schemes'][$scheme]['button_hover'] = $clr;
			//$rgb = themerex_hex2rgb($clr);
			$custom_style .= '
				.sc_audio_player.sc_audio_image .mejs-button.mejs-playpause-button.mejs-play button:hover,
				.sc_audio_player .mejs-button.mejs-playpause-button.mejs-play button:hover,
				.sc_audio_player.sc_audio_info .mejs-controls .mejs-mute button:hover,
				.sc_audio_player.sc_audio_image .mejs-controls .mejs-mute button:hover,
				.weblator-poll-container .btn:hover,
				.weblator-poll-container .panel-footer .weblator-view-results a:hover
				{
					box-shadow: none !important;
					background-color: '.esc_attr($clr).' !important;
				}

				.sc_blogger.layout_date.sc_blogger_horizontal .post_content:hover,
				.sc_blogger.template_classic .post_content.isotope_item_content .post_category:hover,
				.sc_blogger.template_classic .post_format_video .hover_icon.sc_video_play_button:hover:before,
				.sc_blogger.template_portfolio .isotope_item_courses .post_category:hover,
				.menu_user_nav > li > a:hover,
				.search_wrap .search_icon:hover,
				input[type="submit"]:hover,
				input[type="button"]:hover,
				button:hover,
				.sc_button:hover,
				.menu_user_wrap .menu_user_right .menu_button:hover,
				span.fc-button.fc-button-prev.ui-state-default.ui-corner-left.ui-corner-right:hover,
				span.fc-button.fc-button-next.ui-state-default.ui-corner-left.ui-corner-right:hover,
				.flatButton .item .read_more:hover,
				.wpProQuiz_button:hover,
				.weblator-poll-container .btn:hover,
				.weblator-poll-container .panel-footer .weblator-view-results a:hover   {
				  background-color: '.esc_attr($clr).' !important;
				}
			';
		}
		
		// button hover shadow
		$clr = themerex_get_custom_option('button_hover_shadow');
		if (empty($clr) && $scheme!= 'original')	$clr = apply_filters('themerex_filter_get_button_hover_shadow', '');
		if (!empty($clr)) {
			$THEMEREX_GLOBALS['color_schemes'][$scheme]['button_hover_shadow'] = $clr;
			//$rgb = themerex_hex2rgb($clr);
			$custom_style .= '
				.menu_user_nav > li > a:hover,
				.search_wrap .search_icon:hover,
				input[type="submit"]:hover,
				input[type="button"]:hover,
				button:hover,
				.sc_button:hover,
				.menu_user_wrap .menu_user_right .menu_button:hover,
				span.fc-button.fc-button-prev.ui-state-default.ui-corner-left.ui-corner-right:hover,
				span.fc-button.fc-button-next.ui-state-default.ui-corner-left.ui-corner-right:hover,
				.flatButton .item .read_more:hover ,
				.wpProQuiz_button:hover  {
				  box-shadow: 0 3px '.esc_attr($clr).' !important;
				  color: #fff !important;
				}
			';
		}
		
		// user bg 1
		$clr = themerex_get_custom_option('user_bg_1');
		if (empty($clr) && $scheme!= 'original')	$clr = apply_filters('themerex_filter_get_user_bg_1', '');
		if (!empty($clr)) {
			$THEMEREX_GLOBALS['color_schemes'][$scheme]['user_bg_1'] = $clr;
			//$rgb = themerex_hex2rgb($clr);
			$custom_style .= '
				.top_panel_over .top_panel_wrap,
				.menu_main_wrap,
				.current_day				
				{
					background-color: '.esc_attr($clr).';
				}
				@-moz-document url-prefix(){
				  .menu_main_wrap .menu_main_nav_area ul:after{background-color: '.esc_attr($clr).'; }
				}
			';
		}
		
		// user bg 2
		$clr = themerex_get_custom_option('user_bg_2');
		if (empty($clr) && $scheme!= 'original')	$clr = apply_filters('themerex_filter_get_user_bg_2', '');
		if (!empty($clr)) {
			$THEMEREX_GLOBALS['color_schemes'][$scheme]['user_bg_2'] = $clr;
			//$rgb = themerex_hex2rgb($clr);
			$custom_style .= '
				.copyright_wrap,
				body	{
				 background-color: '.esc_attr($clr).';
				}
				
				.menu_main_wrap{
					border-color: '.esc_attr($clr).';
				}
				.menu_main_wrap .menu_main_nav > li ul::-webkit-scrollbar-thumb  {background-color: '.esc_attr($clr).';}
			';
		}
	
		//Ends New 
		
		return $custom_style;	
	}
}

// Add skin responsive styles
if (!function_exists('themerex_action_add_responsive_alliance')) {
	//Handler of add_action('themerex_action_add_responsive', 'themerex_action_add_responsive_alliance');
	function themerex_action_add_responsive_alliance() {
		if (file_exists(themerex_get_file_dir('skins/alliance/skin-responsive.css'))) 
			wp_enqueue_style( 'theme-skin-responsive-style', themerex_get_file_url('skins/alliance/skin-responsive.css'), array(), null );
	}
}

// Add skin responsive inline styles
if (!function_exists('themerex_filter_add_responsive_inline_alliance')) {
	//Handler of add_filter('themerex_filter_add_responsive_inline', 'themerex_filter_add_responsive_inline_alliance');
	function themerex_filter_add_responsive_inline_alliance($custom_style) {
		return $custom_style;	
	}
}


//------------------------------------------------------------------------------
// Skin's scripts
//------------------------------------------------------------------------------

// Add skin scripts
if (!function_exists('themerex_action_add_scripts_alliance')) {
	//Handler of add_action('themerex_action_add_scripts', 'themerex_action_add_scripts_alliance');
	function themerex_action_add_scripts_alliance() {
		if (file_exists(themerex_get_file_dir('skins/alliance/skin.js')))
			wp_enqueue_script( 'theme-skin-script', themerex_get_file_url('skins/alliance/skin.js'), array(), null );
		if (themerex_get_theme_option('show_theme_customizer') == 'yes' && file_exists(themerex_get_file_dir('skins/alliance/skin.customizer.js')))
			wp_enqueue_script( 'theme-skin-customizer-script', themerex_get_file_url('skins/alliance/skin.customizer.js'), array(), null );
	}
}

// Add skin scripts inline
if (!function_exists('themerex_action_add_scripts_inline_alliance')) {
	//Handler of add_action('themerex_action_add_scripts_inline', 'themerex_action_add_scripts_inline_alliance');
	function themerex_action_add_scripts_inline_alliance() {
		echo '<script type="text/javascript">'
			. 'jQuery(document).ready(function() {'
			. "if (THEMEREX_GLOBALS['theme_font']=='') THEMEREX_GLOBALS['theme_font'] = 'Roboto';"
			. "THEMEREX_GLOBALS['link_color'] = '" . apply_filters('themerex_filter_get_link_color', themerex_get_custom_option('link_color')) . "';"
			. "THEMEREX_GLOBALS['menu_color'] = '" . apply_filters('themerex_filter_get_menu_color', themerex_get_custom_option('menu_color')) . "';"
			. "THEMEREX_GLOBALS['user_color'] = '" . apply_filters('themerex_filter_get_user_color', themerex_get_custom_option('user_color')) . "';"
			. "});"
			. "</script>";
	}
}


//------------------------------------------------------------------------------
// Get skin's colors
//------------------------------------------------------------------------------

//New
// Return user menu main color 1 (if not set in the theme options)
if (!function_exists('themerex_filter_get_main_color_1_alliance')) {
	//Handler of add_filter('themerex_filter_get_main_color_1', 'themerex_filter_get_main_color_1_alliance', 10, 1);
	function themerex_filter_get_main_color_1_alliance($clr) {
		return empty($clr) ? themerex_get_scheme_color('main_color_1') : $clr;
	}
}
// Return user menu main color 2 (if not set in the theme options)
if (!function_exists('themerex_filter_get_main_color_2_alliance')) {
	//Handler of add_filter('themerex_filter_get_main_color_2', 'themerex_filter_get_main_color_2_alliance', 10, 1);
	function themerex_filter_get_main_color_2_alliance($clr) {
		return empty($clr) ? themerex_get_scheme_color('main_color_2') : $clr;
	}
}

// Return button hover (if not set in the theme options)
if (!function_exists('themerex_filter_get_button_hover_alliance')) {
	//Handler of add_filter('themerex_filter_get_button_hover', 'themerex_filter_get_button_hover_alliance', 10, 1);
	function themerex_filter_get_button_hover_alliance($clr) {
		return empty($clr) ? themerex_get_scheme_color('button_hover') : $clr;
	}
}

// Return button hover shadow (if not set in the theme options)
if (!function_exists('themerex_filter_get_button_hover_shadow_alliance')) {
	//Handler of add_filter('themerex_filter_get_button_hover_shadow', 'themerex_filter_get_button_hover_shadow_alliance', 10, 1);
	function themerex_filter_get_button_hover_shadow_alliance($clr) {
		return empty($clr) ? themerex_get_scheme_color('button_hover_shadow') : $clr;
	}
}

// Return user_bg_1 (if not set in the theme options)
if (!function_exists('themerex_filter_get_user_bg_1_alliance')) {
	//Handler of add_filter('themerex_filter_get_user_bg_1', 'themerex_filter_get_user_bg_1_alliance', 10, 1);
	function themerex_filter_get_user_bg_1_alliance($clr) {
		return empty($clr) ? themerex_get_scheme_color('user_bg_1') : $clr;
	}
}

// Return user_bg_2 (if not set in the theme options)
if (!function_exists('themerex_filter_get_user_bg_2_alliance')) {
	//Handler of add_filter('themerex_filter_get_user_bg_2', 'themerex_filter_get_user_bg_2_alliance', 10, 1);
	function themerex_filter_get_user_bg_2_alliance($clr) {
		return empty($clr) ? themerex_get_scheme_color('user_bg_2') : $clr;
	}
}
?>