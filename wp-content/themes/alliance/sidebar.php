<?php
/**
 * The Sidebar containing the main widget areas.
 */

$sidebar_show  = (is_404() ? 'none' : themerex_get_custom_option('show_sidebar_main'));
$sidebar_parts = explode(' ', $sidebar_show);
$sidebar_tint  = !empty($sidebar_parts[0]) ? $sidebar_parts[0] : 'light';
$sidebar_style = !empty($sidebar_parts[1]) ? $sidebar_parts[1] : $sidebar_tint;

$is_press = false;
if(function_exists('bp_current_component'))
	if(bp_current_component()) $is_press = true;
	
if(function_exists('is_bbpress'))
	if(is_bbpress()) $is_press = true;

if (!themerex_sc_param_is_off($sidebar_show) && !$is_press) {
	?>
	<div class="sidebar widget_area bg_tint_<?php echo esc_attr($sidebar_tint); ?> sidebar_style_<?php echo esc_attr($sidebar_style); ?>" role="complementary"> 
		<div class="grid-sizer"></div><div class="gutter-sizer"></div>
		<?php
		wp_enqueue_script( 'grid-script', themerex_get_file_url('fw/js/grid/grid.js'), array('jquery'), null, true );
		do_action( 'before_sidebar' );
		global $THEMEREX_GLOBALS;
		if (!empty($THEMEREX_GLOBALS['reviews_markup'])) 
			echo '<aside class="column-1_1 widget widget_reviews"><h5 class="widget_title">Reviews</h5>' . ($THEMEREX_GLOBALS['reviews_markup']) . '</aside>';
		$THEMEREX_GLOBALS['current_sidebar'] = 'main';
		if ( is_active_sidebar( themerex_get_custom_option('sidebar_main') ) ) {
			if ( ! dynamic_sidebar( themerex_get_custom_option('sidebar_main') ) ) {
				// Put here html if user no set widgets in sidebar
			}
		}
		do_action( 'after_sidebar' );
		?>
	</div> <!-- /.sidebar -->
	<?php
}
?>