Version 2.4.1
    Fixed:
    - Update plugins to their latest versions
    - Compatibility with Gutenberg and other PageBuilders

    Changed:
    - Move shortcodes, tools and widgets to Universal Services plugin

Version 2.4
	Fixed:
	- Login form
	- Registration form
	- Compatibility with WordPress 4.9.6
    - Update plugins to their latest versions
    - Improve theme styles
    - Update documentation

    Changed:
    - Move Importer to Universal Services plugin
    - Make forms GDPR-compliant

    Added:
    - GDPR Framework plugin


Version 2.3.1
	Documentation is updated
	Visual Composer is updated
	
Version 2.3
	Compatibility with PHP7
	Plugins updated.

Version 2.2
	Theme files are updated
	Booked updated to it last version 
	Content Timeline updated to it last version 
	Essential Grid updated to it last version 
	Visual Composer updated to it last version 

Version 2.1
	Social login improved

Version 2.0
    Google Maps updated
	  
Version 1.9
    Visual Composer updated to it last version 
	  
Version 1.8
    Visual Composer updated to it last version 
		
Version 1.7.1
	Widget Calendar fixed
	
Version 1.7
	Visual Composer updated
	Fixed 
		- Menu in the IE browser
		- Bug's of the single page
		- Blog responsive

Version 1.6
	Design of Woocommerce elements been updated

Version 1.5
   Visual Composer updated
   
Version 1.4
   Responsive fixed
   Simple Members Only added
		
Version 1.3
   Theme files bugs fixes

Version 1.2
	Fixed
		- Responsive layout 
		- Menu 

Version 1.1
   Dummy Data Installer added
   Color schemes added

Version 1.0
	Release
   