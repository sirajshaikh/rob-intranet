<?php
/**
 * ThemeREX Framework: less manipulations
 *
 * @package	themerex
 * @since	themerex 1.0
 */

// Disable direct call
if ( ! defined( 'ABSPATH' ) ) { exit; }


// Theme init
if (!function_exists('themerex_less_theme_setup')) {
	//Handler of add_action( 'themerex_action_before_init_theme', 'themerex_less_theme_setup' );
	function themerex_less_theme_setup() {
		// Recompile LESS and save CSS
		add_filter("themerex_filter_save_options", 'themerex_less_save_stylesheet', 10, 3);
	}
}

if (!function_exists('themerex_less_theme_setup2')) {
	//Handler of add_action( 'themerex_action_after_init_theme', 'themerex_less_theme_setup2' );
	function themerex_less_theme_setup2() {
		// Theme first run - compile and save css
		$theme_data = wp_get_theme();
		$slug = str_replace(' ', '_', trim(themerex_strtolower((string) $theme_data->get('Name'))));
		$option_name = 'themerex_'.strip_tags($slug).'_less_compiled';
		if ( get_option($option_name, false) === false ) {
			add_option($option_name, 1, '', 'yes');
			themerex_less_save_stylesheet(themerex_options_get_all_values(), 'general');
		} else if ( !is_admin() && themerex_get_theme_option('debug_mode')=='yes' ) {
			//themerex_less_save_stylesheet(themerex_options_get_all_values(), 'general');
		}
	}
}



/* LESS
-------------------------------------------------------------------------------- */

// Save custom stylesheet when save theme options
if ( !function_exists( 'themerex_less_save_stylesheet' ) ) {
	//Handler of add_filter("themerex_filter_save_options", 'themerex_less_save_stylesheet', 10, 3);
	function themerex_less_save_stylesheet($options, $override, $slug) {
		if ($override == 'general') {
			// Compile CSS from LESS files
			themerex_compile_less($options);
		}
		return $options;
	}
}


// Register LESS Parser
if (!function_exists('themerex_compile_less')) {	
	function themerex_compile_less($options) {
	
		// Load and create LESS Parser
		require_once( themerex_get_file_dir('lib/less/Less.php') );
		$parser = new Less_Parser( array( 
			'compress' => themerex_get_theme_option('debug_mode')=='no'
		) );
	
		list($less_vars, $main_styles) = themerex_prepare_less($options);
		
		// Collect .less files in parent and child themes
		$theme_dir = get_template_directory();
		$list = themerex_collect_files($theme_dir, 'less');
		$child_dir = get_stylesheet_directory();
		if ($theme_dir != $child_dir) $list = array_merge($list, themerex_collect_files($child_dir, 'less'));
		
		// Prepare separate array with less utils (not compile it alone - only with main files)
		$utils = array();
		$utils_time = 0;
		if (count($list) > 0) {
			foreach($list as $k=>$file) {
				$fname = basename($file);
				if ($fname[0]=='_') {
					$utils[] = $file;
					$list[$k] = '';
					$tmp = filemtime($file);
					if ($utils_time < $tmp) $utils_time = $tmp;
				}
			}
		}
		
		
		// Complile all .less files
		if (count($list) > 0) {
			foreach($list as $file) {
				if (empty($file)) continue;
				// Check if time of .css file after .less - skip current .less
				$file_css = substr_replace($file , 'css', strrpos($file , '.') + 1);
				$css_time = filemtime($file_css);
				if (file_exists($file_css) && $css_time >= filemtime($file) && ($utils_time==0 || $css_time > $utils_time)) continue;
				// Compile current .less file
				try {
					// Parse main file
					$parser->parseFile( $file, '');
					// Parse less utils
					if (count($utils) > 0) {
						foreach($utils as $utility) {
							$parser->parseFile( $utility, '');
						}
					}
					// Parse less vars (from Theme Options)
					$parser->parse($less_vars);
					// Add styles to the theme stylesheet
					if ($file == $theme_dir . '/style.less') {
						$parser->parse($main_styles);
					}
					$css = $parser->getCss();
					$parser->Reset();
					// If it main theme style - append CSS after header comments
					if ($file == $theme_dir . '/style.less') {
						// Append to the main Theme Style CSS
						$theme_css = themerex_fgc( get_template_directory() . '/style.css' );
						$css = themerex_substr($theme_css, 0, themerex_strpos($theme_css, '*/')+2) . "\n\n" . $css;
					}
					// Save compiled CSS
					themerex_fpc( substr_replace($file , 'css', strrpos($file , '.') + 1), $css);
				} catch (Exception $e) {
					if (themerex_get_theme_option('debug_mode')=='yes') dfl($e->getMessage());
				}
			}
		}
	}
}


// Prepare LESS variables after save/change options
if (!function_exists('themerex_prepare_less')) {	
	function themerex_prepare_less($options) {
		$vars = $styles = '';
	
		$styles = 'html.csstransforms body { background-color: #ff0000; }';
		
		return array($vars, $styles);
	}
}


?>