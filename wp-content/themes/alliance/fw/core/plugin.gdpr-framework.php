<?php
/* GDPR Framework support functions
------------------------------------------------------------------------------- */


// Check if gdpr framework installed and activated
if ( !function_exists( 'themerex_exists_gdpr' ) ) {
	function themerex_exists_gdpr() {
		return defined('GDPR_FRAMEWORK_VERSION');
	}
}
?>