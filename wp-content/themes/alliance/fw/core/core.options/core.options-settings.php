<?php

/* Theme setup section
-------------------------------------------------------------------- */

if ( !function_exists( 'themerex_options_settings_theme_setup2' ) ) {
	add_action( 'themerex_action_after_init_theme', 'themerex_options_settings_theme_setup2', 1 );
	function themerex_options_settings_theme_setup2() {
		if (themerex_options_is_used()) {
			global $THEMEREX_GLOBALS;
			// Replace arrays with actual parameters
			$lists = array();
			foreach ($THEMEREX_GLOBALS['options'] as $k=>$v) {
				if (isset($v['options']) && is_array($v['options'])) {
					foreach ($v['options'] as $k1=>$v1) {
						if (themerex_substr($k1, 0, 10) == '$themerex_' || themerex_substr($v1, 0, 10) == '$themerex_') {
							$list_func = themerex_substr(themerex_substr($k1, 0, 10) == '$themerex_' ? $k1 : $v1, 1);
							unset($THEMEREX_GLOBALS['options'][$k]['options'][$k1]);
							if (isset($lists[$list_func]))
								$THEMEREX_GLOBALS['options'][$k]['options'] = themerex_array_merge($THEMEREX_GLOBALS['options'][$k]['options'], $lists[$list_func]);
							else {
								if (function_exists($list_func)) {
									$THEMEREX_GLOBALS['options'][$k]['options'] = $lists[$list_func] = themerex_array_merge($THEMEREX_GLOBALS['options'][$k]['options'], $list_func == 'themerex_get_list_menus' ? $list_func(true) : $list_func());
							   	} else
							   		echo sprintf(__('Wrong function name %s in the theme options array', 'alliance'), $list_func);
							}
						}
					}
				}
			}
		}
	}
}

// Reset old Theme Options on theme first run
if ( !function_exists( 'themerex_options_reset' ) ) {
	function themerex_options_reset($clear=true) {
		$theme_data = wp_get_theme();
		$slug = str_replace(' ', '_', trim(themerex_strtolower((string) $theme_data->get('Name'))));
		$option_name = 'themerex_'.strip_tags($slug).'_options_reset';
		if ( get_option($option_name, false) === false ) {	// && (string) $theme_data->get('Version') == '1.0'
			if ($clear) {
				global $wpdb;
		//		$wpdb->query('delete from '.esc_sql($wpdb->options).' where option_name like "themerex_options%"');
				$wpdb->query(
					$wpdb->prepare( 
					'
						DELETE FROM '.esc_sql($wpdb->options).'
						WHERE option_name
						LIKE %s
					',
						'themerex_options%' 
					)
				);
			}
			add_option($option_name, 1, '', 'yes');
		}
	}
}

// Prepare default Theme Options
if ( !function_exists( 'themerex_options_settings_theme_setup' ) ) {
	add_action( 'themerex_action_before_init_theme', 'themerex_options_settings_theme_setup', 2 );	// Priority 1 for add themerex_filter handlers
	function themerex_options_settings_theme_setup() {
		global $THEMEREX_GLOBALS;
		
		// Remove 'false' to clear all saved Theme Options on next run.
		// Attention! Use this way only on new theme installation, not in updates!
		themerex_options_reset(false);
		
		// Prepare arrays 
		$THEMEREX_GLOBALS['options_params'] = array(
			'list_fonts'		=> array('$themerex_get_list_fonts' => ''),
			'list_fonts_styles'	=> array('$themerex_get_list_fonts_styles' => ''),
			'list_socials' 		=> array('$themerex_get_list_socials' => ''),
			'list_icons' 		=> array('$themerex_get_list_icons' => ''),
			'list_posts_types' 	=> array('$themerex_get_list_posts_types' => ''),
			'list_categories' 	=> array('$themerex_get_list_categories' => ''),
			'list_menus'		=> array('$themerex_get_list_menus' => ''),
			'list_sidebars'		=> array('$themerex_get_list_sidebars' => ''),
			'list_positions' 	=> array('$themerex_get_list_sidebars_positions' => ''),
			'list_tints'	 	=> array('$themerex_get_list_bg_tints' => ''),
			'list_sidebar_styles' => array('$themerex_get_list_sidebar_styles' => ''),
			'list_skins'		=> array('$themerex_get_list_skins' => ''),
			'list_color_schemes'=> array('$themerex_get_list_color_schemes' => ''),
			'list_body_styles'	=> array('$themerex_get_list_body_styles' => ''),
			'list_blog_styles'	=> array('$themerex_get_list_templates_blog' => ''),
			'list_single_styles'=> array('$themerex_get_list_templates_single' => ''),
			'list_article_styles'=> array('$themerex_get_list_article_styles' => ''),
			'list_animations_in' => array('$themerex_get_list_animations_in' => ''),
			'list_animations_out'=> array('$themerex_get_list_animations_out' => ''),
			'list_filters'		=> array('$themerex_get_list_portfolio_filters' => ''),
			'list_hovers'		=> array('$themerex_get_list_hovers' => ''),
			'list_hovers_dir'	=> array('$themerex_get_list_hovers_directions' => ''),
			'list_sliders' 		=> array('$themerex_get_list_sliders' => ''),
			'list_popups' 		=> array('$themerex_get_list_popup_engines' => ''),
			'list_gmap_styles' 	=> array('$themerex_get_list_googlemap_styles' => ''),
			'list_yes_no' 		=> array('$themerex_get_list_yesno' => ''),
			'list_on_off' 		=> array('$themerex_get_list_onoff' => ''),
			'list_show_hide' 	=> array('$themerex_get_list_showhide' => ''),
			'list_sorting' 		=> array('$themerex_get_list_sortings' => ''),
			'list_ordering' 	=> array('$themerex_get_list_orderings' => ''),
			'list_locations' 	=> array('$themerex_get_list_dedicated_locations' => '')
			);


		// Theme options array
		$THEMEREX_GLOBALS['options'] = array(

		
		//###############################
		//#### Customization         #### 
		//###############################
		'partition_customization' => array(
					"title" => __('Customization', 'alliance'),
					"start" => "partitions",
					"override" => "category,courses_group,page,post",
					"icon" => "iconadmin-cog-alt",
					"type" => "partition"
					),
		
		
		// Customization -> General
		//-------------------------------------------------
		
		'customization_general' => array(
					"title" => __('General', 'alliance'),
					"override" => "category,courses_group,page,post",
					"icon" => 'iconadmin-cog',
					"start" => "customization_tabs",
					"type" => "tab"
					),
		
		'info_custom_1' => array(
					"title" => __('Theme customization general parameters', 'alliance'),
					"desc" => __('Customize colors and enable responsive layouts for the small screens', 'alliance'),
					"override" => "category,courses_group,page,post",
					"type" => "info"
					),
	
		"icon" => array(
					"title" => __('Page icon', 'alliance'),
					"desc" => __('Select icon for output before post/category name in some layouts', 'alliance'),
					"override" => "category,courses_group,post,page",
					"std" => "",
					"options" => $THEMEREX_GLOBALS['options_params']['list_icons'],
					"style" => "select",
					"type" => "icons"
					),

		"category_color" => array(
					"title" => __('Category color', 'alliance'),
					"override" => "category,courses_group",
					"desc" => __('Used as background or text color for some elements', 'alliance'),
					"std" => "#67d3e0",
					"type" => "color"),
		
		//New 
		
		"main_color_1" => array(
					"title" => __('Main color 1', 'alliance'),
					"override" => "category,courses_group,post,page",
					"desc" => __('Used as background or text color for some elements', 'alliance'),
					"std" => "#67d3e0",
					"type" => "color"),
					
		"main_color_2" => array(
					"title" => __('Main color 2', 'alliance'),
					"override" => "category,courses_group,post,page",
					"desc" => __('Used as background color for some elements', 'alliance'),
					"std" => "#f5393d",
					"type" => "color"),
					
		"info_custom_50" => array(
					"title" => __('Button settings', 'alliance'),
					"desc" => __('Customize colors', 'alliance'),
					"override" => "category,courses_group,post,page,forum",
					"type" => "info"),
		
		"button_hover" => array(
					"title" => __('Button color', 'alliance'),
					"override" => "category,courses_group,post,page",
					"desc" => __('Used as background color for buttons on hover', 'alliance'),
					"std" => "#8cc80c",
					"type" => "color"),
		
		"button_hover_shadow" => array(
					"title" => __('Button shadow', 'alliance'),
					"override" => "category,courses_group,post,page",
					"desc" => __('Used as shadow color for buttons on hover', 'alliance'),
					"std" => "#6E9E08",
					"type" => "color"),
		
		"info_custom_51" => array(
					"title" => __('User panel settings', 'alliance'),
					"desc" => __('Customize colors', 'alliance'),
					"override" => "category,courses_group,post,page,forum",
					"type" => "info"),
					
		'user_bg_1' => array(
					"title" => __('Background color',  'alliance'),
					"desc" => __('Used as background color for user panel',  'alliance'),
					"override" => "category,courses_group,post,page",
					"std" => "#2a2f43",
					"type" => "color"
					),
					
		'user_bg_2' => array(
					"title" => __('Shadow color',  'alliance'),
					"desc" => __('Used as shadow color for user panel',  'alliance'),
					"override" => "category,courses_group,post,page",
					"std" => "#222636",
					"type" => "color"
					),
					
		//Ends New 
		
		'css_animation' => array(
					"title" => __('Extended CSS animations', 'alliance'),
					"desc" => __('Do you want use extended animations effects on your site?', 'alliance'),
					"std" => "yes",
					"options" => $THEMEREX_GLOBALS['options_params']['list_yes_no'],
					"type" => "switch"
					),

		'remember_visitors_settings' => array(
					"title" => __('Remember visitor\'s settings', 'alliance'),
					"desc" => __('To remember the settings that were made by the visitor, when navigating to other pages or to limit their effect only within the current page', 'alliance'),
					"std" => "no",
					"options" => $THEMEREX_GLOBALS['options_params']['list_yes_no'],
					"type" => "switch"
					),
					
		'responsive_layouts' => array(
					"title" => __('Responsive Layouts', 'alliance'),
					"desc" => __('Do you want use responsive layouts on small screen or still use main layout?', 'alliance'),
					"std" => "yes",
					"options" => $THEMEREX_GLOBALS['options_params']['list_yes_no'],
					"type" => "switch"
					),

		
		
		// Customization -> Body Style
		//-------------------------------------------------
		
		'customization_body' => array(
					"title" => __('Body style', 'alliance'),
					"override" => "category,courses_group,post,page",
					"icon" => 'iconadmin-picture-1',
					"type" => "tab"
					),
		
		'info_custom_3' => array(
					"title" => __('Body parameters', 'alliance'),
					"desc" => __('Background color, pattern and image used only for fixed body style.', 'alliance'),
					"override" => "category,courses_group,post,page",
					"type" => "info"
					),
					
		'body_style' => array(
					"title" => __('Body style', 'alliance'),
					"desc" => __('Select body style:<br><b>wide</b> - page fill whole window with centered content,<br><b>fullwide</b> - page content stretched on the full width of the window (with few left and right paddings)', 'alliance'),
					"divider" => false,
					"override" => "category,courses_group,post,page",
					"std" => "wide",
					"options" => $THEMEREX_GLOBALS['options_params']['list_body_styles'],
					"dir" => "horizontal",
					"type" => "radio"
					),
		
		'body_filled' => array(
					"title" => __('Fill body', 'alliance'),
					"desc" => __('Fill the body background with the solid color (white or grey) or leave it transparend to show background image (or video)', 'alliance'),
					"override" => "category,courses_group,post,page",
					"std" => "yes",
					"options" => $THEMEREX_GLOBALS['options_params']['list_yes_no'],
					"type" => "switch"
					),
		
		'bg_color' => array(
					"title" => __('Background color',  'alliance'),
					"desc" => __('Body background color',  'alliance'),
					"override" => "category,courses_group,post,page",
					"std" => "#f4f7f9",
					"type" => "color"
					),
		
		'bg_pattern' => array(
					"title" => __('Background predefined pattern',  'alliance'),
					"desc" => __('Select theme background pattern (first case - without pattern)',  'alliance'),
					"override" => "category,courses_group,post,page",
					"std" => "",
					"options" => array(
						0 => themerex_get_file_url('/images/spacer.png'),
						1 => themerex_get_file_url('/images/bg/pattern_1.png'),
						2 => themerex_get_file_url('/images/bg/pattern_2.png'),
						3 => themerex_get_file_url('/images/bg/pattern_3.png'),
						4 => themerex_get_file_url('/images/bg/pattern_4.png'),
						5 => themerex_get_file_url('/images/bg/pattern_5.png'),
						6 => themerex_get_file_url('/images/bg/pattern_6.png'),
						7 => themerex_get_file_url('/images/bg/pattern_7.png'),
						8 => themerex_get_file_url('/images/bg/pattern_8.png'),
						9 => themerex_get_file_url('/images/bg/pattern_9.png')
					),
					"style" => "list",
					"type" => "images"
					),
		
		'bg_custom_pattern' => array(
					"title" => __('Background custom pattern',  'alliance'),
					"desc" => __('Select or upload background custom pattern. If selected - use it instead the theme predefined pattern (selected in the field above)',  'alliance'),
					"override" => "category,courses_group,post,page",
					"std" => "",
					"type" => "media"
					),
		
		'bg_custom_image' => array(
					"title" => __('Background custom image',  'alliance'),
					"desc" => __('Select or upload background custom image. If selected - use it instead the theme predefined image (selected in the field above)',  'alliance'),
					"override" => "category,courses_group,post,page",
					"std" => "",
					"type" => "media"
					),
		
		'bg_custom_image_position' => array( 
					"title" => __('Background custom image position',  'alliance'),
					"desc" => __('Select custom image position',  'alliance'),
					"override" => "category,courses_group,post,page",
					"std" => "left_top",
					"options" => array(
						'left_top' => "Left Top",
						'center_top' => "Center Top",
						'right_top' => "Right Top",
						'left_center' => "Left Center",
						'center_center' => "Center Center",
						'right_center' => "Right Center",
						'left_bottom' => "Left Bottom",
						'center_bottom' => "Center Bottom",
						'right_bottom' => "Right Bottom",
					),
					"type" => "select"
					),
		
		'show_video_bg' => array(
					"title" => __('Show video background',  'alliance'),
					"desc" => __("Show video on the site background (only for Fullscreen body style)", 'alliance'),
					"override" => "category,courses_group,post,page",
					"std" => "no",
					"options" => $THEMEREX_GLOBALS['options_params']['list_yes_no'],
					"type" => "switch"
					),
		
		'video_bg_youtube_code' => array(
					"title" => __('Youtube code for video bg',  'alliance'),
					"desc" => __("Youtube code of video", 'alliance'),
					"override" => "category,courses_group,post,page",
					"std" => "",
					"type" => "text"
					),
		
		'video_bg_url' => array(
					"title" => __('Local video for video bg',  'alliance'),
					"desc" => __("URL to video-file (uploaded on your site)", 'alliance'),
					"readonly" =>false,
					"override" => "category,courses_group,post,page",
					"before" => array(	'title' => __('Choose video', 'alliance'),
										'action' => 'media_upload',
										'multiple' => false,
										'linked_field' => '',
										'type' => 'video',
										'captions' => array('choose' => __( 'Choose Video', 'alliance'),
															'update' => __( 'Select Video', 'alliance')
														)
								),
					"std" => "",
					"type" => "media"
					),
		
		'video_bg_overlay' => array(
					"title" => __('Use overlay for video bg', 'alliance'),
					"desc" => __('Use overlay texture for the video background', 'alliance'),
					"override" => "category,courses_group,post,page",
					"std" => "no",
					"options" => $THEMEREX_GLOBALS['options_params']['list_yes_no'],
					"type" => "switch"
					),

            'privacy_text' => array(
                "title" => esc_html__("Text with Privacy Policy link", 'alliance'),
                "desc"  => wp_kses_data( __("Specify text with Privacy Policy link for the checkbox 'I agree ...'", 'alliance') ),
                "std"   => wp_kses_post( __( 'I agree that my submitted data is being collected and stored.', 'alliance') ),
                "type"  => "text"
            ),
		
		
		
		// Customization -> Logo
		//-------------------------------------------------
		
		'customization_logo' => array(
					"title" => __('Logo', 'alliance'),
					"override" => "category,courses_group,post,page",
					"icon" => 'iconadmin-heart-1',
					"type" => "tab"
					),
		
		'info_custom_4' => array(
					"title" => __('Main logo', 'alliance'),
					"desc" => __('Select or upload logos for the site\'s header and select it position', 'alliance'),
					"override" => "category,courses_group,post,page",
					"type" => "info"
					),

		'favicon' => array(
					"title" => __('Favicon', 'alliance'),
					"desc" => __('Upload a 16px x 16px image that will represent your website\'s favicon.<br /><em>To ensure cross-browser compatibility, we recommend converting the favicon into .ico format before uploading. (www.favicon.cc)</em>', 'alliance'),
					"divider" => false,
					"std" => "",
					"type" => "media"
					),

		'logo_dark' => array(
					"title" => __('Logo image', 'alliance'),
					"desc" => __('Main logo image for the header', 'alliance'),
					"override" => "category,courses_group,post,page",
					"std" => "",
					"type" => "media"
					),

		'logo_bg' => array(
					"title" => __('Logo background',  'alliance'),
					"desc" => __('Logo background color',  'alliance'),
					"override" => "category,courses_group,post,page",
					"std" => "#67d3e0",
					"type" => "color"
					),
		

		'logo_text' => array(
					"title" => __('Logo text', 'alliance'),
					"desc" => __('Logo text - display it after logo image', 'alliance'),
					"override" => "category,courses_group,post,page",
					"std" => '',
					"type" => "text"
					),

		'logo_slogan' => array(
					"title" => __('Logo slogan', 'alliance'),
					"desc" => __('Logo slogan - display it under logo image (instead the site slogan)', 'alliance'),
					"override" => "category,courses_group,post,page",
					"std" => '',
					"type" => "text"
					),
		
		
		// Customization -> Menus
		//-------------------------------------------------
		
		"customization_menus" => array(
					"title" => __('Menus', 'alliance'),
					"override" => "category,courses_group,post,page",
					"icon" => 'iconadmin-menu',
					"type" => "tab"),
		
		"info_custom_8" => array(
					"title" => __("User's menu area components", 'alliance'),
					"desc" => __("Select parts for the user's menu area", 'alliance'),
					"override" => "category,courses_group,page,post",
					"type" => "info"),
		
		"show_menu_user" => array(
					"title" => __('Show user menu area', 'alliance'),
					"desc" => __('Show user menu area on top of page', 'alliance'),
					"divider" => false,
					"override" => "category,courses_group,post,page",
					"std" => "yes",
					"options" => $THEMEREX_GLOBALS['options_params']['list_yes_no'],
					"type" => "switch"),
					
		"menu_user" => array(
					"title" => __('Select user menu',  'alliance'),
					"desc" => __('Select user menu for the current page',  'alliance'),
					"override" => "category,courses_group,post,page",
					"std" => "default",
					"options" => $THEMEREX_GLOBALS['options_params']['list_menus'],
					"type" => "select"),
	
		"show_currency" => array(
					"title" => __('Show currency selector', 'alliance'),
					"desc" => __('Show currency selector in the user menu', 'alliance'),
					"override" => "category,courses_group,post,page",
					"std" => "yes",
					"options" => $THEMEREX_GLOBALS['options_params']['list_yes_no'],
					"type" => "switch"),
		
		"show_cart" => array(
					"title" => __('Show cart button', 'alliance'),
					"desc" => __('Show cart button in the user menu', 'alliance'),
					"override" => "category,courses_group,post,page",
					"std" => "shop",
					"options" => array(
						'hide'   => __('Hide', 'alliance'),
						'always' => __('Always', 'alliance'),
						'shop'   => __('Only on shop pages', 'alliance')
					),
					"type" => "checklist"),
		
		"show_languages" => array(
					"title" => __('Show language selector', 'alliance'),
					"desc" => __('Show language selector in the user menu (if WPML plugin installed and current page/post has multilanguage version)', 'alliance'),
					"override" => "category,courses_group,post,page",
					"std" => "yes",
					"options" => $THEMEREX_GLOBALS['options_params']['list_yes_no'],
					"type" => "switch"),
		
		"show_login" => array( 
					"title" => __('Show Login/Logout buttons', 'alliance'),
					"desc" => __('Show Login and Logout buttons in the user menu area', 'alliance'),
					"override" => "category,courses_group,post,page",
					"std" => "yes",
					"options" => $THEMEREX_GLOBALS['options_params']['list_yes_no'],
					"type" => "switch"),
		
		"show_bookmarks" => array(
					"title" => __('Show bookmarks', 'alliance'),
					"desc" => __('Show bookmarks selector in the user menu', 'alliance'),
					"override" => "category,courses_group,post,page",
					"std" => "yes",
					"options" => $THEMEREX_GLOBALS['options_params']['list_yes_no'],
					"type" => "switch"),

		"show_regist" => array( 
					"title" => __('Show Register buttons', 'alliance'),
					"desc" => __('Show Register button in the user menu area', 'alliance'),
					"override" => "category,courses_group,post,page",
					"std" => "no",
					"options" => $THEMEREX_GLOBALS['options_params']['list_yes_no'],
					"type" => "switch"),
	
		
		"info_custom_9" => array( 
					"title" => __("Table of Contents (TOC)", 'alliance'),
					"desc" => __("Table of Contents for the current page. Automatically created if the page contains objects with id starting with 'toc_'", 'alliance'),
					"override" => "category,courses_group,page,post",
					"type" => "info"),
		
		"menu_toc" => array( 
					"title" => __('TOC position', 'alliance'),
					"desc" => __('Show TOC for the current page', 'alliance'),
					"override" => "category,courses_group,post,page",
					"std" => "float",
					"options" => array(
						'hide'  => __('Hide', 'alliance'),
						'fixed' => __('Fixed', 'alliance'),
						'float' => __('Float', 'alliance')
					),
					"type" => "checklist"),
		
		"menu_toc_home" => array(
					"title" => __('Add "Home" into TOC', 'alliance'),
					"desc" => __('Automatically add "Home" item into table of contents - return to home page of the site', 'alliance'),
					"override" => "category,courses_group,post,page",
					"std" => "yes",
					"options" => $THEMEREX_GLOBALS['options_params']['list_yes_no'],
					"type" => "switch"),
		
		"menu_toc_top" => array( 
					"title" => __('Add "To Top" into TOC', 'alliance'),
					"desc" => __('Automatically add "To Top" item into table of contents - scroll to top of the page', 'alliance'),
					"override" => "category,courses_group,post,page",
					"std" => "yes",
					"options" => $THEMEREX_GLOBALS['options_params']['list_yes_no'],
					"type" => "switch"),
		
		
		
		
		
		// Customization -> Sidebars
		//-------------------------------------------------
		
		"customization_sidebars" => array( 
					"title" => __('Sidebars', 'alliance'),
					"icon" => "iconadmin-indent-right",
					"override" => "category,courses_group,post,page",
					"type" => "tab"),
		
		"info_custom_10" => array( 
					"title" => __('Custom sidebars', 'alliance'),
					"desc" => __('In this section you can create unlimited sidebars. You can fill them with widgets in the menu Appearance - Widgets', 'alliance'),
					"type" => "info"),
		
		"custom_sidebars" => array(
					"title" => __('Custom sidebars',  'alliance'),
					"desc" => __('Manage custom sidebars. You can use it with each category (page, post) independently',  'alliance'),
					"divider" => false,
					"std" => "",
					"cloneable" => true,
					"type" => "text"),
		
		"info_custom_11" => array(
					"title" => __('Sidebars settings', 'alliance'),
					"desc" => __('Show / Hide and Select sidebar in each location', 'alliance'),
					"override" => "category,courses_group,post,page,forum",
					"type" => "info"),
		
		'show_sidebar_main' => array( 
					"title" => __('Show main sidebar',  'alliance'),
					"desc" => __('Show or hide the main sidebar',  'alliance'),
					"override" => "category,courses_group,post,page,forum",
					"std" => "light",
					"options" => $THEMEREX_GLOBALS['options_params']['list_sidebar_styles'],
					"dir" => "horizontal",
					"type" => "checklist"),

		
		"sidebar_main" => array( 
					"title" => __('Select main sidebar',  'alliance'),
					"desc" => __('Select main sidebar for the blog page',  'alliance'),
					"override" => "category,courses_group,post,page",
					"divider" => false,
					"std" => "sidebar_main",
					"options" => $THEMEREX_GLOBALS['options_params']['list_sidebars'],
					"type" => "select"),
		
		"show_sidebar_footer" => array(
					"title" => __('Show footer sidebar', 'alliance'),
					"desc" => __('Show or hide the footer sideba', 'alliance'),
					"override" => "category,courses_group,post,page",
					"std" => "light",
					"options" => $THEMEREX_GLOBALS['options_params']['list_sidebar_styles'],
					"dir" => "horizontal",
					"type" => "checklist"),
		
		"sidebar_footer" => array( 
					"title" => __('Select footer sidebar',  'alliance'),
					"desc" => __('Select footer sidebar for the blog page',  'alliance'),
					"override" => "category,courses_group,post,page",
					"divider" => false,
					"std" => "sidebar_footer",
					"options" => $THEMEREX_GLOBALS['options_params']['list_sidebars'],
					"type" => "select"),
		
		"sidebar_footer_columns" => array( 
					"title" => __('Footer sidebar columns',  'alliance'),
					"desc" => __('Select columns number for the footer sidebar',  'alliance'),
					"override" => "category,courses_group,post,page",
					"divider" => false,
					"std" => 3,
					"min" => 1,
					"max" => 6,
					"type" => "spinner"),
		
		
		
		// Customization -> Slider
		//-------------------------------------------------
		
		"customization_slider" => array( 
					"title" => __('Slider', 'alliance'),
					"icon" => "iconadmin-picture",
					"override" => "category,courses_group,page",
					"type" => "tab"),
		
		"info_custom_13" => array(
					"title" => __('Main slider parameters', 'alliance'),
					"desc" => __('Select parameters for main slider (you can override it in each category and page)', 'alliance'),
					"override" => "category,courses_group,page",
					"type" => "info"),
					
		"show_slider" => array(
					"title" => __('Show Slider', 'alliance'),
					"desc" => __('Do you want to show slider on each page (post)', 'alliance'),
					"divider" => false,
					"override" => "category,courses_group,page",
					"std" => "no",
					"options" => $THEMEREX_GLOBALS['options_params']['list_yes_no'],
					"type" => "switch"),
					
		"slider_display" => array(
					"title" => __('Slider display', 'alliance'),
					"desc" => __('How display slider: boxed (fixed width and height), fullwide (fixed height) or fullscreen', 'alliance'),
					"override" => "category,courses_group,page",
					"std" => "none",
					"options" => array(
						"boxed"=>__("Boxed", 'alliance'),
						"fullwide"=>__("Fullwide", 'alliance'),
						"fullscreen"=>__("Fullscreen", 'alliance')
					),
					"type" => "checklist"),
		
		"slider_height" => array(
					"title" => __("Height (in pixels)", 'alliance'),
					"desc" => __("Slider height (in pixels) - only if slider display with fixed height.", 'alliance'),
					"override" => "category,courses_group,page",
					"std" => '',
					"min" => 100,
					"step" => 10,
					"type" => "spinner"),
		
		"slider_engine" => array(
					"title" => __('Slider engine', 'alliance'),
					"desc" => __('What engine use to show slider?', 'alliance'),
					"override" => "category,courses_group,page",
					"std" => "flex",
					"options" => $THEMEREX_GLOBALS['options_params']['list_sliders'],
					"type" => "radio"),
		
		"slider_alias" => array(
					"title" => __('Layer Slider: Alias (for Revolution) or ID (for Royal)',  'alliance'),
					"desc" => __("Revolution Slider alias or Royal Slider ID (see in slider settings on plugin page)", 'alliance'),
					"override" => "category,courses_group,page",
					"std" => "",
					"type" => "text"),
		
		"slider_category" => array(
					"title" => __('Posts Slider: Category to show', 'alliance'),
					"desc" => __('Select category to show in Flexslider (ignored for Revolution and Royal sliders)', 'alliance'),
					"override" => "category,courses_group,page",
					"std" => "",
					"options" => themerex_array_merge(array(0 => __('- Select category -', 'alliance')), $THEMEREX_GLOBALS['options_params']['list_categories']),
					"type" => "select",
					"multiple" => true,
					"style" => "list"),
		
		"slider_posts" => array(
					"title" => __('Posts Slider: Number posts or comma separated posts list',  'alliance'),
					"desc" => __("How many recent posts display in slider or comma separated list of posts ID (in this case selected category ignored)", 'alliance'),
					"override" => "category,courses_group,page",
					"std" => "5",
					"type" => "text"),
		
		"slider_orderby" => array(
					"title" => __("Posts Slider: Posts order by",  'alliance'),
					"desc" => __("Posts in slider ordered by date (default), comments, views, author rating, users rating, random or alphabetically", 'alliance'),
					"override" => "category,courses_group,page",
					"std" => "date",
					"options" => $THEMEREX_GLOBALS['options_params']['list_sorting'],
					"type" => "select"),
		
		"slider_order" => array(
					"title" => __("Posts Slider: Posts order", 'alliance'),
					"desc" => __('Select the desired ordering method for posts', 'alliance'),
					"override" => "category,courses_group,page",
					"std" => "desc",
					"options" => $THEMEREX_GLOBALS['options_params']['list_ordering'],
					"size" => "big",
					"type" => "switch"),
					
		"slider_interval" => array(
					"title" => __("Posts Slider: Slide change interval", 'alliance'),
					"desc" => __("Interval (in ms) for slides change in slider", 'alliance'),
					"override" => "category,courses_group,page",
					"std" => 7000,
					"min" => 100,
					"step" => 100,
					"type" => "spinner"),
		
		"slider_pagination" => array(
					"title" => __("Posts Slider: Pagination", 'alliance'),
					"desc" => __("Choose pagination style for the slider", 'alliance'),
					"override" => "category,courses_group,page",
					"std" => "no",
					"options" => array(
						'no'   => __('None', 'alliance'),
						'yes'  => __('Dots', 'alliance'), 
						'over' => __('Titles', 'alliance')
					),
					"type" => "checklist"),
		
		"slider_infobox" => array(
					"title" => __("Posts Slider: Show infobox", 'alliance'),
					"desc" => __("Do you want to show post's title, reviews rating and description on slides in slider", 'alliance'),
					"override" => "category,courses_group,page",
					"std" => "slide",
					"options" => array(
						'no'    => __('None',  'alliance'),
						'slide' => __('Slide', 'alliance'), 
						'fixed' => __('Fixed', 'alliance')
					),
					"type" => "checklist"),
					
		"slider_info_category" => array(
					"title" => __("Posts Slider: Show post's category", 'alliance'),
					"desc" => __("Do you want to show post's category on slides in slider", 'alliance'),
					"override" => "category,courses_group,page",
					"std" => "yes",
					"options" => $THEMEREX_GLOBALS['options_params']['list_yes_no'],
					"type" => "switch"),
					
		"slider_info_reviews" => array(
					"title" => __("Posts Slider: Show post's reviews rating", 'alliance'),
					"desc" => __("Do you want to show post's reviews rating on slides in slider", 'alliance'),
					"override" => "category,courses_group,page",
					"std" => "yes",
					"options" => $THEMEREX_GLOBALS['options_params']['list_yes_no'],
					"type" => "switch"),
					
		"slider_info_descriptions" => array(
					"title" => __("Posts Slider: Show post's descriptions", 'alliance'),
					"desc" => __("How many characters show in the post's description in slider. 0 - no descriptions", 'alliance'),
					"override" => "category,courses_group,page",
					"std" => 0,
					"min" => 0,
					"step" => 10,
					"type" => "spinner"),
		
		
		
		
		// Customization -> Header & Footer
		//-------------------------------------------------
		
		'customization_header_footer' => array(
					"title" => __("Header &amp; Footer", 'alliance'),
					"override" => "category,courses_group,post,page",
					"icon" => 'iconadmin-window',
					"type" => "tab"),
		
		
		"info_footer_1" => array(
					"title" => __("Header settings", 'alliance'),
					"desc" => __("Select components of the page header, set style and put the content for the user's header area", 'alliance'),
					"override" => "category,courses_group,page,post",
					"type" => "info"),
		
		"show_page_top" => array(
					"title" => __('Show Top of page section', 'alliance'),
					"desc" => __('Show top section with post/page/category title and breadcrumbs', 'alliance'),
					"override" => "category,courses_group,post,page",
					"std" => "yes",
					"options" => $THEMEREX_GLOBALS['options_params']['list_yes_no'],
					"type" => "switch"),
		
		"show_page_title" => array(
					"title" => __('Show Page title', 'alliance'),
					"desc" => __('Show post/page/category title', 'alliance'),
					"override" => "category,courses_group,post,page",
					"std" => "yes",
					"options" => $THEMEREX_GLOBALS['options_params']['list_yes_no'],
					"type" => "switch"),
		
		"show_breadcrumbs" => array(
					"title" => __('Show Breadcrumbs', 'alliance'),
					"desc" => __('Show path to current category (post, page)', 'alliance'),
					"override" => "category,courses_group,post,page",
					"std" => "yes",
					"options" => $THEMEREX_GLOBALS['options_params']['list_yes_no'],
					"type" => "switch"),
		
		"breadcrumbs_max_level" => array(
					"title" => __('Breadcrumbs max nesting', 'alliance'),
					"desc" => __("Max number of the nested categories in the breadcrumbs (0 - unlimited)", 'alliance'),
					"std" => "0",
					"min" => 0,
					"max" => 100,
					"step" => 1,
					"type" => "spinner"),
		
		
		
		
		"info_footer_2" => array(
					"title" => __("Footer settings", 'alliance'),
					"desc" => __("Select components of the footer, set style and put the content for the user's footer area", 'alliance'),
					"override" => "category,courses_group,page,post",
					"type" => "info"),
		
		"show_user_footer" => array(
					"title" => __("Show user's footer", 'alliance'),
					"desc" => __("Show custom user's footer", 'alliance'),
					"divider" => false,
					"override" => "category,courses_group,page,post",
					"std" => "no",
					"options" => $THEMEREX_GLOBALS['options_params']['list_yes_no'],
					"type" => "switch"),
		
		"user_footer_content" => array(
					"title" => __("User's footer content", 'alliance'),
					"desc" => __('Put footer html-code and/or shortcodes here. You can use any html-tags and shortcodes', 'alliance'),
					"override" => "category,courses_group,page,post",
					"std" => "",
					"rows" => "10",
					"type" => "editor"),
		
		"show_contacts_in_footer" => array(
					"title" => __('Show Contacts in footer', 'alliance'),
					"desc" => __('Show contact information area in footer: site logo, contact info and large social icons', 'alliance'),
					"override" => "category,courses_group,post,page",
					"std" => "dark",
					"options" => array(
						'hide' 	=> __('Hide', 'alliance'),
						'dark'	=> __('Show', 'alliance')
					),
					"dir" => "horizontal",
					"type" => "checklist"),

		"show_copyright_in_footer" => array(
					"title" => __('Show Copyright area in footer', 'alliance'),
					"desc" => __('Show area with copyright information and small social icons in footer', 'alliance'),
					"override" => "category,courses_group,post,page",
					"std" => "yes",
					"options" => $THEMEREX_GLOBALS['options_params']['list_yes_no'],
					"type" => "switch"),

		"footer_copyright" => array(
					"title" => __('Footer copyright text',  'alliance'),
					"desc" => __("Copyright text to show in footer area (bottom of site)", 'alliance'),
					"override" => "category,courses_group,page,post",
					"std" => "ThemeREX &copy; {Y} All Rights Reserved ",
					"rows" => "10",
					"type" => "editor"),
		
		
		"info_footer_3" => array(
					"title" => __('Testimonials in Footer', 'alliance'),
					"desc" => __('Select parameters for Testimonials in the Footer (you can override it in each category and page)', 'alliance'),
					"override" => "category,courses_group,page,post",
					"type" => "info"),

		"show_testimonials_in_footer" => array(
					"title" => __('Show Testimonials in footer', 'alliance'),
					"desc" => __('Show Testimonials slider in footer. For correct operation of the slider (and shortcode testimonials) you must fill out Testimonials posts on the menu "Testimonials"', 'alliance'),
					"override" => "category,courses_group,post,page",
					"divider" => false,
					"std" => "none",
					"options" => $THEMEREX_GLOBALS['options_params']['list_tints'],
					"type" => "checklist"),

		"testimonials_count" => array( 
					"title" => __('Testimonials count', 'alliance'),
					"desc" => __('Number testimonials to show', 'alliance'),
					"override" => "category,courses_group,post,page",
					"std" => 3,
					"step" => 1,
					"min" => 1,
					"max" => 10,
					"type" => "spinner"),

		"testimonials_bg_image" => array( 
					"title" => __('Testimonials bg image', 'alliance'),
					"desc" => __('Select image or put image URL from other site to use it as testimonials block background', 'alliance'),
					"override" => "category,courses_group,post,page",
					"readonly" => false,
					"std" => "",
					"type" => "media"),

		"testimonials_bg_color" => array( 
					"title" => __('Testimonials bg color', 'alliance'),
					"desc" => __('Select color to use it as testimonials block background', 'alliance'),
					"override" => "category,courses_group,post,page",
					"std" => "",
					"type" => "color"),

		"testimonials_bg_overlay" => array( 
					"title" => __('Testimonials bg overlay', 'alliance'),
					"desc" => __('Select background color opacity to create overlay effect on background', 'alliance'),
					"override" => "category,courses_group,post,page",
					"std" => 0,
					"step" => 0.1,
					"min" => 0,
					"max" => 1,
					"type" => "spinner"),
		
		
		"info_footer_4" => array(
					"title" => __('Twitter in Footer', 'alliance'),
					"desc" => __('Select parameters for Twitter stream in the Footer (you can override it in each category and page)', 'alliance'),
					"override" => "category,courses_group,page,post",
					"type" => "info"),

		"show_twitter_in_footer" => array(
					"title" => __('Show Twitter in footer', 'alliance'),
					"desc" => __('Show Twitter slider in footer. For correct operation of the slider (and shortcode twitter) you must fill out the Twitter API keys on the menu "Appearance - Theme Options - Socials"', 'alliance'),
					"override" => "category,courses_group,post,page",
					"divider" => false,
					"std" => "none",
					"options" => $THEMEREX_GLOBALS['options_params']['list_tints'],
					"type" => "checklist"),

		"twitter_count" => array( 
					"title" => __('Twitter count', 'alliance'),
					"desc" => __('Number twitter to show', 'alliance'),
					"override" => "category,courses_group,post,page",
					"std" => 3,
					"step" => 1,
					"min" => 1,
					"max" => 10,
					"type" => "spinner"),

		"twitter_bg_image" => array( 
					"title" => __('Twitter bg image', 'alliance'),
					"desc" => __('Select image or put image URL from other site to use it as Twitter block background', 'alliance'),
					"override" => "category,courses_group,post,page",
					"std" => "",
					"type" => "media"),

		"twitter_bg_color" => array( 
					"title" => __('Twitter bg color', 'alliance'),
					"desc" => __('Select color to use it as Twitter block background', 'alliance'),
					"override" => "category,courses_group,post,page",
					"std" => "",
					"type" => "color"),

		"twitter_bg_overlay" => array( 
					"title" => __('Twitter bg overlay', 'alliance'),
					"desc" => __('Select background color opacity to create overlay effect on background', 'alliance'),
					"override" => "category,courses_group,post,page",
					"std" => 0,
					"step" => 0.1,
					"min" => 0,
					"max" => 1,
					"type" => "spinner"),


		"info_footer_5" => array(
					"title" => __('Google map parameters', 'alliance'),
					"desc" => __('Select parameters for Google map (you can override it in each category and page)', 'alliance'),
					"override" => "category,courses_group,page,post",
					"type" => "info"),
					
		"show_googlemap" => array(
					"title" => __('Show Google Map', 'alliance'),
					"desc" => __('Do you want to show Google map on each page (post)', 'alliance'),
					"divider" => false,
					"override" => "category,courses_group,page,post",
					"std" => "no",
					"options" => $THEMEREX_GLOBALS['options_params']['list_yes_no'],
					"type" => "switch"),
		
		"googlemap_height" => array(
					"title" => __("Map height", 'alliance'),
					"desc" => __("Map height (default - in pixels, allows any CSS units of measure)", 'alliance'),
					"override" => "category,courses_group,page",
					"std" => 400,
					"min" => 100,
					"step" => 10,
					"type" => "spinner"),
		
		"googlemap_address" => array(
					"title" => __('Address to show on map',  'alliance'),
					"desc" => __("Enter address to show on map center", 'alliance'),
					"override" => "category,courses_group,page,post",
					"std" => "",
					"type" => "text"),
		
		"googlemap_latlng" => array(
					"title" => __('Latitude and Longtitude to show on map',  'alliance'),
					"desc" => __("Enter coordinates (separated by comma) to show on map center (instead of address)", 'alliance'),
					"override" => "category,courses_group,page,post",
					"std" => "",
					"type" => "text"),
		
		"googlemap_zoom" => array(
					"title" => __('Google map initial zoom',  'alliance'),
					"desc" => __("Enter desired initial zoom for Google map", 'alliance'),
					"override" => "category,courses_group,page,post",
					"std" => 16,
					"min" => 1,
					"max" => 20,
					"step" => 1,
					"type" => "spinner"),
		
		"googlemap_style" => array(
					"title" => __('Google map style',  'alliance'),
					"desc" => __("Select style to show Google map", 'alliance'),
					"override" => "category,courses_group,page,post",
					"std" => 'style1',
					"options" => $THEMEREX_GLOBALS['options_params']['list_gmap_styles'],
					"type" => "select"),
		
		"googlemap_marker" => array(
					"title" => __('Google map marker',  'alliance'),
					"desc" => __("Select or upload png-image with Google map marker", 'alliance'),
					"std" => '',
					"type" => "media"),
		
		
		
		
		// Customization -> Media
		//-------------------------------------------------
		
		'customization_media' => array(
					"title" => __('Media', 'alliance'),
					"override" => "category,courses_group,post,page",
					"icon" => 'iconadmin-picture',
					"type" => "tab"),
		
		"info_media_1" => array(
					"title" => __('Retina ready', 'alliance'),
					"desc" => __("Additional parameters for the Retina displays", 'alliance'),
					"type" => "info"),
					
		"retina_ready" => array(
					"title" => __('Image dimensions', 'alliance'),
					"desc" => __('What dimensions use for uploaded image: Original or "Retina ready" (twice enlarged)', 'alliance'),
					"divider" => false,
					"std" => "1",
					"size" => "medium",
					"options" => array("1"=>__("Original", 'alliance'), "2"=>__("Retina", 'alliance')),
					"type" => "switch"),
		
		"info_media_2" => array(
					"title" => __('Media Substitution parameters', 'alliance'),
					"desc" => __("Set up the media substitution parameters and slider's options", 'alliance'),
					"override" => "category,courses_group,page,post",
					"type" => "info"),
		
		"substitute_gallery" => array(
					"title" => __('Substitute standard Wordpress gallery', 'alliance'),
					"desc" => __('Substitute standard Wordpress gallery with our slider on the single pages', 'alliance'),
					"divider" => false,
					"override" => "category,courses_group,post,page",
					"std" => "no",
					"options" => $THEMEREX_GLOBALS['options_params']['list_yes_no'],
					"type" => "switch"),
					
		"substitute_slider_engine" => array(
					"title" => __('Substitution Slider engine', 'alliance'),
					"desc" => __('What engine use to show slider instead standard gallery?', 'alliance'),
					"override" => "category,courses_group,post,page",
					"std" => "swiper",
					"options" => array(
						//"chop" => __("Chop slider", 'alliance'),
						"swiper" => __("Swiper slider", 'alliance')
					),
					"type" => "radio"),
		
		"gallery_instead_image" => array(
					"title" => __('Show gallery instead featured image', 'alliance'),
					"desc" => __('Show slider with gallery instead featured image on blog streampage and in the related posts section for the gallery posts', 'alliance'),
					"override" => "category,courses_group,post,page",
					"std" => "yes",
					"options" => $THEMEREX_GLOBALS['options_params']['list_yes_no'],
					"type" => "switch"),
		
		"gallery_max_slides" => array(
					"title" => __('Max images number in the slider', 'alliance'),
					"desc" => __('Maximum images number from gallery into slider', 'alliance'),
					"override" => "category,courses_group,post,page",
					"std" => "5",
					"min" => 2,
					"max" => 10,
					"type" => "spinner"),
		
		"popup_engine" => array(
					"title" => __('Gallery popup engine', 'alliance'),
					"desc" => __('Select engine to show popup windows with galleries', 'alliance'),
					"std" => "magnific",
					"options" => $THEMEREX_GLOBALS['options_params']['list_popups'],
					"type" => "select"),
		
		"popup_gallery" => array(
					"title" => __('Enable Gallery mode in the popup', 'alliance'),
					"desc" => __('Enable Gallery mode in the popup or show only single image', 'alliance'),
					"std" => "yes",
					"options" => $THEMEREX_GLOBALS['options_params']['list_yes_no'],
					"type" => "switch"),
		
		
		"substitute_audio" => array(
					"title" => __('Substitute audio tags', 'alliance'),
					"desc" => __('Substitute audio tag with source from soundcloud to embed player', 'alliance'),
					"override" => "category,courses_group,post,page",
					"std" => "yes",
					"options" => $THEMEREX_GLOBALS['options_params']['list_yes_no'],
					"type" => "switch"),
		
		"substitute_video" => array(
					"title" => __('Substitute video tags', 'alliance'),
					"desc" => __('Substitute video tags with embed players or leave video tags unchanged (if you use third party plugins for the video tags)', 'alliance'),
					"override" => "category,courses_group,post,page",
					"std" => "yes",
					"options" => $THEMEREX_GLOBALS['options_params']['list_yes_no'],
					"type" => "switch"),
		
		"use_mediaelement" => array(
					"title" => __('Use Media Element script for audio and video tags', 'alliance'),
					"desc" => __('Do you want use the Media Element script for all audio and video tags on your site or leave standard HTML5 behaviour?', 'alliance'),
					"std" => "yes",
					"options" => $THEMEREX_GLOBALS['options_params']['list_yes_no'],
					"type" => "switch"),
		
		
		
		
		
		// Customization -> Typography
		//-------------------------------------------------
		
		'customization_typography' => array(
					"title" => __("Typography", 'alliance'),
					"icon" => 'iconadmin-font',
					"override" => "category,courses_group,post,page",
					"type" => "tab"),
		
		"info_typo_1" => array(
					"title" => __('Typography settings', 'alliance'),
					"desc" => __('Select fonts, sizes and styles for the headings and paragraphs. You can use Google fonts and custom fonts.<br><br>How to install custom @font-face fonts into the theme?<br>All @font-face fonts are located in "theme_name/css/font-face/" folder in the separate subfolders for the each font. Subfolder name is a font-family name!<br>Place full set of the font files (for each font style and weight) and css-file named stylesheet.css in the each subfolder.<br>Create your @font-face kit by using Fontsquirrel @font-face Generator and then extract the font kit (with folder in the kit) into the "theme_name/css/font-face" folder to install.', 'alliance'),
					"override" => "category,courses_group,post,page",
					"type" => "info"),
		
		"typography_custom" => array(
					"title" => __('Use custom typography', 'alliance'),
					"desc" => __('Use custom font settings or leave theme-styled fonts', 'alliance'),
					"divider" => false,
					"override" => "category,courses_group,post,page",
					"std" => "yes",
					"options" => $THEMEREX_GLOBALS['options_params']['list_yes_no'],
					"type" => "switch"),
		
		"typography_h1_font" => array(
					"title" => __('Heading 1', 'alliance'),
					"desc" => '',
					"divider" => false,
					"override" => "category,courses_group,post,page",
					"columns" => "3_8 first",
					"std" => "Open Sans",
					"options" => $THEMEREX_GLOBALS['options_params']['list_fonts'],
					"type" => "fonts"),
		
		"typography_h1_size" => array(
					"title" => __('Size', 'alliance'),
					"desc" => '',
					"divider" => false,
					"override" => "category,courses_group,post,page",
					"columns" => "1_8",
					"std" => "45",
					"step" => 1,
					"from" => 12,
					"to" => 60,
					"type" => "select"),
		
		"typography_h1_lineheight" => array(
					"title" => __('Line height', 'alliance'),
					"desc" => '',
					"divider" => false,
					"override" => "category,courses_group,post,page",
					"columns" => "1_8",
					"std" => "54",
					"step" => 1,
					"from" => 12,
					"to" => 100,
					"type" => "select"),
		
		"typography_h1_weight" => array(
					"title" => __('Weight', 'alliance'),
					"desc" => '',
					"divider" => false,
					"override" => "category,courses_group,post,page",
					"columns" => "1_8",
					"std" => "600",
					"step" => 100,
					"from" => 100,
					"to" => 900,
					"type" => "select"),
		
		"typography_h1_style" => array(
					"title" => __('Style', 'alliance'),
					"desc" => '',
					"divider" => false,
					"override" => "category,courses_group,post,page",
					"columns" => "1_8",
					"std" => "",
					"multiple" => true,
					"options" => $THEMEREX_GLOBALS['options_params']['list_fonts_styles'],
					"type" => "checklist"),
		
		"typography_h1_color" => array(
					"title" => __('Color', 'alliance'),
					"desc" => '',
					"divider" => false,
					"override" => "category,courses_group,post,page",
					"columns" => "1_8",
					"std" => "#2a2f43",
					"style" => "custom",
					"type" => "color"),
		
		"typography_h2_font" => array(
					"title" => __('Heading 2', 'alliance'),
					"desc" => '',
					"divider" => false,
					"override" => "category,courses_group,post,page",
					"columns" => "3_8 first",
					"std" => "Open Sans",
					"options" => $THEMEREX_GLOBALS['options_params']['list_fonts'],
					"type" => "fonts"),
		
		"typography_h2_size" => array(
					"title" => '',
					"desc" => '',
					"divider" => false,
					"columns" => "1_8",
					"override" => "category,courses_group,post,page",
					"std" => "36",
					"step" => 1,
					"from" => 12,
					"to" => 60,
					"type" => "select"),
		
		"typography_h2_lineheight" => array(
					"title" => '',
					"desc" => '',
					"divider" => false,
					"override" => "category,courses_group,post,page",
					"columns" => "1_8",
					"std" => "43",
					"step" => 1,
					"from" => 12,
					"to" => 100,
					"type" => "select"),
		
		"typography_h2_weight" => array(
					"title" => '',
					"desc" => '',
					"divider" => false,
					"override" => "category,courses_group,post,page",
					"columns" => "1_8",
					"std" => "600",
					"step" => 100,
					"from" => 100,
					"to" => 900,
					"type" => "select"),
		
		"typography_h2_style" => array(
					"title" => '',
					"desc" => '',
					"divider" => false,
					"override" => "category,courses_group,post,page",
					"columns" => "1_8",
					"std" => "",
					"multiple" => true,
					"options" => $THEMEREX_GLOBALS['options_params']['list_fonts_styles'],
					"type" => "checklist"),
		
		"typography_h2_color" => array(
					"title" => '',
					"desc" => '',
					"divider" => false,
					"override" => "category,courses_group,post,page",
					"columns" => "1_8",
					"std" => "#2a2f43",
					"style" => "custom",
					"type" => "color"),
		
		"typography_h3_font" => array(
					"title" => __('Heading 3', 'alliance'),
					"desc" => '',
					"divider" => false,
					"override" => "category,courses_group,post,page",
					"columns" => "3_8 first",
					"std" => "Open Sans",
					"options" => $THEMEREX_GLOBALS['options_params']['list_fonts'],
					"type" => "fonts"),
		
		"typography_h3_size" => array(
					"title" => '',
					"desc" => '',
					"divider" => false,
					"override" => "category,courses_group,post,page",
					"columns" => "1_8",
					"std" => "24",
					"step" => 1,
					"from" => 12,
					"to" => 60,
					"type" => "select"),
		
		"typography_h3_lineheight" => array(
					"title" => '',
					"desc" => '',
					"divider" => false,
					"override" => "category,courses_group,post,page",
					"columns" => "1_8",
					"std" => "28",
					"step" => 1,
					"from" => 12,
					"to" => 100,
					"type" => "select"),
		
		"typography_h3_weight" => array(
					"title" => '',
					"desc" => '',
					"divider" => false,
					"override" => "category,courses_group,post,page",
					"columns" => "1_8",
					"std" => "600",
					"step" => 100,
					"from" => 100,
					"to" => 900,
					"type" => "select"),
		
		"typography_h3_style" => array(
					"title" => '',
					"desc" => '',
					"divider" => false,
					"override" => "category,courses_group,post,page",
					"columns" => "1_8",
					"std" => "",
					"multiple" => true,
					"options" => $THEMEREX_GLOBALS['options_params']['list_fonts_styles'],
					"type" => "checklist"),
		
		"typography_h3_color" => array(
					"title" => '',
					"desc" => '',
					"divider" => false,
					"override" => "category,courses_group,post,page",
					"columns" => "1_8",
					"std" => "#2a2f43",
					"style" => "custom",
					"type" => "color"),
		
		"typography_h4_font" => array(
					"title" => __('Heading 4', 'alliance'),
					"desc" => '',
					"divider" => false,
					"override" => "category,courses_group,post,page",
					"columns" => "3_8 first",
					"std" => "Open Sans",
					"options" => $THEMEREX_GLOBALS['options_params']['list_fonts'],
					"type" => "fonts"),
		
		"typography_h4_size" => array(
					"title" => '',
					"desc" => '',
					"divider" => false,
					"override" => "category,courses_group,post,page",
					"columns" => "1_8",
					"std" => "22",
					"step" => 1,
					"from" => 12,
					"to" => 60,
					"type" => "select"),
		
		"typography_h4_lineheight" => array(
					"title" => '',
					"desc" => '',
					"divider" => false,
					"override" => "category,courses_group,post,page",
					"columns" => "1_8",
					"std" => "26",
					"step" => 1,
					"from" => 12,
					"to" => 100,
					"type" => "select"),
		
		"typography_h4_weight" => array(
					"title" => '',
					"desc" => '',
					"divider" => false,
					"override" => "category,courses_group,post,page",
					"columns" => "1_8",
					"std" => "700",
					"step" => 100,
					"from" => 100,
					"to" => 900,
					"type" => "select"),
		
		"typography_h4_style" => array(
					"title" => '',
					"desc" => '',
					"divider" => false,
					"override" => "category,courses_group,post,page",
					"columns" => "1_8",
					"std" => "",
					"multiple" => true,
					"options" => $THEMEREX_GLOBALS['options_params']['list_fonts_styles'],
					"type" => "checklist"),
		
		"typography_h4_color" => array(
					"title" => '',
					"desc" => '',
					"divider" => false,
					"override" => "category,courses_group,post,page",
					"columns" => "1_8",
					"std" => "#2a2f43",
					"style" => "custom",
					"type" => "color"),
		
		"typography_h5_font" => array(
					"title" => __('Heading 5', 'alliance'),
					"desc" => '',
					"divider" => false,
					"override" => "category,courses_group,post,page",
					"columns" => "3_8 first",
					"std" => "Open Sans",
					"options" => $THEMEREX_GLOBALS['options_params']['list_fonts'],
					"type" => "fonts"),
		
		"typography_h5_size" => array(
					"title" => '',
					"desc" => '',
					"divider" => false,
					"override" => "category,courses_group,post,page",
					"columns" => "1_8",
					"std" => "20",
					"step" => 1,
					"from" => 12,
					"to" => 60,
					"type" => "select"),
		
		"typography_h5_lineheight" => array(
					"title" => '',
					"desc" => '',
					"divider" => false,
					"override" => "category,courses_group,post,page",
					"columns" => "1_8",
					"std" => "24",
					"step" => 1,
					"from" => 12,
					"to" => 100,
					"type" => "select"),
		
		"typography_h5_weight" => array(
					"title" => '',
					"desc" => '',
					"divider" => false,
					"columns" => "1_8",
					"override" => "category,courses_group,post,page",
					"std" => "700",
					"step" => 100,
					"from" => 100,
					"to" => 900,
					"type" => "select"),
		
		"typography_h5_style" => array(
					"title" => '',
					"desc" => '',
					"divider" => false,
					"override" => "category,courses_group,post,page",
					"columns" => "1_8",
					"std" => "",
					"multiple" => true,
					"options" => $THEMEREX_GLOBALS['options_params']['list_fonts_styles'],
					"type" => "checklist"),
		
		"typography_h5_color" => array(
					"title" => '',
					"desc" => '',
					"divider" => false,
					"override" => "category,courses_group,post,page",
					"columns" => "1_8",
					"std" => "#2a2f43",
					"style" => "custom",
					"type" => "color"),
		
		"typography_h6_font" => array(
					"title" => __('Heading 6', 'alliance'),
					"desc" => '',
					"divider" => false,
					"override" => "category,courses_group,post,page",
					"columns" => "3_8 first",
					"std" => "Open Sans",
					"options" => $THEMEREX_GLOBALS['options_params']['list_fonts'],
					"type" => "fonts"),
		
		"typography_h6_size" => array(
					"title" => '',
					"desc" => '',
					"divider" => false,
					"override" => "category,courses_group,post,page",
					"columns" => "1_8",
					"std" => "14",
					"step" => 1,
					"from" => 12,
					"to" => 60,
					"type" => "select"),
		
		"typography_h6_lineheight" => array(
					"title" => '',
					"desc" => '',
					"divider" => false,
					"override" => "category,courses_group,post,page",
					"columns" => "1_8",
					"std" => "20",
					"step" => 1,
					"from" => 12,
					"to" => 100,
					"type" => "select"),
		
		"typography_h6_weight" => array(
					"title" => '',
					"desc" => '',
					"divider" => false,
					"override" => "category,courses_group,post,page",
					"columns" => "1_8",
					"std" => "700",
					"step" => 100,
					"from" => 100,
					"to" => 900,
					"type" => "select"),
		
		"typography_h6_style" => array(
					"title" => '',
					"desc" => '',
					"divider" => false,
					"override" => "category,courses_group,post,page",
					"columns" => "1_8",
					"std" => "",
					"multiple" => true,
					"options" => $THEMEREX_GLOBALS['options_params']['list_fonts_styles'],
					"type" => "checklist"),
		
		"typography_h6_color" => array(
					"title" => '',
					"desc" => '',
					"divider" => false,
					"override" => "category,courses_group,post,page",
					"columns" => "1_8",
					"std" => "#2a2f43",
					"style" => "custom",
					"type" => "color"),
		
		"typography_p_font" => array(
					"title" => __('Paragraph text', 'alliance'),
					"desc" => '',
					"divider" => false,
					"override" => "category,courses_group,post,page",
					"columns" => "3_8 first",
					"std" => "Open Sans",
					"options" => $THEMEREX_GLOBALS['options_params']['list_fonts'],
					"type" => "fonts"),
		
		"typography_p_size" => array(
					"title" => '',
					"desc" => '',
					"divider" => false,
					"override" => "category,courses_group,post,page",
					"columns" => "1_8",
					"std" => "14",
					"step" => 1,
					"from" => 12,
					"to" => 60,
					"type" => "select"),
		
		"typography_p_lineheight" => array(
					"title" => '',
					"desc" => '',
					"divider" => false,
					"override" => "category,courses_group,post,page",
					"columns" => "1_8",
					"std" => "24",
					"step" => 1,
					"from" => 12,
					"to" => 100,
					"type" => "select"),
		
		"typography_p_weight" => array(
					"title" => '',
					"desc" => '',
					"divider" => false,
					"override" => "category,courses_group,post,page",
					"columns" => "1_8",
					"std" => "400",
					"step" => 100,
					"from" => 100,
					"to" => 900,
					"type" => "select"),
		
		"typography_p_style" => array(
					"title" => '',
					"desc" => '',
					"divider" => false,
					"override" => "category,courses_group,post,page",
					"columns" => "1_8",
					"std" => "",
					"multiple" => true,
					"options" => $THEMEREX_GLOBALS['options_params']['list_fonts_styles'],
					"type" => "checklist"),
		
		"typography_p_color" => array(
					"title" => '',
					"desc" => '',
					"divider" => false,
					"override" => "category,courses_group,post,page",
					"columns" => "1_8 last",
					"std" => "#7f828f",
					"style" => "custom",
					"type" => "color"),
		
		
		
		
		
		
		//###############################
		//#### Blog and Single pages #### 
		//###############################
		"partition_blog" => array(
					"title" => __('Blog &amp; Single', 'alliance'),
					"icon" => "iconadmin-docs",
					"override" => "category,courses_group,post,page",
					"type" => "partition"),
		
		
		
		// Blog -> Stream page
		//-------------------------------------------------
		
		'blog_tab_stream' => array(
					"title" => __('Stream page', 'alliance'),
					"start" => 'blog_tabs',
					"icon" => "iconadmin-docs",
					"override" => "category,courses_group,post,page",
					"type" => "tab"),
		
		"info_blog_1" => array(
					"title" => __('Blog streampage parameters', 'alliance'),
					"desc" => __('Select desired blog streampage parameters (you can override it in each category)', 'alliance'),
					"override" => "category,courses_group,post,page",
					"type" => "info"),
		
		"blog_style" => array(
					"title" => __('Blog style', 'alliance'),
					"desc" => __('Select desired blog style', 'alliance'),
					"divider" => false,
					"override" => "category,courses_group,page",
					"std" => "excerpt",
					"options" => $THEMEREX_GLOBALS['options_params']['list_blog_styles'],
					"type" => "select"),
		
		"article_style" => array(
					"title" => __('Article style', 'alliance'),
					"desc" => __('Select article display method: boxed or stretch', 'alliance'),
					"override" => "category,courses_group,page",
					"std" => "stretch",
					"options" => $THEMEREX_GLOBALS['options_params']['list_article_styles'],
					"size" => "medium",
					"type" => "switch"),
		
		"hover_style" => array(
					"title" => __('Hover style', 'alliance'),
					"desc" => __('Select desired hover style (only for Blog style = Portfolio)', 'alliance'),
					"override" => "category,courses_group,page",
					"std" => "square effect_shift",
					"options" => $THEMEREX_GLOBALS['options_params']['list_hovers'],
					"type" => "select"),
		
		"hover_dir" => array(
					"title" => __('Hover dir', 'alliance'),
					"desc" => __('Select hover direction (only for Blog style = Portfolio and Hover style = Circle or Square)', 'alliance'),
					"override" => "category,courses_group,page",
					"std" => "left_to_right",
					"options" => $THEMEREX_GLOBALS['options_params']['list_hovers_dir'],
					"type" => "select"),
		
		"dedicated_location" => array(
					"title" => __('Dedicated location', 'alliance'),
					"desc" => __('Select location for the dedicated content or featured image in the "excerpt" blog style', 'alliance'),
					"override" => "category,courses_group,page,post",
					"std" => "default",
					"options" => $THEMEREX_GLOBALS['options_params']['list_locations'],
					"type" => "select"),
		
		"show_filters" => array(
					"title" => __('Show filters', 'alliance'),
					"desc" => __('Show filter buttons (only for Blog style = Portfolio, Masonry, Classic)', 'alliance'),
					"override" => "category,courses_group,page",
					"std" => "hide",
					"options" => $THEMEREX_GLOBALS['options_params']['list_filters'],
					"type" => "checklist"),
		
		"blog_sort" => array(
					"title" => __('Blog posts sorted by', 'alliance'),
					"desc" => __('Select the desired sorting method for posts', 'alliance'),
					"override" => "category,courses_group,page",
					"std" => "date",
					"options" => $THEMEREX_GLOBALS['options_params']['list_sorting'],
					"dir" => "vertical",
					"type" => "radio"),
		
		"blog_order" => array(
					"title" => __('Blog posts order', 'alliance'),
					"desc" => __('Select the desired ordering method for posts', 'alliance'),
					"override" => "category,courses_group,page",
					"std" => "desc",
					"options" => $THEMEREX_GLOBALS['options_params']['list_ordering'],
					"size" => "big",
					"type" => "switch"),
		
		"posts_per_page" => array(
					"title" => __('Blog posts per page',  'alliance'),
					"desc" => __('How many posts display on blog pages for selected style. If empty or 0 - inherit system wordpress settings',  'alliance'),
					"override" => "category,courses_group,page",
					"std" => "12",
					"mask" => "?99",
					"type" => "text"),
		
		"post_excerpt_maxlength" => array(
					"title" => __('Excerpt maxlength for streampage',  'alliance'),
					"desc" => __('How many characters from post excerpt are display in blog streampage (only for Blog style = Excerpt). 0 - do not trim excerpt.',  'alliance'),
					"override" => "category,courses_group,page",
					"std" => "250",
					"mask" => "?9999",
					"type" => "text"),
		
		"post_excerpt_maxlength_masonry" => array(
					"title" => __('Excerpt maxlength for classic and masonry',  'alliance'),
					"desc" => __('How many characters from post excerpt are display in blog streampage (only for Blog style = Classic or Masonry). 0 - do not trim excerpt.',  'alliance'),
					"override" => "category,courses_group,page",
					"std" => "150",
					"mask" => "?9999",
					"type" => "text"),
		
		
		
		
		// Blog -> Single page
		//-------------------------------------------------
		
		'blog_tab_single' => array(
					"title" => __('Single page', 'alliance'),
					"icon" => "iconadmin-doc",
					"override" => "category,courses_group,post,page",
					"type" => "tab"),
		
		
		"info_blog_2" => array(
					"title" => __('Single (detail) pages parameters', 'alliance'),
					"desc" => __('Select desired parameters for single (detail) pages (you can override it in each category and single post (page))', 'alliance'),
					"override" => "category,courses_group,post,page",
					"type" => "info"),
		
		"single_style" => array(
					"title" => __('Single page style', 'alliance'),
					"desc" => __('Select desired style for single page', 'alliance'),
					"divider" => false,
					"override" => "category,courses_group,page,post",
					"std" => "single-standard",
					"options" => $THEMEREX_GLOBALS['options_params']['list_single_styles'],
					"dir" => "horizontal",
					"type" => "radio"),
		
		"show_featured_image" => array(
					"title" => __('Show featured image before post',  'alliance'),
					"desc" => __("Show featured image (if selected) before post content on single pages", 'alliance'),
					"override" => "category,courses_group,post,page",
					"std" => "yes",
					"options" => $THEMEREX_GLOBALS['options_params']['list_yes_no'],
					"type" => "switch"),
		
		"show_post_title" => array(
					"title" => __('Show post title', 'alliance'),
					"desc" => __('Show area with post title on single pages', 'alliance'),
					"override" => "category,courses_group,post,page",
					"std" => "yes",
					"options" => $THEMEREX_GLOBALS['options_params']['list_yes_no'],
					"type" => "switch"),
		
		"show_post_title_on_quotes" => array(
					"title" => __('Show post title on links, chat, quote, status', 'alliance'),
					"desc" => __('Show area with post title on single and blog pages in specific post formats: links, chat, quote, status', 'alliance'),
					"override" => "category,courses_group,page",
					"std" => "no",
					"options" => $THEMEREX_GLOBALS['options_params']['list_yes_no'],
					"type" => "switch"),
		
		"show_post_info" => array(
					"title" => __('Show post info', 'alliance'),
					"desc" => __('Show area with post info on single pages', 'alliance'),
					"override" => "category,courses_group,post,page",
					"std" => "yes",
					"options" => $THEMEREX_GLOBALS['options_params']['list_yes_no'],
					"type" => "switch"),
		
		"show_text_before_readmore" => array(
					"title" => __('Show text before "Read more" tag', 'alliance'),
					"desc" => __('Show text before "Read more" tag on single pages', 'alliance'),
					"std" => "yes",
					"options" => $THEMEREX_GLOBALS['options_params']['list_yes_no'],
					"type" => "switch"),
					
		"show_post_author" => array(
					"title" => __('Show post author details',  'alliance'),
					"desc" => __("Show post author information block on single post page", 'alliance'),
					"override" => "category,courses_group,post,page",
					"std" => "yes",
					"options" => $THEMEREX_GLOBALS['options_params']['list_yes_no'],
					"type" => "switch"),
		
		"show_post_tags" => array(
					"title" => __('Show post tags',  'alliance'),
					"desc" => __("Show tags block on single post page", 'alliance'),
					"override" => "category,courses_group,post,page",
					"std" => "yes",
					"options" => $THEMEREX_GLOBALS['options_params']['list_yes_no'],
					"type" => "switch"),
		
		"show_post_counters" => array(
					"title" => __('Show post counters',  'alliance'),
					"desc" => __("Show counters block on single post page", 'alliance'),
					"override" => "category,courses_group,post,page",
					"std" => "yes",
					"options" => $THEMEREX_GLOBALS['options_params']['list_yes_no'],
					"type" => "switch"),
		
		"show_post_related" => array(
					"title" => __('Show related posts',  'alliance'),
					"desc" => __("Show related posts block on single post page", 'alliance'),
					"override" => "category,courses_group,post",
					"std" => "yes",
					"options" => $THEMEREX_GLOBALS['options_params']['list_yes_no'],
					"type" => "switch"),

		"post_related_count" => array(
					"title" => __('Related posts number',  'alliance'),
					"desc" => __("How many related posts showed on single post page", 'alliance'),
					"override" => "category,courses_group,post,page",
					"std" => "2",
					"step" => 1,
					"min" => 2,
					"max" => 8,
					"type" => "spinner"),

		"post_related_columns" => array(
					"title" => __('Related posts columns',  'alliance'),
					"desc" => __("How many columns used to show related posts on single post page. 1 - use scrolling to show all related posts", 'alliance'),
					"override" => "category,courses_group,post,page",
					"std" => "2",
					"step" => 1,
					"min" => 1,
					"max" => 4,
					"type" => "spinner"),
		
		"post_related_sort" => array(
					"title" => __('Related posts sorted by', 'alliance'),
					"desc" => __('Select the desired sorting method for related posts', 'alliance'),
		//			"override" => "category,courses_group,page",
					"std" => "date",
					"options" => $THEMEREX_GLOBALS['options_params']['list_sorting'],
					"type" => "select"),
		
		"post_related_order" => array(
					"title" => __('Related posts order', 'alliance'),
					"desc" => __('Select the desired ordering method for related posts', 'alliance'),
		//			"override" => "category,courses_group,page",
					"std" => "desc",
					"options" => $THEMEREX_GLOBALS['options_params']['list_ordering'],
					"size" => "big",
					"type" => "switch"),
		
		"show_post_comments" => array(
					"title" => __('Show comments',  'alliance'),
					"desc" => __("Show comments block on single post page", 'alliance'),
					"override" => "category,courses_group,post,page",
					"std" => "yes",
					"options" => $THEMEREX_GLOBALS['options_params']['list_yes_no'],
					"type" => "switch"),
		
		
		
		// Blog -> Other parameters
		//-------------------------------------------------
		
		'blog_tab_general' => array(
					"title" => __('Other parameters', 'alliance'),
					"icon" => "iconadmin-newspaper",
					"override" => "category,courses_group,page",
					"type" => "tab"),
		
		"info_blog_3" => array(
					"title" => __('Other Blog parameters', 'alliance'),
					"desc" => __('Select excluded categories, substitute parameters, etc.', 'alliance'),
					"type" => "info"),
		
		"exclude_cats" => array(
					"title" => __('Exclude categories', 'alliance'),
					"desc" => __('Select categories, which posts are exclude from blog page', 'alliance'),
					"divider" => false,
					"std" => "",
					"options" => $THEMEREX_GLOBALS['options_params']['list_categories'],
					"multiple" => true,
					"style" => "list",
					"type" => "select"),
		
		"blog_pagination" => array(
					"title" => __('Blog pagination', 'alliance'),
					"desc" => __('Select type of the pagination on blog streampages', 'alliance'),
					"std" => "pages",
					"override" => "category,courses_group,page",
					"options" => array(
						'pages'    => __('Standard page numbers', 'alliance'),
						'viewmore' => __('"View more" button', 'alliance'),
						'infinite' => __('Infinite scroll', 'alliance')
					),
					"dir" => "vertical",
					"type" => "radio"),
		
		"blog_pagination_style" => array(
					"title" => __('Blog pagination style', 'alliance'),
					"desc" => __('Select pagination style for standard page numbers', 'alliance'),
					"std" => "pages",
					"override" => "category,courses_group,page",
					"options" => array(
						'pages'  => __('Page numbers list', 'alliance'),
						'slider' => __('Slider with page numbers', 'alliance')
					),
					"dir" => "vertical",
					"type" => "radio"),
		
		"blog_counters" => array(
					"title" => __('Blog counters', 'alliance'),
					"desc" => __('Select counters, displayed near the post title', 'alliance'),
					"std" => "views,likes,rating,comments",
					"override" => "category,courses_group,page",
					"options" => array(
						'views' => __('Views', 'alliance'),
						'likes' => __('Likes', 'alliance'),
						'rating' => __('Rating', 'alliance'),
						'comments' => __('Comments', 'alliance')
					),
					"dir" => "vertical",
					"multiple" => true,
					"type" => "checklist"),
		
		"close_category" => array(
					"title" => __("Post's category announce", 'alliance'),
					"desc" => __('What category display in announce block (over posts thumb) - original or nearest parental', 'alliance'),
					"std" => "parental",
					"override" => "category,courses_group,page",
					"options" => array(
						'parental' => __('Nearest parental category', 'alliance'),
						'original' => __("Original post's category", 'alliance')
					),
					"dir" => "vertical",
					"type" => "radio"),
		
		"show_date_after" => array(
					"title" => __('Show post date after', 'alliance'),
					"desc" => __('Show post date after N days (before - show post age)', 'alliance'),
					"override" => "category,courses_group,page",
					"std" => "30",
					"mask" => "?99",
					"type" => "text"),
		
		
		
		
		
		//###############################
		//#### Reviews               #### 
		//###############################
		"partition_reviews" => array(
					"title" => __('Reviews', 'alliance'),
					"icon" => "iconadmin-newspaper",
					"override" => "category,courses_group",
					"type" => "partition"),
		
		"info_reviews_1" => array(
					"title" => __('Reviews criterias', 'alliance'),
					"desc" => __('Set up list of reviews criterias. You can override it in any category.', 'alliance'),
					"override" => "category,courses_group",
					"type" => "info"),
		
		"show_reviews" => array(
					"title" => __('Show reviews block',  'alliance'),
					"desc" => __("Show reviews block on single post page and average reviews rating after post's title in stream pages", 'alliance'),
					"divider" => false,
					"override" => "category,courses_group",
					"std" => "yes",
					"options" => $THEMEREX_GLOBALS['options_params']['list_yes_no'],
					"type" => "switch"),
		
		"reviews_max_level" => array(
					"title" => __('Max reviews level',  'alliance'),
					"desc" => __("Maximum level for reviews marks", 'alliance'),
					"std" => "5",
					"options" => array(
						'5'=>__('5 stars', 'alliance'), 
						'10'=>__('10 stars', 'alliance'), 
						'100'=>__('100%', 'alliance')
					),
					"type" => "radio",
					),
		
		"reviews_style" => array(
					"title" => __('Show rating as',  'alliance'),
					"desc" => __("Show rating marks as text or as stars/progress bars.", 'alliance'),
					"std" => "stars",
					"options" => array(
						'text' => __('As text (for example: 7.5 / 10)', 'alliance'), 
						'stars' => __('As stars or bars', 'alliance'),
						'heart' => __('With heart icon', 'alliance')
					),
					"dir" => "vertical",
					"type" => "radio"),
		
		"reviews_criterias_levels" => array(
					"title" => __('Reviews Criterias Levels', 'alliance'),
					"desc" => __('Words to mark criterials levels. Just write the word and press "Enter". Also you can arrange words.', 'alliance'),
					"std" => __("bad,poor,normal,good,great", 'alliance'),
					"type" => "tags"),
		
		"reviews_first" => array(
					"title" => __('Show first reviews',  'alliance'),
					"desc" => __("What reviews will be displayed first: by author or by visitors. Also this type of reviews will display under post's title.", 'alliance'),
					"std" => "author",
					"options" => array(
						'author' => __('By author', 'alliance'),
						'users' => __('By visitors', 'alliance')
						),
					"dir" => "horizontal",
					"type" => "radio"),
		
		"reviews_second" => array(
					"title" => __('Hide second reviews',  'alliance'),
					"desc" => __("Do you want hide second reviews tab in widgets and single posts?", 'alliance'),
					"std" => "show",
					"options" => $THEMEREX_GLOBALS['options_params']['list_show_hide'],
					"size" => "medium",
					"type" => "switch"),
		
		"reviews_can_vote" => array(
					"title" => __('What visitors can vote',  'alliance'),
					"desc" => __("What visitors can vote: all or only registered", 'alliance'),
					"std" => "all",
					"options" => array(
						'all'=>__('All visitors', 'alliance'), 
						'registered'=>__('Only registered', 'alliance')
					),
					"dir" => "horizontal",
					"type" => "radio"),
		
		"reviews_criterias" => array(
					"title" => __('Reviews criterias',  'alliance'),
					"desc" => __('Add default reviews criterias.',  'alliance'),
					"override" => "category,courses_group",
					"std" => "",
					"cloneable" => true,
					"type" => "text"),

		"reviews_marks" => array(
					"std" => "",
					"type" => "hidden"),
		
		
		
		
		
		//###############################
		//#### Contact info          #### 
		//###############################
		"partition_contacts" => array(
					"title" => __('Contact info', 'alliance'),
					"icon" => "iconadmin-mail-1",
					"type" => "partition"),
		
		"info_contact_1" => array(
					"title" => __('Contact information', 'alliance'),
					"desc" => __('Company address, phones and e-mail', 'alliance'),
					"type" => "info"),
		
		"contact_email" => array(
					"title" => __('Contact form email', 'alliance'),
					"desc" => __('E-mail for send contact form and user registration data', 'alliance'),
					"divider" => false,
					"std" => "",
					"before" => array('icon'=>'iconadmin-mail-1'),
					"type" => "text"),
		
		"contact_address_1" => array(
					"title" => __('Company address (part 1)', 'alliance'),
					"desc" => __('Company country, post code and city', 'alliance'),
					"std" => "",
					"before" => array('icon'=>'iconadmin-home'),
					"type" => "text"),
		
		"contact_address_2" => array(
					"title" => __('Company address (part 2)', 'alliance'),
					"desc" => __('Street and house number', 'alliance'),
					"std" => "",
					"before" => array('icon'=>'iconadmin-home'),
					"type" => "text"),
		
		"contact_phone" => array(
					"title" => __('Phone', 'alliance'),
					"desc" => __('Phone number', 'alliance'),
					"std" => "",
					"before" => array('icon'=>'iconadmin-phone'),
					"type" => "text"),
		
		"contact_fax" => array(
					"title" => __('Fax', 'alliance'),
					"desc" => __('Fax number', 'alliance'),
					"std" => "",
					"before" => array('icon'=>'iconadmin-phone'),
					"type" => "text"),
		
		"contact_info" => array(
					"title" => __('Contacts in header', 'alliance'),
					"desc" => __('String with contact info in the site header', 'alliance'),
					"std" => "",
					"before" => array('icon'=>'iconadmin-home'),
					"type" => "text"),
		
		"info_contact_2" => array(
					"title" => __('Contact and Comments form', 'alliance'),
					"desc" => __('Maximum length of the messages in the contact form shortcode and in the comments form', 'alliance'),
					"type" => "info"),
		
		"message_maxlength_contacts" => array(
					"title" => __('Contact form message', 'alliance'),
					"desc" => __("Message's maxlength in the contact form shortcode", 'alliance'),
					"std" => "1000",
					"min" => 0,
					"max" => 10000,
					"step" => 100,
					"type" => "spinner"),
		
		"message_maxlength_comments" => array(
					"title" => __('Comments form message', 'alliance'),
					"desc" => __("Message's maxlength in the comments form", 'alliance'),
					"std" => "1000",
					"min" => 0,
					"max" => 10000,
					"step" => 100,
					"type" => "spinner"),
		
		"info_contact_3" => array(
					"title" => __('Default mail function', 'alliance'),
					"desc" => __('What function you want to use for sending mail: the built-in Wordpress or standard PHP function? Attention! Some plugins may not work with one of them and you always have the ability to switch to alternative.', 'alliance'),
					"type" => "info"),
		
		"mail_function" => array(
					"title" => __("Mail function", 'alliance'),
					"desc" => __("What function you want to use for sending mail?", 'alliance'),
					"std" => "wp_mail",
					"size" => "medium",
					"options" => array(
						'wp_mail' => __('WP mail', 'alliance'),
						'mail' => __('PHP mail', 'alliance')
					),
					"type" => "switch"),
		
		
		
		
		//###############################
		//#### Socials               #### 
		//###############################
		"partition_socials" => array(
					"title" => __('Socials', 'alliance'),
					"icon" => "iconadmin-users-1",
					"override" => "category,courses_group,page,post",
					"type" => "partition"),
		
		"info_socials_1" => array(
					"title" => __('Social networks', 'alliance'),
					"desc" => __("Social networks list for site footer and Social widget", 'alliance'),
					"type" => "info"),
		
		"social_icons" => array(
					"title" => __('Social networks',  'alliance'),
					"desc" => __('Select icon and write URL to your profile in desired social networks.',  'alliance'),
					"divider" => false,
					"std" => array(array('url'=>'', 'icon'=>'')),
					"options" => $THEMEREX_GLOBALS['options_params']['list_socials'],
					"cloneable" => true,
					"size" => "small",
					"style" => 'images',
					"type" => "socials"),
		
		"info_socials_2" => array(
					"title" => __('Share buttons', 'alliance'),
					"override" => "category,courses_group,page",
					"desc" => __("Add button's code for each social share network.<br>
					In share url you can use next macro:<br>
					<b>{url}</b> - share post (page) URL,<br>
					<b>{title}</b> - post title,<br>
					<b>{image}</b> - post image,<br>
					<b>{descr}</b> - post description (if supported)<br>
					For example:<br>
					<b>Facebook</b> share string: <em>http://www.facebook.com/sharer.php?u={link}&amp;t={title}</em><br>
					<b>Delicious</b> share string: <em>http://delicious.com/save?url={link}&amp;title={title}&amp;note={descr}</em>", 'alliance'),
					"type" => "info"),
		
		"show_share" => array(
					"title" => __('Show social share buttons',  'alliance'),
					"override" => "category,courses_group,page,post",
					"desc" => __("Show social share buttons block", 'alliance'),
					"std" => "horizontal",
					"options" => array(
						'hide'		=> __('Hide', 'alliance'),
						'vertical'	=> __('Vertical', 'alliance'),
						'horizontal'=> __('Horizontal', 'alliance')
					),
					"type" => "checklist"),

		"show_share_counters" => array(
					"title" => __('Show share counters',  'alliance'),
					"override" => "category,courses_group,page",
					"desc" => __("Show share counters after social buttons", 'alliance'),
					"std" => "yes",
					"options" => $THEMEREX_GLOBALS['options_params']['list_yes_no'],
					"type" => "switch"),

		"share_caption" => array(
					"title" => __('Share block caption',  'alliance'),
					"override" => "category,courses_group,page",
					"desc" => __('Caption for the block with social share buttons',  'alliance'),
					"std" => __('Share:', 'alliance'),
					"type" => "text"),
		
		"share_buttons" => array(
					"title" => __('Share buttons',  'alliance'),
					"desc" => __('Select icon and write share URL for desired social networks.<br><b>Important!</b> If you leave text field empty - internal theme link will be used (if present).',  'alliance'),
					"std" => array(array('url'=>'', 'icon'=>'')),
					"options" => $THEMEREX_GLOBALS['options_params']['list_socials'],
					"cloneable" => true,
					"size" => "small",
					"style" => 'images',
					"type" => "socials"),
		
		
		"info_socials_3" => array(
					"title" => __('Twitter API keys', 'alliance'),
					"desc" => __("Put to this section Twitter API 1.1 keys.<br>
					You can take them after registration your application in <strong>https://apps.twitter.com/</strong>", 'alliance'),
					"type" => "info"),
		
		"twitter_username" => array(
					"title" => __('Twitter username',  'alliance'),
					"desc" => __('Your login (username) in Twitter',  'alliance'),
					"divider" => false,
					"std" => "",
					"type" => "text"),
		
		"twitter_consumer_key" => array(
					"title" => __('Consumer Key',  'alliance'),
					"desc" => __('Twitter API Consumer key',  'alliance'),
					"divider" => false,
					"std" => "",
					"type" => "text"),
		
		"twitter_consumer_secret" => array(
					"title" => __('Consumer Secret',  'alliance'),
					"desc" => __('Twitter API Consumer secret',  'alliance'),
					"divider" => false,
					"std" => "",
					"type" => "text"),
		
		"twitter_token_key" => array(
					"title" => __('Token Key',  'alliance'),
					"desc" => __('Twitter API Token key',  'alliance'),
					"divider" => false,
					"std" => "",
					"type" => "text"),
		
		"twitter_token_secret" => array(
					"title" => __('Token Secret',  'alliance'),
					"desc" => __('Twitter API Token secret',  'alliance'),
					"divider" => false,
					"std" => "",
					"type" => "text"),
		"info_socials_4" => array(
			"title" => __('Login via Social network', 'alliance'),
			"desc" => __("Settings for the Login via Social networks", 'alliance'),
			"type" => "info"),

		"social_login" => array(
			"title" => __('Social plugin shortcode',  'alliance'),
			"desc" => __('Social plugin shortcode like [plugin_shortcode]',  'alliance'),
			"divider" => false,
			"std" => "",
			"type" => "text"),
		
		
		
		
		
		
		//###############################
		//#### Search parameters     #### 
		//###############################
		"partition_search" => array(
					"title" => __('Search', 'alliance'),
					"icon" => "iconadmin-search-1",
					"type" => "partition"),
		
		"info_search_1" => array(
					"title" => __('Search parameters', 'alliance'),
					"desc" => __('Enable/disable AJAX search and output settings for it', 'alliance'),
					"type" => "info"),
		
		"show_search" => array(
					"title" => __('Show search field', 'alliance'),
					"desc" => __('Show search field in the top area and side menus', 'alliance'),
					"divider" => false,
					"std" => "yes",
					"options" => $THEMEREX_GLOBALS['options_params']['list_yes_no'],
					"type" => "switch"),
		
		"use_ajax_search" => array(
					"title" => __('Enable AJAX search', 'alliance'),
					"desc" => __('Use incremental AJAX search for the search field in top of page', 'alliance'),
					"std" => "yes",
					"options" => $THEMEREX_GLOBALS['options_params']['list_yes_no'],
					"type" => "switch"),
		
		"ajax_search_min_length" => array(
					"title" => __('Min search string length',  'alliance'),
					"desc" => __('The minimum length of the search string',  'alliance'),
					"std" => 4,
					"min" => 3,
					"type" => "spinner"),
		
		"ajax_search_delay" => array(
					"title" => __('Delay before search (in ms)',  'alliance'),
					"desc" => __('How much time (in milliseconds, 1000 ms = 1 second) must pass after the last character before the start search',  'alliance'),
					"std" => 500,
					"min" => 300,
					"max" => 1000,
					"step" => 100,
					"type" => "spinner"),
		
		"ajax_search_types" => array(
					"title" => __('Search area', 'alliance'),
					"desc" => __('Select post types, what will be include in search results. If not selected - use all types.', 'alliance'),
					"std" => "",
					"options" => $THEMEREX_GLOBALS['options_params']['list_posts_types'],
					"multiple" => true,
					"style" => "list",
					"type" => "select"),
		
		"ajax_search_posts_count" => array(
					"title" => __('Posts number in output',  'alliance'),
					"desc" => __('Number of the posts to show in search results',  'alliance'),
					"std" => 5,
					"min" => 1,
					"max" => 10,
					"type" => "spinner"),
		
		"ajax_search_posts_image" => array(
					"title" => __("Show post's image", 'alliance'),
					"desc" => __("Show post's thumbnail in the search results", 'alliance'),
					"std" => "yes",
					"options" => $THEMEREX_GLOBALS['options_params']['list_yes_no'],
					"type" => "switch"),
		
		"ajax_search_posts_date" => array(
					"title" => __("Show post's date", 'alliance'),
					"desc" => __("Show post's publish date in the search results", 'alliance'),
					"std" => "yes",
					"options" => $THEMEREX_GLOBALS['options_params']['list_yes_no'],
					"type" => "switch"),
		
		"ajax_search_posts_author" => array(
					"title" => __("Show post's author", 'alliance'),
					"desc" => __("Show post's author in the search results", 'alliance'),
					"std" => "yes",
					"options" => $THEMEREX_GLOBALS['options_params']['list_yes_no'],
					"type" => "switch"),
		
		"ajax_search_posts_counters" => array(
					"title" => __("Show post's counters", 'alliance'),
					"desc" => __("Show post's counters (views, comments, likes) in the search results", 'alliance'),
					"std" => "yes",
					"options" => $THEMEREX_GLOBALS['options_params']['list_yes_no'],
					"type" => "switch"),
		
		
		
		
		
		//###############################
		//#### Service               #### 
		//###############################
		
		"partition_service" => array(
					"title" => __('Service', 'alliance'),
					"icon" => "iconadmin-wrench",
					"type" => "partition"),
		
		"info_service_1" => array(
					"title" => __('Theme functionality', 'alliance'),
					"desc" => __('Basic theme functionality settings', 'alliance'),
					"type" => "info"),
		
		"use_ajax_views_counter" => array(
					"title" => __('Use AJAX post views counter', 'alliance'),
					"desc" => __('Use javascript for post views count (if site work under the caching plugin) or increment views count in single page template', 'alliance'),
					"std" => "yes",
					"options" => $THEMEREX_GLOBALS['options_params']['list_yes_no'],
					"type" => "switch"),

		"admin_add_filters" => array(
					"title" => __('Additional filters in the admin panel', 'alliance'),
					"desc" => __('Show additional filters (on post formats, tags and categories) in admin panel page "Posts". <br>Attention! If you have more than 2.000-3.000 posts, enabling this option may cause slow load of the "Posts" page! If you encounter such slow down, simply open Appearance - Theme Options - Service and set "No" for this option.', 'alliance'),
					"std" => "yes",
					"options" => $THEMEREX_GLOBALS['options_params']['list_yes_no'],
					"type" => "switch"),

		"show_overriden_taxonomies" => array(
					"title" => __('Show overriden options for taxonomies', 'alliance'),
					"desc" => __('Show extra column in categories list, where changed (overriden) theme options are displayed.', 'alliance'),
					"std" => "yes",
					"options" => $THEMEREX_GLOBALS['options_params']['list_yes_no'],
					"type" => "switch"),

		"show_overriden_posts" => array(
					"title" => __('Show overriden options for posts and pages', 'alliance'),
					"desc" => __('Show extra column in posts and pages list, where changed (overriden) theme options are displayed.', 'alliance'),
					"std" => "yes",
					"options" => $THEMEREX_GLOBALS['options_params']['list_yes_no'],
					"type" => "switch"),
		
		"admin_dummy_data" => array(
					"title" => __('Enable Dummy Data Installer', 'alliance'),
					"desc" => __('Show "Install Dummy Data" in the menu "Appearance". <b>Attention!</b> When you install dummy data all content of your site will be replaced!', 'alliance'),
					"std" => "yes",
					"options" => $THEMEREX_GLOBALS['options_params']['list_yes_no'],
					"type" => "switch"),

		"admin_dummy_timeout" => array(
					"title" => __('Dummy Data Installer Timeout',  'alliance'),
					"desc" => __('Web-servers set the time limit for the execution of php-scripts. By default, this is 30 sec. Therefore, the import process will be split into parts. Upon completion of each part - the import will resume automatically! The import process will try to increase this limit to the time, specified in this field.',  'alliance'),
					"std" => 1200,
					"min" => 30,
					"max" => 1800,
					"type" => "spinner"),
		
		"admin_update_notifier" => array(
					"title" => __('Enable Update Notifier', 'alliance'),
					"desc" => __('Show update notifier in admin panel. <b>Attention!</b> When this option is enabled, the theme periodically (every few hours) will communicate with our server, to check the current version. When the connection is slow, it may slow down Dashboard.', 'alliance'),
					"std" => "no",
					"options" => $THEMEREX_GLOBALS['options_params']['list_yes_no'],
					"type" => "switch"),
		
		"admin_emailer" => array(
					"title" => __('Enable Emailer in the admin panel', 'alliance'),
					"desc" => __('Allow to use ThemeREX Emailer for mass-volume e-mail distribution and management of mailing lists in "Tools - Emailer"', 'alliance'),
					"std" => "yes",
					"options" => $THEMEREX_GLOBALS['options_params']['list_yes_no'],
					"type" => "switch"),

		"clear_shortcodes" => array(
					"title" => __('Remove line breaks around shortcodes', 'alliance'),
					"desc" => __('Do you want remove spaces and line breaks around shortcodes? <b>Be attentive!</b> This option thoroughly tested on our theme, but may affect third party plugins.', 'alliance'),
					"std" => "yes",
					"options" => $THEMEREX_GLOBALS['options_params']['list_yes_no'],
					"type" => "switch"),
		
		"debug_mode" => array(
					"title" => __('Debug mode', 'alliance'),
					"desc" => __('In debug mode we are using unpacked scripts and styles, else - using minified scripts and styles (if present). <b>Attention!</b> If you have modified the source code in the js or css files, regardless of this option will be used latest (modified) version stylesheets and scripts. You can re-create minified versions of files using on-line services (for example http://yui.2clics.net/) or utility <b>yuicompressor-x.y.z.jar</b>', 'alliance'),
					"std" => "no",
					"options" => $THEMEREX_GLOBALS['options_params']['list_yes_no'],
					"type" => "switch"),
		
		"packed_scripts" => array(
					"title" => __('Use packed css and js files', 'alliance'),
					"desc" => __('Do you want to use one packed css and one js file with most theme scripts and styles instead many separate files (for speed up page loading). This reduces the number of HTTP requests when loading pages.', 'alliance'),
					"std" => "no",
					"options" => $THEMEREX_GLOBALS['options_params']['list_yes_no'],
					"type" => "switch"),
		
		"gtm_code" => array(
					"title" => __('Google tags manager or Google analitics code',  'alliance'),
					"desc" => __('Put here Google Tags Manager (GTM) code from your account: Google analitics, remarketing, etc. This code will be placed after open body tag.',  'alliance'),
					"cols" => 80,
					"rows" => 20,
					"std" => "",
					"type" => "textarea"),
		
		"gtm_code2" => array(
					"title" => __('Google remarketing code',  'alliance'),
					"desc" => __('Put here Google Remarketing code from your account. This code will be placed before close body tag.',  'alliance'),
					"divider" => false,
					"cols" => 80,
					"rows" => 20,
					"std" => "",
					"type" => "textarea"),
					 
		'api_google' => array(
					 "title" => esc_html__('Google API Key', 'alliance'),
					 "desc" => wp_kses_data( __("Insert Google API Key for browsers into the field above to generate Google Maps", 'alliance') ),
					 "std" => "",
					 "type" => "text")
		);
	}
}
?>