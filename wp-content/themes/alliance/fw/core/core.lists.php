<?php
/**
 * ThemeREX Framework: return lists
 *
 * @package themerex
 * @since themerex 1.0
 */

// Disable direct call
if ( ! defined( 'ABSPATH' ) ) { exit; }


// Return list of the animations
if ( !function_exists( 'themerex_get_list_animations' ) ) {
	function themerex_get_list_animations($prepend_inherit=false) {
		global $THEMEREX_GLOBALS;
		if (isset($THEMEREX_GLOBALS['list_animations']))
			$list = $THEMEREX_GLOBALS['list_animations'];
		else {
			$list = array();
			$list['none']			= __('- None -',	'alliance');
			$list['bounced']		= __('Bounced',		'alliance');
			$list['flash']			= __('Flash',		'alliance');
			$list['flip']			= __('Flip',		'alliance');
			$list['pulse']			= __('Pulse',		'alliance');
			$list['rubberBand']		= __('Rubber Band',	'alliance');
			$list['shake']			= __('Shake',		'alliance');
			$list['swing']			= __('Swing',		'alliance');
			$list['tada']			= __('Tada',		'alliance');
			$list['wobble']			= __('Wobble',		'alliance');
			$THEMEREX_GLOBALS['list_animations'] = $list = apply_filters('themerex_filter_list_animations', $list);
		}
		return $prepend_inherit ? themerex_array_merge(array('inherit' => __("Inherit", 'alliance')), $list) : $list;
	}
}


// Return list of the enter animations
if ( !function_exists( 'themerex_get_list_animations_in' ) ) {
	function themerex_get_list_animations_in($prepend_inherit=false) {
		global $THEMEREX_GLOBALS;
		if (isset($THEMEREX_GLOBALS['list_animations_in']))
			$list = $THEMEREX_GLOBALS['list_animations_in'];
		else {
			$list = array();
			$list['none']			= __('- None -',	'alliance');
			$list['bounceIn']		= __('Bounce In',			'alliance');
			$list['bounceInUp']		= __('Bounce In Up',		'alliance');
			$list['bounceInDown']	= __('Bounce In Down',		'alliance');
			$list['bounceInLeft']	= __('Bounce In Left',		'alliance');
			$list['bounceInRight']	= __('Bounce In Right',		'alliance');
			$list['fadeIn']			= __('Fade In',				'alliance');
			$list['fadeInUp']		= __('Fade In Up',			'alliance');
			$list['fadeInDown']		= __('Fade In Down',		'alliance');
			$list['fadeInLeft']		= __('Fade In Left',		'alliance');
			$list['fadeInRight']	= __('Fade In Right',		'alliance');
			$list['fadeInUpBig']	= __('Fade In Up Big',		'alliance');
			$list['fadeInDownBig']	= __('Fade In Down Big',	'alliance');
			$list['fadeInLeftBig']	= __('Fade In Left Big',	'alliance');
			$list['fadeInRightBig']	= __('Fade In Right Big',	'alliance');
			$list['flipInX']		= __('Flip In X',			'alliance');
			$list['flipInY']		= __('Flip In Y',			'alliance');
			$list['lightSpeedIn']	= __('Light Speed In',		'alliance');
			$list['rotateIn']		= __('Rotate In',			'alliance');
			$list['rotateInUpLeft']		= __('Rotate In Down Left',	'alliance');
			$list['rotateInUpRight']	= __('Rotate In Up Right',	'alliance');
			$list['rotateInDownLeft']	= __('Rotate In Up Left',	'alliance');
			$list['rotateInDownRight']	= __('Rotate In Down Right','alliance');
			$list['rollIn']				= __('Roll In',			'alliance');
			$list['slideInUp']			= __('Slide In Up',		'alliance');
			$list['slideInDown']		= __('Slide In Down',	'alliance');
			$list['slideInLeft']		= __('Slide In Left',	'alliance');
			$list['slideInRight']		= __('Slide In Right',	'alliance');
			$list['zoomIn']				= __('Zoom In',			'alliance');
			$list['zoomInUp']			= __('Zoom In Up',		'alliance');
			$list['zoomInDown']			= __('Zoom In Down',	'alliance');
			$list['zoomInLeft']			= __('Zoom In Left',	'alliance');
			$list['zoomInRight']		= __('Zoom In Right',	'alliance');
			$THEMEREX_GLOBALS['list_animations_in'] = $list = apply_filters('themerex_filter_list_animations_in', $list);
		}
		return $prepend_inherit ? themerex_array_merge(array('inherit' => __("Inherit", 'alliance')), $list) : $list;
	}
}


// Return list of the out animations
if ( !function_exists( 'themerex_get_list_animations_out' ) ) {
	function themerex_get_list_animations_out($prepend_inherit=false) {
		global $THEMEREX_GLOBALS;
		if (isset($THEMEREX_GLOBALS['list_animations_out']))
			$list = $THEMEREX_GLOBALS['list_animations_out'];
		else {
			$list = array();
			$list['none']			= __('- None -',	'alliance');
			$list['bounceOut']		= __('Bounce Out',			'alliance');
			$list['bounceOutUp']	= __('Bounce Out Up',		'alliance');
			$list['bounceOutDown']	= __('Bounce Out Down',		'alliance');
			$list['bounceOutLeft']	= __('Bounce Out Left',		'alliance');
			$list['bounceOutRight']	= __('Bounce Out Right',	'alliance');
			$list['fadeOut']		= __('Fade Out',			'alliance');
			$list['fadeOutUp']		= __('Fade Out Up',			'alliance');
			$list['fadeOutDown']	= __('Fade Out Down',		'alliance');
			$list['fadeOutLeft']	= __('Fade Out Left',		'alliance');
			$list['fadeOutRight']	= __('Fade Out Right',		'alliance');
			$list['fadeOutUpBig']	= __('Fade Out Up Big',		'alliance');
			$list['fadeOutDownBig']	= __('Fade Out Down Big',	'alliance');
			$list['fadeOutLeftBig']	= __('Fade Out Left Big',	'alliance');
			$list['fadeOutRightBig']= __('Fade Out Right Big',	'alliance');
			$list['flipOutX']		= __('Flip Out X',			'alliance');
			$list['flipOutY']		= __('Flip Out Y',			'alliance');
			$list['hinge']			= __('Hinge Out',			'alliance');
			$list['lightSpeedOut']	= __('Light Speed Out',		'alliance');
			$list['rotateOut']		= __('Rotate Out',			'alliance');
			$list['rotateOutUpLeft']	= __('Rotate Out Down Left',	'alliance');
			$list['rotateOutUpRight']	= __('Rotate Out Up Right',		'alliance');
			$list['rotateOutDownLeft']	= __('Rotate Out Up Left',		'alliance');
			$list['rotateOutDownRight']	= __('Rotate Out Down Right',	'alliance');
			$list['rollOut']			= __('Roll Out',		'alliance');
			$list['slideOutUp']			= __('Slide Out Up',		'alliance');
			$list['slideOutDown']		= __('Slide Out Down',	'alliance');
			$list['slideOutLeft']		= __('Slide Out Left',	'alliance');
			$list['slideOutRight']		= __('Slide Out Right',	'alliance');
			$list['zoomOut']			= __('Zoom Out',			'alliance');
			$list['zoomOutUp']			= __('Zoom Out Up',		'alliance');
			$list['zoomOutDown']		= __('Zoom Out Down',	'alliance');
			$list['zoomOutLeft']		= __('Zoom Out Left',	'alliance');
			$list['zoomOutRight']		= __('Zoom Out Right',	'alliance');
			$THEMEREX_GLOBALS['list_animations_out'] = $list = apply_filters('themerex_filter_list_animations_out', $list);
		}
		return $prepend_inherit ? themerex_array_merge(array('inherit' => __("Inherit", 'alliance')), $list) : $list;
	}
}


// Return list of categories
if ( !function_exists( 'themerex_get_list_categories' ) ) {
	function themerex_get_list_categories($prepend_inherit=false) {
		global $THEMEREX_GLOBALS;
		if (isset($THEMEREX_GLOBALS['list_categories']))
			$list = $THEMEREX_GLOBALS['list_categories'];
		else {
			$list = array();
			$args = array(
				'type'                     => 'post',
				'child_of'                 => 0,
				'parent'                   => '',
				'orderby'                  => 'name',
				'order'                    => 'ASC',
				'hide_empty'               => 0,
				'hierarchical'             => 1,
				'exclude'                  => '',
				'include'                  => '',
				'number'                   => '',
				'taxonomy'                 => 'category',
				'pad_counts'               => false );
			$taxonomies = get_categories( $args );
			foreach ($taxonomies as $cat) {
				$list[$cat->term_id] = $cat->name;
			}
			$THEMEREX_GLOBALS['list_categories'] = $list;
		}
		return $prepend_inherit ? themerex_array_merge(array('inherit' => __("Inherit", 'alliance')), $list) : $list;
	}
}


// Return list of taxonomies
if ( !function_exists( 'themerex_get_list_terms' ) ) {
	function themerex_get_list_terms($prepend_inherit=false, $taxonomy='category') {
		global $THEMEREX_GLOBALS;
		if (isset($THEMEREX_GLOBALS['list_taxonomies_'.($taxonomy)]))
			$list = $THEMEREX_GLOBALS['list_taxonomies_'.($taxonomy)];
		else {
			$list = array();
			$args = array(
				'child_of'                 => 0,
				'parent'                   => '',
				'orderby'                  => 'name',
				'order'                    => 'ASC',
				'hide_empty'               => 0,
				'hierarchical'             => 1,
				'exclude'                  => '',
				'include'                  => '',
				'number'                   => '',
				'taxonomy'                 => $taxonomy,
				'pad_counts'               => false );
			$taxonomies = get_terms( $taxonomy, $args );
			foreach ($taxonomies as $cat) {
				if(!empty($cat->term_id) && !empty($cat->name))
					$list[$cat->term_id] = $cat->name;	// . ($taxonomy!='category' ? ' /'.($cat->taxonomy).'/' : '');
			}
			$THEMEREX_GLOBALS['list_taxonomies_'.($taxonomy)] = $list;
		}
		return $prepend_inherit ? themerex_array_merge(array('inherit' => __("Inherit", 'alliance')), $list) : $list;
	}
}

// Return list of post's types
if ( !function_exists( 'themerex_get_list_posts_types' ) ) {
	function themerex_get_list_posts_types($prepend_inherit=false) {
		global $THEMEREX_GLOBALS;
		if (isset($THEMEREX_GLOBALS['list_posts_types']))
			$list = $THEMEREX_GLOBALS['list_posts_types'];
		else {
			$list = array();
	
			// Return only theme inheritance supported post types
			$THEMEREX_GLOBALS['list_posts_types'] = $list = apply_filters('themerex_filter_list_post_types', $list);
		}
		return $prepend_inherit ? themerex_array_merge(array('inherit' => __("Inherit", 'alliance')), $list) : $list;
	}
}


// Return list post items from any post type and taxonomy
if ( !function_exists( 'themerex_get_list_posts' ) ) {
	function themerex_get_list_posts($prepend_inherit=false, $opt=array()) {
		$opt = array_merge(array(
			'post_type'			=> 'post',
			'post_status'		=> 'publish',
			'taxonomy'			=> 'category',
			'taxonomy_value'	=> '',
			'posts_per_page'	=> -1,
			'orderby'			=> 'post_date',
			'order'				=> 'desc',
			'return'			=> 'id'
			), is_array($opt) ? $opt : array('post_type'=>$opt));

		global $THEMEREX_GLOBALS;
		$hash = 'list_posts_'.($opt['post_type']).'_'.($opt['taxonomy']).'_'.($opt['taxonomy_value']).'_'.($opt['orderby']).'_'.($opt['order']).'_'.($opt['return']).'_'.($opt['posts_per_page']);
		if (isset($THEMEREX_GLOBALS[$hash]))
			$list = $THEMEREX_GLOBALS[$hash];
		else {
			$list = array();
			$list['none'] = __("- Not selected -", 'alliance');
			$args = array(
				'post_type' => $opt['post_type'],
				'post_status' => $opt['post_status'],
				'posts_per_page' => $opt['posts_per_page'],
				'ignore_sticky_posts' => true,
				'orderby'	=> $opt['orderby'],
				'order'		=> $opt['order']
			);
			if (!empty($opt['taxonomy_value'])) {
				$args['tax_query'] = array(
					array(
						'taxonomy' => $opt['taxonomy'],
						'field' => (int) $opt['taxonomy_value'] > 0 ? 'id' : 'slug',
						'terms' => $opt['taxonomy_value']
					)
				);
			}
			$posts = get_posts( $args );
			foreach ($posts as $post) {
				$list[$opt['return']=='id' ? $post->ID : $post->post_title] = $post->post_title;
			}
			$THEMEREX_GLOBALS[$hash] = $list;
		}
		return $prepend_inherit ? themerex_array_merge(array('inherit' => __("Inherit", 'alliance')), $list) : $list;
	}
}


// Return list of registered users
if ( !function_exists( 'themerex_get_list_users' ) ) {
	function themerex_get_list_users($prepend_inherit=false, $roles=array('administrator', 'editor', 'author', 'contributor', 'shop_manager')) {
		global $THEMEREX_GLOBALS;
		if (isset($THEMEREX_GLOBALS['list_users']))
			$list = $THEMEREX_GLOBALS['list_users'];
		else {
			$list = array();
			$list['none'] = __("- Not selected -", 'alliance');
			$args = array(
				'orderby'	=> 'display_name',
				'order'		=> 'ASC' );
			$users = get_users( $args );
			foreach ($users as $user) {
				$accept = true;
				if (is_array($user->roles)) {
					if (count($user->roles) > 0) {
						$accept = false;
						foreach ($user->roles as $role) {
							if (in_array($role, $roles)) {
								$accept = true;
								break;
							}
						}
					}
				}
				if ($accept) $list[$user->user_login] = $user->display_name;
			}
			$THEMEREX_GLOBALS['list_users'] = $list;
		}
		return $prepend_inherit ? themerex_array_merge(array('inherit' => __("Inherit", 'alliance')), $list) : $list;
	}
}


// Return sliders list, prepended inherit and main sidebars item (if need)
if ( !function_exists( 'themerex_get_list_sliders' ) ) {
	function themerex_get_list_sliders($prepend_inherit=false) {
		global $THEMEREX_GLOBALS;
		if (isset($THEMEREX_GLOBALS['list_sliders']))
			$list = $THEMEREX_GLOBALS['list_sliders'];
		else {
			$list = array();
			$list["swiper"] = __("Posts slider (Swiper)", 'alliance');
			if (themerex_exists_revslider())
				$list["revo"] = __("Layer slider (Revolution)", 'alliance');
			if (themerex_exists_royalslider())
				$list["royal"] = __("Layer slider (Royal)", 'alliance');
			$THEMEREX_GLOBALS['list_sliders'] = $list = apply_filters('themerex_filter_list_sliders', $list);
		}
		return $prepend_inherit ? themerex_array_merge(array('inherit' => __("Inherit", 'alliance')), $list) : $list;
	}
}

// Return list with popup engines
if ( !function_exists( 'themerex_get_list_popup_engines' ) ) {
	function themerex_get_list_popup_engines($prepend_inherit=false) {
		global $THEMEREX_GLOBALS;
		if (isset($THEMEREX_GLOBALS['list_popup_engines']))
			$list = $THEMEREX_GLOBALS['list_popup_engines'];
		else {
			$list = array();
			$list["pretty"] = __("Pretty photo", 'alliance');
			$list["magnific"] = __("Magnific popup", 'alliance');
			$THEMEREX_GLOBALS['list_popup_engines'] = $list = apply_filters('themerex_filter_list_popup_engines', $list);
		}
		return $prepend_inherit ? themerex_array_merge(array('inherit' => __("Inherit", 'alliance')), $list) : $list;
	}
}

// Return menus list, prepended inherit
if ( !function_exists( 'themerex_get_list_menus' ) ) {
	function themerex_get_list_menus($prepend_inherit=false) {
		global $THEMEREX_GLOBALS;
		if (isset($THEMEREX_GLOBALS['list_menus']))
			$list = $THEMEREX_GLOBALS['list_menus'];
		else {
			$list = array();
			$list['default'] = __("Default", 'alliance');
			$menus = wp_get_nav_menus();
			if ($menus) {
				foreach ($menus as $menu) {
					$list[$menu->slug] = $menu->name;
				}
			}
			$THEMEREX_GLOBALS['list_menus'] = $list;
		}
		return $prepend_inherit ? themerex_array_merge(array('inherit' => __("Inherit", 'alliance')), $list) : $list;
	}
}

// Return custom sidebars list, prepended inherit and main sidebars item (if need)
if ( !function_exists( 'themerex_get_list_sidebars' ) ) {
	function themerex_get_list_sidebars($prepend_inherit=false) {
		global $THEMEREX_GLOBALS;
		if (isset($THEMEREX_GLOBALS['list_sidebars'])) {
			$list = $THEMEREX_GLOBALS['list_sidebars'];
		} else {
			$list = isset($THEMEREX_GLOBALS['registered_sidebars']) ? $THEMEREX_GLOBALS['registered_sidebars'] : array();
			$THEMEREX_GLOBALS['list_sidebars'] = $list;
		}
		return $prepend_inherit ? themerex_array_merge(array('inherit' => __("Inherit", 'alliance')), $list) : $list;
	}
}

// Return sidebars positions
if ( !function_exists( 'themerex_get_list_sidebars_positions' ) ) {
	function themerex_get_list_sidebars_positions($prepend_inherit=false) {
		global $THEMEREX_GLOBALS;
		if (isset($THEMEREX_GLOBALS['list_sidebars_positions']))
			$list = $THEMEREX_GLOBALS['list_sidebars_positions'];
		else {
			$list = array();
			$list['left']  = __('Left',  'alliance');
			$list['right'] = __('Right', 'alliance');
			$THEMEREX_GLOBALS['list_sidebars_positions'] = $list;
		}
		return $prepend_inherit ? themerex_array_merge(array('inherit' => __("Inherit", 'alliance')), $list) : $list;
	}
}

// Return sidebars class
if ( !function_exists( 'themerex_get_sidebar_class' ) ) {
	function themerex_get_sidebar_class($style, $pos) {
		return themerex_sc_param_is_off($style) ? 'sidebar_hide' : 'sidebar_show sidebar_'.($pos);
	}
}

// Return body styles list, prepended inherit
if ( !function_exists( 'themerex_get_list_body_styles' ) ) {
	function themerex_get_list_body_styles($prepend_inherit=false) {
		global $THEMEREX_GLOBALS;
		if (isset($THEMEREX_GLOBALS['list_body_styles']))
			$list = $THEMEREX_GLOBALS['list_body_styles'];
		else {
			$list = array();
			$list['wide']		= __('Wide',		'alliance');
			$list['fullwide']	= __('Fullwide',	'alliance');
			$THEMEREX_GLOBALS['list_body_styles'] = $list = apply_filters('themerex_filter_list_body_styles', $list);
		}
		return $prepend_inherit ? themerex_array_merge(array('inherit' => __("Inherit", 'alliance')), $list) : $list;
	}
}

// Return skins list, prepended inherit
if ( !function_exists( 'themerex_get_list_skins' ) ) {
	function themerex_get_list_skins($prepend_inherit=false) {
		$list = array(
			'alliance' => esc_html__('Alliance', 'alliance')
		);
		return $prepend_inherit ? themerex_array_merge(array('inherit' => __("Inherit", 'alliance')), $list) : $list;
	}
}

// Return templates list, prepended inherit
if ( !function_exists( 'themerex_get_list_templates' ) ) {
	function themerex_get_list_templates($mode='') {
		global $THEMEREX_GLOBALS;
		if (isset($THEMEREX_GLOBALS['list_templates_'.($mode)]))
			$list = $THEMEREX_GLOBALS['list_templates_'.($mode)];
		else {
			$list = array();
			foreach ($THEMEREX_GLOBALS['registered_templates'] as $k=>$v) {
				if ($mode=='' || themerex_strpos($v['mode'], $mode)!==false)
					$list[$k] = !empty($v['title']) ? $v['title'] : themerex_strtoproper($v['layout']);
			}
			$THEMEREX_GLOBALS['list_templates_'.($mode)] = $list;
		}
		return $list;
	}
}

// Return blog styles list, prepended inherit
if ( !function_exists( 'themerex_get_list_templates_blog' ) ) {
	function themerex_get_list_templates_blog($prepend_inherit=false) {
		global $THEMEREX_GLOBALS;
		if (isset($THEMEREX_GLOBALS['list_templates_blog']))
			$list = $THEMEREX_GLOBALS['list_templates_blog'];
		else {
			$list = themerex_get_list_templates('blog');
			$THEMEREX_GLOBALS['list_templates_blog'] = $list;
		}
		return $prepend_inherit ? themerex_array_merge(array('inherit' => __("Inherit", 'alliance')), $list) : $list;
	}
}

// Return blogger styles list, prepended inherit
if ( !function_exists( 'themerex_get_list_templates_blogger' ) ) {
	function themerex_get_list_templates_blogger($prepend_inherit=false) {
		global $THEMEREX_GLOBALS;
		if (isset($THEMEREX_GLOBALS['list_templates_blogger']))
			$list = $THEMEREX_GLOBALS['list_templates_blogger'];
		else {
			$list = themerex_array_merge(themerex_get_list_templates('blogger'), themerex_get_list_templates('blog'));
			$THEMEREX_GLOBALS['list_templates_blogger'] = $list;
		}
		return $prepend_inherit ? themerex_array_merge(array('inherit' => __("Inherit", 'alliance')), $list) : $list;
	}
}

// Return single page styles list, prepended inherit
if ( !function_exists( 'themerex_get_list_templates_single' ) ) {
	function themerex_get_list_templates_single($prepend_inherit=false) {
		global $THEMEREX_GLOBALS;
		if (isset($THEMEREX_GLOBALS['list_templates_single']))
			$list = $THEMEREX_GLOBALS['list_templates_single'];
		else {
			$list = themerex_get_list_templates('single');
			$THEMEREX_GLOBALS['list_templates_single'] = $list;
		}
		return $prepend_inherit ? themerex_array_merge(array('inherit' => __("Inherit", 'alliance')), $list) : $list;
	}
}

// Return article styles list, prepended inherit
if ( !function_exists( 'themerex_get_list_article_styles' ) ) {
	function themerex_get_list_article_styles($prepend_inherit=false) {
		global $THEMEREX_GLOBALS;
		if (isset($THEMEREX_GLOBALS['list_article_styles']))
			$list = $THEMEREX_GLOBALS['list_article_styles'];
		else {
			$list = array();
			$list["boxed"]   = __('Boxed', 'alliance');
			$list["stretch"] = __('Stretch', 'alliance');
			$THEMEREX_GLOBALS['list_article_styles'] = $list;
		}
		return $prepend_inherit ? themerex_array_merge(array('inherit' => __("Inherit", 'alliance')), $list) : $list;
	}
}

// Return color schemes list, prepended inherit
if ( !function_exists( 'themerex_get_list_color_schemes' ) ) {
	function themerex_get_list_color_schemes($prepend_inherit=false) {
		global $THEMEREX_GLOBALS;
		if (isset($THEMEREX_GLOBALS['list_color_schemes']))
			$list = $THEMEREX_GLOBALS['list_color_schemes'];
		else {
			$list = array();
			if (!empty($THEMEREX_GLOBALS['color_schemes'])) {
				foreach ($THEMEREX_GLOBALS['color_schemes'] as $k=>$v) {
					$list[$k] = $v['title'];
				}
			}
			$THEMEREX_GLOBALS['list_color_schemes'] = $list;
		}
		return $prepend_inherit ? themerex_array_merge(array('inherit' => __("Inherit", 'alliance')), $list) : $list;
	}
}

// Return button styles list, prepended inherit
if ( !function_exists( 'themerex_get_list_button_styles' ) ) {
	function themerex_get_list_button_styles($prepend_inherit=false) {
		global $THEMEREX_GLOBALS;
		if (isset($THEMEREX_GLOBALS['list_button_styles']))
			$list = $THEMEREX_GLOBALS['list_button_styles'];
		else {
			$list = array();
			$list["custom"]	= __('Custom', 'alliance');
			$list["link"] 	= __('As links', 'alliance');
			$list["menu"] 	= __('As main menu', 'alliance');
			$list["user"] 	= __('As user menu', 'alliance');
			$THEMEREX_GLOBALS['list_button_styles'] = $list;
		}
		return $prepend_inherit ? themerex_array_merge(array('inherit' => __("Inherit", 'alliance')), $list) : $list;
	}
}

// Return post-formats filters list, prepended inherit
if ( !function_exists( 'themerex_get_list_post_formats_filters' ) ) {
	function themerex_get_list_post_formats_filters($prepend_inherit=false) {
		global $THEMEREX_GLOBALS;
		if (isset($THEMEREX_GLOBALS['list_post_formats_filters']))
			$list = $THEMEREX_GLOBALS['list_post_formats_filters'];
		else {
			$list = array();
			$list["no"]      = __('All posts', 'alliance');
			$list["thumbs"]  = __('With thumbs', 'alliance');
			$list["reviews"] = __('With reviews', 'alliance');
			$list["video"]   = __('With videos', 'alliance');
			$list["audio"]   = __('With audios', 'alliance');
			$list["gallery"] = __('With galleries', 'alliance');
			$THEMEREX_GLOBALS['list_post_formats_filters'] = $list;
		}
		return $prepend_inherit ? themerex_array_merge(array('inherit' => __("Inherit", 'alliance')), $list) : $list;
	}
}

// Return scheme color
if (!function_exists('themerex_get_scheme_color')) {
	function themerex_get_scheme_color($clr) {
		global $THEMEREX_GLOBALS;
		$scheme = themerex_get_custom_option('color_scheme');
		if (empty($scheme) || empty($THEMEREX_GLOBALS['color_schemes'][$scheme])) $scheme = 'original';
		return isset($THEMEREX_GLOBALS['color_schemes'][$scheme][$clr]) ? $THEMEREX_GLOBALS['color_schemes'][$scheme][$clr] : '';
	}
}

// Return portfolio filters list, prepended inherit
if ( !function_exists( 'themerex_get_list_portfolio_filters' ) ) {
	function themerex_get_list_portfolio_filters($prepend_inherit=false) {
		global $THEMEREX_GLOBALS;
		if (isset($THEMEREX_GLOBALS['list_portfolio_filters']))
			$list = $THEMEREX_GLOBALS['list_portfolio_filters'];
		else {
			$list = array();
			$list["hide"] = __('Hide', 'alliance');
			$list["tags"] = __('Tags', 'alliance');
			$list["categories"] = __('Categories', 'alliance');
			$THEMEREX_GLOBALS['list_portfolio_filters'] = $list;
		}
		return $prepend_inherit ? themerex_array_merge(array('inherit' => __("Inherit", 'alliance')), $list) : $list;
	}
}

// Return hover styles list, prepended inherit
if ( !function_exists( 'themerex_get_list_hovers' ) ) {
	function themerex_get_list_hovers($prepend_inherit=false) {
		global $THEMEREX_GLOBALS;
		if (isset($THEMEREX_GLOBALS['list_hovers']))
			$list = $THEMEREX_GLOBALS['list_hovers'];
		else {
			$list = array();
			$list['circle effect1']  = __('Circle Effect 1',  'alliance');
			$list['circle effect2']  = __('Circle Effect 2',  'alliance');
			$list['circle effect3']  = __('Circle Effect 3',  'alliance');
			$list['circle effect4']  = __('Circle Effect 4',  'alliance');
			$list['circle effect5']  = __('Circle Effect 5',  'alliance');
			$list['circle effect6']  = __('Circle Effect 6',  'alliance');
			$list['circle effect7']  = __('Circle Effect 7',  'alliance');
			$list['circle effect8']  = __('Circle Effect 8',  'alliance');
			$list['circle effect9']  = __('Circle Effect 9',  'alliance');
			$list['circle effect10'] = __('Circle Effect 10',  'alliance');
			$list['circle effect11'] = __('Circle Effect 11',  'alliance');
			$list['circle effect12'] = __('Circle Effect 12',  'alliance');
			$list['circle effect13'] = __('Circle Effect 13',  'alliance');
			$list['circle effect14'] = __('Circle Effect 14',  'alliance');
			$list['circle effect15'] = __('Circle Effect 15',  'alliance');
			$list['circle effect16'] = __('Circle Effect 16',  'alliance');
			$list['circle effect17'] = __('Circle Effect 17',  'alliance');
			$list['circle effect18'] = __('Circle Effect 18',  'alliance');
			$list['circle effect19'] = __('Circle Effect 19',  'alliance');
			$list['circle effect20'] = __('Circle Effect 20',  'alliance');
			$list['square effect1']  = __('Square Effect 1',  'alliance');
			$list['square effect2']  = __('Square Effect 2',  'alliance');
			$list['square effect3']  = __('Square Effect 3',  'alliance');
	//		$list['square effect4']  = __('Square Effect 4',  'alliance');
			$list['square effect5']  = __('Square Effect 5',  'alliance');
			$list['square effect6']  = __('Square Effect 6',  'alliance');
			$list['square effect7']  = __('Square Effect 7',  'alliance');
			$list['square effect8']  = __('Square Effect 8',  'alliance');
			$list['square effect9']  = __('Square Effect 9',  'alliance');
			$list['square effect10'] = __('Square Effect 10',  'alliance');
			$list['square effect11'] = __('Square Effect 11',  'alliance');
			$list['square effect12'] = __('Square Effect 12',  'alliance');
			$list['square effect13'] = __('Square Effect 13',  'alliance');
			$list['square effect14'] = __('Square Effect 14',  'alliance');
			$list['square effect15'] = __('Square Effect 15',  'alliance');
			$list['square effect_dir']   = __('Square Effect Dir',   'alliance');
			$list['square effect_shift'] = __('Square Effect Shift', 'alliance');
			$list['square effect_book']  = __('Square Effect Book',  'alliance');
			$THEMEREX_GLOBALS['list_hovers'] = $list = apply_filters('themerex_filter_portfolio_hovers', $list);
		}
		return $prepend_inherit ? themerex_array_merge(array('inherit' => __("Inherit", 'alliance')), $list) : $list;
	}
}

// Return extended hover directions list, prepended inherit
if ( !function_exists( 'themerex_get_list_hovers_directions' ) ) {
	function themerex_get_list_hovers_directions($prepend_inherit=false) {
		global $THEMEREX_GLOBALS;
		if (isset($THEMEREX_GLOBALS['list_hovers_directions']))
			$list = $THEMEREX_GLOBALS['list_hovers_directions'];
		else {
			$list = array();
			$list['left_to_right'] = __('Left to Right',  'alliance');
			$list['right_to_left'] = __('Right to Left',  'alliance');
			$list['top_to_bottom'] = __('Top to Bottom',  'alliance');
			$list['bottom_to_top'] = __('Bottom to Top',  'alliance');
			$list['scale_up']      = __('Scale Up',  'alliance');
			$list['scale_down']    = __('Scale Down',  'alliance');
			$list['scale_down_up'] = __('Scale Down-Up',  'alliance');
			$list['from_left_and_right'] = __('From Left and Right',  'alliance');
			$list['from_top_and_bottom'] = __('From Top and Bottom',  'alliance');
			$THEMEREX_GLOBALS['list_hovers_directions'] = $list = apply_filters('themerex_filter_portfolio_hovers_directions', $list);
		}
		return $prepend_inherit ? themerex_array_merge(array('inherit' => __("Inherit", 'alliance')), $list) : $list;
	}
}


// Return list of the label positions in the custom forms
if ( !function_exists( 'themerex_get_list_label_positions' ) ) {
	function themerex_get_list_label_positions($prepend_inherit=false) {
		global $THEMEREX_GLOBALS;
		if (isset($THEMEREX_GLOBALS['list_label_positions']))
			$list = $THEMEREX_GLOBALS['list_label_positions'];
		else {
			$list = array();
			$list['top']	= __('Top',		'alliance');
			$list['bottom']	= __('Bottom',		'alliance');
			$list['left']	= __('Left',		'alliance');
			$list['over']	= __('Over',		'alliance');
			$THEMEREX_GLOBALS['list_label_positions'] = $list = apply_filters('themerex_filter_label_positions', $list);
		}
		return $prepend_inherit ? themerex_array_merge(array('inherit' => __("Inherit", 'alliance')), $list) : $list;
	}
}

// Return background tints list, prepended inherit
if ( !function_exists( 'themerex_get_list_bg_tints' ) ) {
	function themerex_get_list_bg_tints($prepend_inherit=false) {
		global $THEMEREX_GLOBALS;
		if (isset($THEMEREX_GLOBALS['list_bg_tints']))
			$list = $THEMEREX_GLOBALS['list_bg_tints'];
		else {
			$list = array();
			$list['none']  = __('None',  'alliance');
			$list['dark']  = __('Show',  'alliance');
			$THEMEREX_GLOBALS['list_bg_tints'] = $list = apply_filters('themerex_filter_bg_tints', $list);
		}
		return $prepend_inherit ? themerex_array_merge(array('inherit' => __("Inherit", 'alliance')), $list) : $list;
	}
}

// Return background tints list for sidebars, prepended inherit
if ( !function_exists( 'themerex_get_list_sidebar_styles' ) ) {
	function themerex_get_list_sidebar_styles($prepend_inherit=false) {
		global $THEMEREX_GLOBALS;
		if (isset($THEMEREX_GLOBALS['list_sidebar_styles']))
			$list = $THEMEREX_GLOBALS['list_sidebar_styles'];
		else {
			$list = array();
			$list['none']  = __('None',  'alliance');
			$list['dark']  = __('Show',  'alliance');
			$THEMEREX_GLOBALS['list_sidebar_styles'] = $list = apply_filters('themerex_filter_sidebar_styles', $list);
		}
		return $prepend_inherit ? themerex_array_merge(array('inherit' => __("Inherit", 'alliance')), $list) : $list;
	}
}

// Return custom fields types list, prepended inherit
if ( !function_exists( 'themerex_get_list_field_types' ) ) {
	function themerex_get_list_field_types($prepend_inherit=false) {
		global $THEMEREX_GLOBALS;
		if (isset($THEMEREX_GLOBALS['list_field_types']))
			$list = $THEMEREX_GLOBALS['list_field_types'];
		else {
			$list = array();
			$list['text']     = __('Text',  'alliance');
			$list['textarea'] = __('Text Area','alliance');
			$list['password'] = __('Password',  'alliance');
			$list['radio']    = __('Radio',  'alliance');
			$list['checkbox'] = __('Checkbox',  'alliance');
			$list['button']   = __('Button','alliance');
			$THEMEREX_GLOBALS['list_field_types'] = $list = apply_filters('themerex_filter_field_types', $list);
		}
		return $prepend_inherit ? themerex_array_merge(array('inherit' => __("Inherit", 'alliance')), $list) : $list;
	}
}

// Return Google map styles
if ( !function_exists( 'themerex_get_list_googlemap_styles' ) ) {
	function themerex_get_list_googlemap_styles($prepend_inherit=false) {
		global $THEMEREX_GLOBALS;
		if (isset($THEMEREX_GLOBALS['list_googlemap_styles']))
			$list = $THEMEREX_GLOBALS['list_googlemap_styles'];
		else {
			$list = array();
			$list['default'] = __('Default', 'alliance');
			$list['simple'] = __('Simple', 'alliance');
			$list['greyscale'] = __('Greyscale', 'alliance');
			$list['greyscale2'] = __('Greyscale 2', 'alliance');
			$list['invert'] = __('Invert', 'alliance');
			$list['dark'] = __('Dark', 'alliance');
			$list['style1'] = __('Custom style 1', 'alliance');
			$list['style2'] = __('Custom style 2', 'alliance');
			$list['style3'] = __('Custom style 3', 'alliance');
			$list['style4'] = __('Custom style 4', 'alliance');
			$THEMEREX_GLOBALS['list_googlemap_styles'] = $list = apply_filters('themerex_filter_googlemap_styles', $list);
		}
		return $prepend_inherit ? themerex_array_merge(array('inherit' => __("Inherit", 'alliance')), $list) : $list;
	}
}

// Return iconed classes list
if ( !function_exists( 'themerex_get_list_icons' ) ) {
	function themerex_get_list_icons($prepend_inherit=false) {
		global $THEMEREX_GLOBALS;
		if (isset($THEMEREX_GLOBALS['list_icons']))
			$list = $THEMEREX_GLOBALS['list_icons'];
		else
			$THEMEREX_GLOBALS['list_icons'] = $list = themerex_parse_icons_classes(themerex_get_file_dir("css/fontello/css/fontello-codes.css"));
		return $prepend_inherit ? themerex_array_merge(array('inherit' => __("Inherit", 'alliance')), $list) : $list;
	}
}

// Return socials list
if ( !function_exists( 'themerex_get_list_socials' ) ) {
	function themerex_get_list_socials($prepend_inherit=false) {
		global $THEMEREX_GLOBALS;
		if (isset($THEMEREX_GLOBALS['list_socials']))
			$list = $THEMEREX_GLOBALS['list_socials'];
		else
			$THEMEREX_GLOBALS['list_socials'] = $list = themerex_get_list_files("images/socials", "png");
		return $prepend_inherit ? themerex_array_merge(array('inherit' => __("Inherit", 'alliance')), $list) : $list;
	}
}

// Return flags list
if ( !function_exists( 'themerex_get_list_flags' ) ) {
	function themerex_get_list_flags($prepend_inherit=false) {
		global $THEMEREX_GLOBALS;
		if (isset($THEMEREX_GLOBALS['list_flags']))
			$list = $THEMEREX_GLOBALS['list_flags'];
		else
			$THEMEREX_GLOBALS['list_flags'] = $list = themerex_get_list_files("images/flags", "png");
		return $prepend_inherit ? themerex_array_merge(array('inherit' => __("Inherit", 'alliance')), $list) : $list;
	}
}

// Return list with 'Yes' and 'No' items
if ( !function_exists( 'themerex_get_list_yesno' ) ) {
	function themerex_get_list_yesno($prepend_inherit=false) {
		global $THEMEREX_GLOBALS;
		if (isset($THEMEREX_GLOBALS['list_yesno']))
			$list = $THEMEREX_GLOBALS['list_yesno'];
		else {
			$list = array();
			$list["yes"] = __("Yes", 'alliance');
			$list["no"]  = __("No", 'alliance');
			$THEMEREX_GLOBALS['list_yesno'] = $list;
		}
		return $prepend_inherit ? themerex_array_merge(array('inherit' => __("Inherit", 'alliance')), $list) : $list;
	}
}

// Return list with 'On' and 'Of' items
if ( !function_exists( 'themerex_get_list_onoff' ) ) {
	function themerex_get_list_onoff($prepend_inherit=false) {
		global $THEMEREX_GLOBALS;
		if (isset($THEMEREX_GLOBALS['list_onoff']))
			$list = $THEMEREX_GLOBALS['list_onoff'];
		else {
			$list = array();
			$list["on"] = __("On", 'alliance');
			$list["off"] = __("Off", 'alliance');
			$THEMEREX_GLOBALS['list_onoff'] = $list;
		}
		return $prepend_inherit ? themerex_array_merge(array('inherit' => __("Inherit", 'alliance')), $list) : $list;
	}
}

// Return list with 'Show' and 'Hide' items
if ( !function_exists( 'themerex_get_list_showhide' ) ) {
	function themerex_get_list_showhide($prepend_inherit=false) {
		global $THEMEREX_GLOBALS;
		if (isset($THEMEREX_GLOBALS['list_showhide']))
			$list = $THEMEREX_GLOBALS['list_showhide'];
		else {
			$list = array();
			$list["show"] = __("Show", 'alliance');
			$list["hide"] = __("Hide", 'alliance');
			$THEMEREX_GLOBALS['list_showhide'] = $list;
		}
		return $prepend_inherit ? themerex_array_merge(array('inherit' => __("Inherit", 'alliance')), $list) : $list;
	}
}

// Return list with 'Ascending' and 'Descending' items
if ( !function_exists( 'themerex_get_list_orderings' ) ) {
	function themerex_get_list_orderings($prepend_inherit=false) {
		global $THEMEREX_GLOBALS;
		if (isset($THEMEREX_GLOBALS['list_orderings']))
			$list = $THEMEREX_GLOBALS['list_orderings'];
		else {
			$list = array();
			$list["asc"] = __("Ascending", 'alliance');
			$list["desc"] = __("Descending", 'alliance');
			$THEMEREX_GLOBALS['list_orderings'] = $list;
		}
		return $prepend_inherit ? themerex_array_merge(array('inherit' => __("Inherit", 'alliance')), $list) : $list;
	}
}

// Return list with 'Horizontal' and 'Vertical' items
if ( !function_exists( 'themerex_get_list_directions' ) ) {
	function themerex_get_list_directions($prepend_inherit=false) {
		global $THEMEREX_GLOBALS;
		if (isset($THEMEREX_GLOBALS['list_directions']))
			$list = $THEMEREX_GLOBALS['list_directions'];
		else {
			$list = array();
			$list["horizontal"] = __("Horizontal", 'alliance');
			$list["vertical"] = __("Vertical", 'alliance');
			$THEMEREX_GLOBALS['list_directions'] = $list;
		}
		return $prepend_inherit ? themerex_array_merge(array('inherit' => __("Inherit", 'alliance')), $list) : $list;
	}
}

// Return list with float items
if ( !function_exists( 'themerex_get_list_floats' ) ) {
	function themerex_get_list_floats($prepend_inherit=false) {
		global $THEMEREX_GLOBALS;
		if (isset($THEMEREX_GLOBALS['list_floats']))
			$list = $THEMEREX_GLOBALS['list_floats'];
		else {
			$list = array();
			$list["none"] = __("None", 'alliance');
			$list["left"] = __("Float Left", 'alliance');
			$list["right"] = __("Float Right", 'alliance');
			$THEMEREX_GLOBALS['list_floats'] = $list;
		}
		return $prepend_inherit ? themerex_array_merge(array('inherit' => __("Inherit", 'alliance')), $list) : $list;
	}
}

// Return list with alignment items
if ( !function_exists( 'themerex_get_list_alignments' ) ) {
	function themerex_get_list_alignments($justify=false, $prepend_inherit=false) {
		global $THEMEREX_GLOBALS;
		if (isset($THEMEREX_GLOBALS['list_alignments']))
			$list = $THEMEREX_GLOBALS['list_alignments'];
		else {
			$list = array();
			$list["none"] = __("None", 'alliance');
			$list["left"] = __("Left", 'alliance');
			$list["center"] = __("Center", 'alliance');
			$list["right"] = __("Right", 'alliance');
			if ($justify) $list["justify"] = __("Justify", 'alliance');
			$THEMEREX_GLOBALS['list_alignments'] = $list;
		}
		return $prepend_inherit ? themerex_array_merge(array('inherit' => __("Inherit", 'alliance')), $list) : $list;
	}
}

// Return sorting list items
if ( !function_exists( 'themerex_get_list_sortings' ) ) {
	function themerex_get_list_sortings($prepend_inherit=false) {
		global $THEMEREX_GLOBALS;
		if (isset($THEMEREX_GLOBALS['list_sortings']))
			$list = $THEMEREX_GLOBALS['list_sortings'];
		else {
			$list = array();
			$list["date"] = __("Date", 'alliance');
			$list["title"] = __("Alphabetically", 'alliance');
			$list["views"] = __("Popular (views count)", 'alliance');
			$list["comments"] = __("Most commented (comments count)", 'alliance');
			$list["author_rating"] = __("Author rating", 'alliance');
			$list["users_rating"] = __("Visitors (users) rating", 'alliance');
			$list["random"] = __("Random", 'alliance');
			$THEMEREX_GLOBALS['list_sortings'] = $list = apply_filters('themerex_filter_list_sortings', $list);
		}
		return $prepend_inherit ? themerex_array_merge(array('inherit' => __("Inherit", 'alliance')), $list) : $list;
	}
}

// Return list with columns widths
if ( !function_exists( 'themerex_get_list_columns' ) ) {
	function themerex_get_list_columns($prepend_inherit=false) {
		global $THEMEREX_GLOBALS;
		if (isset($THEMEREX_GLOBALS['list_columns']))
			$list = $THEMEREX_GLOBALS['list_columns'];
		else {
			$list = array();
			$list["none"] = __("None", 'alliance');
			$list["1_1"] = __("100%", 'alliance');
			$list["1_2"] = __("1/2", 'alliance');
			$list["1_3"] = __("1/3", 'alliance');
			$list["2_3"] = __("2/3", 'alliance');
			$list["1_4"] = __("1/4", 'alliance');
			$list["3_4"] = __("3/4", 'alliance');
			$list["1_5"] = __("1/5", 'alliance');
			$list["2_5"] = __("2/5", 'alliance');
			$list["3_5"] = __("3/5", 'alliance');
			$list["4_5"] = __("4/5", 'alliance');
			$list["1_6"] = __("1/6", 'alliance');
			$list["5_6"] = __("5/6", 'alliance');
			$list["1_7"] = __("1/7", 'alliance');
			$list["2_7"] = __("2/7", 'alliance');
			$list["3_7"] = __("3/7", 'alliance');
			$list["4_7"] = __("4/7", 'alliance');
			$list["5_7"] = __("5/7", 'alliance');
			$list["6_7"] = __("6/7", 'alliance');
			$list["1_8"] = __("1/8", 'alliance');
			$list["3_8"] = __("3/8", 'alliance');
			$list["5_8"] = __("5/8", 'alliance');
			$list["7_8"] = __("7/8", 'alliance');
			$list["1_9"] = __("1/9", 'alliance');
			$list["2_9"] = __("2/9", 'alliance');
			$list["4_9"] = __("4/9", 'alliance');
			$list["5_9"] = __("5/9", 'alliance');
			$list["7_9"] = __("7/9", 'alliance');
			$list["8_9"] = __("8/9", 'alliance');
			$list["1_10"]= __("1/10", 'alliance');
			$list["3_10"]= __("3/10", 'alliance');
			$list["7_10"]= __("7/10", 'alliance');
			$list["9_10"]= __("9/10", 'alliance');
			$list["1_11"]= __("1/11", 'alliance');
			$list["2_11"]= __("2/11", 'alliance');
			$list["3_11"]= __("3/11", 'alliance');
			$list["4_11"]= __("4/11", 'alliance');
			$list["5_11"]= __("5/11", 'alliance');
			$list["6_11"]= __("6/11", 'alliance');
			$list["7_11"]= __("7/11", 'alliance');
			$list["8_11"]= __("8/11", 'alliance');
			$list["9_11"]= __("9/11", 'alliance');
			$list["10_11"]= __("10/11", 'alliance');
			$list["1_12"]= __("1/12", 'alliance');
			$list["5_12"]= __("5/12", 'alliance');
			$list["7_12"]= __("7/12", 'alliance');
			$list["10_12"]= __("10/12", 'alliance');
			$list["11_12"]= __("11/12", 'alliance');
			$THEMEREX_GLOBALS['list_columns'] = $list = apply_filters('themerex_filter_list_columns', $list);
		}
		return $prepend_inherit ? themerex_array_merge(array('inherit' => __("Inherit", 'alliance')), $list) : $list;
	}
}

// Return list of locations for the dedicated content
if ( !function_exists( 'themerex_get_list_dedicated_locations' ) ) {
	function themerex_get_list_dedicated_locations($prepend_inherit=false) {
		global $THEMEREX_GLOBALS;
		if (isset($THEMEREX_GLOBALS['list_dedicated_locations']))
			$list = $THEMEREX_GLOBALS['list_dedicated_locations'];
		else {
			$list = array();
			$list["default"] = __('As in the post defined', 'alliance');
			$list["center"]  = __('Above the text of the post', 'alliance');
			$list["left"]    = __('To the left the text of the post', 'alliance');
			$list["right"]   = __('To the right the text of the post', 'alliance');
			$list["alter"]   = __('Alternates for each post', 'alliance');
			$THEMEREX_GLOBALS['list_dedicated_locations'] = $list = apply_filters('themerex_filter_list_dedicated_locations', $list);
		}
		return $prepend_inherit ? themerex_array_merge(array('inherit' => __("Inherit", 'alliance')), $list) : $list;
	}
}

// Return post-format name
if ( !function_exists( 'themerex_get_post_format_name' ) ) {
	function themerex_get_post_format_name($format, $single=true) {
		$name = '';
		if ($format=='gallery')		$name = $single ? __('gallery', 'alliance') : __('galleries', 'alliance');
		else if ($format=='video')	$name = $single ? __('video', 'alliance') : __('videos', 'alliance');
		else if ($format=='audio')	$name = $single ? __('audio', 'alliance') : __('audios', 'alliance');
		else if ($format=='image')	$name = $single ? __('image', 'alliance') : __('images', 'alliance');
		else if ($format=='quote')	$name = $single ? __('quote', 'alliance') : __('quotes', 'alliance');
		else if ($format=='link')	$name = $single ? __('link', 'alliance') : __('links', 'alliance');
		else if ($format=='status')	$name = $single ? __('status', 'alliance') : __('statuses', 'alliance');
		else if ($format=='aside')	$name = $single ? __('aside', 'alliance') : __('asides', 'alliance');
		else if ($format=='chat')	$name = $single ? __('chat', 'alliance') : __('chats', 'alliance');
		else						$name = $single ? __('standard', 'alliance') : __('standards', 'alliance');
		return apply_filters('themerex_filter_list_post_format_name', $name, $format);
	}
}

// Return post-format icon name (from Fontello library)
if ( !function_exists( 'themerex_get_post_format_icon' ) ) {
	function themerex_get_post_format_icon($format) {
		$icon = 'icon-';
		if ($format=='gallery')		$icon .= 'picture-2';
		else if ($format=='video')	$icon .= 'video-2';
		else if ($format=='audio')	$icon .= 'musical-2';
		else if ($format=='image')	$icon .= 'picture-boxed-2';
		else if ($format=='quote')	$icon .= 'quote-2';
		else if ($format=='link')	$icon .= 'link-2';
		else if ($format=='status')	$icon .= 'agenda-2';
		else if ($format=='aside')	$icon .= 'chat-2';
		else if ($format=='chat')	$icon .= 'chat-all-2';
		else						$icon .= 'book-2';
		return apply_filters('themerex_filter_list_post_format_icon', $icon, $format);
	}
}

// Return fonts styles list, prepended inherit
if ( !function_exists( 'themerex_get_list_fonts_styles' ) ) {
	function themerex_get_list_fonts_styles($prepend_inherit=false) {
		global $THEMEREX_GLOBALS;
		if (isset($THEMEREX_GLOBALS['list_fonts_styles']))
			$list = $THEMEREX_GLOBALS['list_fonts_styles'];
		else {
			$list = array();
			$list['i'] = __('I','alliance');
			$list['u'] = __('U', 'alliance');
			$THEMEREX_GLOBALS['list_fonts_styles'] = $list;
		}
		return $prepend_inherit ? themerex_array_merge(array('inherit' => __("Inherit", 'alliance')), $list) : $list;
	}
}

// Return Google fonts list
if ( !function_exists( 'themerex_get_list_fonts' ) ) {
	function themerex_get_list_fonts($prepend_inherit=false) {
		global $THEMEREX_GLOBALS;
		if (isset($THEMEREX_GLOBALS['list_fonts']))
			$list = $THEMEREX_GLOBALS['list_fonts'];
		else {
			$list = array();
			$list = themerex_array_merge($list, themerex_get_list_fonts_custom());
			// Google and custom fonts list:
			//$list['Advent Pro'] = array(
			//		'family'=>'sans-serif',																						// (required) font family
			//		'link'=>'Advent+Pro:100,100italic,300,300italic,400,400italic,500,500italic,700,700italic,900,900italic',	// (optional) if you use Google font repository
			//		'css'=>themerex_get_file_url('/css/font-face/Advent-Pro/stylesheet.css')									// (optional) if you use custom font-face
			//		);
			$list['Advent Pro'] = array('family'=>'sans-serif');
			$list['Alegreya Sans'] = array('family'=>'sans-serif');
			$list['Arimo'] = array('family'=>'sans-serif');
			$list['Asap'] = array('family'=>'sans-serif');
			$list['Averia Sans Libre'] = array('family'=>'cursive');
			$list['Averia Serif Libre'] = array('family'=>'cursive');
			$list['Bree Serif'] = array('family'=>'serif',);
			$list['Cabin'] = array('family'=>'sans-serif');
			$list['Cabin Condensed'] = array('family'=>'sans-serif');
			$list['Caudex'] = array('family'=>'serif');
			$list['Comfortaa'] = array('family'=>'cursive');
			$list['Cousine'] = array('family'=>'sans-serif');
			$list['Crimson Text'] = array('family'=>'serif');
			$list['Cuprum'] = array('family'=>'sans-serif');
			$list['Dosis'] = array('family'=>'sans-serif');
			$list['Economica'] = array('family'=>'sans-serif');
			$list['Exo'] = array('family'=>'sans-serif');
			$list['Expletus Sans'] = array('family'=>'cursive');
			$list['Karla'] = array('family'=>'sans-serif');
			$list['Lato'] = array('family'=>'sans-serif');
			$list['Lekton'] = array('family'=>'sans-serif');
			$list['Lobster Two'] = array('family'=>'cursive');
			$list['Maven Pro'] = array('family'=>'sans-serif');
			$list['Merriweather'] = array('family'=>'serif');
			$list['Montserrat'] = array('family'=>'sans-serif');
			$list['Neuton'] = array('family'=>'serif');
			$list['Noticia Text'] = array('family'=>'serif');
			$list['Old Standard TT'] = array('family'=>'serif');
			$list['Open Sans'] = array('family'=>'sans-serif');
			$list['Orbitron'] = array('family'=>'sans-serif');
			$list['Oswald'] = array('family'=>'sans-serif');
			$list['Overlock'] = array('family'=>'cursive');
			$list['Oxygen'] = array('family'=>'sans-serif');
			$list['PT Serif'] = array('family'=>'serif');
			$list['Puritan'] = array('family'=>'sans-serif');
			$list['Raleway'] = array('family'=>'sans-serif');
			$list['Roboto'] = array('family'=>'sans-serif');
			$list['Roboto Slab'] = array('family'=>'sans-serif');
			$list['Roboto Condensed'] = array('family'=>'sans-serif');
			$list['Rosario'] = array('family'=>'sans-serif');
			$list['Share'] = array('family'=>'cursive');
			$list['Signika'] = array('family'=>'sans-serif');
			$list['Signika Negative'] = array('family'=>'sans-serif');
			$list['Source Sans Pro'] = array('family'=>'sans-serif');
			$list['Tinos'] = array('family'=>'serif');
			$list['Ubuntu'] = array('family'=>'sans-serif');
			$list['Vollkorn'] = array('family'=>'serif');
			$THEMEREX_GLOBALS['list_fonts'] = $list = apply_filters('themerex_filter_list_fonts', $list);
		}
		return $prepend_inherit ? themerex_array_merge(array('inherit' => __("Inherit", 'alliance')), $list) : $list;
	}
}

// Return Custom font-face list
if ( !function_exists( 'themerex_get_list_fonts_custom' ) ) {
	function themerex_get_list_fonts_custom($prepend_inherit=false) {
		static $list = false;
		if (is_array($list)) return $list;
		$fonts = themerex_get_global('required_custom_fonts');
		$list = array();
		if (is_array($fonts)) {
			foreach ($fonts as $font) {
				if (($url = themerex_get_file_url('css/font-face/'.trim($font).'/stylesheet.css'))!='') {
					$list[sprintf(esc_html__('%s (uploaded font)', 'alliance'), $font)] = array('css' => $url);
				}
			}
		}
		return $list;
	}
}
?>