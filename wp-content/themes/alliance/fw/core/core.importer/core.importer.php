<?php
// Disable direct call
if ( ! defined( 'ABSPATH' ) ) { exit; }


// Theme init
if (!function_exists('themerex_importer_theme_setup')) {
	add_action( 'themerex_action_after_init_theme', 'themerex_importer_theme_setup' );		// Fire this action after load theme options
	function themerex_importer_theme_setup() {
		if (is_admin() && current_user_can('import') && themerex_get_theme_option('admin_dummy_data')=='yes') {
			new themerex_dummy_data_importer();
		}
	}
}

class themerex_dummy_data_importer {

	// Theme specific settings
	var $options = array(
		'debug'					=> true,						// Enable debug output
		'enable_importer'		=> true,						// Show Importer section
		'enable_exporter'		=> true,						// Show Exporter section
		'data_type'				=> 'vc',						// Default dummy data type
		'file_with_content'		=> array(
			'no_vc'				=> 'demo/dummy_data.xml',		// Name of the file with demo content without VC wrappers
			'vc'				=> 'demo/dummy_data_vc.xml'		// Name of the file with demo content for Visual Composer
			),
		'file_with_options'		=> 'demo/theme_options.txt',	// Name of the file with theme options
		'file_with_postmeta'	=> 'demo/theme_postmeta.txt',	// Name of the file with post meta
		'file_with_widgets'		=> 'demo/widgets.txt',			// Name of the file with widgets data
		'file_with_timeline'	=> 'demo/timeline.txt',			// Name of the file with timeline data
		'file_with_grid'		=> 'demo/grid.txt',				// Name of the file with grid data
		'file_with_events'		=> 'demo/events.txt',			// Name of the file with events data
		'file_with_users'		=> 'demo/users.txt',			// Name of the file with users data
		'file_with_chart'		=> 'demo/chart.txt',			// Name of the file with chart data
		'file_with_quiz'		=> 'demo/quiz.txt',				// Name of the file with quiz data
		'file_with_bbpress'		=> 'demo/bbpress.txt',			// Name of the file with bbpress data
		'file_with_booking'		=> 'demo/booking.txt',			// Name of the file with booking data
		'folder_with_revsliders'=> '',							// Name of the folder with revolution sliders data
		'domain_dev'			=> '.dnw',						// Domain on developer's server
		'domain_demo'			=> '.net',						// Domain on demo-server
		'demo_url'				=> 'http://alliance.themerex.net/',	// URL of the demo site - need for change URLs in the custom menu items
		'uploads_folder'		=> 'imports',					// Folder with images in demo data
		'upload_attachments'	=> true,						// Upload attachments images
		'import_posts'			=> true,						// Import posts
		'import_to'				=> true,						// Import Theme Options
		'import_widgets'		=> true,						// Import widgets
		'import_timeline'		=> true,						// Import timeline
		'import_grid'			=> true,						// Import grid
		'import_events'			=> true,						// Import events
		'import_users'			=> true,						// Import users
		'import_chart'			=> true,						// Import chart
		'import_quiz'			=> true,						// Import quiz
		'import_bbpress'		=> true,						// Import bbpress
		'import_booking'		=> false,						// Import booking
		'import_sliders'		=> false,						// Import sliders
		'overwrite_content'		=> true,						// Overwrite existing content
		'show_on_front'			=> 'page',						// Reading settings
		'page_on_front'			=> 'Home Style 1',				// Homepage title
		'page_for_posts'		=> 'All posts',					// Blog streampage title
		'menus'					=> array(						// Menus locations and names
			'menu-main'	=> 'Main menu'
		),
		'taxonomies'		=> array(),							// List of required taxonomies: 'post_type' => 'taxonomy', ...
		'required_plugins'		=> array( 
			//'woocommerce',
			'visual_composer',
			//'revslider'
			
		),
		'wooc_options'			=> array(						// Options slugs for WooCommerce
			'shop_catalog_image_size', 'shop_single_image_size', 'shop_thumbnail_image_size',
			'woocommerce_shop_page_display', 'woocommerce_category_archive_display', 'woocommerce_default_catalog_orderby',
			'woocommerce_cart_redirect_after_add', 'woocommerce_enable_ajax_add_to_cart'
			
		),
		'wooc_pages'			=> array(						// Options slugs and pages titles for WooCommerce pages
			'woocommerce_shop_page_id' 				=> 'Shop',
			'woocommerce_cart_page_id' 				=> 'Cart',
			'woocommerce_checkout_page_id' 			=> 'Checkout',
			'woocommerce_pay_page_id' 				=> 'Checkout &#8594; Pay',
			'woocommerce_thanks_page_id' 			=> 'Order Received',
			'woocommerce_myaccount_page_id' 		=> 'My Account',
			'woocommerce_edit_address_page_id'		=> 'Edit My Address',
			'woocommerce_view_order_page_id'		=> 'View Order',
			'woocommerce_change_password_page_id'	=> 'Change Password',
			'woocommerce_logout_page_id'			=> 'Logout',
			'woocommerce_lost_password_page_id'		=> 'Lost Password'
		)
	);

	var $error    = '';				// Error message
	var $success  = '';				// Success message
	var $result   = 0;				// Import posts percent (if break inside)
	var $last_slider = 0;			// Last imported slider number

	var $nonce    = '';
	var $export_options = '';
	var $export_postmeta = '';
	var $export_widgets = '';
	var $export_timeline = '';
	var $export_grid = '';
	var $export_events = '';
	var $export_users = '';
	var $export_chart = '';
	var $export_quiz = '';
	var $export_bbpress = '';
	var $export_booking = '';
	var $uploads_url = '';
	var $uploads_dir = '';
	var $import_log = '';
	var $import_last_id = 0;

	//-----------------------------------------------------------------------------------
	// Constuctor
	//-----------------------------------------------------------------------------------
	function __construct() {
	    $this->options = apply_filters('themerex_filter_importer_options', $this->options);
		$this->nonce = wp_create_nonce(__FILE__);
		$uploads_info = wp_upload_dir();
		$this->uploads_dir = $uploads_info['basedir'];
		$this->uploads_url = $uploads_info['baseurl'];
		if ($this->options['debug']) define('IMPORT_DEBUG', true);
		$this->import_log = themerex_get_file_dir('core/core.importer/importer.log');
		$log = explode('|', themerex_fgc($this->import_log));
		$this->import_last_id = (int) $log[0];
		$this->result = empty($log[1]) ? 0 : (int) $log[1];
		$this->last_slider = empty($log[2]) ? '' : $log[2];
		add_action('admin_menu', array($this, 'admin_menu_item'));
	}

	//-----------------------------------------------------------------------------------
	// Admin Interface
	//-----------------------------------------------------------------------------------
	function admin_menu_item() {
		if ( current_user_can( 'manage_options' ) ) {
			// In this case menu item is add in admin menu 'Appearance'
			add_theme_page(__('Install Dummy Data', 'themerex'), __('Install Dummy Data', 'themerex'), 'edit_theme_options', 'trx_importer', array($this, 'build_page'));

			// In this case menu item is add in admin menu 'Tools'
			//add_management_page(__('Theme Demo', 'themerex'), __('Theme Demo', 'themerex'), 'manage_options', 'trx_importer', array($this, 'build_page'));
		}
	}
	
	
	//-----------------------------------------------------------------------------------
	// Build the Main Page
	//-----------------------------------------------------------------------------------
	function build_page() {
		
		$after_importer = false;

		do {
			if ( isset($_POST['importer_action']) ) {
				if ( !isset($_POST['nonce']) || !wp_verify_nonce( $_POST['nonce'], __FILE__ ) ) {
					$this->error = __('Incorrect WP-nonce data! Operation canceled!', 'themerex');
					break;
				}
				if ($this->checkRequiredPlugins()) {
					$this->options['overwrite_content']	= $_POST['importer_action']=='overwrite';
					$this->options['data_type'] 		= $_POST['data_type']=='vc' ? 'vc' : 'no_vc';
					$this->options['upload_attachments']= isset($_POST['importer_upload']);
					$this->options['import_posts']		= isset($_POST['importer_posts']);
					$this->options['import_to']			= isset($_POST['importer_to']);
					$this->options['import_widgets']	= isset($_POST['importer_widgets']);
					$this->options['import_timeline']	= isset($_POST['importer_timeline']);
					$this->options['import_grid']		= isset($_POST['importer_grid']);
					$this->options['import_events']		= isset($_POST['importer_events']);
					$this->options['import_users']		= isset($_POST['importer_users']);
					$this->options['import_chart']		= isset($_POST['importer_chart']);
					$this->options['import_quiz']		= isset($_POST['importer_quiz']);
					$this->options['import_bbpress']	= isset($_POST['importer_bbpress']);
					$this->options['import_booking']	= isset($_POST['importer_booking']);
					$this->options['import_sliders']	= isset($_POST['importer_sliders']);
					$this->import_last_id = (int) $_POST['last_id'];
					?>
					<div class="trx_importer_log">
						<?php
						$this->importer();
						$after_importer = true;
						?>
						<script type="text/javascript">
							jQuery(document).ready(function() {
								jQuery('.trx_importer_log').remove();
								<?php if ($this->import_last_id > 0 || (!empty($this->last_slider) && $this->options['import_sliders'])) { ?>
								setTimeout(function() {
									jQuery('#trx_importer_continue').trigger('click');
								}, 3000);
								<?php } ?>
							});
						</script>
					</div>
					<?php
				}
			} else if ( isset($_POST['exporter_action']) ) {
				if ( !isset($_POST['nonce']) || !wp_verify_nonce( $_POST['nonce'], __FILE__ ) ) {
					$this->error = __('Incorrect WP-nonce data! Operation canceled!', 'themerex');
					break;
				}
				$this->exporter();
			}
		} while (false);
		?>
		<div class="trx_importer">
			<div class="trx_importer_result">
				<?php if (!empty($this->error)) { ?>
				<p>&nbsp;</p>
				<div class="error">
					<p><?php echo ($this->error); ?></p>
				</div>
				<p>&nbsp;</p>
				<?php } ?>
				<?php if (!empty($this->success)) { ?>
				<p>&nbsp;</p>
				<div class="updated">
					<p><?php echo ($this->success); ?></p>
				</div>
				<p>&nbsp;</p>
				<?php } ?>
			</div>
	
			<?php if (empty($this->success) && $this->options['enable_importer']) { ?>
				<div class="trx_importer_section"<?php echo ($after_importer ? ' style="display:none;"' : ''); ?>>
					<h2 class="trx_title"><?php _e('ThemeREX Importer', 'themerex'); ?></h2>
					<p><b><?php _e('Attention! Important info:', 'themerex'); ?></b></p>
					<ol>
						<li><?php _e('Data import can take a long time (sometimes more than 10 minutes) - please wait until the end of the procedure, do not navigate away from the page.', 'themerex'); ?></li>
						<li><?php _e('Web-servers set the time limit for the execution of php-scripts. Therefore, the import process will be split into parts. Upon completion of each part - the import will resume automatically!', 'themerex'); ?></li>
						<li><?php _e('We recommend that you select the first option to import (with the replacement of existing content) - so you get a complete copy of our demo site', 'themerex'); ?></li>
						<li><?php _e('We also encourage you to leave the enabled check box "Upload attachments" - to download the demo version of the images', 'themerex'); ?></li>
					</ol>
	
					<form id="trx_importer_form" action="#" method="post">
	
						<input type="hidden" value="<?php echo esc_attr($this->nonce); ?>" name="nonce" />
						<input type="hidden" value="0" name="last_id" />
	
						<p>
						<input type="radio" <?php echo ($this->options['overwrite_content'] ? 'checked="checked"' : ''); ?> value="overwrite" name="importer_action" id="importer_action_over" /><label for="importer_action_over"><?php _e('Overwrite existing content', 'themerex'); ?></label><br>
						<?php _e('In this case <b>all existing content will be erased</b>! But you get full copy of the our demo site <b>(recommended)</b>.', 'themerex'); ?>
						</p>
	
						<p>
						<input type="radio" <?php echo !$this->options['overwrite_content'] ? 'checked="checked"' : ''; ?> value="append" name="importer_action" id="importer_action_append" /><label for="importer_action_append"><?php _e('Append to existing content', 'themerex'); ?></label><br>
						<?php _e('In this case demo data append to the existing content! Warning! In many cases you do not have exact copy of the demo site.', 'themerex'); ?>
						</p>
	
						<p><b><?php _e('Select the data to import:', 'themerex'); ?></b></p>
						<p>
						<?php
						$checked = 'checked="checked"';
						if (!empty($this->options['file_with_content']['vc']) && file_exists(themerex_get_file_dir($this->options['file_with_content']['vc']))) {
							?>
							<input type="radio" <?php echo ($this->options['data_type']=='vc' ? $checked : ''); ?> value="vc" name="data_type" id="data_type_vc" /><label for="data_type_vc"><?php _e('Import data for edit in the Visual Composer', 'themerex'); ?></label><br>
							<?php
							if ($this->options['data_type']=='vc') $checked = '';
						}
						if (!empty($this->options['file_with_content']['no_vc']) && file_exists(themerex_get_file_dir($this->options['file_with_content']['no_vc']))) {
							?>
							<input type="radio" <?php echo ($this->options['data_type']=='no_vc' || $checked ? $checked : ''); ?> value="no_vc" name="data_type" id="data_type_no_vc" /><label for="data_type_no_vc"><?php _e('Import data without Visual Composer wrappers', 'themerex'); ?></label>
							<?php
						}
						?>
						</p>
						<p>
						<input type="checkbox" <?php echo ($this->options['import_posts'] ? 'checked="checked"' : ''); ?> value="1" name="importer_posts" id="importer_posts" /> <label for="importer_posts"><?php _e('Import posts', 'themerex'); ?></label><br>
						<input type="checkbox" <?php echo ($this->options['upload_attachments'] ? 'checked="checked"' : ''); ?> value="1" name="importer_upload" id="importer_upload" /> <label for="importer_upload"><?php _e('Upload attachments', 'themerex'); ?></label>
						</p>
						<p>
						<input type="checkbox" <?php echo ($this->options['import_to'] ? 'checked="checked"' : ''); ?> value="1" name="importer_to" id="importer_to" /> <label for="importer_to"><?php _e('Import Theme Options', 'themerex'); ?></label><br>
						<input type="checkbox" <?php echo ($this->options['import_widgets'] ? 'checked="checked"' : ''); ?> value="1" name="importer_widgets" id="importer_widgets" /> <label for="importer_widgets"><?php _e('Import Widgets', 'themerex'); ?></label><br>
						<input type="checkbox" <?php echo ($this->options['import_timeline'] ? 'checked="checked"' : ''); ?> value="1" name="importer_timeline" id="importer_timeline" /> <label for="importer_timeline"><?php _e('Import Timeline', 'themerex'); ?></label><br>		
						<input type="checkbox" <?php echo ($this->options['import_grid'] ? 'checked="checked"' : ''); ?> value="1" name="importer_grid" id="importer_grid" /> <label for="importer_grid"><?php _e('Import Essential Grid', 'themerex'); ?></label><br>		
						<input type="checkbox" <?php echo ($this->options['import_events'] ? 'checked="checked"' : ''); ?> value="1" name="importer_events" id="importer_events" /> <label for="importer_events"><?php _e('Import Events', 'themerex'); ?></label><br>		
						<input type="checkbox" <?php echo ($this->options['import_users'] ? 'checked="checked"' : ''); ?> value="1" name="importer_users" id="importer_users" /> <label for="importer_users"><?php _e('Import Users', 'themerex'); ?></label><br>		
						<input type="checkbox" <?php echo ($this->options['import_chart'] ? 'checked="checked"' : ''); ?> value="1" name="importer_chart" id="importer_chart" /> <label for="importer_chart"><?php _e('Import Chart', 'themerex'); ?></label><br>		
						<input type="checkbox" <?php echo ($this->options['import_quiz'] ? 'checked="checked"' : ''); ?> value="1" name="importer_quiz" id="importer_quiz" /> <label for="importer_quiz"><?php _e('Import Quiz', 'themerex'); ?></label><br>		
						<input type="checkbox" <?php echo ($this->options['import_bbpress'] ? 'checked="checked"' : ''); ?> value="1" name="importer_bbpress" id="importer_bbpress" /> <label for="importer_bbpress"><?php _e('Import Bbpress', 'themerex'); ?></label><br>		
				<!--		<input type="checkbox" <?php echo ($this->options['import_sliders'] ? 'checked="checked"' : ''); ?> value="1" name="importer_sliders" id="importer_sliders" /> <label for="importer_sliders"><?php _e('Import Sliders', 'themerex'); ?></label> -->
						</p>
	
						<div class="trx_buttons">
							<?php if ($this->import_last_id > 0 || (!empty($this->last_slider) && $this->options['import_sliders'])) { ?>
								<h4 class="trx_importer_complete"><?php sprintf(__('Import posts completed by %s', 'themerex'), $this->result.'%'); ?></h4>
								<input type="submit" value="<?php printf(__('Continue import (from ID=%s)', 'themerex'), $this->import_last_id); ?>" onClick="this.form.last_id.value='<?php echo esc_attr($this->import_last_id); ?>'" id="trx_importer_continue">
								<input type="submit" value="<?php _e('Start import again', 'themerex'); ?>">
							<?php } else { ?>
								<input type="submit" value="<?php _e('Start import', 'themerex'); ?>">
							<?php } ?>
						</div>
					</form>
				</div>
			<?php } ?>

			<?php if (empty($this->success) && $this->options['enable_exporter']) { ?>
				<div class="trx_exporter_section"<?php echo ($after_importer ? ' style="display:none;"' : ''); ?>>
					<h2 class="trx_title"><?php _e('ThemeREX Exporter', 'themerex'); ?></h2>
					<form id="trx_exporter_form" action="#" method="post">
	
						<input type="hidden" value="<?php echo esc_attr($this->nonce); ?>" name="nonce" />
						<input type="hidden" value="all" name="exporter_action" />
	
						<div class="trx_buttons">
							<?php if ($this->export_options!='') { ?>
							<h4><?php _e('Theme options', 'themerex'); ?></h4>
							<textarea rows="10" cols="80"><?php echo esc_html($this->export_options); ?></textarea>
							<h4><?php _e('Widgets', 'themerex'); ?></h4>
							<textarea rows="10" cols="80"><?php echo esc_html($this->export_widgets); ?></textarea>
							<h4><?php _e('Timeline', 'themerex'); ?></h4>
							<textarea rows="10" cols="80"><?php echo esc_html($this->export_timeline); ?></textarea>
							<h4><?php _e('Essential Grid', 'themerex'); ?></h4>
							<textarea rows="10" cols="80"><?php echo esc_html($this->export_grid); ?></textarea>
							<h4><?php _e('Events', 'themerex'); ?></h4>
							<textarea rows="10" cols="80"><?php echo esc_html($this->export_events); ?></textarea>
							<h4><?php _e('Users', 'themerex'); ?></h4>
							<textarea rows="10" cols="80"><?php echo esc_html($this->export_users); ?></textarea>
							<h4><?php _e('Chart', 'themerex'); ?></h4>
							<textarea rows="10" cols="80"><?php echo esc_html($this->export_chart); ?></textarea>
							<h4><?php _e('Quiz', 'themerex'); ?></h4>
							<textarea rows="10" cols="80"><?php echo esc_html($this->export_quiz); ?></textarea>
							<h4><?php _e('Bbpress', 'themerex'); ?></h4>
							<textarea rows="10" cols="80"><?php echo esc_html($this->export_bbpress); ?></textarea>
							<?php } else { ?>
							<input type="submit" value="<?php _e('Export Theme Options', 'themerex'); ?>">
							<?php } ?>
						</div>
	
					</form>
				</div>
			<?php } ?>
		</div>
		<?php
	}
	
	
	//-----------------------------------------------------------------------------------
	// Export dummy data
	//-----------------------------------------------------------------------------------
	function exporter() {
		global $wpdb;
		$suppress = $wpdb->suppress_errors();

		// Export theme and categories options and VC templates
		$rows = $wpdb->get_results( "SELECT option_name, option_value FROM " . esc_sql($wpdb->options) . " WHERE option_name LIKE 'themerex_options%' OR option_name='wpb_js_templates'" );
		$options = array();
		if (count($rows) > 0) {
			foreach ($rows as $row) {
				$options[$row->option_name] = $this->prepare_uploads(unserialize($row->option_value));
			}
		}
		// Export WooCommerce Options
		if (themerex_exists_woocommerce()) {
			if (count($this->options['wooc_options']) > 0) {
				foreach ($this->options['wooc_options'] as $opt) {
					$rows = $wpdb->get_results( "SELECT option_name, option_value FROM " . esc_sql($wpdb->options) . " WHERE option_name='" . esc_sql($opt) . "'" );
					if (count($rows) > 0) {
						foreach ($rows as $row) {
							$options[$row->option_name] = maybe_unserialize($row->option_value);
						}
					}
				}
			}
		}
		$this->export_options = base64_encode(str_replace($this->options['domain_dev'], $this->options['domain_demo'], serialize($options)));

		// Export widgets
		$rows = $wpdb->get_results( "SELECT option_name, option_value FROM " . esc_sql($wpdb->options) . " WHERE option_name = 'sidebars_widgets' OR option_name LIKE 'widget_%'" );
		$options = array();
		if (count($rows) > 0) {
			foreach ($rows as $row) {
				$options[$row->option_name] = $this->prepare_uploads(unserialize($row->option_value));
			}
		}
		$this->export_widgets = base64_encode(str_replace($this->options['domain_dev'], $this->options['domain_demo'], serialize($options)));

		$wpdb->suppress_errors( $suppress );
		
		// Export timeline
		global $wpdb;
		$rows = $wpdb->get_results('SELECT * FROM ' . $wpdb->prefix . 'ctimelines');
		$N=0;
		$options = array();
		if (count($rows) > 0) {
			foreach ($rows as $row) {
				$options[$N] = $this->prepare_uploads(maybe_unserialize($row->id)); $N++;
				$options[$N] = $this->prepare_uploads(maybe_unserialize($row->name)); $N++;
				$options[$N] = $this->prepare_uploads(maybe_unserialize($row->settings)); $N++;
				$options[$N] = $this->prepare_uploads(maybe_unserialize($row->items)); $N++;
			}
		}
		$this->export_timeline = base64_encode(str_replace($this->options['domain_dev'], $this->options['domain_demo'], serialize($options)));

		$wpdb->suppress_errors( $suppress );
		
		
		// Export grid
		global $wpdb;
		$rows = $wpdb->get_results('SELECT * FROM ' . $wpdb->prefix . 'eg_grids');
		$N=0;
		$options = array();
		if (count($rows) > 0) {
			foreach ($rows as $row) {
				$options[$N] = $this->prepare_uploads(maybe_unserialize($row->id)); $N++;
				$options[$N] = $this->prepare_uploads(maybe_unserialize($row->name)); $N++;
				$options[$N] = $this->prepare_uploads(maybe_unserialize($row->handle)); $N++;
				$options[$N] = $this->prepare_uploads(maybe_unserialize($row->postparams)); $N++;
				$options[$N] = $this->prepare_uploads(maybe_unserialize($row->params)); $N++;
				$options[$N] = $this->prepare_uploads(maybe_unserialize($row->layers)); $N++;
			}
		}
		$this->export_grid = base64_encode(str_replace($this->options['domain_dev'], $this->options['domain_demo'], serialize($options)));

		$wpdb->suppress_errors( $suppress );
		
		// Export events
		global $wpdb;
		$rows = $wpdb->get_results('SELECT * FROM ' . $wpdb->prefix . 'eo_events');
		$N=0;
		$options = array();
		if (count($rows) > 0) {
			foreach ($rows as $row) {
				$options[$N] = $this->prepare_uploads(maybe_unserialize($row->event_id)); $N++;
				$options[$N] = $this->prepare_uploads(maybe_unserialize($row->post_id)); $N++;
				$options[$N] = $this->prepare_uploads(maybe_unserialize($row->StartDate));  $N++;
				$options[$N] = $this->prepare_uploads(maybe_unserialize($row->EndDate)); $N++;
				$options[$N] = $this->prepare_uploads(maybe_unserialize($row->StartTime)); $N++;
				$options[$N] = $this->prepare_uploads(maybe_unserialize($row->FinishTime)); $N++;
			}
		}
		$this->export_events = base64_encode(str_replace($this->options['domain_dev'], $this->options['domain_demo'], serialize($options)));

		$wpdb->suppress_errors( $suppress );
	
	
		// Export users
		global $wpdb;
		$rows = $wpdb->get_results('SELECT * FROM ' . $wpdb->prefix . 'users WHERE ID > 1');
		$N=0;
		$options = array();
		if (count($rows) > 0) {
			foreach ($rows as $row) {
				$options[$N] = $this->prepare_uploads(maybe_unserialize($row->ID)); $N++;
				$options[$N] = $this->prepare_uploads(maybe_unserialize($row->user_login)); $N++;
				$options[$N] = $this->prepare_uploads(maybe_unserialize($row->user_pass));  $N++;
				$options[$N] = $this->prepare_uploads(maybe_unserialize($row->user_nicename)); $N++;
				$options[$N] = $this->prepare_uploads(maybe_unserialize($row->user_email)); $N++;
				$options[$N] = $this->prepare_uploads(maybe_unserialize($row->display_name)); $N++;
			}
		}
		$this->export_users = base64_encode(str_replace($this->options['domain_dev'], $this->options['domain_demo'], serialize($options)));

		$wpdb->suppress_errors( $suppress );

		// Export chart
		global $wpdb;
		$rows = $wpdb->get_results('SELECT * FROM ' . $wpdb->prefix . 'weblator_polls');
		$N=0;
		$options = array();
		if (count($rows) > 0) {
			foreach ($rows as $row) {
				$options[$N] = $this->prepare_uploads(maybe_unserialize($row->id)); $N++;
				$options[$N] = $this->prepare_uploads(maybe_unserialize($row->poll_name)); $N++;
				$options[$N] = $this->prepare_uploads(maybe_unserialize($row->poll_chart_type));  $N++;
				$options[$N] = $this->prepare_uploads(maybe_unserialize($row->poll_allow_view_results)); $N++;
				$options[$N] = $this->prepare_uploads(maybe_unserialize($row->poll_one_vote)); $N++;
				$options[$N] = $this->prepare_uploads(maybe_unserialize($row->poll_one_vote_userid)); $N++;
				$options[$N] = $this->prepare_uploads(maybe_unserialize($row->poll_one_vote_cookie)); $N++;
				$options[$N] = $this->prepare_uploads(maybe_unserialize($row->poll_one_vote_ip)); $N++;
				$options[$N] = $this->prepare_uploads(maybe_unserialize($row->poll_is_live)); $N++;
				$options[$N] = $this->prepare_uploads(maybe_unserialize($row->poll_max_width)); $N++;
				$options[$N] = $this->prepare_uploads(maybe_unserialize($row->poll_min_width)); $N++;
				$options[$N] = $this->prepare_uploads(maybe_unserialize($row->chart_legend)); $N++;
				$options[$N] = $this->prepare_uploads(maybe_unserialize($row->chart_legend_position)); $N++;
				$options[$N] = $this->prepare_uploads(maybe_unserialize($row->chart_legend_font_size)); $N++;
				$options[$N] = $this->prepare_uploads(maybe_unserialize($row->chart_legend_font_style)); $N++;
				$options[$N] = $this->prepare_uploads(maybe_unserialize($row->chart_legend_font_colour)); $N++;
			}
		}
		
		$rows = $wpdb->get_results('SELECT * FROM ' . $wpdb->prefix . 'weblator_poll_options');
		$options[$N] =  $this->prepare_uploads(maybe_unserialize('new_table_wp_weblator_poll_options')); $N++;
		if (count($rows) > 0) {
			foreach ($rows as $row) {
				$options[$N] = $this->prepare_uploads(maybe_unserialize($row->id)); $N++;
				$options[$N] = $this->prepare_uploads(maybe_unserialize($row->poll_id)); $N++;
				$options[$N] = $this->prepare_uploads(maybe_unserialize($row->option_value));  $N++;
				$options[$N] = $this->prepare_uploads(maybe_unserialize($row->option_order)); $N++;
				$options[$N] = $this->prepare_uploads(maybe_unserialize($row->option_colour)); $N++;
			}
		}
		
		$rows = $wpdb->get_results('SELECT * FROM ' . $wpdb->prefix . 'weblator_poll_votes');
		$options[$N] =  $this->prepare_uploads(maybe_unserialize('new_table_wp_weblator_poll_votes')); $N++;
		if (count($rows) > 0) {
			foreach ($rows as $row) {
				$options[$N] = $this->prepare_uploads(maybe_unserialize($row->id)); $N++;
				$options[$N] = $this->prepare_uploads(maybe_unserialize($row->poll_id)); $N++;
				$options[$N] = $this->prepare_uploads(maybe_unserialize($row->option_id));  $N++;
				$options[$N] = $this->prepare_uploads(maybe_unserialize($row->user_id)); $N++;
				$options[$N] = $this->prepare_uploads(maybe_unserialize($row->ipv4)); $N++;
			}
		}
		$this->export_chart = base64_encode(str_replace($this->options['domain_dev'], $this->options['domain_demo'], serialize($options)));

		$wpdb->suppress_errors( $suppress );
		
		
		// Export quiz
		global $wpdb;
		$rows = $wpdb->get_results('SELECT * FROM ' . $wpdb->prefix . 'wp_pro_quiz_master');
		$N=0;
		$options = array();
		if (count($rows) > 0) {
			foreach ($rows as $row) {
				$options[$N] = $this->prepare_uploads(maybe_unserialize($row->id)); $N++;
				$options[$N] = $this->prepare_uploads(maybe_unserialize($row->name)); $N++;
				$options[$N] = $this->prepare_uploads(maybe_unserialize($row->text));  $N++;
				$options[$N] = $this->prepare_uploads(maybe_unserialize($row->time_limit)); $N++;
				$options[$N] = $this->prepare_uploads(maybe_unserialize($row->statistics_ip_lock)); $N++;
				$options[$N] = $this->prepare_uploads(maybe_unserialize($row->quiz_run_once_type)); $N++;
				$options[$N] = $this->prepare_uploads(maybe_unserialize($row->show_max_question_value)); $N++;
				$options[$N] = $this->prepare_uploads(($row->toplist_data)); $N++;
				$options[$N] = $this->prepare_uploads(($row->quiz_modus)); $N++;
				$options[$N] = $this->prepare_uploads(($row->autostart)); $N++;
				$options[$N] = $this->prepare_uploads(($row->admin_email)); $N++;
				$options[$N] = $this->prepare_uploads(($row->user_email)); $N++;
				$options[$N] = $this->prepare_uploads(($row->plugin_container)); $N++;
			}
		}
		
		$rows = $wpdb->get_results('SELECT * FROM ' . $wpdb->prefix . 'wp_pro_quiz_question');
		$options[$N] =  $this->prepare_uploads(maybe_unserialize('new_table_wp_wp_pro_quiz_question')); $N++;
		if (count($rows) > 0) {
			foreach ($rows as $row) {
				$options[$N] = $this->prepare_uploads(maybe_unserialize($row->id)); $N++;
				$options[$N] = $this->prepare_uploads(maybe_unserialize($row->quiz_id)); $N++;
				$options[$N] = $this->prepare_uploads(maybe_unserialize($row->online)); $N++;
				$options[$N] = $this->prepare_uploads(maybe_unserialize($row->sort)); $N++;
				$options[$N] = $this->prepare_uploads(maybe_unserialize($row->title));  $N++;
				$options[$N] = $this->prepare_uploads(maybe_unserialize($row->points));  $N++;
				$options[$N] = $this->prepare_uploads(maybe_unserialize($row->question)); $N++;
				$options[$N] = $this->prepare_uploads(maybe_unserialize($row->answer_type)); $N++;
				$options[$N] = $this->prepare_uploads(($row->answer_data)); $N++;
				$options[$N] = $this->prepare_uploads(maybe_unserialize($row->matrix_sort_answer_criteria_width)); $N++;
			}
		}
	
		$this->export_quiz = base64_encode(str_replace($this->options['domain_dev'], $this->options['domain_demo'], serialize($options)));

		$wpdb->suppress_errors( $suppress );
		
		
		// Export bbpress
		global $wpdb;
		$rows = $wpdb->get_results('SELECT * FROM ' . $wpdb->prefix . 'bp_activity');
		$N=0;
		$options = array();
		if (count($rows) > 0) {
			foreach ($rows as $row) {
				$options[$N] = $this->prepare_uploads(maybe_unserialize($row->id)); $N++;
				$options[$N] = $this->prepare_uploads(maybe_unserialize($row->user_id)); $N++;
				$options[$N] = $this->prepare_uploads(maybe_unserialize($row->component));  $N++;
				$options[$N] = $this->prepare_uploads(maybe_unserialize($row->type)); $N++;
				$options[$N] = $this->prepare_uploads(maybe_unserialize($row->action)); $N++;
				$options[$N] = $this->prepare_uploads(maybe_unserialize($row->content)); $N++;
				$options[$N] = $this->prepare_uploads(maybe_unserialize($row->primary_link)); $N++;
				$options[$N] = $this->prepare_uploads(maybe_unserialize($row->item_id)); $N++;
				$options[$N] = $this->prepare_uploads(maybe_unserialize($row->secondary_item_id)); $N++;
				$options[$N] = $this->prepare_uploads(maybe_unserialize($row->date_recorded)); $N++;
			}
		}
		
		$rows = $wpdb->get_results('SELECT * FROM ' . $wpdb->prefix . 'bp_activity_meta');
		$options[$N] =  $this->prepare_uploads(maybe_unserialize('new_table_wp_bp_activity_meta')); $N++;
		if (count($rows) > 0) {
			foreach ($rows as $row) {
				$options[$N] = $this->prepare_uploads(maybe_unserialize($row->id)); $N++;
				$options[$N] = $this->prepare_uploads(maybe_unserialize($row->activity_id)); $N++;
				$options[$N] = $this->prepare_uploads(maybe_unserialize($row->meta_key)); $N++;
				$options[$N] = $this->prepare_uploads(maybe_unserialize($row->meta_value)); $N++;
			}
		}
		
		$rows = $wpdb->get_results('SELECT * FROM ' . $wpdb->prefix . 'bp_notifications');
		$options[$N] =  $this->prepare_uploads(maybe_unserialize('new_table_wp_bp_notifications')); $N++;
		if (count($rows) > 0) {
			foreach ($rows as $row) {
				$options[$N] = $this->prepare_uploads(maybe_unserialize($row->id)); $N++;
				$options[$N] = $this->prepare_uploads(maybe_unserialize($row->user_id)); $N++;
				$options[$N] = $this->prepare_uploads(maybe_unserialize($row->item_id)); $N++;
				$options[$N] = $this->prepare_uploads(maybe_unserialize($row->secondary_item_id)); $N++;
				$options[$N] = $this->prepare_uploads(maybe_unserialize($row->component_name)); $N++;
				$options[$N] = $this->prepare_uploads(maybe_unserialize($row->component_action)); $N++;
				$options[$N] = $this->prepare_uploads(maybe_unserialize($row->date_notified)); $N++;
				$options[$N] = $this->prepare_uploads(maybe_unserialize($row->is_new)); $N++;
			}
		}
		
		$rows = $wpdb->get_results('SELECT * FROM ' . $wpdb->prefix . 'bp_xprofile_data');
		$options[$N] =  $this->prepare_uploads(maybe_unserialize('new_table_wp_bp_xprofile_data')); $N++;
		if (count($rows) > 0) {
			foreach ($rows as $row) {
				$options[$N] = $this->prepare_uploads(maybe_unserialize($row->id)); $N++;
				$options[$N] = $this->prepare_uploads(maybe_unserialize($row->field_id)); $N++;
				$options[$N] = $this->prepare_uploads(maybe_unserialize($row->user_id)); $N++;
				$options[$N] = $this->prepare_uploads(($row->value)); $N++;
				$options[$N] = $this->prepare_uploads(maybe_unserialize($row->last_updated)); $N++;
			}
		}
		
		$rows = $wpdb->get_results('SELECT * FROM ' . $wpdb->prefix . 'bp_xprofile_fields');
		$options[$N] =  $this->prepare_uploads(maybe_unserialize('new_table_wp_bp_xprofile_fields')); $N++;
		if (count($rows) > 0) {
			foreach ($rows as $row) {
				$options[$N] = $this->prepare_uploads(maybe_unserialize($row->id)); $N++;
				$options[$N] = $this->prepare_uploads(maybe_unserialize($row->group_id)); $N++;
				$options[$N] = $this->prepare_uploads(maybe_unserialize($row->parent_id)); $N++;
				$options[$N] = $this->prepare_uploads(maybe_unserialize($row->type)); $N++;
				$options[$N] = $this->prepare_uploads(maybe_unserialize($row->name)); $N++;
				$options[$N] = $this->prepare_uploads(maybe_unserialize($row->description)); $N++;
				$options[$N] = $this->prepare_uploads(maybe_unserialize($row->is_required)); $N++;
				$options[$N] = $this->prepare_uploads(maybe_unserialize($row->is_default_option)); $N++;
				$options[$N] = $this->prepare_uploads(maybe_unserialize($row->field_order)); $N++;
				$options[$N] = $this->prepare_uploads(maybe_unserialize($row->option_order)); $N++;
				$options[$N] = $this->prepare_uploads(maybe_unserialize($row->order_by)); $N++;
				$options[$N] = $this->prepare_uploads(maybe_unserialize($row->can_delete)); $N++;
			}
		}
		
		$rows = $wpdb->get_results('SELECT * FROM ' . $wpdb->prefix . 'bp_xprofile_meta');
		$options[$N] =  $this->prepare_uploads(maybe_unserialize('new_table_wp_bp_xprofile_meta')); $N++;
		if (count($rows) > 0) {
			foreach ($rows as $row) {
				$options[$N] = $this->prepare_uploads(maybe_unserialize($row->id)); $N++;
				$options[$N] = $this->prepare_uploads(maybe_unserialize($row->object_id)); $N++;
				$options[$N] = $this->prepare_uploads(maybe_unserialize($row->object_type)); $N++;
				$options[$N] = $this->prepare_uploads(maybe_unserialize($row->meta_key)); $N++;
				$options[$N] = $this->prepare_uploads(maybe_unserialize($row->meta_value)); $N++;
			}
		}
		
		$rows = $wpdb->get_results('SELECT * FROM ' . $wpdb->prefix . 'bro_images');
		$options[$N] =  $this->prepare_uploads(maybe_unserialize('new_table_wp_bro_images')); $N++;
		if (count($rows) > 0) {
			foreach ($rows as $row) {
				$options[$N] = $this->prepare_uploads(maybe_unserialize($row->id)); $N++;
				$options[$N] = $this->prepare_uploads(maybe_unserialize($row->native_path)); $N++;
				$options[$N] = $this->prepare_uploads(maybe_unserialize($row->native_url)); $N++;
				$options[$N] = $this->prepare_uploads(maybe_unserialize($row->path)); $N++;
				$options[$N] = $this->prepare_uploads(maybe_unserialize($row->url)); $N++;
				$options[$N] = $this->prepare_uploads(maybe_unserialize($row->width)); $N++;
				$options[$N] = $this->prepare_uploads(maybe_unserialize($row->height)); $N++;
			}
		}
	
		$rows = $wpdb->get_results("SELECT * FROM " . $wpdb->prefix . "posts WHERE post_type = 'topic' OR post_type = 'reply' OR post_type = 'buddydrive-file'");
		$options[$N] =  $this->prepare_uploads(maybe_unserialize('new_table_wp_posts')); $N++;
		if (count($rows) > 0) {
			foreach ($rows as $row) {
				$options[$N] = $this->prepare_uploads(maybe_unserialize($row->ID)); $N++;
				$options[$N] = $this->prepare_uploads(maybe_unserialize($row->post_author)); $N++;
			}
		}
		
		$this->export_bbpress = base64_encode(str_replace($this->options['domain_dev'], $this->options['domain_demo'], serialize($options)));

		$wpdb->suppress_errors( $suppress );


		// Export booking
		global $wpdb;
		$rows = $wpdb->get_results('SELECT * FROM ' . $wpdb->prefix . 'booking');
		$N=0;
		$options = array();
		if (count($rows) > 0) {
			foreach ($rows as $row) {
				$options[$N] = $this->prepare_uploads(maybe_unserialize($row->booking_id)); $N++;
				$options[$N] = $this->prepare_uploads(maybe_unserialize($row->is_new));  $N++;
				$options[$N] = $this->prepare_uploads(maybe_unserialize($row->sort_date)); $N++;
				$options[$N] = $this->prepare_uploads(maybe_unserialize($row->modification_date)); $N++;
				$options[$N] = $this->prepare_uploads(maybe_unserialize($row->form)); $N++;
				$options[$N] = $this->prepare_uploads(maybe_unserialize($row->booking_type)); $N++;
			}
		}
		
		$rows = $wpdb->get_results('SELECT * FROM ' . $wpdb->prefix . 'bookingdates');
		$options[$N] =  $this->prepare_uploads(maybe_unserialize('new_table_wp_bookingdates')); $N++;
		if (count($rows) > 0) {
			foreach ($rows as $row) {
				$options[$N] = $this->prepare_uploads(maybe_unserialize($row->booking_id)); $N++;
				$options[$N] = $this->prepare_uploads(maybe_unserialize($row->booking_date)); $N++;
				$options[$N] = $this->prepare_uploads(maybe_unserialize($row->approved)); $N++;
			}
		}
		
		$rows = $wpdb->get_results('SELECT * FROM ' . $wpdb->prefix . 'booking_calendars');
		$options[$N] =  $this->prepare_uploads(maybe_unserialize('new_table_wp_booking_calendars')); $N++;
		if (count($rows) > 0) {
			foreach ($rows as $row) {
				$options[$N] = $this->prepare_uploads(maybe_unserialize($row->calendar_id)); $N++;
				$options[$N] = $this->prepare_uploads(maybe_unserialize($row->category_id)); $N++;
				$options[$N] = $this->prepare_uploads(maybe_unserialize($row->calendar_title)); $N++;
				$options[$N] = $this->prepare_uploads(maybe_unserialize($row->calendar_email)); $N++;
				$options[$N] = $this->prepare_uploads(maybe_unserialize($row->calendar_active)); $N++;
			}
		}
		
		$rows = $wpdb->get_results('SELECT * FROM ' . $wpdb->prefix . 'booking_categories');
		$options[$N] =  $this->prepare_uploads(maybe_unserialize('new_table_wp_booking_categories')); $N++;
		if (count($rows) > 0) {
			foreach ($rows as $row) {
				$options[$N] = $this->prepare_uploads(maybe_unserialize($row->category_id)); $N++;
				$options[$N] = $this->prepare_uploads(maybe_unserialize($row->category_name)); $N++;
			}
		}
		
		
		$rows = $wpdb->get_results('SELECT * FROM ' . $wpdb->prefix . 'booking_config');
		$options[$N] =  $this->prepare_uploads(maybe_unserialize('new_table_wp_booking_config')); $N++;
		if (count($rows) > 0) {
			foreach ($rows as $row) {
				$options[$N] = $this->prepare_uploads(maybe_unserialize($row->config_id)); $N++;
				$options[$N] = $this->prepare_uploads(maybe_unserialize($row->config_name)); $N++;
				$options[$N] = $this->prepare_uploads(maybe_unserialize($row->config_value)); $N++;
			}
		}
		
		$rows = $wpdb->get_results('SELECT * FROM ' . $wpdb->prefix . 'booking_slots');
		$options[$N] =  $this->prepare_uploads(maybe_unserialize('new_table_wp_booking_slots')); $N++;
		if (count($rows) > 0) {
			foreach ($rows as $row) {
				$options[$N] = $this->prepare_uploads(maybe_unserialize($row->slot_id)); $N++;
				$options[$N] = $this->prepare_uploads(maybe_unserialize($row->slot_special_text)); $N++;
				$options[$N] = $this->prepare_uploads(maybe_unserialize($row->slot_special_mode)); $N++;
				$options[$N] = $this->prepare_uploads(maybe_unserialize($row->slot_date)); $N++;
				$options[$N] = $this->prepare_uploads(maybe_unserialize($row->slot_time_from)); $N++;
				$options[$N] = $this->prepare_uploads(maybe_unserialize($row->slot_time_to)); $N++;
				$options[$N] = $this->prepare_uploads(maybe_unserialize($row->slot_price)); $N++;
				$options[$N] = $this->prepare_uploads(maybe_unserialize($row->slot_av)); $N++;
				$options[$N] = $this->prepare_uploads(maybe_unserialize($row->slot_av_max)); $N++;
				$options[$N] = $this->prepare_uploads(maybe_unserialize($row->slot_active)); $N++;
				$options[$N] = $this->prepare_uploads(maybe_unserialize($row->calendar_id)); $N++;          
			}
		}
		
		$this->export_booking = base64_encode(str_replace($this->options['domain_dev'], $this->options['domain_demo'], serialize($options)));

		$wpdb->suppress_errors( $suppress );
	}
	
	
	//-----------------------------------------------------------------------------------
	// Import dummy data
	//-----------------------------------------------------------------------------------
	function importer() {
		?>
		<p>&nbsp;</p>
		<div class="error">
			<h4><?php echo __('Import progress:', 'themerex') . ' <span id="import_progress_value">' . ($this->last_slider > 0 ? 99 : $this->result) . '</span>%'; ?></h4>
			<p><?php echo __('Status:', 'themerex'); ?> <span id="import_progress_status"></span></p>
			<p><?php echo __('Data import can take a long time (sometimes more than 10 minutes)!', 'themerex')
				. '<br>' . __('Please wait until the end of the procedure, do not navigate away from the page!', 'themerex'); ?></p>
		</div>
		<p>&nbsp;</p>
		<?php
		// Import posts, pages, menu items, etc.
		$result = 100;
		if ($this->options['import_posts'] && (empty($this->last_slider) || !$this->options['import_sliders'])) {
			// Load WP Importer class
			if ( !defined('WP_LOAD_IMPORTERS') ) define('WP_LOAD_IMPORTERS', true); // we are loading importers
			if ( !class_exists('WP_Import') ) {
				require(themerex_get_file_dir('core/core.importer/wordpress-importer.php'));
			}
			if ( class_exists( 'WP_Import' ) ) {
				$result = $this->import_posts();
				if ($result >= 100) {
					if (in_array('woocommerce', $this->options['required_plugins']))
						$this->setup_woocommerce_pages();
					$this->setup_menus();
				} else {
					$log = explode('|', themerex_fgc($this->import_log));
					$this->import_last_id = (int) $log[0];
				}
			}
		}

		// Import Theme Options
		if ($result>=100 && $this->options['import_to'] && (empty($this->last_slider) || !$this->options['import_sliders'])) {
			themerex_options_reset();
			$this->import_theme_options();
		}

		// Import Widgets
		if ($result>=100 && $this->options['import_widgets'] && (empty($this->last_slider) || !$this->options['import_sliders']))
			$this->import_widgets(); 
		
		// Import Timeline
		if ($result>=100 && $this->options['import_timeline'] && (empty($this->last_slider) || !$this->options['import_sliders']))
		$this->import_timeline();
	
		// Import Grid
		if ($result>=100 && $this->options['import_grid'] && (empty($this->last_slider) || !$this->options['import_sliders']))
		$this->import_grid();
	
	   // Import Events
		if ($result>=100 && $this->options['import_events'] && (empty($this->last_slider) || !$this->options['import_sliders']))
		$this->import_events();

	   // Import Users
		if ($result>=100 && $this->options['import_users'] && (empty($this->last_slider) || !$this->options['import_sliders']))
		$this->import_users();
	
	   // Import Chart
		if ($result>=100 && $this->options['import_chart'] && (empty($this->last_slider) || !$this->options['import_sliders']))
		$this->import_chart();

    	// Import Quiz
		if ($result>=100 && $this->options['import_quiz'] && (empty($this->last_slider) || !$this->options['import_sliders']))
		$this->import_quiz();

    	// Import Bbpress
		if ($result>=100 && $this->options['import_bbpress'] && (empty($this->last_slider) || !$this->options['import_sliders']))
		$this->import_bbpress();

		// Import Booking
		if ($result>=100 && $this->options['import_booking'] && (empty($this->last_slider) || !$this->options['import_sliders']))
		$this->import_booking();

		// Import Sliders
		if ($result>=100 && $this->options['import_sliders'])
			$this->import_sliders();

		// Setup Front page and Blog page
		if ($result>=100 && $this->options['import_posts'] && (empty($this->last_slider) || !$this->options['import_sliders'])) {
			// Set reading options
			$home_page = get_page_by_title( $this->options['page_on_front'] );
			$posts_page = get_page_by_title( $this->options['page_for_posts'] );
			if ($home_page->ID && $posts_page->ID) {
				update_option('show_on_front', $this->options['show_on_front']);
				update_option('page_on_front', $home_page->ID); 	// Front Page
				update_option('page_for_posts', $posts_page->ID);	// Blog Page
			}

			// Flush rules after install
			flush_rewrite_rules();
		}
		// finally redirect to success page
		if ($result >= 100 && (empty($this->last_slider) || !$this->options['import_sliders'])) 
			$this->success = __('Congratulations! Import demo data finished successfull!', 'themerex');
		else {
			$this->error = '<h4>' . sprintf(__('Import progress: %s.', 'themerex'), $result.'%') . '</h4>'
				. __('Due to the expiration of the time limit for the execution of scripts on your server, the import process is interrupted!', 'themerex')
				. '<br>' . __('After 3 seconds, the import will continue automatically!', 'themerex');
			$this->result = $result;
		}
	}
	
	//==========================================================================================
	// Utilities
	//==========================================================================================

	// Check for required plugings
	function checkRequiredPlugins() {
		$not_installed = '';
		if (in_array('visual_composer', $this->options['required_plugins']) && $_POST['data_type']=='vc' && !themerex_exists_visual_composer() )
			$not_installed .= '<br>Visual Composer';
		if (in_array('woocommerce', $this->options['required_plugins']) && !themerex_exists_woocommerce() )
			$not_installed .= '<br>WooCommerce';
		if (in_array('revslider', $this->options['required_plugins']) && !themerex_exists_revslider())
			$not_installed .= '<br>Revolution Slider';
		if (in_array('instagram', $this->options['required_plugins']) && !themerex_exists_instagram())
			$not_installed .= '<br>Instagram Widget';
		$not_installed = apply_filters('themerex_filter_importer_required_plugins', $not_installed);
		if ($not_installed) {
			$this->error = '<b>'.__('Attention! For correct installation of the demo data, you must install and activate the following plugins: ', 'themerex').'</b>'.($not_installed);
			$this->options['enable_importer'] = false;
			return false;
		}
		return true;
	}


	// Import XML file with posts data
	function import_posts() {
		if (empty($this->options['file_with_content'][$this->options['data_type']])) return;
		echo ($this->import_last_id == 0 
			? '<h3>'.__('Start Import', 'themerex').'</h3>'
			: '<h3>'.sprintf(__('Continue Import from ID=%d', 'themerex'), $this->import_last_id).'</h3>');
		echo '<b>' . __('Import Posts (pages, menus, attachments, etc) ...', 'themerex').'</b><br>'; flush();
		$theme_xml = themerex_get_file_dir($this->options['file_with_content'][$this->options['data_type']]);
		$importer = new WP_Import();
		$importer->fetch_attachments = $this->options['upload_attachments'];
		$importer->overwrite = $this->options['overwrite_content'];
		$importer->debug = $this->options['debug'];
		$importer->uploads_folder = $this->options['uploads_folder'];
		$importer->demo_url = $this->options['demo_url'];
		$importer->start_from_id = $this->import_last_id;
		$importer->import_log = $this->import_log;
		if ($this->import_last_id == 0) $this->clear_tables();
		$this->prepare_taxonomies();
		if (!$this->options['debug']) ob_start();
		$result = $importer->import($theme_xml);
		if (!$this->options['debug']) ob_end_clean();
		if ($result>=100) themerex_fpc($this->import_log, '');
		return $result;
	}
	
	
	// Delete all data from tables
	function clear_tables() {
		global $wpdb;
		if ($this->options['overwrite_content']) {
			echo '<br><b>'.__('Clear tables ...', 'themerex').'</b><br>'; flush();
			if ($this->options['import_posts']) {
				$res = $wpdb->query("TRUNCATE TABLE " . esc_sql($wpdb->comments));
				if ( is_wp_error( $res ) ) echo __( 'Failed truncate table "comments".', 'themerex' ) . ' ' . ($res->get_error_message()) . '<br />';
				$res = $wpdb->query("TRUNCATE TABLE " . esc_sql($wpdb->commentmeta));
				if ( is_wp_error( $res ) ) echo __( 'Failed truncate table "commentmeta".', 'themerex' ) . ' ' . ($res->get_error_message()) . '<br />';
				$res = $wpdb->query("TRUNCATE TABLE " . esc_sql($wpdb->postmeta));
				if ( is_wp_error( $res ) ) echo __( 'Failed truncate table "postmeta".', 'themerex' ) . ' ' . ($res->get_error_message()) . '<br />';
				$res = $wpdb->query("TRUNCATE TABLE " . esc_sql($wpdb->posts));
				if ( is_wp_error( $res ) ) echo __( 'Failed truncate table "posts".', 'themerex' ) . ' ' . ($res->get_error_message()) . '<br />';
				$res = $wpdb->query("TRUNCATE TABLE " . esc_sql($wpdb->terms));
				if ( is_wp_error( $res ) ) echo __( 'Failed truncate table "terms".', 'themerex' ) . ' ' . ($res->get_error_message()) . '<br />';
				$res = $wpdb->query("TRUNCATE TABLE " . esc_sql($wpdb->term_relationships));
				if ( is_wp_error( $res ) ) echo __( 'Failed truncate table "term_relationships".', 'themerex' ) . ' ' . ($res->get_error_message()) . '<br />';
				$res = $wpdb->query("TRUNCATE TABLE " . esc_sql($wpdb->term_taxonomy));
				if ( is_wp_error( $res ) ) echo __( 'Failed truncate table "term_taxonomy".', 'themerex' ) . ' ' . ($res->get_error_message()) . '<br />';
			}
		}
	}

	
	// Prepare additional taxes
	function prepare_taxonomies() {
		if (isset($this->options['taxonomies']) && is_array($this->options['taxonomies']) && count($this->options['taxonomies']) > 0) {
			foreach ($this->options['taxonomies'] as $type=>$tax) {
				themerex_require_data( 'taxonomy', $tax, array(
					'post_type'			=> array( $type ),
					'hierarchical'		=> false,
					'query_var'			=> $tax,
					'rewrite'			=> true,
					'public'			=> false,
					'show_ui'			=> false,
					'show_admin_column'	=> false,
					'_builtin'			=> false
					)
				);
			}
		}
	}
	

	// Set WooCommerce pages
	function setup_woocommerce_pages() {
		foreach ($this->options['wooc_pages'] as $woo_page_name => $woo_page_title) {
			$woopage = get_page_by_title( $woo_page_title );
			if ($woopage->ID) {
				update_option($woo_page_name, $woopage->ID);
			}
		}
		// We no longer need to install pages
		delete_option( '_wc_needs_pages' );
		delete_transient( '_wc_activation_redirect' );
	}


	// Set imported menus to registered theme locations
	function setup_menus() {
		echo '<script>'
			. 'document.getElementById("import_progress_status").innerHTML = "' . __('Setup menus ...', 'themerex') .'";'
			. '</script>';
		echo '<br><b>'.__('Setup menus ...', 'themerex').'</b><br>'; flush();
		$locations = get_theme_mod( 'nav_menu_locations' );
		$menus = wp_get_nav_menus();
		if ($menus) {
			foreach ($menus as $menu) {
				foreach ($this->options['menus'] as $loc=>$name) {
					if ($menu->name == $name)
						$locations[$loc] = $menu->term_id;
				}
			}
			set_theme_mod( 'nav_menu_locations', $locations );
		}
	}


	// Import theme options
	function import_theme_options() {
		if (empty($this->options['file_with_options'])) return;
		echo '<script>'
			. 'document.getElementById("import_progress_status").innerHTML = "' . __('Import Theme Options ...', 'themerex') .'";'
			. '</script>';
		echo '<br><b>'.__('Import Theme Options ...', 'themerex').'</b><br>'; flush();
		$theme_options_txt = themerex_fgc(themerex_get_file_dir($this->options['file_with_options']));
		$data = unserialize( base64_decode( $theme_options_txt) );
		// Replace upload url in options
		if (count($data) > 0) {
			foreach ($data as $k=>$v) {
				if (is_array($v) && count($v) > 0) {
					foreach ($v as $k1=>$v1) {
						$v[$k1] = $this->replace_uploads($v1);
					}
				} else
					$v = $this->replace_uploads($v);
				update_option( $k, $v );
			}
		}
		themerex_load_main_options();
	}


	// Import post meta options
	function import_postmeta() {
		if (empty($this->options['file_with_postmeta'])) return;
		$theme_options_txt = themerex_fgc(themerex_get_file_dir($this->options['file_with_postmeta']));
		$data = unserialize( base64_decode( $theme_options_txt) );
		// Replace upload url in options
		foreach ($data as $k=>$v) {
			foreach ($v as $k1=>$v1) {
				$v[$k1] = $this->replace_uploads($v1);
			}
			update_post_meta( $k, $v['key'], $v['value'] );
		}
	}


	// Import widgets
	function import_widgets() {
		if (empty($this->options['file_with_widgets'])) return;
		echo '<script>'
			. 'document.getElementById("import_progress_status").innerHTML = "' . __('Import Widgets ...', 'themerex') .'";'
			. '</script>';
		echo '<br><b>'.__('Import Widgets ...', 'themerex').'</b><br>'; flush();
		$widgets_txt = themerex_fgc(themerex_get_file_dir($this->options['file_with_widgets']));
		$data = unserialize( base64_decode( $widgets_txt ) );
		foreach ($data as $k=>$v) {
			update_option( $k, $this->replace_uploads($v) );
		}
	}

	
	// Import Timeline
	function import_timeline() {
		if (empty($this->options['file_with_timeline'])) return;
		echo '<script>'
			. 'document.getElementById("import_progress_status").innerHTML = "' . __('Import Timeline ...', 'themerex') .'";'
			. '</script>';
		echo '<br><b>'.__('Import Timeline ...', 'themerex').'</b><br>'; flush();
		$timeline_txt = themerex_fgc(themerex_get_file_dir($this->options['file_with_timeline']));
		$data = unserialize( base64_decode( $timeline_txt ) );
		global $wpdb;
		if ($this->options['overwrite_content']) $wpdb->query("DELETE FROM wp_ctimelines");
		$count = count($data);
		for($i = 0; $i < $count; $i++)
		{
			$id = $data[$i];
			$name = $data[$i + 1];
			$settings = $data[$i + 2];
			$items = $data[$i + 3]; 
				
			$is_row = $wpdb->get_row("SELECT * FROM wp_ctimelines WHERE id = ".$id);
			if ($is_row != null) {
				$wpdb->update( 
					'wp_ctimelines', 
					array( 'name' => $name, 
						   'settings' => $settings, 
						   'items' => $items 
						   ), 
					array( 'ID' => $id ), 
					array( '%s', '%s', '%s'	), 
					array( '%d' ) 
				);
			} else {
				$wpdb->insert( 
					'wp_ctimelines', 
					array( 'id' => $id, 
						   'name' => $name, 
						   'settings' => $settings, 
						   'items' => $items 
					), 
					array( '%d', '%s', '%s', '%s' ) 
				);
			}
			$i = $i + 3;
		}
	}
	
		
	// Import Grid
	function import_grid() {
		if (empty($this->options['file_with_grid'])) return;
		echo '<script>'
			. 'document.getElementById("import_progress_status").innerHTML = "' . __('Import Essential Grid ...', 'themerex') .'";'
			. '</script>';
		echo '<br><b>'.__('Import Essential Grid ...', 'themerex').'</b><br>'; flush();
		$grid_txt = themerex_fgc(themerex_get_file_dir($this->options['file_with_grid']));
		$data = unserialize( base64_decode( $grid_txt ) );
		global $wpdb;
		if ($this->options['overwrite_content']) $wpdb->query("DELETE FROM wp_eg_grids");
		$count = count($data);
		for($i = 0; $i < $count; $i++)
		{
			$id = $data[$i];
			$name = $data[$i + 1];
			$handle = $data[$i + 2];
			$postparams = $data[$i + 3]; 
			$params = $data[$i + 4];
			$layers = $data[$i + 5];
				
			$is_row = $wpdb->get_row("SELECT * FROM wp_eg_grids WHERE id = ".$id);
			if ($is_row != null) { 
				$wpdb->update( 
					'wp_eg_grids', 
					array(  'name' => $name, 
							'handle' => $handle, 
							'postparams' => $postparams, 
							'params' => $params, 
							'layers' => $layers
					), 
					array( 'ID' => $id ), 
					array( '%s', '%s', '%s', '%s', '%s'	), 
					array( '%d' ) 
				);
			} else { 
				$wpdb->insert( 
					'wp_eg_grids', 
					array(  'id' => $id, 
							'name' => $name, 
							'handle' => $handle, 
							'postparams' => $postparams, 
							'params' => $params, 
							'layers' => $layers
					), 
					array( '%d', '%s', '%s', '%s', '%s', '%s') 
				);
			}
			$i = $i + 5;
		}
	}
	
	
	// Import Events
	function import_events() {
		if (empty($this->options['file_with_events'])) return;
		echo '<script>'
			. 'document.getElementById("import_progress_status").innerHTML = "' . __('Import Events ...', 'themerex') .'";'
			. '</script>';
		echo '<br><b>'.__('Import Events ...', 'themerex').'</b><br>'; flush();
		$events_txt = themerex_fgc(themerex_get_file_dir($this->options['file_with_events']));
		$data = unserialize( base64_decode( $events_txt ) );
		global $wpdb;
		if ($this->options['overwrite_content']) $wpdb->query("DELETE FROM wp_eo_events");
		$count = count($data);
		for($i = 0; $i < $count; $i++)
		{
			$event_id = $data[$i];
			$post_id = $data[$i + 1];
			$StartDate = $data[$i + 2];
			$EndDate = $data[$i + 3]; 
			$StartTime = $data[$i + 4];
			$FinishTime = $data[$i + 5];
				
			$is_row = $wpdb->get_row("SELECT * FROM wp_eo_events WHERE post_id = ".$event_id);
			if ($is_row != null) { 
				$wpdb->update( 
					'wp_eo_events', 
					array( 'post_id' => $post_id, 'StartDate' => $StartDate, 'EndDate' => $EndDate, 'StartTime' => $StartTime, 'FinishTime' => $FinishTime), 
					array( 'ID' => $event_id ), 
					array( '%d', '%s', '%s', '%s', '%s'	), 
					array( '%d' ) 
				);
			} else { 
				$wpdb->insert( 
					'wp_eo_events', 
					array( 'event_id' => $event_id, 'post_id' => $post_id, 'StartDate' => $StartDate, 'EndDate' => $EndDate, 'StartTime' => $StartTime, 'FinishTime' => $FinishTime), 
					array( '%d', '%d', '%s', '%s', '%s', '%s') 
				);
			}
			$i = $i + 5;
		}
	}
	
	
	// Import Users
	function import_users() {
		if (empty($this->options['file_with_users'])) return;
		echo '<script>'
			. 'document.getElementById("import_progress_status").innerHTML = "' . __('Import Users ...', 'themerex') .'";'
			. '</script>';
		echo '<br><b>'.__('Import Users ...', 'themerex').'</b><br>'; flush();
		$users_txt = themerex_fgc(themerex_get_file_dir($this->options['file_with_users']));
		$data = unserialize( base64_decode( $users_txt ) );
		global $wpdb;
		$count = count($data);
		for($i = 0; $i < $count; $i++)
		{
			$ID = $data[$i]; 
			$user_login = $data[$i + 1];
			$user_pass = $data[$i + 2];
			$user_nicename = $data[$i + 3]; 
			$user_email = $data[$i + 4];
			$display_name = $data[$i + 5];
				
			$is_row = $wpdb->get_row("SELECT * FROM wp_users WHERE ID = ".$ID);
			if ($is_row != null) { 
				$wpdb->update( 
					'wp_users', 
					array( 'user_login' => $user_login, 'user_pass' => $user_pass, 'user_nicename' => $user_nicename, 'user_email' => $user_email, 'display_name' => $display_name), 
					array( 'ID' => $ID ), 
					array( '%s', '%s', '%s', '%s', '%s'	), 
					array( '%d' ) 
				);
			} else { 
				$wpdb->insert( 
					'wp_users', 
					array( 'ID' => $ID, 'user_login' => $user_login, 'user_pass' => $user_pass, 'user_nicename' => $user_nicename, 'user_email' => $user_email, 'display_name' => $display_name), 
					array( '%d', '%s', '%s', '%s', '%s', '%s') 
				);
			}
			$i = $i + 5;
		}
	}
	
	
	// Import Chart
	function import_chart() {
		if (empty($this->options['file_with_chart'])) return;
		echo '<script>'
			. 'document.getElementById("import_progress_status").innerHTML = "' . __('Import Chart ...', 'themerex') .'";'
			. '</script>';
		echo '<br><b>'.__('Import Chart ...', 'themerex').'</b><br>'; flush();
		$chart_txt = themerex_fgc(themerex_get_file_dir($this->options['file_with_chart']));
		$data = unserialize( base64_decode( $chart_txt ) );
		
		global $wpdb;
		if ($this->options['overwrite_content'])
		{	
			$wpdb->query("DELETE FROM wp_weblator_polls");
			$wpdb->query("DELETE FROM wp_weblator_poll_options");
			$wpdb->query("DELETE FROM wp_weblator_poll_votes");
		}
		$count = count($data);
		$table_options = $table_votes = false;
		
		$table_options =  array_search('new_table_wp_weblator_poll_options', $data);
		$table_votes = array_search('new_table_wp_weblator_poll_votes', $data);
		
		for($i = 0; $i < $count; $i++)
		{	
			if($i < $table_options)
			{
				$id = $data[$i];
				$poll_name = $data[$i + 1];
				$poll_chart_type = $data[$i + 2];
				$poll_allow_view_results = $data[$i + 3]; 
				$poll_one_vote = $data[$i + 4];
				$poll_one_vote_userid = $data[$i + 5];
				$poll_one_vote_cookie = $data[$i + 6];
				$poll_one_vote_ip = $data[$i + 7];
				$poll_is_live = $data[$i + 8];
				$poll_max_width = $data[$i + 9];
				$poll_min_width = $data[$i + 10];
				$chart_legend = $data[$i + 11];
				$chart_legend_position = $data[$i + 12];
				$chart_legend_font_size = $data[$i + 13];
				$chart_legend_font_style = $data[$i + 14];
				$chart_legend_font_colour = $data[$i + 15];
					
				$is_row = $wpdb->get_row("SELECT * FROM wp_weblator_polls WHERE id = ".$id);
				if ($is_row != null) { 
					$wpdb->update( 
						'wp_weblator_polls', 
						array( 'poll_name' => $poll_name, 'poll_chart_type' => $poll_chart_type, 'poll_allow_view_results' => $poll_allow_view_results, 'poll_one_vote' => $poll_one_vote, 'poll_one_vote_userid' => $poll_one_vote_userid, 'poll_one_vote_userid' => $poll_one_vote_userid, 'poll_one_vote_cookie' => $poll_one_vote_cookie, 'poll_one_vote_ip' => $poll_one_vote_ip 					, 'poll_is_live' => $poll_is_live, 'poll_max_width' => $poll_max_width, 'poll_min_width' => $poll_min_width, 'chart_legend' => $chart_legend, 'chart_legend_position' => $chart_legend_position, 'chart_legend_font_size' => $chart_legend_font_size, 'chart_legend_font_style' => $chart_legend_font_style, 'chart_legend_font_colour' => $chart_legend_font_colour), 
						array( 'ID' => $id ), 
						array( '%s', '%d', '%d', '%d', '%d', '%d', '%d', '%d', '%d', '%d', '%d', '%s', '%d', '%s', '%s'	), 
						array( '%d' ) 
					);
				} else { 
					$wpdb->insert( 
						'wp_weblator_polls', 
						array( 'id' => $id, 'poll_name' => $poll_name, 'poll_chart_type' => $poll_chart_type, 'poll_allow_view_results' => $poll_allow_view_results, 'poll_one_vote' => $poll_one_vote, 'poll_one_vote_userid' => $poll_one_vote_userid, 'poll_one_vote_userid' => $poll_one_vote_userid, 'poll_one_vote_cookie' => $poll_one_vote_cookie, 'poll_one_vote_ip' => $poll_one_vote_ip 					, 'poll_is_live' => $poll_is_live, 'poll_max_width' => $poll_max_width, 'poll_min_width' => $poll_min_width, 'chart_legend' => $chart_legend, 'chart_legend_position' => $chart_legend_position, 'chart_legend_font_size' => $chart_legend_font_size, 'chart_legend_font_style' => $chart_legend_font_style, 'chart_legend_font_colour' => $chart_legend_font_colour), 
						array( '%d', '%s', '%d', '%d', '%d', '%d', '%d', '%d', '%d', '%d', '%d', '%d', '%s', '%d', '%s', '%s') 
					);
				}
				$i = $i + 15;
			}
			
			if($i > $table_options && $i < $table_votes)
			{
				$id = $data[$i];
				$poll_id = $data[$i + 1];
				$option_value = $data[$i + 2];
				$option_order = $data[$i + 3]; 
				$option_colour = $data[$i + 4];
					
				$is_row = $wpdb->get_row("SELECT * FROM wp_weblator_poll_options WHERE id = ".$id);
				if ($is_row != null) { 
					$wpdb->update( 
						'wp_weblator_poll_options', 
						array( 'poll_id' => $poll_id, 'option_value' => $option_value, 'option_order' => $option_order, 'option_colour' => $option_colour), 
						array( 'ID' => $id ), 
						array( '%d', '%s', '%d', '%s'), 
						array( '%d' ) 
					);
				} else { 
					$wpdb->insert( 
						'wp_weblator_poll_options', 
						array( 'id' => $id, 'poll_id' => $poll_id, 'option_value' => $option_value, 'option_order' => $option_order, 'option_colour' => $option_colour), 
						array( '%d', '%d', '%s', '%d', '%s') 
					);
				}
				$i = $i + 4;
			}
			
			if($i > $table_votes)
			{
				$id = $data[$i];
				$poll_id = $data[$i + 1];
				$option_id = $data[$i + 2];
				$user_id = $data[$i + 3]; 
				$ipv4 = $data[$i + 4];
					
				$is_row = $wpdb->get_row("SELECT * FROM wp_weblator_poll_votes WHERE id = ".$id);
				if ($is_row != null) { 
					$wpdb->update( 
						'wp_weblator_poll_votes', 
						array( 'poll_id' => $poll_id, 'option_id' => $option_id, 'user_id' => $user_id, 'ipv4' => $ipv4), 
						array( 'ID' => $id ), 
						array( '%d', '%d', '%d', '%s'), 
						array( '%d' ) 
					);
				} else { 
					$wpdb->insert( 
						'wp_weblator_poll_votes', 
						array( 'id' => $id, 'poll_id' => $poll_id, 'option_id' => $option_id, 'user_id' => $user_id, 'ipv4' => $ipv4), 
						array( '%d', '%d', '%d', '%d', '%s') 
					);
				}
				$i = $i + 4;
			}
		}
	}


	// Import Quiz
	function import_quiz() {
		if (empty($this->options['file_with_quiz'])) return;
		echo '<script>'
			. 'document.getElementById("import_progress_status").innerHTML = "' . __('Import Quiz ...', 'themerex') .'";'
			. '</script>';
		echo '<br><b>'.__('Import Quiz ...', 'themerex').'</b><br>'; flush();
		$quiz_txt = themerex_fgc(themerex_get_file_dir($this->options['file_with_quiz']));
		$data = unserialize( base64_decode( $quiz_txt ) );
		
		global $wpdb;
		if ($this->options['overwrite_content'])
		{	
			$wpdb->query("DELETE FROM wp_wp_pro_quiz_master");
			$wpdb->query("DELETE FROM wp_wp_pro_quiz_question");
		}
		$count = count($data);
		$table_question =  array_search('new_table_wp_wp_pro_quiz_question', $data);
		
		for($i = 0; $i < $count; $i++)
		{	
			if($i < $table_question)
			{
				$id = $data[$i];
				$name = $data[$i + 1];
				$text = $data[$i + 2];
				$time_limit = $data[$i + 3]; 
				$statistics_ip_lock = $data[$i + 4];
				$quiz_run_once_type = $data[$i + 5];
				$show_max_question_value = $data[$i + 6];
				$toplist_data = $data[$i + 7];
				$quiz_modus = $data[$i + 8];
				$autostart = $data[$i + 9];
				$admin_email = $data[$i + 10];
				$user_email = $data[$i + 11];
				$plugin_container = $data[$i + 12];
					
				$is_row = $wpdb->get_row("SELECT * FROM wp_wp_pro_quiz_master WHERE id = ".$id);
				if ($is_row != null) { 
					$wpdb->update( 
						'wp_wp_pro_quiz_master', 
						array( 'name' => $name, 'text' => $text, 'time_limit' => $time_limit, 'statistics_ip_lock' => $statistics_ip_lock, 'quiz_run_once_type' => $quiz_run_once_type, 'show_max_question_value' => $show_max_question_value, 'toplist_data' => $toplist_data, 'quiz_modus' => $quiz_modus, 'autostart' => $autostart, 'admin_email' => $admin_email, 'user_email' => $user_email, 'plugin_container' => $plugin_container), 
						array( 'ID' => $id ), 
						array( '%s', '%s', '%d', '%d', '%d', '%d', '%s', '%d', '%d', '%s', '%s', '%s'), 
						array( '%d' ) 
					);
				} else { 
					$wpdb->insert( 
						'wp_wp_pro_quiz_master', 
						array( 'id' => $id, 'name' => $name, 'text' => $text, 'time_limit' => $time_limit, 'statistics_ip_lock' => $statistics_ip_lock, 'quiz_run_once_type' => $quiz_run_once_type, 'show_max_question_value' => $show_max_question_value, 'toplist_data' => $toplist_data, 'quiz_modus' => $quiz_modus, 'autostart' => $autostart, 'admin_email' => $admin_email, 'user_email' => $user_email, 'plugin_container' => $plugin_container), 
						array( '%d', '%s', '%s', '%d', '%d', '%d', '%d', '%s', '%d', '%d', '%s', '%s', '%s') 
					);
				}
				$i = $i + 12;
			}
			
			if($i > $table_question)
			{
				$id = $data[$i];
				$quiz_id = $data[$i + 1];
				$online = $data[$i + 2];
				$sort = $data[$i + 3];
				$title = $data[$i + 4];
				$points = $data[$i + 5];
				$question = $data[$i + 6]; 
				$answer_type = $data[$i + 7]; 
				$answer_data = $data[$i + 8];
				$matrix_sort_answer_criteria_width = $data[$i + 9];
				
				$is_row = $wpdb->get_row("SELECT * FROM wp_wp_pro_quiz_question WHERE id = ".$id);
				if ($is_row != null) { 
					$wpdb->update( 
						'wp_wp_pro_quiz_question', 
						array( 'quiz_id' => $quiz_id, 'online' => $online, 'sort' => $sort, 'title' => $title, 'points' => $points, 'question' => $question, 'answer_type' => $answer_type, 'answer_data' => $answer_data,  'matrix_sort_answer_criteria_width' => $matrix_sort_answer_criteria_width), 
						array( 'ID' => $id ), 
						array( '%d', '%d', '%d', '%s', '%d', '%s', '%d', '%s', '%d'), 
						array( '%d' ) 
					);
				} else { 
					$wpdb->insert( 
						'wp_wp_pro_quiz_question', 
						array( 'id' => $id, 'quiz_id' => $quiz_id, 'online' => $online, 'sort' => $sort, 'title' => $title, 'points' => $points, 'question' => $question, 'answer_type' => $answer_type, 'answer_data' => $answer_data,  'matrix_sort_answer_criteria_width' => $matrix_sort_answer_criteria_width),  
						array( '%d', '%d', '%d', '%d', '%s', '%d', '%s', '%d', '%s', '%d') 
					);
				}
				$i = $i + 9;
			}
		}
	}
	
	
	// Import Bbpress
	function import_bbpress() {
		if (empty($this->options['file_with_bbpress'])) return;
		echo '<script>'
			. 'document.getElementById("import_progress_status").innerHTML = "' . __('Import Bbpress ...', 'themerex') .'";'
			. '</script>';
		echo '<br><b>'.__('Import Bbpress ...', 'themerex').'</b><br>'; flush();
		$bbpress_txt = themerex_fgc(themerex_get_file_dir($this->options['file_with_bbpress']));
		$data = unserialize( base64_decode( $bbpress_txt ) );
		
		global $wpdb;
		
		if ($this->options['overwrite_content'])
		{	
			$wpdb->query("DELETE FROM wp_bp_activity");
			$wpdb->query("DELETE FROM wp_bp_activity_meta");
			$wpdb->query("DELETE FROM wp_bp_notifications");
			$wpdb->query("DELETE FROM wp_bp_xprofile_data");
			$wpdb->query("DELETE FROM wp_bp_xprofile_fields");
			$wpdb->query("DELETE FROM wp_bp_xprofile_meta");
			$wpdb->query("DELETE FROM wp_bro_images");
		}
		
		$count = count($data);
		
		$activity_meta =  array_search('new_table_wp_bp_activity_meta', $data);
		$notifications = array_search('new_table_wp_bp_notifications', $data);
		$xprofile_data  = array_search('new_table_wp_bp_xprofile_data', $data);
		$xprofile_fields = array_search('new_table_wp_bp_xprofile_fields', $data);
		$xprofile_meta = array_search('new_table_wp_bp_xprofile_meta', $data);
		$bro_images = array_search('new_table_wp_bro_images', $data);
		$posts = array_search('new_table_wp_posts', $data);
		
		for($i = 0; $i < $count; $i++)
		{	
			if($i < $activity_meta)
			{
				$id = $data[$i];
				$user_id = $data[$i + 1];
				$component = $data[$i + 2];
				$type = $data[$i + 3]; 
				$action = $data[$i + 4];
				$content = $data[$i + 5];
				$primary_link = $data[$i + 6];
				$item_id = $data[$i + 7];
				$secondary_item_id = $data[$i + 8];
				$date_recorded = $data[$i + 9];
					
				$is_row = $wpdb->get_row("SELECT * FROM wp_bp_activity WHERE id = ".$id);
				if ($is_row != null) { 
					$wpdb->update( 
						'wp_bp_activity', 
						array( 'user_id' => $user_id, 
							   'component' => $component,
							   'type' => $type,
							   'action' => $action,
							   'content' => $content,
							   'primary_link' => $primary_link,
							   'item_id' => $item_id,
							   'secondary_item_id' => $secondary_item_id,
							   'date_recorded' => $date_recorded,
								),
						array( 'ID' => $id ), 
						array( '%d', '%s', '%s', '%s', '%s', '%s', '%d', '%d', '%s'), 
						array( '%d' ) 
					);
				} else { 
					$wpdb->insert( 
						'wp_bp_activity', 
						array( 'id' => $id, 
							   'user_id' => $user_id, 
							   'component' => $component,
							   'type' => $type,
							   'action' => $action,
							   'content' => $content,
							   'primary_link' => $primary_link,
							   'item_id' => $item_id,
							   'secondary_item_id' => $secondary_item_id,
							   'date_recorded' => $date_recorded,
								),
						array( '%d', '%d', '%s', '%s', '%s', '%s', '%s', '%d', '%d', '%s') 
					);
				}
				$i = $i + 9;
			}
			
			if($i < $notifications && $i > $activity_meta)
			{
				$id = $data[$i];
				$activity_id = $data[$i + 1];
				$meta_key = $data[$i + 2];
				$meta_value = $data[$i + 3]; 
					
				$is_row = $wpdb->get_row("SELECT * FROM wp_bp_activity_meta WHERE id = ".$id);
				if ($is_row != null) { 
					$wpdb->update( 
						'wp_bp_activity_meta', 
						array( 'activity_id' => $activity_id, 
							   'meta_key' => $meta_key,
							   'meta_value' => $meta_value
								),
						array( 'ID' => $id ), 
						array( '%d', '%s', '%s'), 
						array( '%d' ) 
					);
				} else { 
					$wpdb->insert( 
						'wp_bp_activity_meta', 
						array( 'id' => $id, 
							   'activity_id' => $activity_id, 
							   'meta_key' => $meta_key,
							   'meta_value' => $meta_value
								),
						array( '%d', '%d', '%s', '%s') 
					);
				}
				$i = $i + 3;
			}
			
			if($i < $xprofile_data && $i > $notifications)
			{
				$id = $data[$i];
				$user_id = $data[$i + 1];
				$item_id = $data[$i + 2];
				$secondary_item_id = $data[$i + 3]; 
				$component_name = $data[$i + 4]; 
				$component_action = $data[$i + 5]; 
				$date_notified = $data[$i + 6]; 
				$is_new = $data[$i + 7]; 
					
				$is_row = $wpdb->get_row("SELECT * FROM wp_bp_notifications WHERE id = ".$id);
				if ($is_row != null) { 
					$wpdb->update( 
						'wp_bp_notifications', 
						array( 'user_id' => $user_id, 
							   'item_id' => $item_id,
							   'secondary_item_id' => $secondary_item_id,
							   'component_name' => $component_name,
							   'component_action' => $component_action,
							   'date_notified' => $date_notified,
							   'is_new' => $is_new
								),
						array( 'ID' => $id ), 
						array( '%d', '%d', '%d', '%s', '%s', '%s', '%d'), 
						array( '%d' ) 
					);
				} else { 
					$wpdb->insert( 
						'wp_bp_notifications', 
						array( 'id' => $id, 
							   'user_id' => $user_id, 
							   'item_id' => $item_id,
							   'secondary_item_id' => $secondary_item_id,
							   'component_name' => $component_name,
							   'component_action' => $component_action,
							   'date_notified' => $date_notified,
							   'is_new' => $is_new
								),
						array( '%d', '%d', '%d', '%d', '%s', '%s', '%s', '%d') 
					);
				}
				$i = $i + 7;
			}
			
			if($i < $xprofile_fields && $i > $xprofile_data)
			{
				$id = $data[$i];
				$field_id = $data[$i + 1];
				$user_id = $data[$i + 2];
				$value = $data[$i + 3]; 
				$last_updated = $data[$i + 4]; 
					
				$is_row = $wpdb->get_row("SELECT * FROM wp_bp_xprofile_data WHERE id = ".$id);
				if ($is_row != null) { 
					$wpdb->update( 
						'wp_bp_xprofile_data', 
						array( 'field_id' => $field_id, 
							   'user_id' => $user_id,
							   'value' => $value,
							   'last_updated' => $last_updated
								),
						array( 'ID' => $id ), 
						array( '%d', '%d', '%s', '%s'), 
						array( '%d' ) 
					);
				} else { 
					$wpdb->insert( 
						'wp_bp_xprofile_data', 
						array( 'id' => $id, 
							   'field_id' => $field_id, 
							   'user_id' => $user_id,
							   'value' => $value,
							   'last_updated' => $last_updated
								),
						array( '%d', '%d', '%d', '%s', '%s') 
					);
				}
				$i = $i + 4;
			}
			
			if($i < $xprofile_meta && $i > $xprofile_fields)
			{
				$id = $data[$i];
				$group_id = $data[$i + 1];
				$parent_id = $data[$i + 2];
				$type = $data[$i + 3]; 
				$name = $data[$i + 4]; 
				$description = $data[$i + 5]; 
				$is_required = $data[$i + 6]; 
				$is_default_option = $data[$i + 7]; 
				$field_order = $data[$i + 8]; 
				$option_order = $data[$i + 9]; 
				$order_by = $data[$i + 10]; 
				$can_delete = $data[$i + 11]; 
					
				$is_row = $wpdb->get_row("SELECT * FROM wp_bp_xprofile_fields WHERE id = ".$id);
				if ($is_row != null) { 
					$wpdb->update( 
						'wp_bp_xprofile_fields', 
						array( 'group_id' => $group_id, 
							   'parent_id' => $parent_id,
							   'type' => $type,
							   'name' => $name,
							   'description' => $description,
							   'is_required' => $is_required,
							   'is_default_option' => $is_default_option,
							   'field_order' => $field_order,
							   'option_order' => $option_order,
							   'order_by' => $order_by,
							   'can_delete' => $can_delete
								),
						array( 'ID' => $id ), 
						array( '%d', '%d', '%s', '%s', '%s', '%d', '%d', '%d', '%d', '%s', '%d'), 
						array( '%d' ) 
					);
				} else { 
					$wpdb->insert( 
						'wp_bp_xprofile_fields', 
						array( 'id' => $id, 
							   'group_id' => $group_id, 
							   'parent_id' => $parent_id,
							   'type' => $type,
							   'name' => $name,
							   'description' => $description,
							   'is_required' => $is_required,
							   'is_default_option' => $is_default_option,
							   'field_order' => $field_order,
							   'option_order' => $option_order,
							   'order_by' => $order_by,
							   'can_delete' => $can_delete
								),
						array( '%d', '%d', '%d', '%s', '%s', '%s', '%d', '%d', '%d', '%d', '%s', '%d') 
					);
				}
				$i = $i + 11;
			}
		
			if($i < $bro_images && $i > $xprofile_meta)
			{
				$id = $data[$i];
				$object_id = $data[$i + 1];
				$object_type = $data[$i + 2];
				$meta_key = $data[$i + 3]; 
				$meta_value = $data[$i + 4]; 
					
				$is_row = $wpdb->get_row("SELECT * FROM wp_bp_xprofile_meta WHERE id = ".$id);
				if ($is_row != null) { 
					$wpdb->update( 
						'wp_bp_xprofile_meta', 
						array( 'object_id' => $object_id, 
							   'object_type' => $object_type,
							   'meta_key' => $meta_key,
							   'meta_value' => $meta_value
								),
						array( 'ID' => $id ), 
						array( '%d', '%s', '%s', '%s'), 
						array( '%d' ) 
					);
				} else { 
					$wpdb->insert( 
						'wp_bp_xprofile_meta', 
						array( 'id' => $id, 
							   'object_id' => $object_id, 
							   'object_type' => $object_type,
							   'meta_key' => $meta_key,
							   'meta_value' => $meta_value
								),
						array( '%d', '%d', '%s', '%s', '%s') 
					);
				}
				$i = $i + 4;
			}
			
			if($i < $posts && $i > $bro_images)
			{
				$id = $data[$i];
				$native_path = $this->replace_uploads($data[$i + 1]);
				$native_url = $this->replace_uploads($data[$i + 2]);
				$path = $this->replace_uploads($data[$i + 3]); 
				$url = $this->replace_uploads($data[$i + 4]); 
				$width = $data[$i + 5]; 
				$height = $data[$i + 6]; 
					
				$is_row = $wpdb->get_row("SELECT * FROM wp_bro_images WHERE id = ".$id);
				if ($is_row != null) { 
					$wpdb->update( 
						'wp_bro_images', 
						array( 'native_path' => $native_path, 
							   'native_url' => $native_url,
							   'path' => $path,
							   'url' => $url,
							   'width' => $width,
							   'height' => $height
								),
						array( 'ID' => $id ), 
						array( '%s', '%s', '%s', '%s', '%d', '%d'), 
						array( '%d' ) 
					);
				} else { 
					$wpdb->insert( 
						'wp_bro_images', 
						array( 'id' => $id, 
							   'native_path' => $native_path, 
							   'native_url' => $native_url,
							   'path' => $path,
							   'url' => $url,
							   'width' => $width,
							   'height' => $height
								),
						array( '%d', '%s', '%s', '%s', '%s', '%d', '%d') 
					);
				}
				$i = $i + 6;
			}
			
			if($i > $posts)
			{
				$id = $data[$i];
				$post_author = $data[$i + 1];
					
				$is_row = $wpdb->get_row("SELECT * FROM wp_posts WHERE id = ".$id);
				if ($is_row != null) { 
					$wpdb->update( 
						'wp_posts', 
						array( 'post_author' => $post_author
								),
						array( 'ID' => $id ), 
						array( '%d'), 
						array( '%d' ) 
					);
				} 
				$i = $i + 1;
			}
			
		};
	}
	
	
	
	// Import Booking
	function import_booking() {
		if (empty($this->options['file_with_booking'])) return;
		echo '<script>'
			. 'document.getElementById("import_progress_status").innerHTML = "' . __('Import Booking Calendar ...', 'themerex') .'";'
			. '</script>';
		echo '<br><b>'.__('Import Booking Calendar ...', 'themerex').'</b><br>'; flush();
		$booking_txt = themerex_fgc(themerex_get_file_dir($this->options['file_with_booking']));
		$data = unserialize( base64_decode( $booking_txt ) );
		
		global $wpdb;
		
		$table_name = $wpdb->prefix . 'booking';
		if($wpdb->get_var("SHOW TABLES LIKE '$table_name'") != $table_name) 
		{
			$sql = "CREATE TABLE $table_name (
			  booking_id bigint(20) NOT NULL AUTO_INCREMENT,
			  sync_gid varchar(200) DEFAULT NULL,
			  is_new bigint(20) DEFAULT NULL,
			  status varchar(200) DEFAULT NULL,
			  sort_date datetime DEFAULT NULL,
			  modification_date datetime DEFAULT NULL,
			  form text DEFAULT NULL,
			  booking_type bigint(10) DEFAULT NULL,
			  UNIQUE KEY id (booking_id)
			);";

			require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
			dbDelta( $sql );
		}
		
		$table_name = $wpdb->prefix . 'bookingdates';
		if($wpdb->get_var("SHOW TABLES LIKE '$table_name'") != $table_name) 
		{
			$sql = "CREATE TABLE $table_name (
			  booking_id bigint(20) NOT NULL AUTO_INCREMENT,
			  booking_date datetime DEFAULT NULL,
			  approved bigint(20) DEFAULT NULL,
			  UNIQUE KEY id (booking_id)
			);";

			require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
			dbDelta( $sql );
		}
		

		if ($this->options['overwrite_content'])
		{	
			$wpdb->query("DELETE FROM wp_booking");
			$wpdb->query("DELETE FROM wp_bookingdates");
			$wpdb->query("DELETE FROM wp_booking_calendars");
			$wpdb->query("DELETE FROM wp_booking_categories");
			$wpdb->query("DELETE FROM wp_booking_config");
			$wpdb->query("DELETE FROM wp_booking_slots");
		}
		
		$count = count($data);
		
		$bookingdates =  array_search('new_table_wp_bookingdates', $data);
		$booking_calendars = array_search('new_table_wp_booking_calendars', $data);
		$booking_categories  = array_search('new_table_wp_booking_categories', $data);
		$booking_config  = array_search('new_table_wp_booking_config', $data);
		$booking_slots = array_search('new_table_wp_booking_slots', $data);
		
		for($i = 0; $i < $count; $i++)
		{	
			if($i < $bookingdates)
			{
				$booking_id = $data[$i];
				$is_new = $data[$i + 1];
				$sort_date = $data[$i + 2];
				$modification_date = $data[$i + 3]; 
				$form = $data[$i + 4];
				$booking_type = $data[$i + 5];
					
				$is_row = $wpdb->get_row("SELECT * FROM wp_booking WHERE booking_id = ".$booking_id);
				if ($is_row != null) { 
					$wpdb->update( 
						'wp_booking', 
						array( 'is_new' => $is_new, 
							   'sort_date' => $sort_date,
							   'modification_date' => $modification_date,
							   'form' => $form,
							   'booking_type' => $booking_type
								),
						array( 'booking_id' => $booking_id ), 
						array( '%d', '%s', '%s', '%s', '%d'), 
						array( '%d' ) 
					);
				} else { 
					$wpdb->insert( 
						'wp_booking', 
						array( 'booking_id' => $booking_id, 
							   'is_new' => $is_new, 
							   'sort_date' => $sort_date,
							   'modification_date' => $modification_date,
							   'form' => $form,
							   'booking_type' => $booking_type
								),
						array( '%d', '%d', '%s', '%s', '%s', '%d') 
					);
				}
				$i = $i + 5;
			}
			
			if($i < $booking_calendars && $i > $bookingdates)
			{
				$booking_id = $data[$i];
				$booking_date = $data[$i + 1];
				$approved = $data[$i + 2];
					
				$is_row = $wpdb->get_row("SELECT * FROM wp_bookingdates WHERE booking_id = ".$booking_id);
				if ($is_row != null) { 
					$wpdb->update( 
						'wp_bookingdates', 
						array( 'booking_date' => $booking_date, 
							   'approved' => $approved
								),
						array( 'booking_id' => $booking_id ), 
						array( '%s', '%d'), 
						array( '%d' ) 
					);
				} else { 
					$wpdb->insert( 
						'wp_bookingdates', 
						array( 'booking_id' => $booking_id, 
							   'booking_date' => $booking_date, 
							   'approved' => $approved
								),
						array( '%d', '%s', '%d') 
					);
				}
				$i = $i + 2;
			}
			
			if($i < $booking_categories && $i > $booking_calendars)
			{
				$calendar_id = $data[$i];
				$category_id = $data[$i + 1];
				$calendar_title = $data[$i + 2];
				$calendar_email = $data[$i + 3]; 
				$calendar_active = $data[$i + 4]; 
					
				$is_row = $wpdb->get_row("SELECT * FROM wp_booking_calendars WHERE calendar_id = ".$calendar_id);
				if ($is_row != null) { 
					$wpdb->update( 
						'wp_booking_calendars', 
						array( 'category_id' => $category_id, 
							   'calendar_title' => $calendar_title,
							   'calendar_email' => $calendar_email,
							   'calendar_active' => $calendar_active
								),
						array( 'calendar_id' => $calendar_id ), 
						array( '%d', '%s', '%s', '%d'), 
						array( '%d' ) 
					);
				} else { 
					$wpdb->insert( 
						'wp_booking_calendars', 
						array( 'calendar_id' => $calendar_id, 
							   'category_id' => $category_id, 
							   'calendar_title' => $calendar_title,
							   'calendar_email' => $calendar_email,
							   'calendar_active' => $calendar_active
								),
						array( '%d', '%d', '%s', '%s', '%d') 
					);
				}
				$i = $i + 4;
			}
			
			
			if($i < $booking_config && $i > $booking_categories)
			{
				$category_id = $data[$i];
				$category_name = $data[$i + 1];
					
				$is_row = $wpdb->get_row("SELECT * FROM wp_booking_categories WHERE category_id = ".$category_id);
				if ($is_row != null) { 
					$wpdb->update( 
						'wp_booking_categories', 
						array( 'category_name' => $category_name
								),
						array( 'category_id' => $category_id ), 
						array( '%s'), 
						array( '%d' ) 
					);
				} else { 
					$wpdb->insert( 
						'wp_booking_categories', 
						array( 'category_id' => $category_id, 
							   'category_name' => $category_name
								),
						array( '%d', '%s') 
					);
				}
				$i = $i + 1;
			}
			
			if($i < $booking_slots && $i > $booking_config)
			{
				$config_id = $data[$i];
				$config_name = $data[$i + 1];
				$config_value = $data[$i + 2];
					
				$is_row = $wpdb->get_row("SELECT * FROM wp_booking_config WHERE config_id = ".$config_id);
				if ($is_row != null) { 
					$wpdb->update( 
						'wp_booking_config', 
						array( 'config_name' => $config_name,
							   'config_value' => $config_value
								),
						array( 'config_id' => $config_id ), 
						array( '%s', '%s'), 
						array( '%d' ) 
					);
				} else { 
					$wpdb->insert( 
						'wp_booking_config', 
						array( 'config_id' => $config_id, 
							   'config_name' => $config_name,
							   'config_value' => $config_value
								),
						array( '%d', '%s', '%s') 
					);
				}
				$i = $i + 2;
			}
			
			if($i > $booking_slots)
			{
				$slot_id = $data[$i];
				$slot_special_text = $data[$i + 1];
				$slot_special_mode = $data[$i + 2];
				$slot_date = $data[$i + 3]; 
				$slot_time_from = $data[$i + 4]; 
				$slot_time_to = $data[$i + 5]; 
				$slot_price = $data[$i + 6]; 
				$slot_av = $data[$i + 7]; 
				$slot_av_max = $data[$i + 8]; 
				$slot_active = $data[$i + 9]; 
				$calendar_id = $data[$i + 10]; 
					
				$is_row = $wpdb->get_row("SELECT * FROM wp_booking_slots WHERE slot_id = ".$slot_id);
				if ($is_row != null) { 
					$wpdb->update( 
						'wp_booking_slots', 
						array( 'slot_special_text' => $slot_special_text, 
							   'slot_special_mode' => $slot_special_mode,
							   'slot_date' => $slot_date,
							   'slot_time_from' => $slot_time_from,
							   'slot_time_to' => $slot_time_to,
							   'slot_price' => $slot_price,
							   'slot_av' => $slot_av,
							   'slot_av_max' => $slot_av_max,
							   'slot_active' => $slot_active,
							   'calendar_id' => $calendar_id
								),
						array( 'slot_id' => $slot_id ), 
						array( '%s', '%d', '%s', '%s', '%s', '%d', '%d', '%d', '%d', '%d'), 
						array( '%d' ) 
					);
				} else { 
					$wpdb->insert( 
						'wp_booking_slots', 
						array( 'slot_id' => $slot_id, 
							  'slot_special_text' => $slot_special_text, 
							   'slot_special_mode' => $slot_special_mode,
							   'slot_date' => $slot_date,
							   'slot_time_from' => $slot_time_from,
							   'slot_time_to' => $slot_time_to,
							   'slot_price' => $slot_price,
							   'slot_av' => $slot_av,
							   'slot_av_max' => $slot_av_max,
							   'slot_active' => $slot_active,
							   'calendar_id' => $calendar_id
								),
						array( '%d', '%s', '%d', '%s', '%s', '%s', '%d', '%d', '%d', '%d', '%d') 
					);
				}
				$i = $i + 10;
			}
		}; 
	}



	// Import sliders
	function import_sliders() {
		// Revolution Sliders
		if (themerex_exists_revslider() && file_exists(WP_PLUGIN_DIR.'/revslider/revslider.php')) {
			require_once(WP_PLUGIN_DIR.'/revslider/revslider.php');
			$dir = themerex_get_folder_dir($this->options['folder_with_revsliders']);
			if ( is_dir($dir) ) {
				$hdir = @opendir( $dir );
				if ( $hdir ) {
					echo '<script>'
						. 'document.getElementById("import_progress_status").innerHTML = "' . __('Import Revolution sliders ...', 'themerex') .'";'
						. '</script>';
					echo '<br><b>'.__('Import Revolution sliders ...', 'themerex').'</b><br>'; flush();
					$slider = new RevSlider();
					$counter = 0;
					while (($file = readdir( $hdir ) ) !== false ) {
						$counter++;
						if ($counter <= $this->last_slider) continue;
						$pi = pathinfo( ($dir) . '/' . ($file) );
						if ( substr($file, 0, 1) == '.' || is_dir( ($dir) . '/' . ($file) ) || $pi['extension']!='zip' )
							continue;
if ($this->options['debug']) printf(__('Slider "%s":', 'themerex'), $file);
						if (!is_array($_FILES)) $_FILES = array();
						$_FILES["import_file"] = array("tmp_name" => ($dir) . '/' . ($file));
						$response = $slider->importSliderFromPost();
						if ($response["success"] == false) { 
if ($this->options['debug']) echo ' '.__('import error:', 'themerex').'<br>'.ddo($response);
						} else {
if ($this->options['debug']) echo ' '.__('imported', 'themerex').'<br>';
						}
						flush();
						break;
					}
					@closedir( $hdir );
					// Write last slider into log
					themerex_fpc($this->import_log, $file ? '0|100|'.intval($counter) : '');
					$this->last_slider = $file ? $counter : 0;
				}
			}
		} else {
			if ($this->options['debug']) { printf(__('Can not locate Revo plugin: %s', 'themerex'), WP_PLUGIN_DIR.'/revslider/revslider.php<br>'); flush(); }
		}
	}

	
	// Replace uploads dir to new url
	function replace_uploads($str) {
		if (is_array($str)) {
			foreach ($str as $k=>$v) {
				$str[$k] = $this->replace_uploads($v);
			}
		} else if (is_string($str)) {
			while (($pos = themerex_strpos($str, "/{$this->options['uploads_folder']}/"))!==false) {
				$pos0 = $pos;
				while ($pos0) {
					if (themerex_substr($str, $pos0, 5)=='http:')
						break;
					$pos0--;
				}
				$str = ($pos0 > 0 ? themerex_substr($str, 0, $pos0) : '') . ($this->uploads_url) . themerex_substr($str, $pos+themerex_strlen($this->options['uploads_folder'])+1);
			}
		}
		return $str;
	}

	
	// Replace uploads dir to imports then export data
	function prepare_uploads($str) {
		if ($this->options['uploads_folder']=='uploads') return $str;
		if (is_array($str)) {
			foreach ($str as $k=>$v) {
				$str[$k] = $this->prepare_uploads($v);
			}
		} else if (is_string($str)) {
			$str = str_replace('/uploads/', "/{$this->options['uploads_folder']}/", $str);
		}
		return $str;
	}
}
?>