<?php

// Disable direct call
if ( ! defined( 'ABSPATH' ) ) { exit; }


/* Theme setup section
-------------------------------------------------------------------- */

if ( !function_exists( 'themerex_template_excerpt_theme_setup' ) ) {
	add_action( 'themerex_action_before_init_theme', 'themerex_template_excerpt_theme_setup', 1 );
	function themerex_template_excerpt_theme_setup() {
		themerex_add_template(array(
			'layout' => 'excerpt',
			'mode'   => 'blog',
			'title'  => __('Excerpt', 'alliance'),
			'thumb_title'  => __('Large image (crop)', 'alliance'),
			'w'		 => 750,
			'h'		 => 422
		));
	}
}

// Template output
if ( !function_exists( 'themerex_template_excerpt_output' ) ) {
	function themerex_template_excerpt_output($post_options, $post_data) {
		$show_title = true;	//!in_array($post_data['post_format'], array('aside', 'chat', 'status', 'link', 'quote'));
        if(function_exists('themerex_sc_in_shortcode_blogger')){
            $tag = themerex_sc_in_shortcode_blogger(true) ? 'div' : 'article';
        } else{
            $tag = 'article';
        }
		$show_post_info = (themerex_get_custom_option('show_post_info', '', $post_data['post_id']) == 'yes' ? themerex_get_custom_option('show_post_info') : 'no');
		?>
		<<?php echo ($tag); ?> <?php post_class('post_item post_item_excerpt post_featured_' . esc_attr($post_options['post_class']) . ' post_format_'.esc_attr($post_data['post_format']) . ($post_options['number']%2==0 ? ' even' : ' odd') . ($post_options['number']==0 ? ' first' : '') . ($post_options['number']==$post_options['posts_on_page']? ' last' : '') . ($post_options['add_view_more'] ? ' viewmore' : '')); ?>>
			<?php
			if ($post_data['post_flags']['sticky']) {
				?><span class="sticky_label">sticky</span><?php
			}
					
			if ($show_title && $post_options['location'] == 'center' && !empty($post_data['post_title'])) {
				?><h2 class="post_title"><a href="<?php echo esc_url($post_data['post_link']); ?>"><?php echo ($post_data['post_title']); ?></a></h2><?php
			}
			
			if (!$post_data['post_protected'] && (!empty($post_options['dedicated']) || $post_data['post_thumb'] || $post_data['post_gallery'] || $post_data['post_video'] || $post_data['post_audio'])) {
				?>
				<div class="post_featured">
				<?php
				if (!empty($post_options['dedicated'])) {
					echo ($post_options['dedicated']);
				} else if ($post_data['post_thumb'] || $post_data['post_gallery'] || $post_data['post_video'] || $post_data['post_audio']) {
					require(themerex_get_file_dir('templates/parts/post-featured.php'));
				}
				?>
				</div>
			<?php
			}
			?>
	
			<div class="post_content clearfix">
				<?php
				/*
				if ($post_data['post_likes'] > 0 && $post_data['post_format'] == 'gallery') {
					echo '<div class="post_item_likes icon-heart-3">'.$post_data['post_likes'].'</div>';
				}*/
			
				if ($show_title && $post_options['location'] != 'center' && !empty($post_data['post_title']) && $post_data['post_format'] != 'quote' && $post_data['post_format'] != 'link') {
					?><h2 class="post_title"><a href="<?php echo esc_url($post_data['post_link']); ?>"><?php echo ($post_data['post_title']); ?></a></h2><?php 
				}
				
				if (!$post_data['post_protected'] && $show_post_info == 'yes' && $post_data['post_format'] != 'quote') {
					require(themerex_get_file_dir('templates/parts/post-info.php')); 
				}
				?>
		
				<?php 
				if(($post_data['post_excerpt'] || $post_data['post_protected']) && !in_array($post_data['post_format'], array('audio', 'gallery'))){
				?>
				<div class="post_descr">
				<?php
					if ($post_data['post_protected']) {
						echo ($post_data['post_excerpt']); 
					} else {
						if ($post_data['post_excerpt']) {
							echo in_array($post_data['post_format'], array('quote', 'link', 'chat', 'aside', 'status')) ? $post_data['post_excerpt'] : '<p>'.trim(themerex_strshort($post_data['post_excerpt'], isset($post_options['descr']) ? $post_options['descr'] : themerex_get_custom_option('post_excerpt_maxlength'))).'</p>';
						}
					}
					if (empty($post_options['readmore'])) $post_options['readmore'] = __('READ MORE', 'alliance');
					if (!themerex_sc_param_is_off($post_options['readmore']) && !in_array($post_data['post_format'], array('quote', 'link', 'chat', 'aside', 'status')) && function_exists('themerex_sc_button')) {
						echo themerex_sc_button(array('class'=>"read_more", 'bg_color'=>themerex_get_custom_option('main_color_1'), 'link'=>esc_url($post_data['post_link']), 'size'=>"medium"), ($post_options['readmore']));
					}
				?>
				</div>
				<?php
				}
				
				if ($post_data['post_format'] == 'quote' && $show_post_info == 'yes') {
					require(themerex_get_file_dir('templates/parts/post-info.php')); 
				}
				?>
			</div>	<!-- /.post_content -->

		</<?php echo ($tag); ?>>	<!-- /.post_item -->

	<?php
	}
}
?>