<?php
$is_press = false;
if(function_exists('bp_current_component'))
	if(bp_current_component()) $is_press = true;
	
if(function_exists('is_bbpress'))
	if(is_bbpress()) $is_press = true;
	
	if(!$is_press)
	{ ?>
			<div class="post_info post_info_top">
				<?php
				$info_parts = array_merge(array(
					'snippets' => false,	// For singular post/page/course/team etc.
					'date' => true,
					'author' => true,
					'terms' => true,
					'counters' => true,
					'shedule' => false,		// For single course
					'length' => false		// For single course
					), isset($info_parts) && is_array($info_parts) ? $info_parts : array());
									
				if (in_array($post_data['post_type'], array('courses', 'lesson'))) {
					$course_start = is_single() ? themerex_get_custom_option('date_start') : themerex_get_custom_option('date_start', '', $post_data['post_id'], $post_data['post_type']);	//!!!!!
					if (empty($course_start) || themerex_is_inherit_option($course_start)) $course_start = $post_data['post_date'];
					$course_end  = is_single() ? themerex_get_custom_option('date_end') : themerex_get_custom_option('date_end', '', $post_data['post_id'], $post_data['post_type']);	//!!!!!
					$course_shed = is_single() ? themerex_get_custom_option('shedule') : themerex_get_custom_option('shedule', '', $post_data['post_id'], $post_data['post_type']);	//!!!!!
					if ($info_parts['date']) { 
					?>
						<div class="post_info_item post_info_posted"><?php
							echo (empty($course_end) || themerex_is_inherit_option($course_end) || $course_end >= date('Y-m-d') 
								? ($course_start >= date('Y-m-d') 
									? __('Starts on', 'alliance') 
									: __('Started on', 'alliance'))
								: __('Finished on', 'alliance')); ?> <a href="<?php echo esc_url($post_data['post_link']); ?>" class="post_info_date<?php echo esc_attr($info_parts['snippets'] ? ' date updated' : ''); ?>"<?php echo ($info_parts['snippets'] ? ' itemprop="datePublished" content="'.esc_attr($course_start).'"' : ''); ?>><?php echo date(get_option('date_format'), strtotime(empty($course_end) || themerex_is_inherit_option($course_end) || $course_end >= date('Y-m-d') ? $course_start : $course_end)); ?></a></div>
					<?php
					}
					if ($info_parts['shedule'] && !empty($course_shed)) {
					?>
						<div class="post_info_item post_info_time"><?php echo ($course_shed); ?></div>
					<?php
					}
					if ($info_parts['length'] && !empty($course_end)) {
					?>
						<div class="post_info_item post_info_length"><?php _e('Length', 'alliance'); ?> <div class="post_info_months"><?php themerex_show_layout(themerex_get_date_difference($course_start, $course_end, 2)); ?></div></div>
					<?php
					}
					if ($info_parts['terms'] && !empty($post_data['post_terms'][$post_data['post_taxonomy']]->terms_links)) {
					?>
						<div class="post_info_item post_info_tags"><?php _e('Category', 'alliance'); ?> <?php echo join(', ', $post_data['post_terms'][$post_data['post_taxonomy']]->terms_links); ?></div>
					<?php
					}
					if ($info_parts['author'] && $post_data['post_type']=='lesson') {
						$teacher_id = is_single() ? themerex_get_custom_option('teacher') : themerex_get_custom_option('teacher', '', $post_data['post_id'], $post_data['post_type']);	//!!!!!
						$teacher_post = get_post($teacher_id);
						$teacher_link = get_permalink($teacher_id);
					?>
						<div class="post_info_item post_info_posted_by<?php echo ($info_parts['snippets'] ? ' vcard' : ''); ?>"<?php echo ($info_parts['snippets'] ? ' itemprop="author"' : ''); ?>><?php _e('Teacher', 'alliance'); ?> <a href="<?php echo esc_url($teacher_link); ?>" class="post_info_author"><?php echo esc_html($teacher_post->post_title); ?></a></div>
					<?php 
					}
				} else {
					if ($info_parts['date']) {
					?>
						<div class="post_info_item post_info_posted"><a href="<?php echo esc_url($post_data['post_link']); ?>" class="post_info_date<?php echo esc_attr($info_parts['snippets'] ? ' date updated' : ''); ?>"<?php echo ($info_parts['snippets'] ? ' itemprop="datePublished" content="'.get_the_date('Y-m-d').'"' : ''); ?>><?php echo ($post_data['post_format'] == 'gallery' ? esc_html($post_data['post_date']) : esc_html($post_data['post_date_shirt'])); ?></a></div>
					<?php
					}
					if ($info_parts['author'] && $post_data['post_format'] != 'gallery') {
					?>
						<div class="post_info_item post_info_posted_by<?php echo ($info_parts['snippets'] ? ' vcard' : ''); ?>"<?php echo ($info_parts['snippets'] ? ' itemprop="author"' : ''); ?>><?php _e('by', 'alliance'); ?> <a href="<?php echo esc_url($post_data['post_author_url']); ?>" class="post_info_author"><?php echo ($post_data['post_author']); ?></a></div>
					<?php 
					}						
					$post_categories = wp_get_post_categories( $post_data['post_id'], array('fields' => 'all') );
					if ($info_parts['terms'] && !empty($post_categories)) {	
					?>
						<div class="post_info_item post_info_tags"><?php _e('in', 'alliance'); ?> 
						<?php 
							foreach($post_categories as $c){	
								echo '<a href="'.get_category_link($c).'" class="post_category">'.$c->name.'</a>';
							}
						?>
						</div>
						<?php
					}
				}
				if ($info_parts['counters'] && $post_data['post_format'] != 'gallery' && $post_data['post_format'] != 'link') {
				?>
					<div class="post_info_item post_info_counters"><?php require(themerex_get_file_dir('templates/parts/counters.php')); ?></div>
				<?php
				}
				?>
			</div>
	<?php
	} ?>
