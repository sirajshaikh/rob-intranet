<?php
global $THEMEREX_VACANCY_TABLE;

$vacancy_category = '';
$vacancy_location = '';

if (isset($_POST)) {
  if (isset($_POST["category"])){
    $vacancy_category = $_POST['category'];
  }
   if (isset($_POST["location"])){
    $vacancy_location = $_POST['location'];
  }
}

$options = '<option value="">Any Category</option>';
$terms   = get_terms('vacancy_group', array('fields' => 'all'));
foreach ($terms as $term) {
    $options .= '<option value="' . $term->name . '">' . $term->name . '</option>';
}

$args = array(
    'posts_per_page' => 60,
    'post_type' => 'vacancy',
	'orderby' => 'date'
);

$postslist = get_posts($args);
$outputs  = '';
$outputs .= '<form method="POST" class="jobs_search_form" action="">'
				.'<input type="text" class="jobs_search_field" placeholder="All locations" value="" name="location">'
				.'<select class="jobs_categories_filter" name="category">'
					.$options
				.'</select>'
				.'<input type="submit" value="Search" class="jobs_search_submit">'
			.'</form>'
			.'<div class="sc_table recent_jobs">'
				.'<table summary="1" cellspacing="0">'
					.'<thead>'
					.'<tr>'
						.'<th colspan="4">Recent Jobs</th>'
					.'</tr>'
					.'</thead>'
					.'<tbody>'
						.'<tr>'
							.'<td>category</td>'
							.'<td>location</td>'
							.'<td>employment</td>'
							.'<td>salary</td>'
						.'</tr>';

foreach ($postslist as $post):
    setup_postdata($post);
    $has_category = false;
	$has_location = false;
	
    $post_id    = $post->ID;
    $post_type  = get_post_type($post_id);
    $post_tax   = themerex_get_taxonomy_categories_by_post_type($post_type);
    $post_terms = themerex_get_terms_by_post_id(array(
        'post_id' => $post_id,
        'taxonomy' => $post_tax
    ));
    $post_meta  = get_post_meta($post_id, 'vacancy_data', true);
    $post_link  = !isset($show_links) || $show_links ? get_permalink($post_id) : '';
	
    $categories = '';
	if (!empty($post_terms['vacancy_group']->terms)) {
		foreach ($post_terms['vacancy_group']->terms as $cat) {
			$term_id = (int) $cat->term_id;
			$categories .= '<a href="' . $post_link . '"><span class="post_category">' . $cat->name . '</span></a> ';
			if($cat->name == $vacancy_category || empty($vacancy_category))
				$has_category = true;			
		}
	}				
	
    $location   = $post_meta['vacancy_item_location'];
    $employment = $post_meta['vacancy_item_employment'];
    $empl_color = ($employment == 'Freelance' ? '#ee3940' : ($employment == 'Full Time' ? '#8bc63f' : '#28c3d4'));
    $salary     = $post_meta['vacancy_item_salary'];
    $has_location = ($vacancy_location == $location ? true : (empty($vacancy_location) ? true : false));
	
    if($has_category && $has_location) 
    $outputs .= '<tr>' 
				. '<td class="categories">' . $categories . '</td>' 
				. '<td class="location"><a href="' . $post_link . '">' . $location . '</a></td>' 
				. '<td class="employment"><a href="' . $post_link . '"><span style="background: ' . $empl_color . '">' . $employment . '</span></a></td>' 
				. '<td class="salary"><a href="' . $post_link . '">$ ' . $salary . '</a></td></tr>';
endforeach;

$outputs .= '</tbody></table></div>';

$THEMEREX_VACANCY_TABLE = $outputs;

wp_reset_postdata();
?>
















