			<?php 
				// WP custom header
				$header_image = $header_image2 = $header_color = '';
				if ($top_panel_style=='dark') {
					if (($header_image = get_header_image()) == '') {
						$header_image = themerex_get_custom_option('top_panel_bg_image');
					}
					if (file_exists(themerex_get_file_dir('skins/'.($theme_skin).'/images/bg_over.png'))) {
						$header_image2 = themerex_get_file_url('skins/'.($theme_skin).'/images/bg_over.png');
					}
					$header_color = apply_filters('themerex_filter_get_link_color', themerex_get_custom_option('top_panel_bg_color'));
				}
			?>

			<header class="top_panel_wrap">
				
				<?php if (themerex_get_custom_option('show_menu_user')=='yes') {
                    if(!$THEMEREX_GLOBALS['logo_text'] && !$THEMEREX_GLOBALS['logo_'.($logo_style)]){
                        $THEMEREX_GLOBALS['logo_text'] = get_bloginfo('name');
                        }
				    ?>
					<div class="menu_user_wrap">
						<div class="logo" style="background-color: <?php echo themerex_get_custom_option('logo_bg') ?>;">
							<a href="<?php echo esc_url(home_url()); ?>"><?php echo !empty($THEMEREX_GLOBALS['logo_'.($logo_style)]) ? '<img src="'.esc_url($THEMEREX_GLOBALS['logo_'.($logo_style)]).'" class="logo_main" alt="img">' : ''; ?><?php echo ($THEMEREX_GLOBALS['logo_text'] ? '<span class="logo_text">'.($THEMEREX_GLOBALS['logo_text']).'</span>' : ''); ?><?php echo ($THEMEREX_GLOBALS['logo_slogan'] ? '<span class="logo_slogan">' . esc_html($THEMEREX_GLOBALS['logo_slogan']) . '</span>' : ''); ?></a>
						</div>
						<div class="menu_user_area menu_user_right menu_user_nav_area<?php echo (empty($THEMEREX_GLOBALS['menu_user']) ? ' m_top' : '');?>">  
							<?php require_once( themerex_get_file_dir('templates/parts/user-panel.php') ); ?>
							<?php if (themerex_get_custom_option('show_search')=='yes' && function_exists('themerex_sc_search')) echo themerex_sc_search(array('open'=>"no")); ?>
							<?php themerex_show_layout(trim(themerex_get_custom_option('contact_info'))); ?>
							<div class="menu_button show_menu">Show Menu</div>
						</div>
					</div> 
				<?php } else { ?>
					<div class="menu_user_wrap">
						<div class="logo" style="background-color: <?php echo themerex_get_custom_option('logo_bg') ?>;">
							<a href="<?php echo esc_url(home_url()); ?>"><?php echo !empty($THEMEREX_GLOBALS['logo_'.($logo_style)]) ? '<img src="'.esc_url($THEMEREX_GLOBALS['logo_'.($logo_style)]).'" class="logo_main" alt="img">' : ''; ?><?php echo ($THEMEREX_GLOBALS['logo_text'] ? '<span class="logo_text">'.($THEMEREX_GLOBALS['logo_text']).'</span>' : ''); ?><?php echo ($THEMEREX_GLOBALS['logo_slogan'] ? '<span class="logo_slogan">' . esc_html($THEMEREX_GLOBALS['logo_slogan']) . '</span>' : ''); ?></a>
						</div>
						<div class="menu_user_area menu_user_right menu_user_nav_area">
							<div class="menu_button show_menu">Show Menu</div>
						</div>
					</div>
				<?php } ?>
			</header>
			<aside class="menu_main_wrap">
				<nav role="navigation" class="menu_main_nav_area">
					<?php
					if (empty($THEMEREX_GLOBALS['menu_main'])) $THEMEREX_GLOBALS['menu_main'] = themerex_get_nav_menu('menu_main');
					if (empty($THEMEREX_GLOBALS['menu_main'])) $THEMEREX_GLOBALS['menu_main'] = themerex_get_nav_menu();
					if (empty($THEMEREX_GLOBALS['menu_main'])) echo "<div class='empty_menu'>You can create new menu or choose existing in the Appearance > Menus.</div>";
					echo ($THEMEREX_GLOBALS['menu_main']);
					?>
				</nav>
			</aside>
