<?php

// Disable direct call
if ( ! defined( 'ABSPATH' ) ) { exit; }


/* Theme setup section
-------------------------------------------------------------------- */

if ( !function_exists( 'themerex_template_masonry_theme_setup' ) ) {
	add_action( 'themerex_action_before_init_theme', 'themerex_template_masonry_theme_setup', 1 );
	function themerex_template_masonry_theme_setup() {
		themerex_add_template(array(
			'layout' => 'masonry_1',
			'template' => 'masonry',
			'mode'   => 'blog',
			'need_isotope' => true,
			'title'  => __('Masonry tile (different height) /1 columns/', 'alliance'),
			'thumb_title'  => __('Large image', 'alliance'),
			'w'		 => 750,  //!important GO TO THE SHORTCODES.PHP -> trx_blogger
			'h_crop' => 585,
			'h'      => 585
		));
		themerex_add_template(array(
			'layout' => 'masonry_2',
			'template' => 'masonry',
			'mode'   => 'blog',
			'need_isotope' => true,
			'title'  => __('Masonry tile (different height) /2 columns/', 'alliance'),
			'thumb_title'  => __('Large image', 'alliance'),
			'w'		 => 750,  //!important GO TO THE SHORTCODES.PHP -> trx_blogger
			'h_crop' => 585,
			'h'      => 585
		));
		themerex_add_template(array(
			'layout' => 'masonry_3',
			'template' => 'masonry',
			'mode'   => 'blog',
			'need_isotope' => true,
			'title'  => __('Masonry tile /3 columns/', 'alliance'),
			'thumb_title'  => __('Medium image', 'alliance'),
			'w'		 => 400,
			'h_crop' => 315,
			'h'      => 315
		));
		themerex_add_template(array(
			'layout' => 'masonry_4',
			'template' => 'masonry',
			'mode'   => 'blog',
			'need_isotope' => true,
			'title'  => __('Masonry tile /4 columns/', 'alliance'),
			'thumb_title'  => __('Small image', 'alliance'),
			'w'		 => 250,
			'h_crop' => 268,
			'h'      => 268
		));
		// Add template specific scripts
		add_action('themerex_action_blog_scripts', 'themerex_template_masonry_add_scripts');
	}
}

// Add template specific scripts
if (!function_exists('themerex_template_masonry_add_scripts')) {
	//Handler of add_action('themerex_action_blog_scripts', 'themerex_template_masonry_add_scripts');
	function themerex_template_masonry_add_scripts($style) {
		if (in_array(themerex_substr($style, 0, 8), array('classic_', 'masonry_'))) {
			wp_enqueue_script( 'isotope', themerex_get_file_url('js/jquery.isotope.min.js'), array(), null, true );
		}
	}
}

// Template output
if ( !function_exists( 'themerex_template_masonry_output' ) ) {
	function themerex_template_masonry_output($post_options, $post_data) {
		$show_title = !in_array($post_data['post_format'], array('link', 'quote', 'video', 'gallery', 'status'));
		$show_date = in_array($post_data['post_format'], array('standard', 'aside', 'chat'));
		$parts = explode('_', $post_options['layout']);
		$style = $parts[0];
		$columns = max(1, min(4, empty($parts[1]) ? $post_options['columns_count'] : (int) $parts[1]));
        if(function_exists('themerex_sc_in_shortcode_blogger')){
            $tag = themerex_sc_in_shortcode_blogger(true) ? 'div' : 'article';
        } else{
            $tag = 'article';
        }
		$post_id = get_the_ID();
		$post_likes = themerex_get_post_likes($post_id);
		$post_comments = get_comments_number();
		?>
		<div class="isotope_item isotope_item_<?php echo esc_attr($style); ?> <?php echo ($post_data['post_thumb'] ? 'isotope_item_thumb' : '');?> isotope_item_<?php echo esc_attr($post_options['layout']); ?> isotope_column_<?php echo esc_attr($columns); ?>
				<?php
					if ($post_options['filters'] != '') {
						if ($post_options['filters']=='categories' && !empty($post_data['post_terms'][$post_data['post_taxonomy']]->terms_ids))
							echo ' flt_' . join(' flt_', $post_data['post_terms'][$post_data['post_taxonomy']]->terms_ids);
						else if ($post_options['filters']=='tags' && !empty($post_data['post_terms'][$post_data['post_taxonomy_tags']]->terms_ids))
							echo ' flt_' . join(' flt_', $post_data['post_terms'][$post_data['post_taxonomy_tags']]->terms_ids);
					}
				?>">
			   <<?php echo ($tag); ?> class="post_item post_item_<?php echo esc_attr($style); ?> post_item_<?php echo esc_attr($post_options['layout']); ?>
				 <?php echo ' post_format_'.esc_attr($post_data['post_format']) 
					. ($post_options['number']%2==0 ? ' even' : ' odd') 
					. ($post_options['number']==0 ? ' first' : '') 
					. ($post_options['number']==$post_options['posts_on_page'] ? ' last' : '');
				?>">
				
				<?php if ($post_data['post_video'] || $post_data['post_audio'] || $post_data['post_thumb'] ||  $post_data['post_gallery']) { ?>
					<div class="post_featured">
						<?php require(themerex_get_file_dir('templates/parts/post-featured.php')); ?>
					</div>
				<?php } ?>
				
				<?php if (in_array($post_data['post_format'], array('quote', 'link', 'status'))) { ?>
					<div class="post_featured"><div class="content"><?php
						if ($post_data['post_excerpt']) {
							themerex_show_layout($post_data['post_excerpt']);
						}?>
					</div></div>
				<?php } ?>
				
				<div class="post_content isotope_item_content"> 
				<?php if($post_data['post_format'] != 'quote'){ ?>
					<div class="post_info">
						<span class="likes"><?php themerex_show_layout($post_likes); ?></span>
						<span class="comments"><?php themerex_show_layout($post_comments); ?></span>
					</div>
				<?php }
				
					if ($show_title) {
						if (!isset($post_options['links']) || $post_options['links']) {
							?>
							<h3 class="post_title"><a href="<?php echo esc_url($post_data['post_link']); ?>"><?php echo ($post_data['post_title']); ?></a></h3>
							<?php
						} else {
							?>
							<h4 class="post_title"><?php echo ($post_data['post_title']); ?></h4>
							<?php
						}
					}
					if($show_date){ ?>
						<div class="post_date"><?php echo ($post_data['post_date']); ?></div>
							
				<?php }
					if (!$post_data['post_protected'] && $post_options['info']) {
						$info_parts = array('counters'=>false, 'terms'=>false);
						require(themerex_get_file_dir('templates/parts/post-info.php')); 
					} ?>
				</div>				<!-- /.post_content -->
			</<?php echo ($tag); ?>>	<!-- /.post_item -->
		</div>						<!-- /.isotope_item -->
		<?php
	}
}
?>