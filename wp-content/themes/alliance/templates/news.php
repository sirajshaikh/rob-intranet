<?php

// Disable direct call
if ( ! defined( 'ABSPATH' ) ) { exit; }


/* Theme setup section
-------------------------------------------------------------------- */

if ( !function_exists( 'themerex_template_news_theme_setup' ) ) {
	add_action( 'themerex_action_before_init_theme', 'themerex_template_news_theme_setup', 1 );
	function themerex_template_news_theme_setup() {
		themerex_add_template(array(
			'layout' => 'news',
			'mode'   => 'blogger',
			'need_columns' => true,
			'title'  => __('Blogger layout: News', 'alliance'),
			'thumb_title'  => __('Small image', 'alliance'),
			'w'		 => 66,
			'h_crop' => 66,
			'h'		 => 66
		));
	}
}

// Template output
if ( !function_exists( 'themerex_template_news_output' ) ) {
	function themerex_template_news_output($post_options, $post_data) {
		if (themerex_sc_param_is_on($post_options['scroll'])) themerex_enqueue_slider();
		require(themerex_get_file_dir('templates/parts/reviews-summary.php'));
		$date = '<div class="post_date">'.($post_data['post_date']).'</div>';
		$title = '<h4 class="post_title sc_title sc_blogger_title">'
			. (!isset($post_options['links']) || $post_options['links'] ? '<a href="' . esc_url($post_data['post_link']) . '">' : '')
			. ($post_data['post_title'])
			. (!isset($post_options['links']) || $post_options['links'] ? '</a>' : '')
			. '</h4>'
			. ($reviews_summary);
		
		if (themerex_sc_param_is_on($post_options['scroll']) || ($post_options['dir'] == 'horizontal' && $post_options['columns_count'] > 0)) {
			?>
			<div class="<?php echo 'column-1_'.esc_attr($post_options['columns_count']).' column_item_'.esc_attr($post_options['number']); ?><?php 
				echo  ($post_options['number'] % 2 == 1 ? ' odd' : ' even')
					. ($post_options['number'] == 1 ? ' first' : '')
					. ($post_options['number'] == $post_options['posts_on_page'] ? ' last' : '');
					//. (themerex_sc_param_is_on($post_options['scroll']) ? ' sc_scroll_slide swiper-slide' : '');
					?>">
			<?php
		}
		?>
		
		<div class="post_item post_item_news sc_blogger_item<?php echo ($post_options['number'] == $post_options['posts_on_page'] && !themerex_sc_param_is_on($post_options['loadmore']) ? ' sc_blogger_item_last' : '');
			?>">
			<?php 
			if ($post_data['post_video'] || $post_data['post_audio'] || $post_data['post_thumb'] ||  $post_data['post_gallery']) { ?>
				<div class="post_featured">
					<?php require(themerex_get_file_dir('templates/parts/post-featured.php')); ?>
				</div>
				<?php
			} ?>
				<div class="post_inner">
				<?php
					echo ($date);
					echo ($title);
				?>
					<div class="post_content sc_blogger_content">
					<?php
					if ($post_data['post_excerpt']) { 
						echo ($post_data['post_excerpt']); 
					}
					?>
					</div><!-- /.post_content -->
				</div>	
		</div>		<!-- /.post_item -->
		<?php
		if (themerex_sc_param_is_on($post_options['scroll']) || ($post_options['dir'] == 'horizontal' && $post_options['columns_count'] > 0)) {
		?>
			</div>	<!-- /.column-1_x -->
			<?php
		}
	}
}
?>