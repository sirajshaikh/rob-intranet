<div class="links-wrap">
	<?php
	$args = array(
		'post_type'      => 'cpt-links',
		'post_status'    => 'publish',
		'posts_per_page' => -1,
		'orderby' => 'date',
		'order' => 'DESC',
	);
	$links = get_posts( $args );
	foreach ( $links as $post ) : setup_postdata( $post );
		
		$terms = get_the_terms( get_the_ID(), 'link_tag' );
		?>
		<div class="link-item">
			<h4><a href="<?php echo get_field('link_url'); ?>"><u><?php the_title(); ?></u></a></h4>
			
			<?php
			if ( ! empty( $terms ) && ! is_wp_error( $terms ) ){
				foreach ( $terms as $term ) {
					echo '<span class="sc_text padding" style="background: #28c3d4;color: #fff; margin-right:10px;">'.$term->name.'</span>';
				}
			}
			?>
			
			<?php the_content(); ?>
			
		</div>
		
	<?php endforeach; 
	wp_reset_postdata();?>
</div>
