<?php
function intranet_custom_init() {
	
	// Links
	$args_links = array(
		'public' => true,
		'label'  => 'Links',
		'menu_position'  => 5,
		'supports' => array( 'title', 'editor', 'author' )
	);
	register_post_type( 'cpt-links', $args_links );
	
	register_taxonomy(
		'link_tag',
		'cpt-links',
		array(
			'label' => __( 'Tags' ),
			'public' => true,
			'rewrite' => false,
			'hierarchical' => false,
		)
	);
	
	
	// Sponsors
	$args_sponsor = array(
		'public' => true,
		'label'  => 'Sponsors',
		'menu_position'  => 5,
		'supports' => array( 'title', 'thumbnail', 'author' )
	);
	register_post_type( 'cpt-sponsor', $args_sponsor );
}
add_action( 'init', 'intranet_custom_init' );



function links_callback() {
	ob_start();
	get_template_part('tpl-links');
	return ob_get_clean();
}
add_shortcode( 'links', 'links_callback' );


function sponsors_callback() {
	ob_start();
	get_template_part('tpl-sponsors');
	return ob_get_clean();
}
add_shortcode( 'sponsors', 'sponsors_callback' );

//// Test