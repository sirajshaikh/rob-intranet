<div class="sponsors-wrap">
	<?php
	$args = array(
		'post_type'      => 'cpt-sponsor',
		'post_status'    => 'publish',
		'posts_per_page' => -1,
		'orderby' => 'date',
		'order' => 'DESC',
	);
	$links = get_posts( $args );
	foreach ( $links as $post ) : setup_postdata( $post );
		
		$terms = get_the_terms( get_the_ID(), 'link_tag' );
		?>
		<div class="sponsor-item" style="display: inline-block; width: 100%; margin-bottom: 30px;">
			<div class="sponsor-logo" style="float: left; width: 200px;">
				<img src="<?php echo wp_get_attachment_url( get_post_thumbnail_id() ); ?>" />
			</div>
			<div class="sponsor-info" style="float: left;">
				<h4><?php the_title(); ?></h4>
				<span>Email : <?php echo get_field('sponsor_email'); ?></span>
				<br/>
				<span>Phone 1 : <?php echo get_field('sponsor_phone_1'); ?></span>
				<br/>
				<span>Phone 2 : <?php echo get_field('sponsor_phone_2'); ?></span>
				<br/>
				<span>Street : <?php echo get_field('sponsor_street'); ?></span>
				<br/>
				<span>City : <?php echo get_field('sponsor_city'); ?></span>
				<br/>
				<span>State : <?php echo get_field('sponsor_state'); ?></span>
				<br/>
				<span>Zip Code : <?php echo get_field('sponsor_zipcode'); ?></span>
				<br/>
				
				<span>Webinars : </span>
				<br/>
				<?php if( have_rows('sponsor_webinars') ){ ?>
					
					<ul class="sc_list sc_list_style_ul">
						
						<?php while ( have_rows('sponsor_webinars') ) : the_row(); ?>
							<li><a href="<?php echo get_sub_field('webinar_link'); ?>" target="_blank"><?php echo get_sub_field('webinar_name'); ?></a></li>
						<?php endwhile; ?>
						
					</ul>
					
				<?php } ?>
			</div>
		</div>
		
	<?php endforeach; 
	wp_reset_postdata();?>
</div>
