<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'wptest');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '8yu=csO}WjL1`D`}dk.!+no{yj+o&0Gd4#&hU-X*rShg;54^j~~{YEt(@O }VJ&W');
define('SECURE_AUTH_KEY',  '2,n#oe^Gs>:WxZPoNE<oXxl5fBjHjmb}5wm~w_wUj~FzUCy}iY&8A{Kcg;S*w+wL');
define('LOGGED_IN_KEY',    'wmh#A4{szzQg<_y8/AOa11D7gXU{b;=$k.,Bj#}H6QV,A%Y|MI_/mAuhyRKUE}70');
define('NONCE_KEY',        '_L)xFd5DEEhjN+X/2Gf)p!TfOwI:s(ZohJOFN}HQMj93x%_`u;iS[`~|?&.J+-*m');
define('AUTH_SALT',        ' zndP:L{fv+?UdpEGHF`C+jy(8-B5lb%j*K|WY*FC]<72VUg86(_rZkQN;b%e:^h');
define('SECURE_AUTH_SALT', '%vRfs`[-pA4K$8If~HCK3>0bgtox;~;V`$k|G$R:1q?c08!Xv2M8>zE.3[`5+*@7');
define('LOGGED_IN_SALT',   '9IeUw&-]=:Asx1QOzevt|8d-kjW{o-3iUv~M? %i!S-w}K2M&:]Y$Dg4A6c`%*i4');
define('NONCE_SALT',       'GSvn0tFH,a9H@^OWS=Ta{`Aa:P/BL8bQiGZ>pW_PHpaI I#<lTM8yO*oPcU29d$p');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

define('WP_ALLOW_REPAIR', true);
/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
